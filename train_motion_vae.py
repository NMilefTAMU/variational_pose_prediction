import os
import sys
import time

import torch
from torch.utils.data import DataLoader, WeightedRandomSampler
from torch.utils.tensorboard import SummaryWriter

import models.motion_vae as vae_models
import utils.paramUtil as paramUtil
from dataProcessing import dataset
from dataProcessing.dataset_mocap_lobstr import DatasetMocapLoBSTr
from dataProcessing.dataset_mocap_ours import DatasetMocapOurs
from dataProcessing.dataset_mocap_pfnn import DatasetMocapPFNN
from options.train_vae_options import TrainOptions
from trainer.trainer_ours import TrainerOurs
from trainer.trainer_vae import TrainerVAE6d
from utils.utils_ import save_logfile

if __name__ == "__main__":
    parser = TrainOptions()
    opt = parser.parse()
    device = torch.device("cuda:" + str(opt.gpu_id) if torch.cuda.is_available() else "cpu")

    opt.save_root = os.path.join(opt.checkpoints_dir, opt.dataset_type, opt.name)
    opt.model_path = os.path.join(opt.save_root, 'model')
    opt.joints_path = os.path.join(opt.save_root, 'joints')
    opt.log_path = os.path.join(opt.save_root, "log.txt")

    writer = SummaryWriter('runs/' + str(time.time() * 1000) + str(opt.name))
    writer.add_text("Params", str(opt))

    if not os.path.exists(opt.model_path):
        os.makedirs(opt.model_path)
    if not os.path.exists(opt.joints_path):
        os.makedirs(opt.joints_path)

    dataset_path = ""
    input_size = 72
    joints_num = 0
    data = None

    dataset_params      = paramUtil.get_dataset_params(opt)
    dataset_path        = dataset_params['dataset_path']
    input_size          = dataset_params['input_size']
    joints_num          = dataset_params['joints_num']
    label_dec           = dataset_params['label_dec']
    raw_offsets         = dataset_params['raw_offsets']
    kinematic_chain     = dataset_params['kinematic_chain']
    enumerator          = dataset_params['enumerator']
    clip_path           = dataset_params['clip_path']
    motion_desc_file    = dataset_params['motion_desc_file']
    labels              = dataset_params['labels']
    file_prefix         = dataset_params['file_prefix']

    if opt.dataset_type == "mocap":
        data = dataset.MotionFolderDatasetMocap(clip_path, dataset_path, opt)
    elif opt.dataset_type == "mocap_lobstr":
        train_data = DatasetMocapLoBSTr(dataset_path, opt, mode="train")
        valid_data = DatasetMocapLoBSTr(dataset_path, opt, mode="valid")
    elif opt.dataset_type == "mocap_ours":
        train_data = DatasetMocapOurs(dataset_path, opt, mode="train")
        valid_data = DatasetMocapOurs(dataset_path, opt, mode="valid")
    elif opt.dataset_type == "mocap_pfnn":
        train_data = DatasetMocapPFNN(dataset_path, opt, mode="train")
        valid_data = DatasetMocapPFNN(dataset_path, opt, mode="valid")
    else:
        raise NotImplementedError('This dataset is unrecognized!!!')

    opt.dim_category = 0#len(data.labels)
    # arbitrary_len won't limit motion length, but the batch size has to be 1
    train_motion_dataset = train_data
    valid_motion_dataset = valid_data
    if opt.batch_size > len(train_motion_dataset):
        raise ValueError(
            "Batch size of ("
            + str(opt.batch_size)
            + ") must be <= size of dataset ("
            + str(len(train_motion_dataset))
            + ") for given action")
    train_motion_sampler = WeightedRandomSampler(train_motion_dataset.get_sample_weights(), len(train_motion_dataset))
    valid_motion_sampler = WeightedRandomSampler(valid_motion_dataset.get_sample_weights(), len(valid_motion_dataset))
    train_motion_loader = DataLoader(train_motion_dataset, batch_size=opt.batch_size, drop_last=True, num_workers=opt.num_workers, shuffle=False, sampler=train_motion_sampler)
    valid_motion_loader = DataLoader(valid_motion_dataset, batch_size=opt.batch_size, drop_last=True, num_workers=opt.num_workers, shuffle=False, sampler=valid_motion_sampler)
    opt.pose_dim = input_size

    num_tp = 0
    opt.input_size = 0
    if opt.enable_last_p:
        opt.input_size = input_size
    if 'action' in opt.input_types:
        opt.input_size = opt.input_size + opt.dim_category
    if 'time_counter' in opt.input_types:
        opt.input_size = opt.input_size + 1
    if 'tracking3' in opt.input_types:
        opt.input_size = opt.input_size + 9
        num_tp += 3
    if 'vtracking3' in opt.input_types:
        opt.input_size = opt.input_size + 9
        num_tp += 3
    if 'avtracking3' in opt.input_types:
        opt.input_size = opt.input_size + 18
        num_tp += 3
    if 'tracking4' in opt.input_types:
        opt.input_size = opt.input_size + 12
        num_tp += 4
    if 'vtracking4' in opt.input_types:
        opt.input_size = opt.input_size + 12
        num_tp += 4
    if 'avtracking4' in opt.input_types:
        opt.input_size = opt.input_size + 24
        num_tp += 4
    if 'root_velocity' in opt.input_types:
        opt.input_size = opt.input_size + 3

    opt.output_size = input_size

    num_action_labels = -1
    if "action" in opt.loss_functions:
        num_action_labels = train_motion_dataset.get_num_labels()

    if opt.model_type == "vae_6d":
        prior_net = vae_models.GaussianGRU(opt.input_size, opt.dim_z, opt.hidden_size,
                                        opt.prior_hidden_layers, opt.batch_size, device,
                                        lock_eps=False, learning_mode=opt.learning_mode)
        posterior_net = vae_models.GaussianGRU(opt.input_size, opt.dim_z, opt.hidden_size,
                                            opt.posterior_hidden_layers, opt.batch_size, device,
                                            lock_eps=False, learning_mode=opt.learning_mode)

        decoder = vae_models.DecoderGRU6d(opt.input_size + opt.dim_z, opt.output_size, opt.hidden_size,
                                        opt.decoder_hidden_layers,
                                        opt.batch_size, device)

    pc_prior = sum(param.numel() for param in prior_net.parameters())
    print(prior_net)
    print("Total parameters of prior net: {}".format(pc_prior))
    pc_posterior = sum(param.numel() for param in posterior_net.parameters())
    print(posterior_net)
    print("Total parameters of posterior net: {}".format(pc_posterior))
    pc_decoder = sum(param.numel() for param in decoder.parameters())
    print(decoder)
    print("Total parameters of decoder: {}".format(pc_decoder))

    if opt.model_type == "vae_6d" or opt.model_type == "vae_moe":
        trainer = TrainerVAE6d(train_motion_loader, valid_motion_loader, opt, device, raw_offsets, kinematic_chain)
    elif opt.model_type == "vae_ours":
        trainer = TrainerOurs(train_motion_loader, valid_motion_loader, opt, device, raw_offsets, kinematic_chain)

    if opt.model_type in ["vae_6d", "vae", "vae_ours", "vae_moe"]:
        logs = trainer.trainIters(prior_net, posterior_net, decoder, writer)
    else:
        logs = trainer.trainIters()

    # Plotting to Tensorboard

    #plot_loss(logs, os.path.join(opt.save_root, "loss_curve.png"), opt.plot_every)
    save_logfile(logs, opt.log_path)
