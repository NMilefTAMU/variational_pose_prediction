import unittest

import utils.paramUtil as paramUtil

class TestParamUtil(unittest.TestCase):
    def setUp(self):
        self.action_dict = {
            0: 'Stand',
            1: 'Walk',
            2: 'Run',
            3: 'Swim',
        }

    def test_find_id_by_action_name(self):
        key = paramUtil.find_key_by_value(self.action_dict, 'Coding')
        self.assertEqual(key, None)

        key = paramUtil.find_key_by_value(self.action_dict, 'Run')
        self.assertEqual(key, 2)