import torch
from typing import Dict
from environment.choreography import Choreography
from eps.applications.ss_base import SampleSelectBase

class SampleSelectJointPlacement(SampleSelectBase):
    def __init__(self, choreography_file, dataset_class):
        self.choreography = Choreography(choreography_file)

        self.dataset_class = dataset_class

    def detect_error(
        self,
        frame_sample: Dict,
        current_eps: int,
        target_frame: int,
    ) -> bool:
        """
        This isn't really an error, it's more of a check for frame number
        """
        next_event = self.choreography.get_next_event(target_frame)

        if next_event is None:
            return False

        return next_event.detect_error(frame_sample, current_eps, target_frame)

    def select_sample(
        self,
        frame_sample: Dict,
        current_eps: int,
        target_frame: int,
    ) -> int:
        """
        Select best sample based on constraints
        """

        next_event = self.choreography.get_next_event(target_frame)
        return next_event.select_sample(frame_sample, current_eps, target_frame)
