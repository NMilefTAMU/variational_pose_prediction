from abc import ABC, abstractmethod
from typing import Dict

class SampleSelectBase(ABC):
    def __init__(self):
        pass

    def detect_error(
        self,
        frame_sample: Dict,
        current_eps: int,
        target_frame: int,
    ) -> bool:
        False

    def select_sample(
        self,
        frame_sample: Dict,
        current_eps: int,
        target_frame: int,
    ) -> int:
        return current_eps