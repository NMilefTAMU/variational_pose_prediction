from models.pose_network import PoseModelNetwork
from pose_model.pose_model_params import PoseModelParams
import torch
from typing import Dict, List
from eps.applications.ss_base import SampleSelectBase
from utils.forward_kinematics import forward_kinematics_fast


class SampleSelectInvalidPose(SampleSelectBase):
    def __init__(self, device, kinematic_chain, bone_lengths, bone_names, start_frame=0, passthrough=False):
        self.device = device
        self.kinematic_chain = kinematic_chain
        self.bone_lengths = torch.unsqueeze(torch.from_numpy(bone_lengths), 0)
        self.bone_names = bone_names
        self.start_frame = start_frame
        self.passthrough = passthrough

        self.pose_model_params = PoseModelParams()
        self.pose_model_params.load("./checkpoints/eps/pose_model_pos_full_3x.yaml")

        self.pose_model_network = PoseModelNetwork(
            input_size=19 * 3,
            num_levels = 3,
        )
        self.pose_model_network.load_state_dict(
            state_dict = torch.load("./checkpoints/eps/pose_model_pos_full_3x.pth"),
        )
        self.pose_model_network.to(self.device)

    def detect_error(
        self,
        frame_sample: Dict,
        current_eps: int,
        target_frame: int,
    ) -> bool:

        if target_frame < self.start_frame:
            return False

        pos_error_output = self.compute_position_error(frame_sample)
        lower_body_indices = self.get_list_of_lower_body_indices()
        position_error = pos_error_output["position_error"]
        iqr_upper = pos_error_output["iqr_upper"]

        if torch.sum((position_error[current_eps] > iqr_upper)[lower_body_indices]) > 1:
            print("Anomaly detected!", target_frame)
            #indices = (position_error[current_eps] > iqr_upper).nonzero()
            #print(indices.flatten())
            #print(((position_error[current_eps]) / iqr)[indices].flatten())
            return True

        return False

    def select_sample(
        self,
        frame_sample: Dict,
        current_eps: int,
        target_frame: int,
    ) -> int:

        pos_error_output = self.compute_position_error(frame_sample)
        lower_body_indices = self.get_list_of_lower_body_indices()
        position_error = pos_error_output["position_error"]
        iqr_upper = pos_error_output["iqr_upper"]
        iqr = torch.clip(pos_error_output["iqr"], 0.01, 1)

        summed_error = torch.sum(position_error[:, lower_body_indices] / iqr[lower_body_indices], dim=-1)
        target_eps = int(torch.argmin(summed_error).item())

        if self.passthrough:
            return current_eps

        return target_eps

    def compute_position_error(
        self,
        frame_sample: Dict,
    ) -> Dict:
        """
        Computes the position error

        Args:
            frame_sample: Dict

        Returns:
            Dict: position error and statistics
        """

        position = frame_sample["local_position"]
        batch_size = position.shape[0]

        position_pm = self.pose_model_network(position)
        position_pm = forward_kinematics_fast(self.device, position_pm, self.kinematic_chain, self.bone_lengths)

        position_error = torch.norm(
            position_pm["pos"].reshape(batch_size, -1, 3) - position.reshape(batch_size, -1, 3),
            dim=2,
        )

        # compute interquartile range for outlier detection
        q0 = torch.quantile(position_error, 0.0, dim=0)
        q2 = torch.quantile(position_error, 0.5, dim=0)

        iqr_scale = 1.0  # controls sensitivity
        iqr = q2 - q0
        iqr_lower = q0 - (iqr_scale * iqr)
        iqr_upper = q2 + (iqr_scale * iqr)

        output = {}
        output["iqr"] = iqr
        output["iqr_lower"] = iqr_lower
        output["iqr_upper"] = iqr_upper
        output["position_error"] = position_error

        return output

    def get_list_of_lower_body_indices(self) -> List:
        # lower body indices
        lower_body_indices = []
        index = 0
        for bone_name in self.bone_names:
            if bone_name in ["LeftUpLeg", "LeftLeg", "LeftFoot", "LeftToeBase", "RightUpLeg", "RightLeg", "RightFoot", "RightToeBase"]:
                lower_body_indices.append(index)
            index += 1

        return lower_body_indices