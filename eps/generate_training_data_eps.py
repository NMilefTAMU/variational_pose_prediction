import os
import sys

sys.path.insert(0, os.path.join(os.path.dirname(os.path.abspath(__file__)), ".."))

import argparse

import numpy as np
from evaluate_batch import InferenceBatch


def generate_training_data_eps(args, dataset_type):
    inference = InferenceBatch(
        "mocap_ours",
        args.experiment,
        dataset_mode=dataset_type,
        motion_length = -1,
        sequence_names = [],
        lock_eps=True,
        ik_mode=True)

    if args.local:
        root_dir = os.path.join("dataset/dataset_eps_local/output/", dataset_type)
    else:
        root_dir = os.path.join("dataset/dataset_eps_world/output/", dataset_type)

    os.makedirs(root_dir, exist_ok=True)

    num_sequences = inference.get_num_sequences()
    for i in range(num_sequences):
        ours_seq = inference.evaluate_sequence(i)
        gt_seq = inference.gt_sequence(i)

        data = {}
        
        data['ours'] = ours_seq["position"][1:].detach().cpu().numpy()
        data['gt'] = gt_seq["position"][2:].detach().cpu().numpy()

        if args.local:
            pos_ours = ours_seq['position'][1:]
            root_pos_ours = ours_seq['root_pos'][1:].detach().cpu().numpy()
            root_rot_ours = ours_seq['root_rot'][1:].detach().cpu().numpy()
            data['ours'] = inference.convert_to_local(pos_ours, root_pos_ours, root_rot_ours).detach().cpu().numpy()

            pos_gt = gt_seq['position'][2:]
            root_pos_gt = gt_seq['root_pos'][2:].detach().cpu().numpy()
            root_rot_gt = gt_seq['root_rot'][2:].detach().cpu().numpy()
            data['gt'] = inference.convert_to_local(pos_gt, root_pos_gt, root_rot_gt).detach().cpu().numpy()

        name = inference.get_sequence_name(i)

        filename = os.path.join(root_dir, name)
        np.savez(filename, **data)
        print("Saved", filename)


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--experiment", type=str, required=True)
    parser.add_argument("--dataset_type", nargs="+", default=["train", "valid"])
    parser.add_argument("--local", action="store_true")
    args = parser.parse_args()

    for dataset_type in args.dataset_type:
        generate_training_data_eps(args, dataset_type)
