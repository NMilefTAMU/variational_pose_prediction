import torch
import torch.nn as nn
from utils.forward_kinematics import forward_kinematics, forward_kinematics_fast

class GaussianGRU(nn.Module):
    def __init__(self, input_size, output_size, hidden_size, n_layers, batch_size, device, lock_eps=False, learning_mode="vae"):
        super(GaussianGRU, self).__init__()
        self.input_size = input_size
        self.output_size = output_size
        self.hidden_size = hidden_size
        self.n_layers = n_layers
        self.batch_size = batch_size
        self.device = device
        self.lock_eps = lock_eps
        self.eps = None

        self.embed = nn.Linear(input_size, hidden_size)
        self.gru = nn.ModuleList([nn.GRUCell(hidden_size, hidden_size) for i in range(self.n_layers)])

        self.learning_mode = learning_mode
        if learning_mode in ["vae", "vae_fp"]:
            self.mu_net = nn.Linear(hidden_size, output_size)
            self.logvar_net = nn.Linear(hidden_size, output_size)
        elif learning_mode == "supervised":
            self.output_net = nn.Linear(hidden_size, output_size)

        self.hidden = self.init_hidden()

    def init_hidden(self, num_samples=None):
        batch_size = num_samples if num_samples is not None else self.batch_size
        hidden = []
        for i in range(self.n_layers):
            hidden.append(torch.zeros(batch_size, self.hidden_size).requires_grad_(False).to(self.device))
        self.hidden = hidden
        self.eps = None
        return hidden

    def select_new_eps(self, batch_size, num_dim):
        eps = torch.zeros((batch_size, num_dim)).to(self.device).normal_()
        self.eps = eps
        #self.eps = None # forces reparameterization to run again

    def select_new_eps_index(self, num_dim, index=0):
        self.eps[index:index+1] = torch.zeros((1, num_dim)).to(self.device).normal_()

    def get_eps(self):
        return self.eps

    def set_eps(self, eps):
        self.eps = eps

    def get_hidden(self):
        return self.hidden

    def set_hidden(self, hidden):
        for i in range(len(hidden)):
            self.hidden[i] = hidden[i]

    def reparameterize(self, mu, logvar, eps=None):
        s_var = logvar.mul(0.5).exp_()

        if (self.lock_eps and self.eps is None) or (not self.lock_eps):
            eps = s_var.data.new(s_var.size()).normal_()
            self.eps = eps
        else:
            eps = self.eps # use same EPS for each frame

        return eps.mul(s_var).add_(mu)

    def forward(self, inputs):
        embedded = self.embed(inputs.view(-1, self.input_size))
        h_in = embedded
        for i in range(self.n_layers):
            self.hidden[i] = self.gru[i](h_in, self.hidden[i])
            h_in = self.hidden[i]

        if self.learning_mode in ["vae", "vae_fp"]:
            mu = self.mu_net(h_in)
            logvar = self.logvar_net(h_in)
            z = self.reparameterize(mu, logvar, self.eps)
        elif self.learning_mode == "supervised":
            z = self.output_net(h_in)
            mu = None
            logvar = None

        return z, mu, logvar, h_in

class Decoder(nn.Module):
    def __init__(self, input_size, output_size, hidden_size, batch_size, device):
        super(Decoder, self).__init__()
        self.input_size = input_size
        self.output_size = output_size
        self.hidden_size = hidden_size
        self.batch_size = batch_size
        self.device = device
        self.embed = nn.Linear(input_size, hidden_size)
        self.output = nn.Linear(hidden_size, output_size)

    def init_hidden(self, batch_size):
        pass

    def forward(self, inputs):
        h_in = torch.relu(self.embed(inputs))        

        return self.output(h_in)

class DecoderGRU(nn.Module):
    def __init__(self, input_size, output_size, hidden_size, n_layers, batch_size, device):
        super(DecoderGRU, self).__init__()
        self.input_size = input_size
        self.output_size = output_size
        self.hidden_size = hidden_size
        self.batch_size = batch_size
        self.n_layers = n_layers
        self.device = device
        self.embed = nn.Linear(input_size, hidden_size)
        self.gru = nn.ModuleList([nn.GRUCell(hidden_size, hidden_size) for i in range(self.n_layers)])

        self.output = nn.Linear(hidden_size, output_size)
        self.hidden = self.init_hidden()

    def init_hidden(self, num_samples=None):
        batch_size = num_samples if num_samples is not None else self.batch_size
        hidden = []
        for i in range(self.n_layers):
            hidden.append(torch.zeros(batch_size, self.hidden_size).requires_grad_(False).to(self.device))
        self.hidden = hidden
        return hidden

    def get_hidden(self):
        return self.hidden

    def set_hidden(self, hidden):
        for i in range(len(hidden)):
            self.hidden[i] = hidden[i]

    def forward(self, inputs):
        embedded = self.embed(inputs.view(-1, self.input_size))
        h_in = embedded
        for i in range(self.n_layers):
            self.hidden[i] = self.gru[i](h_in, self.hidden[i])
            h_in = self.hidden[i]
        return self.output(h_in), h_in

# generator with the 6D rotation format, root joint has no rotations
class DecoderGRU6d(DecoderGRU):
    def __init__(self, input_size, output_size, hidden_size, n_layers, batch_size, device):
        super(DecoderGRU6d, self).__init__(input_size,
                                            hidden_size, # remove root position
                                            hidden_size,
                                            n_layers,
                                            batch_size,
                                            device)

        self.output_lie = nn.Linear(hidden_size, output_size - 3)
        self.contact = nn.Linear(hidden_size, 2)

    def forward(self, inputs):
        hidden_output, h_mid = super(DecoderGRU6d, self).forward(inputs)

        hid = torch.relu(hidden_output)

        out = self.output_lie(hid)

        # replace output of nn with 
        output = torch.cat((inputs[..., 0:3], out), dim=1)

        # compute contact loss
        contact = torch.sigmoid(self.contact(hid))

        values = {}
        values["output"] = output
        values["h_mid"] = h_mid
        values["contact"] = contact

        return values