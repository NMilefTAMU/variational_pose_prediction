from argparse import Namespace
import copy
from typing import Dict
from models.motion_cvae_conv import DecoderConv, GaussianConv
from models.motion_vae_conv import ConvVAE
import torch
from models.inference.vae_base import VAEFull
from utils.forward_kinematics import forward_kinematics_fast
from utils.pose_processing import extract_tracked_points
from utils.pose_utils import local_to_world


class DecoderVAEConv6dFull4tp(VAEFull):
    def __init__(self, input_size, output_size, hidden_size, n_layers, batch_size, kinematic_chain, bone_lengths, learning_mode, num_frames, device):
        self.batch_size = batch_size
        self.device = device
        self.kinematic_chain = kinematic_chain
        self.bone_lengths = torch.unsqueeze(torch.from_numpy(bone_lengths).float().to(self.device), 0)
        self.learning_mode = learning_mode

        self.num_frames = num_frames

        dim_z = 30

        self.decoder = ConvVAE(input_size, output_size, dim_z, 1, device, True, self.num_frames)
        self.decoder.to(self.device)
        self.decoder.eval()

        self.decoder.select_new_eps(1, dim_z)

        self.reset()

    def reset(self):
        self.eps = torch.zeros((self.batch_size, 30)).normal_().to(self.device)
        self.decoder.set_eps(self.eps)

    def load(self, network_path):
        decoder_state_dict = torch.load(network_path, self.device)["decoder"]
        self.decoder.load_state_dict(decoder_state_dict)

    def evaluate_frame(self, sample, frame):
        output = {}

        with torch.no_grad():
            opt = {}
            opt["dataset_type"] = "mocap_ours"
            opt["input_types"] = ["tracking4", "vtracking4"]
            opt = Namespace(**opt)

            batch_size = self.eps.shape[0]

            # compute frame indices and pad at beginning of sequence
            frame_indices = list(range(max(frame - self.num_frames + 1, 0), frame+1))
            if len(frame_indices) < self.num_frames:
                diff_len = self.num_frames - len(frame_indices)
                padding = [0] * diff_len
                frame_indices = padding + frame_indices

            repeat_input = []
            for frame_index in frame_indices:
                position = extract_tracked_points(sample, frame_index, opt, "position").float().to(self.device)
                velocity = extract_tracked_points(sample, frame_index, opt, "velocity").float().to(self.device)
                repeat_input.append(torch.repeat_interleave(position, batch_size, dim=0))
                repeat_input.append(torch.repeat_interleave(velocity, batch_size, dim=0))

            repeat_input = torch.cat(repeat_input, dim=-1)

            decoder_output = self.decoder(repeat_input)

            fk_output = forward_kinematics_fast(
                self.device,
                decoder_output["output"],
                self.kinematic_chain,
                self.bone_lengths,
            )

            fk_position = torch.reshape(fk_output["pos"], (batch_size, -1, 3))
            fk_rotation = torch.reshape(fk_output["rot"], (batch_size, -1, 3, 3))

            root_position = sample["root_pos"][:, [frame]].float().to(self.device)
            root_rotation = sample["root_rot_mat"][:, [frame]].float().to(self.device)

            world_output = local_to_world(fk_position, fk_rotation, root_position, root_rotation)

            world_output["position"] = torch.reshape(world_output["position"], (batch_size, -1))
            world_output["rotation"] = torch.reshape(world_output["rotation"], (batch_size, -1))

            output["position"] = world_output["position"]
            output["rotation"] = world_output["rotation"]
            output["contact"] = decoder_output["contact"]
            output["eps"] = self.eps

        return output

    def interpolate(self, frame_sample: Dict, src0: int, src1: int, alpha: float):
        self.eps = ((1.0 - alpha) * frame_sample["eps"][src0]) + (alpha * frame_sample["eps"][[src1]])
