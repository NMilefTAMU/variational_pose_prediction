from argparse import Namespace
import copy
from typing import Dict
from models.inference.vae_base import VAEFull
from models.root_pred.occlusion_network_vae import OcclusionNetVAE
import torch
from models.motion_vae import DecoderGRU6d, GaussianGRU
from utils.forward_kinematics import forward_kinematics_fast
from utils.pose_processing import extract_tracked_points
from utils.pose_utils import local_to_world


class DecoderGRU6dFull4tp(VAEFull):
    """
    This module performs all inference steps (latent code sampling, pose estimation, and FK)
    """
    def __init__(self, input_size, output_size, hidden_size, n_layers, batch_size, kinematic_chain, bone_lengths, learning_mode, device):
        self.batch_size = batch_size
        self.device = device
        self.kinematic_chain = kinematic_chain
        self.bone_lengths = torch.unsqueeze(torch.from_numpy(bone_lengths).float().to(self.device), 0)
        self.learning_mode = learning_mode

        self.encoder = GaussianGRU(input_size - 30, 30, hidden_size, n_layers, batch_size, device, learning_mode=self.learning_mode)
        self.encoder.to(self.device)
        self.encoder.eval()

        self.decoder = DecoderGRU6d(input_size, output_size, hidden_size, n_layers, batch_size, device)
        self.decoder.to(self.device)
        self.decoder.eval()

        self.reset()

    def reset(self):
        self.eps = torch.zeros((self.batch_size, 30)).normal_().to(self.device)
        self.decoder.init_hidden(self.batch_size)
        self.encoder.init_hidden(self.batch_size)

    def load(self, network_path):
        decoder_state_dict = torch.load(network_path)["decoder"]
        encoder_state_dict = torch.load(network_path)["prior_net"]
        self.decoder.load_state_dict(decoder_state_dict)
        self.encoder.load_state_dict(encoder_state_dict)

    def evaluate_frame(self, sample, frame):
        output = {}

        with torch.no_grad():
            opt = {}
            opt["dataset_type"] = "mocap_ours"
            opt["input_types"] = ["tracking4", "vtracking4"]
            opt = Namespace(**opt)

            batch_size = self.eps.shape[0]

            position = extract_tracked_points(sample, frame, opt, "position").float().to(self.device)
            velocity = extract_tracked_points(sample, frame, opt, "velocity").float().to(self.device)
            repeat_position = torch.repeat_interleave(position, batch_size, dim=0)
            repeat_velocity = torch.repeat_interleave(velocity, batch_size, dim=0)

            if self.learning_mode == "supervised":
                encoder_input_data = torch.cat((repeat_position, repeat_velocity), dim=-1)
                z, _, _, _ = self.encoder(encoder_input_data)
                decoder_input_data = torch.cat((repeat_position, repeat_velocity, z), dim=-1)
                decoder_output = self.decoder(decoder_input_data)
            else:
                input_data = torch.cat((repeat_position, repeat_velocity, self.eps), dim=-1)
                decoder_output = self.decoder(input_data)

            fk_output = forward_kinematics_fast(
                self.device,
                decoder_output["output"],
                self.kinematic_chain,
                self.bone_lengths,
            )

            fk_position = torch.reshape(fk_output["pos"], (batch_size, -1, 3))
            fk_rotation = torch.reshape(fk_output["rot"], (batch_size, -1, 3, 3))

            root_position = sample["root_pos"][:, [frame]].float().to(self.device)
            root_rotation = sample["root_rot_mat"][:, [frame]].float().to(self.device)

            world_output = local_to_world(fk_position, fk_rotation, root_position, root_rotation)

            world_output["position"] = torch.reshape(world_output["position"], (batch_size, -1))
            world_output["rotation"] = torch.reshape(world_output["rotation"], (batch_size, -1))

            output["position"] = world_output["position"]
            output["rotation"] = world_output["rotation"]
            output["contact"] = decoder_output["contact"]
            output["eps"] = self.eps
            output["hidden_decoder"] = copy.deepcopy(self.decoder.get_hidden())

        return output

    def interpolate(self, frame_sample: Dict, src0: int, src1: int, alpha: float):
        hidden = []
        for i in range(len(frame_sample["hidden_decoder"])):
            hidden_0 = frame_sample["hidden_decoder"][i][[src0]]
            hidden_1 = frame_sample["hidden_decoder"][i][[src1]]

            hidden.append((hidden_0 * (1.0 - alpha)) + (hidden_1 * alpha))

        self.decoder.set_hidden(hidden)
        self.eps = ((1.0 - alpha) * frame_sample["eps"][src0]) + (alpha * frame_sample["eps"][[src1]])


class DecoderGRU6dFull3tp(DecoderGRU6dFull4tp):
    """
    This module performs all inference steps (latent code sampling, pose estimation, and FK)
    """
    def __init__(self, input_size, output_size, hidden_size, n_layers, batch_size, kinematic_chain, bone_lengths, device):
        super(DecoderGRU6dFull3tp, self).__init__(input_size, hidden_size, n_layers, batch_size, kinematic_chain, bone_lengths, device)

        self.wo_network = OcclusionNetVAE(lock_eps=True, learning_mode="vae", device=self.device).to(self.device)
        self.wo_network.init_hidden(1)
        self.wo_network.select_new_eps(1)
        self.wo_network.eval()

        self.reset()

    def load(self, wo_path, decoder_path):
        self.decoder.load_state_dict(decoder_path)
        self.wo_network.load_state_dict(wo_path)

    def evaluate_frame(self, sample, frame):
        output = {}
        import pdb; pdb.set_trace()

        with torch.no_grad():
            data = self.decoder(self.input_data)
            forward_kinematics_fast()

        return output