from abc import abstractmethod

from models.inference.inference_base import InferenceFull


class VAEFull(InferenceFull):
    def __init__(self):
        self.eps = None

    def get_eps(self):
        return self.eps

    def set_eps(self, eps):
        self.eps = eps

    @abstractmethod
    def evaluate_frame(self, sample, frame):
        pass