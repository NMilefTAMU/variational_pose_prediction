from typing import Dict
import torch
import torch.nn as nn

from utils.rotation_util import compute_rotation_matrix_from_ortho6d


# start of our code
class OcclusionNet(nn.Module):
    def __init__(
        self,
        input_size=18,
        output_size=9,
        hidden_size=128,
        n_layers=1,
        batch_size=1,
        lock_eps=False,
        device=torch.device("cpu")):
        
        super(OcclusionNet, self).__init__()

        self.input_size = input_size
        self.output_size = output_size
        self.hidden_size = hidden_size
        self.n_layers = n_layers
        self.batch_size = batch_size
        self.lock_eps = lock_eps
        self.device = device

        # layers
        self.linear0 = nn.Linear(input_size, hidden_size)
        self.gru = nn.ModuleList([nn.GRUCell(hidden_size, hidden_size) for i in range(self.n_layers)])
        self.linear1 = nn.Linear(hidden_size, hidden_size)
        self.hidden = self.init_hidden()

    def init_hidden(self, num_samples=None):
        batch_size = num_samples if num_samples is not None else self.batch_size
        hidden = []
        for i in range(self.n_layers):
            hidden.append(torch.zeros(batch_size, self.hidden_size).requires_grad_(False).to(self.device))
        self.hidden = hidden
        return hidden

    def forward(self, x):
        h_in = torch.relu(self.linear0(x))

        for i in range(self.n_layers):
            self.hidden[i] = self.gru[i](h_in, self.hidden[i])
            h_in = self.hidden[i]

        x0 = self.linear1(h_in)
        return x0

    @staticmethod
    def compute_root_pos_and_rot(x: torch.Tensor, device: torch.device) -> Dict:
        '''
        Convert output to dictionary of root pos and root rot

        Args:
            x: [N, 9]

        Returns:
            dict: contains "root_pos" and "root_rot"
        '''

        output = {}
        output["root_pos"] = x[:, :3]
        output["root_rot_mat"] = compute_rotation_matrix_from_ortho6d(x[:, 3:])
        return output