from typing import Dict
from models.root_pred.occlusion_network import OcclusionNet
import torch
import torch.nn as nn

from utils.rotation_util import compute_rotation_matrix_from_ortho6d


class OcclusionNetEncoder(nn.Module):
    def __init__(
        self,
        input_size=18,
        output_size=9,
        hidden_size=128,
        n_layers=1,
        batch_size=1,
        learning_mode="ae",
        lock_eps=False,
        device=torch.device("cpu")):

        super(OcclusionNetEncoder, self).__init__()

        self.input_size = input_size
        self.output_size = output_size
        self.hidden_size = hidden_size
        self.n_layers = n_layers
        self.batch_size = batch_size
        self.learning_mode = learning_mode
        self.lock_eps = lock_eps
        self.device = device
        self.eps = None

        # layers
        self.embed = nn.Linear(input_size, hidden_size)
        self.gru = nn.ModuleList([nn.GRUCell(hidden_size, hidden_size) for i in range(self.n_layers)])

        if self.learning_mode == "vae":
            self.mu_net = nn.Linear(hidden_size, output_size)
            self.logvar_net = nn.Linear(hidden_size, output_size)
        elif self.learning_mode == "ae":
            self.output_net = nn.Linear(hidden_size, output_size)

        self.hidden = self.init_hidden()

    def init_hidden(self, num_samples=None):
        batch_size = num_samples if num_samples is not None else self.batch_size
        hidden = []
        for i in range(self.n_layers):
            hidden.append(torch.zeros(batch_size, self.hidden_size).requires_grad_(False).to(self.device))
        self.hidden = hidden
        return hidden

    def select_new_eps(self, batch_size, num_dim):
        eps = torch.zeros((batch_size, num_dim)).to(self.device).normal_()
        self.eps = eps

    def select_new_eps_index(self, num_dim, index=0):
        self.eps[index:index+1] = torch.zeros((1, num_dim)).to(self.device).normal_()

    def get_eps(self):
        return self.eps

    def set_eps(self, eps):
        self.eps = eps

    def get_hidden(self):
        return self.hidden

    def set_hidden(self, hidden):
        for i in range(len(hidden)):
            self.hidden[i] = hidden[i]

    def reparameterize(self, mu, logvar, eps=None):
        s_var = logvar.mul(0.5).exp_()

        if (self.lock_eps and self.eps is None) or (not self.lock_eps):
            eps = s_var.data.new(s_var.size()).normal_()
            self.eps = eps
        else:
            eps = self.eps # use same EPS for each frame

        return eps.mul(s_var).add_(mu)

    def forward(self, inputs):
        embedded = self.embed(inputs.view(-1, self.input_size))
        h_in = embedded
        for i in range(self.n_layers):
            self.hidden[i] = self.gru[i](h_in, self.hidden[i])
            h_in = self.hidden[i]

        if self.learning_mode == "vae":
            mu = self.mu_net(h_in)
            logvar = self.logvar_net(h_in)
            z = self.reparameterize(mu, logvar, self.eps)
        elif self.learning_mode == "ae":
            z = self.output_net(h_in)
            mu = None
            logvar = None

        return z, mu, logvar, h_in


class OcclusionNetDecoder(nn.Module):
    def __init__(self, input_size, output_size, hidden_size, n_layers, batch_size, device):
        super(OcclusionNetDecoder, self).__init__()
        self.input_size = input_size
        self.output_size = output_size
        self.hidden_size = hidden_size
        self.batch_size = batch_size
        self.n_layers = n_layers
        self.device = device
        self.embed = nn.Linear(input_size, hidden_size)
        self.gru = nn.ModuleList([nn.GRUCell(hidden_size, hidden_size) for i in range(self.n_layers)])

        self.output = nn.Linear(hidden_size, output_size)
        self.hidden = self.init_hidden()

    def init_hidden(self, num_samples=None):
        batch_size = num_samples if num_samples is not None else self.batch_size
        hidden = []
        for i in range(self.n_layers):
            hidden.append(torch.zeros(batch_size, self.hidden_size).requires_grad_(False).to(self.device))
        self.hidden = hidden
        return hidden

    def get_hidden(self):
        return self.hidden

    def set_hidden(self, hidden):
        for i in range(len(hidden)):
            self.hidden[i] = hidden[i]

    def forward(self, inputs):
        embedded = self.embed(inputs.view(-1, self.input_size))
        h_in = embedded
        for i in range(self.n_layers):
            self.hidden[i] = self.gru[i](h_in, self.hidden[i])
            h_in = self.hidden[i]
        return self.output(h_in), h_in


class OcclusionNetVAE(nn.Module):
    def __init__(self,
        input_size=3*6,
        z_size=8,
        output_size=3+6,
        hidden_size=128,
        n_layers=1,
        batch_size=1,
        learning_mode="ae",
        lock_eps=False,
        device=torch.device("cpu")
    ):
        
        super(OcclusionNetVAE, self).__init__()

        self.input_size = input_size
        self.z_size = z_size
        self.output_size = output_size
        self.hidden_size = hidden_size
        self.n_layers = n_layers
        self.batch_size = batch_size
        self.device = device
        self.learning_mode = learning_mode
        self.lock_eps = lock_eps

        self.encoder = OcclusionNetEncoder(input_size, z_size, hidden_size, n_layers, batch_size, learning_mode, lock_eps, device)
        self.decoder = OcclusionNetDecoder(input_size + z_size, output_size, hidden_size, n_layers, batch_size, device)

    def init_hidden(self, num_samples=None):
        self.encoder.init_hidden(num_samples)
        self.decoder.init_hidden(num_samples)

    def select_new_eps(self, batch_size):
        self.encoder.select_new_eps(batch_size, self.z_size)

    def select_new_eps_index(self, index=0):
        self.encoder.select_new_eps_index(self.z_size, index)

    def get_eps(self):
        return self.encoder.get_eps()

    def set_eps(self, eps):
        self.encoder.set_eps(eps)

    def get_hidden(self):
        return self.encoder.get_hidden()

    def set_hidden(self, hidden):
        self.encoder.set_hidden(hidden)

    def forward(self, inputs):
        z, mu, logvar, _ = self.encoder(inputs)
        output, _ = self.decoder(torch.cat((inputs, z), dim=1))

        output = OcclusionNet.compute_root_pos_and_rot(output, self.device)

        return output, mu, logvar