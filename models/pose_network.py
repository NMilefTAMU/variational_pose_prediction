import torch
import torch.nn as nn

from dataProcessing.dataset_mocap import DatasetMocap


class PoseModelNetwork(nn.Module):
    def __init__(self, input_size=57, output_size=108, num_levels=3):
        super(PoseModelNetwork, self).__init__()

        self.input_size = input_size
        self.output_size = output_size
        self.num_levels = num_levels

        layers = []

        # encoder
        for i in range(self.num_levels):
            in_size = input_size // (2 ** i)
            out_size = input_size // (2 ** (i+1))
            layer = nn.Linear(in_size, out_size)
            layers.append(layer)

        # bottleneck
        in_size = input_size // (2 ** self.num_levels)
        out_size = input_size // (2 ** self.num_levels)
        layer = nn.Linear(in_size, out_size)
        layers.append(layer)

        # decoder
        for i in range(self.num_levels - 1, -1, -1):
            in_size = input_size // (2 ** (i+1))
            out_size = input_size // (2 ** i)
            if i == 0:
                out_size = self.output_size
            layer = nn.Linear(in_size, out_size)
            layers.append(layer)

        self.layers = nn.ModuleList(layers)

    def forward(self, x):
        x_input = x
        for i in range(len(self.layers) - 1):
            x = torch.relu(self.layers[i](x))

        x0 = self.layers[-1](x)
        x0 = torch.cat((x_input[:, 0:3], x0), dim=-1)

        return x0

def extract_lower_body(pose):
    bone_map = DatasetMocap.get_bone_map()
    names = ["Hips", "LeftUpLeg", "LeftLeg", "LeftFoot", "LeftToeBase", "RightUpLeg", "RightLeg", "RightFoot", "RightToeBase"]
    indices = []

    for name in names:
        bone_index = bone_map[name]
        indices.extend([bone_index*3, bone_index*3+1, bone_index*3+2])

    return pose[..., indices]

def convert_to_full_body(pose):
    bone_map = DatasetMocap.get_bone_map()
    names = ["Hips", "LeftUpLeg", "LeftLeg", "LeftFoot", "LeftToeBase", "RightUpLeg", "RightLeg", "RightFoot", "RightToeBase"]

    full_body_pose = torch.zeros_like(pose[..., 0])
    full_body_pose = torch.unsqueeze(full_body_pose, dim=-1)
    full_body_pose = torch.repeat_interleave(full_body_pose, repeats=57, dim=-1)

    index = 0
    for name in names:
        bone_index = bone_map[name]
        full_body_pose[..., bone_index*3:bone_index*3+3] = pose[..., index*3:index*3+3]

        index += 1

    return full_body_pose

def combine_lower_and_full_body(lower_pose, full_body_pose):
    output = convert_to_full_body(lower_pose)

    bone_map = DatasetMocap.get_bone_map()
    lower_body_names = ["Hips", "LeftUpLeg", "LeftLeg", "LeftFoot", "LeftToeBase", "RightUpLeg", "RightLeg", "RightFoot", "RightToeBase"]

    upper_body_names = list(set(bone_map.keys()) - set(lower_body_names))

    for name in upper_body_names:
        bone_index = bone_map[name]
        output[..., bone_index*3:bone_index*3+3] = full_body_pose[..., bone_index*3:bone_index*3+3]

    return output