import torch
import torch.nn as nn


class PredictionNet(nn.Module):
    def __init__(
        self,
        input_size=18,
        output_size=6,
        hidden_size=128,
        n_layers=1,
        batch_size=1,
        device=torch.device("cpu")
    ):
        super(PredictionNet, self).__init__()

        self.input_size = input_size
        self.output_size = output_size
        self.hidden_size = hidden_size
        self.n_layers = n_layers
        self.batch_size = batch_size
        self.device = device

        self.linear0 = nn.Linear(self.input_size, self.hidden_size)
        self.gru = nn.ModuleList([nn.GRUCell(self.hidden_size, self.hidden_size) for i in range(self.n_layers)])
        self.linear1 = nn.Linear(self.hidden_size, self.output_size)

        self.hidden = self.init_hidden()

    def init_hidden(self, num_samples=None):
        batch_size = num_samples if num_samples is not None else self.batch_size
        hidden = []
        for i in range(self.n_layers):
            hidden.append(torch.zeros(batch_size, self.hidden_size).requires_grad_(False).to(self.device))
        self.hidden = hidden
        return hidden

    def forward(self, x):
        h_in = torch.relu(self.linear0(x))

        for i in range(self.n_layers):
            self.hidden[i] = self.gru[i](h_in, self.hidden[i])
            h_in = self.hidden[i]

        x0 = self.linear1(h_in)
        return x0