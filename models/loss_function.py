import torch
import torch.nn as nn
from utils.forward_kinematics import forward_kinematics

class VelocityLoss(nn.Module):
    """
    Velocity loss
    """
    def __init__(self):
        super(VelocityLoss, self).__init__()
        self.loss = nn.MSELoss()

    def forward(self, eval_pos, gt_pos_prev, gt_pos):
        d_eval = eval_pos - gt_pos_prev
        d_gt = gt_pos - gt_pos_prev

        return self.loss(d_eval, d_gt)

class RotationLoss(nn.Module):
    """
    Rotation loss
    """
    def __init__(self):
        super(RotationLoss, self).__init__()
        self.loss = nn.MSELoss()

    def forward(self, pred_rot, gt_rot):
        return self.loss(pred_rot, gt_rot)

class ForwardKinematicsLoss(nn.Module):
    """
    Forward kinematics loss
    """
    def __init__(self, device):
        super(ForwardKinematicsLoss, self).__init__()
        self.device = device
        self.loss = nn.MSELoss()

    def forward(self, rots, target_joints, kinematic_chain, bone_lengths):
        """
        Forward kinematics loss

        Args:
            rots: [N, 18*6 + 3]
            target_joints: [N, 19*3]
            bone_lengths: [N, 19]

        Returns:
            loss
        """
        generated_joints = forward_kinematics(self.device, rots, kinematic_chain, bone_lengths)
        return self.loss(generated_joints["pos"], target_joints)
