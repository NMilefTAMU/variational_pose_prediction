import argparse
import random
import time
from typing import List

import numpy as np
from environment.environment import Environment
import torch
import yaml
from direct.showbase.ShowBase import ShowBase
from panda3d.core import LPoint3, LVector3, AntialiasAttrib

from evaluate import Inference
from evaluate_batch import InferenceBatch
from evaluate_online import InferenceOnline
from viewer.scene import ViewerScene


class ViewerBase(ShowBase):
    def __init__(self, args):
        super().__init__()
        self.frame = 0
        self.framerate = args.framerate
        self.frame = 0
        self.last_time = 0
        self.playback_speed = 1.0
        self.num_joints = 19
        self.methods = args.methods
        self.num_gen = args.num_gen

        self.play = False
        self.debug = False
        self.debug_bl = False
        self.debug_contact = True
        self.follow_camera = False

        self.scene = ViewerScene(self)

        if args.environment:
            self.environment = Environment(self, args.environment)

        if args.camera_config:
            with open(args.camera_config, 'r') as stream:
                config_file = yaml.safe_load(stream)

                if 'pos' in config_file:
                    self.disableMouse()
                    self.camera_offset = LPoint3(config_file['pos'][0], config_file['pos'][1], config_file['pos'][2])
                    look_at = LPoint3(config_file['look_at'][0], config_file['look_at'][1], config_file['look_at'][2])
                    up = LVector3(0, -1, 0)

                    self.camera.setPos(self.camera_offset)
                    self.camera.lookAt(look_at, up)

                if 'follow' in config_file:
                    if config_file['follow']:
                        self.follow_camera = True
                    else:
                        self.follow_camera = False

                if 'light_pos' in config_file:
                    lpos = config_file['light_pos']
                    self.scene.lightNP.setPos(lpos[0], lpos[1], lpos[2])
                    self.scene.lightNP.lookAt(0, 0, 0)

        self.accept('space', self.toggle_play)
        self.accept('arrow_up', self.increase_playback_speed)
        self.accept('arrow_down', self.decrease_playback_speed)

    def update_time(self):
        # process joints
        current_time = time.time() * 1000
        dT = int((1000 / self.playback_speed)) / self.framerate

        if self.last_time == 0:
            self.last_time = current_time

        if current_time - self.last_time >= dT and self.play:
            self.frame += int((current_time - self.last_time) // dT)
            self.frame = self.frame % self.num_frames
            self.last_time = current_time

    def update_camera(self, root_pos):
        '''
        Update camera position

        Args:
            root_pos: root position (could be numpy array, torch tensor, or list)
        '''
        pos = -np.array([-root_pos[0], 0, root_pos[2]])
        pos = LPoint3(pos[0], pos[1], pos[2])
        offset = self.camera_offset + pos
        self.camera.setPos(offset[0], offset[1], offset[2])

    def toggle_play(self):
        if not self.play:
            self.last_time = time.time() * 1000

        self.play = not self.play

    def increase_playback_speed(self):
        self.playback_speed = min(self.playback_speed * 2, 16)

    def decrease_playback_speed(self):
        self.playback_speed = max(self.playback_speed / 2, 1.0/16.0)

class ViewerBaseOnline(ViewerBase):
    def __init__(self, args, device=None):
        super().__init__(args)

        if device is None:
            self.device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
        else:
            self.device = device
        torch.cuda.set_device(0)

class ViewerArgumentParser:
    def __init__(self):
        self.parser = argparse.ArgumentParser()
        self.parser.add_argument('--camera_config', help='camera configuration file', default=None)
        self.parser.add_argument('--choreography', '-ch', help='choreography file')
        self.parser.add_argument('--dataset', '-d', help='dataset name', required=True)
        self.parser.add_argument('--dataset_type', '-dt', help='dataset type', default="valid")
        self.parser.add_argument('--disable_ik', action="store_true")
        self.parser.add_argument('--disable_interpolation', nargs='+', help='disable input interpolation', default=[])
        self.parser.add_argument('--disable_root_transform', action="store_true", help='disable_root_transform')
        self.parser.add_argument('--environment', '-en', help='environment to load')
        self.parser.add_argument('--experiment', '-e', help='experiment name', required=True)
        self.parser.add_argument('--framerate', '-f', type=int, help='framerate to play the animation', default=60)
        self.parser.add_argument('--inference_mode', type=str, help="inference mode [single, batch]", default="batch")
        self.parser.add_argument('--lobstr_model', type=str, help="LoBSTr model", default='lobstr')
        self.parser.add_argument('--lock_eps', action='store_true', help='lock epsilon in VAE sampling')
        self.parser.add_argument('--lookahead', type=int, help='lookahead number of frames', default=0)
        self.parser.add_argument('--num_gen', type=int, help='number of generated animations', default=1)
        self.parser.add_argument('--num_tp', type=int, help="number of tracking points", default=4)
        self.parser.add_argument('--methods', nargs='+', default=['ours', 'gt', 'lobstr'])
        self.parser.add_argument('--save_action', action="store_true", help='save action labels to a file')
        self.parser.add_argument('--save_npz', action="store_true", help='save output as .npz')
        self.parser.add_argument('--save_z', action="store_true", help='save z parameters to a file')
        self.parser.add_argument('--scene', type=str, help="scene file with configuration")
        self.parser.add_argument('--sequence_names', nargs='+', help='sequence_names')
        self.parser.add_argument('--set_seed', type=int, help='set seed for sampling (e.g., for reproducibility and comparisons)')
        self.parser.add_argument('--smart_eps', type=int, help='number of epsilon values for smart selection algorithm', default=1)
        self.parser.add_argument('--use_eps_net', action="store_true", help="use EPS network")
        self.parser.add_argument('--lobstr_dataset', '-ld', help='LoBSTr dataset', default="ours")
        self.parser.add_argument('--selection_mode', '-sm', help='sample selection mode [ip, jp, none]', default="ip")
        self.parser.add_argument('--sm_start_frame', '-smsf', type=int, help='sample selection start frame', default=0)
        self.parser.add_argument('--sm_passthrough', '-smpt', action="store_true", help="sample selection passthrough")
        self.parser.add_argument('--lock_sample', action='store_true')
        self.parser.add_argument('--vr', action='store_true')
        self.parser.add_argument("--recorded_tp", help="recorded tracked points", default=None)
        self.parser.add_argument("--calibration_tp", type=str, help="sequence name for calibration")

    def parse_args(self):
        args = self.parser.parse_args()
        if args.smart_eps <= 0:
            raise ValueError("number of epsilon values must be >= 1")
        return args
