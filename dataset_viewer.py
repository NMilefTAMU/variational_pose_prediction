import argparse
import time
import os

from direct.showbase.ShowBase import ShowBase
from direct.task import Task

from viewer.scene import ViewerScene
from dataProcessing.dataset_mocap_lobstr import DatasetMocapLoBSTr
from dataProcessing.dataset_mocap_pfnn import DatasetMocapPFNN
from options.options import Options, get_options_profile


class PosePredictionApp(ShowBase):
    def __init__(self, args):
        ShowBase.__init__(self)
        opt = Options(use_profile=False)
        opt.dataset_type = "mocap_lobstr"
        opt.model_type = "vae_ours"
        opt.motion_length = -1
        opt.normalize_inputs = True

        self.play = False
        self.framerate = args.framerate
        self.last_time = 0
        self.frame = 0
        self.current_sequence_index = 0
        self.current_sequence_name = ""
        self.playback_speed = 1.0

        self.joint_scale = 0.025
        self.dataset = DatasetMocapLoBSTr("./dataset/mocap_lobstr/output/", opt, mode="valid")

        scene = ViewerScene(self)

        self.setup_joints()

        self.accept('space', self.toggle_play)
        self.task_mgr.add(self.update, "Update Task")

    def toggle_play(self):
        self.play = not self.play

    def setup_joints(self):
        self.joints = []
        for j in range(self.dataset[0]["position"].shape[1] // 3):
            joint = self.loader.load_model('assets/models/sphere.obj')
            joint.setScale(self.joint_scale, self.joint_scale, self.joint_scale)
            joint.setColor(1.0, 0, 0)
            joint.reparent_to(self.render)
            self.joints.append(joint)

    def render_joints(self, frame_data):
        root_pos = frame_data[0:3]
        for j in range(self.dataset[0]["position"].shape[1] // 3):
            self.joints[j].setPos(
                frame_data[j * 3 + 0] - root_pos[0],
                -frame_data[j * 3 + 1],
                frame_data[j * 3 + 2] - root_pos[2]
            )

    def update(self, task):
        current_time = time.time() * 1000
        dT = int((1000 / self.playback_speed)) / self.framerate

        if self.last_time == 0:
            self.last_time = current_time

        if current_time - self.last_time >= dT and self.play:
            self.frame += int((current_time - self.last_time) // dT)
            self.frame = self.frame % self.dataset[0]["position"].shape[-2]
            self.last_time = current_time

        self.render_joints(self.dataset[0]["position"][self.frame])

        return Task.cont

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('--framerate', '-f', type=int, help='framerate to play the animation', default=30)
    parser.add_argument('--sequence_names', nargs='+', help='sequence_names')
    args = parser.parse_args()

    app = PosePredictionApp(args)
    app.run()
