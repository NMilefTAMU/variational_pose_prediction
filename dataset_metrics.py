'''
Compute metrics and examine full dataset
'''
import csv
import os
import argparse
from argparse import Namespace

import numpy as np
import torch

from dataProcessing.dataset_mocap import DatasetMocap
from options.options import Options
from scipy.spatial.transform.rotation import Rotation as R

def process_dataset(args, mode):
    dataset_dir = "./dataset/mocap_ours/output/"
    files = os.listdir(dataset_dir)
    ds_args = {
        "dataset_type": "mocap_ours",
        "model_type": "vae_6d",
        "motion_length": -1,
        "normalize_mode": "yaw",
    }

    opt = Options(Namespace(**ds_args))
    dataset = DatasetMocap(dataset_dir, opt, mode)

    if args.rotation:
        compute_rotation(args, mode, dataset)
    if args.metrics:
        compute_metrics(args, mode, dataset)


def compute_rotation(args, mode, dataset):
    root_dir = os.path.join("./stats/rot_angles/", mode)
    os.makedirs(root_dir, exist_ok=True)
    for i in range(len(dataset)):
        filename = dataset.files[i].split(".npy")[0]
        sample = dataset[i]
        root_rot = R.from_quat(sample["root_rot"])
        forward = np.zeros((len(root_rot), 3))
        forward[:, 2] = 1
        transformed = root_rot.apply(forward)
        angles = np.degrees(np.arctan2(transformed[:, 0], transformed[:, 2]))
        np.savetxt(os.path.join(root_dir, filename + ".csv"), angles, delimiter=",")

def compute_metrics(args, mode, dataset):
    with open("dataset_metrics_" + mode + ".csv", "w", newline="") as csvfile:
        writer = csv.writer(csvfile, delimiter=",")
        for i in range(len(dataset)):
            filename = dataset.files[i].split(".npy")[0]
            sample = dataset[i]
            contact_l = np.sum(sample["contact"], axis=0)[0]
            contact_r = np.sum(sample["contact"], axis=0)[1]
            length = sample["contact"].shape[0]

            writer.writerow([
                filename,
                contact_l,
                contact_r,
                length])


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--metrics", action="store_true")
    parser.add_argument("--rotation", action="store_true")
    parser.add_argument("--mode", type=str, default="valid")
    args = parser.parse_args()

    process_dataset(args, args.mode)
