import copy
import numpy as np
import torch
from panda3d.core import CardMaker, LVector4, LVector3, TextNode, TransparencyAttrib
from direct.gui.OnscreenText import OnscreenText
from viewer.stats_display import GraphDisplay, set_coords

class PoseErrorDisplay(GraphDisplay):
    def __init__(self, base, bone_map):
        super().__init__(base)

        self.bounds = [0.0, 0.0, 0.3, 1.0]
        set_coords(self.background, self.bounds)
        self.background.setTransparency(TransparencyAttrib.M_none)
        self.background.setColor(LVector3(0.3, 0.3, 0.3))

        self.bone_map = bone_map    
        self.bone_names = self.bone_map.keys() 

        self.bone_text = []
        self.bone_values = []
        self.bone_values_bg = []
        index = 0
        for bone_name in self.bone_names:
            bone_text = OnscreenText(
                text=bone_name,
                parent=base.a2dTopLeft,
                fg=(1,1,1,1),
                pos=(0.0, -0.1 - (index * 0.1)),
                scale=0.05,
                align=TextNode.ALeft,
            )
            self.bone_text.append(bone_text)
            self.bone_value_bounds = [0.15, 0.03 + index * 0.05, 0.28, 0.03 + index * 0.05 + 0.04]

            bone_value_bg = CardMaker("cm")
            bone_value_bg.setFrameFullscreenQuad()
            self.bone_values_bg.append(base.render2d.attachNewNode(bone_value_bg.generate()))
            set_coords(self.bone_values_bg[-1], self.bone_value_bounds)
            self.bone_values_bg[-1].setColor(LVector3(0.7, 0.7, 0.7))
            self.bone_values_bg[-1].setTransparency(TransparencyAttrib.M_none)

            bone_value = CardMaker("cm")
            bone_value.setFrameFullscreenQuad()
            self.bone_values.append(base.render2d.attachNewNode(bone_value.generate()))
            set_coords(self.bone_values[-1], self.bone_value_bounds)
            self.bone_values[-1].setColor(LVector3(1, 0, 0))
            self.bone_values[-1].setTransparency(TransparencyAttrib.M_none)

            index += 1

    def render_new_stats(self, pose_model_positions, network_positions, kinematic_chain):
        position_error = torch.norm(
            pose_model_positions.view(19, 3) - network_positions.view(19, 3),
            dim=-1,
        )

        index = 0
        for bone_name in self.bone_names:
            bone_bounds = [0.15, 0.03 + (18-index) * 0.05, 0.28, 0.03 + (18-index) * 0.05 + 0.04]

            total_bar_length = bone_bounds[2] - bone_bounds[0]
            bar_length = np.clip(position_error[index].item(), 0.01, 1.0) * total_bar_length

            bone_bounds[2] = bone_bounds[0] + bar_length

            set_coords(self.bone_values[index], bone_bounds)
            index += 1