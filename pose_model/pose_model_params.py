import yaml

class PoseModelParams:
    """
    Class for parameters for the pose model network
    """
    def __init__(self):
        self.input_types = ["pos"]
        self.num_levels = 3
        self.recurrent = False
        self.train_mode = "full"

    def save(self, filename: str):
        """
        Save yaml file

        Args:
            filename: yaml file
        """
        with open(filename, "w") as file:
            yaml.dump(self.__dict__, file)

    def load(self, filename: str):
        """
        Load yaml file

        Args:
            filename: yaml file
        """
        with open(filename, 'r') as stream:
            config_file = yaml.safe_load(stream)
            for key, value in config_file.items():    
                self.__setattr__(key, value)
