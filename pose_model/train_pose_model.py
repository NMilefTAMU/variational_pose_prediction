import argparse
import os
import sys
import time

import torch
import torch.nn as nn
import torch.optim as optim
from torch.utils.data import DataLoader
from torch.utils.tensorboard import SummaryWriter


sys.path.insert(0, os.path.join(os.path.dirname(os.path.abspath(__file__)), ".."))

from utils.forward_kinematics import forward_kinematics
import utils.paramUtil as paramUtil
from pose_model.pose_model_params import PoseModelParams

from dataProcessing.dataset_mocap_ours import DatasetMocapOurs
from models.pose_network import PoseModelNetwork, extract_lower_body
from utils.get_opt import get_opt


def calc_accuracy(pred, label):
    pr = torch.round(torch.sigmoid(pred))
    return float(torch.sum(pr == label).float() / torch.numel(pred))

def calc_spatial_error(pred, gt):
    pred_3d = pred.reshape((-1,3))
    gt_3d = gt.reshape((-1,3))
    error = torch.mean(torch.norm(pred_3d - gt_3d, dim=-1))
    return error

def extract_inputs(sample, args):
    position = sample["position"]

    if args.train_mode == "lower":
        position = extract_lower_body(position)

    return position

def train_eps(args):
    num_iterations = args.iters

    filename = "pose_model_" + "_".join(args.input_types) + "_" + args.train_mode + ".pth"

    writer = SummaryWriter("./runs_pose_model/" + str(time.time() * 1000))
    device = torch.device("cuda:" + str(args.gpu_id) if torch.cuda.is_available() else "cpu")

    dataset_opt_path = './checkpoints/vae/' + args.dataset + '/' + args.vae_name + '/opt.txt'
    opt = get_opt(dataset_opt_path, 128, device)
    opt.motion_length = 1

    train_dataset = DatasetMocapOurs("./dataset/mocap_ours/output/", opt, "train")
    train_dataloader = DataLoader(train_dataset, int(args.batch_size), shuffle=True, drop_last=True)
    val_dataset = DatasetMocapOurs("./dataset/mocap_ours/output/", opt, "valid")
    val_dataloader = DataLoader(val_dataset, int(args.batch_size), shuffle=True, drop_last=True)

    dataset_params = paramUtil.get_dataset_params(opt)
    kinematic_chain = dataset_params["kinematic_chain"]

    pose_model_params = PoseModelParams()
    pose_model_params.input_types = args.input_types
    pose_model_params.num_levels = args.num_levels
    pose_model_params.recurrent = args.recurrent
    pose_model_params.train_mode = args.train_mode

    input_dim = 0
    if "pos" in pose_model_params.input_types:
        input_dim += 3
    if "vel" in pose_model_params.input_types:
        input_dim += 3

    if args.train_mode == "full":
        network = PoseModelNetwork(
            input_size=19 * input_dim,
            num_levels=pose_model_params.num_levels,
        )
    elif args.train_mode == "lower":
        network = PoseModelNetwork(
            input_size=9 * input_dim,
            num_levels=pose_model_params.num_levels,
        )

    network.to(device)

    loss_function = nn.MSELoss()
    optimizer = optim.Adam(network.parameters(), lr=0.0002, betas=(0.5, 0.999), weight_decay=0.00001)

    iterations = 1
    while iterations < num_iterations + 1:
        # training loop
        sample = next(iter(train_dataloader))

        optimizer.zero_grad()

        inputs = extract_inputs(sample, args).float().to(device)[:, 0, :]

        output_rot = network(inputs.clone())
        output = forward_kinematics(device, output_rot, kinematic_chain, sample["bone_lengths"])

        train_loss = loss_function(output["pos"], inputs)
        train_accuracy = calc_spatial_error(output["pos"], inputs)

        train_loss.backward()
        optimizer.step()

        # valid loop
        if iterations % 100 == 0:
            val_sample = next(iter(val_dataloader))
            with torch.no_grad():
                optimizer.zero_grad()

                inputs = extract_inputs(val_sample, args).float().to(device)[:, 0, :]

                output_rot = network(inputs.clone())
                output = forward_kinematics(device, output_rot, kinematic_chain, sample["bone_lengths"])

                valid_loss = loss_function(output["pos"], inputs)
                valid_accuracy = calc_spatial_error(output["pos"], inputs)

                print("Step:",
                      str(iterations),
                      "\tTrain Loss:",
                      float(train_loss.item()),
                      "\tValid Loss:",
                      float(valid_loss.item()))

                writer.add_scalar("train_loss", train_loss, iterations)
                writer.add_scalar("train_accuracy", train_accuracy, iterations)
                writer.add_scalar("valid_loss", valid_loss, iterations)
                writer.add_scalar("valid_accuracy", valid_accuracy, iterations)

        iterations += 1

        if iterations % args.save_every == 0:
            dir_name = "./checkpoints/eps/"
            os.makedirs(dir_name, exist_ok=True)
            torch.save(network.state_dict(), os.path.join(dir_name, filename))
            pose_model_params.save(os.path.join(dir_name, filename.split(".pth")[0] + ".yaml"))


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--batch_size", type=int, default=128)
    parser.add_argument("--dataset", type=str, default="mocap_ours")
    parser.add_argument("--framerate", type=int, default=60)
    parser.add_argument("--gpu_id", type=int, default=0)
    parser.add_argument("--input_types", default=["pos"])
    parser.add_argument("--iters", type=int, default=100000)
    parser.add_argument("--num_levels", type=int, default=3)
    parser.add_argument("--recurrent", action="store_true")
    parser.add_argument("--save_every", type=int, default=100)
    parser.add_argument("--train_mode", type=str, default="full")
    parser.add_argument("--vae_name", type=str, default="test_model_6d_8_25b")
    args = parser.parse_args()

    train_eps(args)
