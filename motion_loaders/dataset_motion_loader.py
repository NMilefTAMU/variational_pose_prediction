import utils.paramUtil as paramUtil
from dataProcessing import dataset
from dataProcessing.dataset_mocap_lobstr import DatasetMocapLoBSTr
from dataProcessing.dataset_mocap_ours import DatasetMocapOurs
from dataProcessing.dataset_mocap_pfnn import DatasetMocapPFNN
from torch.utils.data import DataLoader, RandomSampler
from torch.utils.data.dataset import Dataset
from torch.utils.data.sampler import SequentialSampler


class Options:
    def __init__(self, lie_enforce, no_trajectory, motion_length, coarse_grained):
        self.lie_enforce = lie_enforce
        self.no_trajectory = no_trajectory
        self.motion_length = motion_length
        self.coarse_grained = coarse_grained
        self.save_root = './model_file/'
        self.clip_set = './dataset/pose_clip_full.csv'

cached_dataset = {}


def get_dataset_motion_dataset(opt, label=None, mode="train"):
    if opt.opt_path in cached_dataset:
        return cached_dataset[opt.opt_path]

    if opt.dataset_type == "mocap":
        dataset_path = "./dataset/mocap/mocap_3djoints/"
        clip_path = './dataset/mocap/pose_clip.csv'
        mocap_options = Options(False, False, False, 100, True)
        data = dataset.MotionFolderDatasetMocap(clip_path, dataset_path, opt)
        if label is None:
            motion_dataset = dataset.MotionDataset(data, mocap_options)
        else:
            motion_dataset = dataset.MotionDataset4One(data, mocap_options, label)
    elif opt.dataset_type == "mocap_lobstr":
        dataset_path = "./dataset/mocap_lobstr/output/"
        mocap_options = Options(False, False, False, opt.motion_length, True)
        data = DatasetMocapLoBSTr(dataset_path, opt, mode=mode)
        if label is None:
            motion_dataset = dataset.MotionDataset(data, mocap_options)
        else:
            motion_dataset = dataset.MotionDataset4One(data, mocap_options, label)
    elif opt.dataset_type == "mocap_pfnn":
        dataset_path = "./dataset/mocap_pfnn/output/"
        mocap_options = Options(False, False, False, opt.motion_length, True)
        data = DatasetMocapPFNN(dataset_path, opt, mode=mode)
        if label is None:
            motion_dataset = dataset.MotionDataset(data, mocap_options)
        else:
            motion_dataset = dataset.MotionDataset4One(data, mocap_options, label)
    elif opt.dataset_type == "mocap_ours":
        dataset_path = "./dataset/mocap_ours/output/"
        mocap_options = Options(False, False, False, opt.motion_length, True)
        data = DatasetMocapOurs(dataset_path, opt, mode=mode)
        if label is None:
            motion_dataset = dataset.MotionDataset(data, mocap_options)
        else:
            motion_dataset = dataset.MotionDataset4One(data, mocap_options, label)
    else:
        raise NotImplementedError('Unrecognized dataset')

    cached_dataset[opt.opt_path] = motion_dataset
    return motion_dataset



def get_dataset_motion_loader(opt, num_motions, device, label=None, mode="train"):
    print('Generating Ground Truth Motion...')
    motion_dataset = get_dataset_motion_dataset(opt, label, mode)
    if num_motions == -1:
        motion_loader = DataLoader(motion_dataset, batch_size=1, num_workers=1,
            sampler=SequentialSampler(motion_dataset))
    else:
        motion_loader = DataLoader(motion_dataset, batch_size=1, num_workers=1,
            sampler=RandomSampler(motion_dataset, replacement=True, num_samples=num_motions))

    return motion_loader
