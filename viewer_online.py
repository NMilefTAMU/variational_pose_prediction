import time

import numpy as np
import torch
from direct.task import Task
from scipy.spatial.transform import Rotation

from dataProcessing.dataset_mocap_ours import DatasetMocapOurs
from models.root_pred.occlusion_network import OcclusionNet
from utils.pose_processing import extract_tracked_points
from viewer.skeleton import Skeleton
from viewer_base import ViewerArgumentParser, ViewerBaseOnline


class PosePredictionAppOnline(ViewerBaseOnline):
    '''
    Real-time viewer (reads one frame in at a time)
    '''
    def __init__(self, args):
        args.inference_mode = "online"

        super().__init__(args, torch.device("cpu"))

        self.reader = RealTimeReader(self.inference, args.record_vr)

        self.start_pose = torch.zeros((1, 57)).to(self.device)
        self.bone_lengths = self.inference.dataset.bone_lengths[0]

        self.play = True
        self.enable_tracked_points = True

        num_joints = 19
        self.skeleton = Skeleton(self, self.inference.opt, num_joints, color=(0, 1, 0), offset=(0, 0, 0))
        self.num_tp = args.num_tp

        self.task_mgr.add(self.update, "Update Task")

        self.accept('t', self.toggle_tracked_points)

    def toggle_tracked_points(self):
        self.enable_tracked_points = not self.enable_tracked_points

        if self.enable_tracked_points:
            self.skeleton.show_tracked_points()
        else:
            self.skeleton.hide_tracked_points()        

    def update(self, task):
        current_time = time.time() * 1000
        dT = int((1000 / self.playback_speed)) / self.framerate
        
        if self.last_time == 0:
            self.last_time = current_time

        if current_time - self.last_time >= dT and self.play:
            if self.num_tp == 3:
                sample = self.reader.next_frame(waist_occlusion=True)
            elif self.num_tp == 4:
                sample = self.reader.next_frame()

            output = self.inference.evaluate_frame(
                self.start_pose,
                sample["tracked_points"],
                None,
                self.bone_lengths,
                sample["root_pos"],
                Rotation.from_quat(sample["root_rot"].detach().cpu().numpy()))

            self.start_pose = output["local_position"]
            self.skeleton.render_frame(output["position"])

            self.skeleton.render_tracked_points(sample["tracked_points_raw"])
            #self.skeleton.render_root_axes(sample["root_rot"], sample["root_pos"])

            #import pdb; pdb.set_trace()

            self.last_time = current_time
            self.frame += 1

        return Task.cont


class RealTimeReader:
    '''
    Real-time data reader. Reads one frame at a time.
    '''
    def __init__(self, inference, record_vr=False):
        self.frame = 0
        self.inference = inference

        self.sequence = DatasetMocapOurs.convert_sample_to_torch(inference.dataset[0])
        self.num_frames = self.sequence["position"].shape[0]
        self.sequence_name = inference.dataset.get_anim_names()[0]

        self.device = torch.device("cpu")

        input_size = 3 * 6
        output_size = 3 + 6
        hidden_size = 128
        n_layers = 1

        self.wo_network = OcclusionNet(input_size, output_size, hidden_size, n_layers, 1, self.device)
        self.wo_network.load_state_dict(torch.load("checkpoints/waist_occlusion/wo_model.pth"))
        self.wo_network.init_hidden(1)
        self.wo_network.eval()

        self.last_waist_pos = torch.zeros((3))

        self.record_vr = record_vr
        if self.record_vr:
            self.recorded_data = []

    def apply_inv_rot(self, x, inv_rot):
        x_np = inv_rot.apply(x.detach().cpu().numpy())
        return torch.from_numpy(x_np).float().to(self.device)

    def next_frame(self, waist_occlusion=False):
        output = {}

        if waist_occlusion:
            #pos = self.sequence["raw_tracker_pos"][self.frame, :]
            #vel = self.sequence["raw_tracker_vel"][self.frame, :]
            pos = self.sequence["raw_tracker_pos"][self.frame, 3:]
            vel = self.sequence["raw_tracker_vel"][self.frame, 3:]
            wo_input = torch.unsqueeze(torch.cat((pos, vel), dim=-1), dim=0)
            with torch.no_grad():
                wo_output = OcclusionNet.compute_root_pos_and_rot(self.wo_network(wo_input), self.device)
            root_pos = wo_output["root_pos"]
            root_rot = wo_output["root_rot_mat"]
            #import pdb; pdb.set_trace()
            # zero out x and z rotations
            '''
            root_quat = Rotation.from_matrix(root_rot.detach().cpu().numpy()).as_quat()
            root_quat[:, 0] = 0 #x
            root_quat[:, 2] = 0 # z
            mag = np.sqrt((root_quat[:, 1]**2) + (root_quat[:, 3]**2))
            root_quat[:, 3] /= mag # w
            root_quat[:, 1] /= mag # y
            root_rot = torch.from_numpy(Rotation.from_quat(root_quat).as_matrix())
            '''

            root_offset = torch.clone(root_pos)
            root_offset[:, 1] = 0
            inv_root_rot = Rotation.from_matrix(root_rot.detach().cpu().numpy()).inv()

            #root_pos = self.sequence["root_pos"][self.frame:self.frame+1]
            #root_rot = self.sequence["root_rot"][self.frame:self.frame+1]
            #root_offset = root_pos
            #pos = (pos.reshape((-1, 3)) - root_offset).reshape((12))

            #inv_root_rot = Rotation.from_quat(root_rot.detach().cpu().numpy()).inv()

            waist_pos = torch.zeros((3)).to(self.device)
            waist_pos[1] = root_pos[0, 1]
            waist_vel = torch.zeros((3)).to(self.device)

            if self.frame > 0:
                waist_vel = torch.squeeze(root_pos - self.last_waist_pos)
            self.last_waist_pos = root_pos

            pos = torch.cat((root_pos[0], pos), dim=0)
            pos = (pos.reshape((-1,3)) - root_offset).reshape((12))
            vel = torch.cat((waist_vel, vel), dim=0)

            for i in range(4):
                pos[i*3:i*3+3] = self.apply_inv_rot(pos[i*3:i*3+3], inv_root_rot)
                vel[i*3:i*3+3] = self.apply_inv_rot(vel[i*3:i*3+3], inv_root_rot)

            #pos = torch.cat((waist_pos, pos), dim=0)
            #vel = torch.cat((waist_vel, vel), dim=0)

            # hack because head needs to be at start
            temp_pos = torch.clone(pos[9:12])
            temp_vel = torch.clone(vel[9:12])
            pos[9:12] = pos[6:9]
            vel[9:12] = vel[6:9]
            pos[6:9] = pos[3:6]
            vel[6:9] = vel[3:6]
            pos[3:6] = temp_pos
            vel[3:6] = temp_vel

            output["root_pos_raw"] = root_pos
            output["root_pos"] = root_offset
            output["root_rot"] = torch.from_numpy(Rotation.from_matrix(root_rot.detach().cpu().numpy()).as_quat()).float().to(self.device)
            #output["root_rot"] = self.sequence["root_rot"][self.frame:self.frame+1]
        else:
            pos = extract_tracked_points(self.sequence, self.frame, self.inference.opt, "position")
            vel = extract_tracked_points(self.sequence, self.frame, self.inference.opt, "velocity")
            output["root_pos"] = self.sequence["root_pos"][self.frame:self.frame+1]
            output["root_rot"] = self.sequence["root_rot"][self.frame:self.frame+1]

        output["vel"] = vel

        output["tracked_points"] = torch.unsqueeze(torch.cat((pos, vel), dim=-1), dim=0)
        output["tracked_points_raw"] = torch.squeeze(self.sequence["raw_tracker_pos"][self.frame:self.frame+1])

        if self.record_vr:
            record_vr_output = np.zeros((4, 3, 4))

            root_rot_mat = Rotation.from_quat(output["root_rot"][0].detach().cpu().numpy()).as_matrix()
            record_vr_output[0, :3, :3] = root_rot_mat

            index = 0
            for i in [0, 3, 1, 2]:
                record_vr_output[index, 0, 3] = output["tracked_points_raw"][i*3+0]
                record_vr_output[index, 1, 3] = output["tracked_points_raw"][i*3+1]
                record_vr_output[index, 2, 3] = output["tracked_points_raw"][i*3+2]
                index += 1

            self.recorded_data.append(record_vr_output)

        if self.record_vr and self.frame == self.num_frames - 1:
            recorded_data_npy = np.asarray(self.recorded_data)
            np.save("recorded_tp/" + self.sequence_name, recorded_data_npy)

        self.frame += 1

        return output


if __name__ == "__main__":
    parser = ViewerArgumentParser()
    parser.parser.add_argument("--record_vr", action="store_true", help="record in VR format (.npy)")
    args = parser.parse_args()

    app = PosePredictionAppOnline(args)
    app.run()
    