import torch
from evaluate_batch import InferenceBatch
from utils.vr.calibrate import get_vr_pose_scale
from utils.vr.filter import PointFilter, RotFilter
from utils.vr.utils import RecordedVRReader, preprocess_points

class InferenceBatchVR(InferenceBatch):
    def __init__(self,
        dataset,
        experiment,
        motion_length=-1,
        sequence_names=[],
        dataset_mode="train",
        ik_mode=False,
        root_transform=True,
        save_z=False,
        lock_eps=False,
        smart_eps=1,
        disable_interpolation=[],
        waist_occlusion=False,
        use_eps_net=False,
        device=None,
        set_seed=None,
        selection_mode='fs',
        selection_mode_start_frame=0,
        selection_mode_passthrough=False,
        lock_sample=False,
        lookahead=0,
        choreography_file=None,
        recorded_tp=None,
        calibration_tp=None):

        super().__init__(
            dataset,
            experiment,
            motion_length,
            sequence_names,
            dataset_mode,
            ik_mode,
            root_transform,
            save_z,
            lock_eps,
            smart_eps,
            disable_interpolation,
            waist_occlusion,
            use_eps_net,
            set_seed=set_seed,
            selection_mode=selection_mode,
            selection_mode_start_frame=selection_mode_start_frame,
            selection_mode_passthrough=selection_mode_passthrough,
            lock_sample=lock_sample,
            lookahead=lookahead,
            choreography_file=choreography_file)

        self.recorded_tp = recorded_tp
        self.calibration_tp = calibration_tp

        self.rot_filter = RotFilter()

        self.pos_filters = []
        for i in range(4):
            self.pos_filters.append(PointFilter())

        self.recorded_tp = RecordedVRReader(self.recorded_tp, False)
        self.calibration_tp = RecordedVRReader(self.calibration_tp, False)
        for i in range(60):
            cal_pose = self.calibration_tp.get_next_vr_frame_recorded(i, 4)
        self.vr_scale_params = get_vr_pose_scale(cal_pose)

        self.num_frames = len(self.recorded_tp)

        frames = []

        last_frame = None
        for f in range(self.num_frames):
            frame = self.recorded_tp.get_next_vr_frame_recorded(f, 4)
            frame = preprocess_points(frame, last_frame, 4, None, self.vr_scale_params, filter=self.rot_filter, pos_filters=self.pos_filters)
            last_frame = frame
            frames.append(frame)

        self.samples = {}
        self.samples["position"] = torch.zeros((1, self.num_frames, 57)).to(device)
        self.samples["velocity"] = torch.zeros((1, self.num_frames, 57)).to(device)
        self.samples["root_pos"] = torch.zeros((1, self.num_frames, 3)).to(device)
        self.samples["root_rot_mat"] = torch.zeros((1, self.num_frames, 3, 3)).to(device)

        for f in range(self.num_frames):
            positions = torch.from_numpy(frames[f]["input"][:, 0:12]).to(device)
            velocities = torch.from_numpy(frames[f]["input"][:, 12:24]).to(device)
            root_pos = torch.from_numpy(frames[f]["root_pos"]).to(device)
            root_rot_mat = torch.from_numpy(frames[f]["root_rot"].as_matrix()).to(device)

            self.samples["position"][:, f, 0:3] = positions[:, 0:3]  # hips
            self.samples["position"][:, f, 30:33] = positions[:, 3:6]  # head
            self.samples["position"][:, f, 24:27] = positions[:, 9:12]  # right hand
            self.samples["position"][:, f, 15:18] = positions[:, 6:9]  # left hand

            self.samples["velocity"][:, f, 0:3] = velocities[:, 0:3]  # hips
            self.samples["velocity"][:, f, 30:33] = velocities[:, 3:6]  # head
            self.samples["velocity"][:, f, 24:27] = velocities[:, 9:12]  # right hand
            self.samples["velocity"][:, f, 15:18] = velocities[:, 6:9]  # left hand

            self.samples["root_pos"][:, f, :] = root_pos
            self.samples["root_rot_mat"][0, f, :, :] = root_rot_mat

    def get_sample(self, index):
        return self.samples
        
        