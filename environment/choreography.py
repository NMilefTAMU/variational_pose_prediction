import yaml

from environment.event import TargetEvent, BoundaryEvent, Event


class Choreography:
    def __init__(self, choreography_file):
        self.choreography_file = choreography_file

        with open(choreography_file, 'r') as stream:
            choreography_dict = yaml.safe_load(stream)

            self.events = []

            for event_name, event_dict in choreography_dict.items():
                # if range of frames provided, create list of frames
                if type(event_dict["frame"]) is list:
                    frames = []
                    for i in range(event_dict["frame"][0], event_dict["frame"][1]):
                        frames.append(i)
                else:
                    frames = [event_dict["frame"]]

                for frame in frames:
                    if event_dict["type"] == "target":
                        event = TargetEvent(
                            event_name,
                            frame,
                            event_dict["pos"],
                            event_dict["joint"],
                            event_dict["target"],
                        )
                    elif event_dict["type"] == "boundary_xyz":
                        event = BoundaryEvent(
                            event_name,
                            frame,
                            event_dict["joint"],
                            event_dict["bounds"],
                            event_dict["center"],
                        )
                    self.events.append(event)

            # sort by increasing frame
            self.events.sort(key=lambda x: x.frame)

    def get_next_event(self, frame: int) -> Event:
        for event in self.events:
            if event.frame >= frame:
                return event

        return None

    def __len__(self):
        return len(self.events)

    def __str__(self):
        output = ""

        output = "Events:\n"
        for event in self.events:
            output += "\t" + str(event) + "\n"

        return output