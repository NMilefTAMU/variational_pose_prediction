from abc import abstractmethod
from typing import Dict
from dataProcessing.dataset_mocap import DatasetMocap
import torch


class Event:
    def __init__(self):
        pass

    def __str__(self):
        return str(self.__dict__)

    @abstractmethod
    def detect_error(self, frame: int, current_eps: int, frame_sample: Dict) -> bool:
        pass

    @abstractmethod
    def select_sample(self, frame: int, current_eps: int, frame_sample: Dict) -> float:
        return current_eps

class TargetEvent(Event):
    def __init__(self, name, frame, pos, joint, target):
        super(TargetEvent, self).__init__()

        self.frame = frame
        self.pos = pos
        self.joint = joint
        self.target = target

    def detect_error(self, frame_sample: Dict, current_eps: int, target_frame: int) -> bool:
        if target_frame == self.frame:
            return True

        return False

    def select_sample(self, frame_sample: Dict, current_eps: int, target_frame: int) -> float:
        target_joint_index = DatasetMocap.get_bone_index(self.joint)
        joints = frame_sample["position"][:, target_joint_index*3:target_joint_index*3+3]

        diff = joints - torch.tensor(self.pos).to(joints.device)

        # compute the minimum distance
        dist = torch.linalg.norm(diff, dim=1)
        target_eps = int(torch.argmin(dist).item())

        return target_eps

class BoundaryEvent(Event):
    def __init__(self, name, frame, joint, bounds, center):
        super(BoundaryEvent, self).__init__()

        self.frame = frame
        self.joint = joint
        self.bounds = bounds
        self.center = center

    def detect_error(self, frame_sample: Dict, current_eps: int, target_frame: int) -> bool:
        if target_frame == self.frame:
            target_joint_index = DatasetMocap.get_bone_index(self.joint)
            joint_pos = frame_sample["position"][:, target_joint_index*3:target_joint_index*3+3]

            collided_samples, non_collided_samples = self.detect_collision(joint_pos[[current_eps]])
            if len(collided_samples) == 1:
                return True

        return False

    def select_sample(self, frame_sample: Dict, current_eps: int, target_frame: int) -> float:
        target_joint_index = DatasetMocap.get_bone_index(self.joint)
        joint_pos = frame_sample["position"][:, target_joint_index*3:target_joint_index*3+3]

        collided_samples, non_collided_samples = self.detect_collision(joint_pos)
        if current_eps not in collided_samples:
            return current_eps
        
        if len(non_collided_samples) > 0:
            joint_pos[non_collided_samples]
            center = torch.tensor(self.center).unsqueeze(0).to(joint_pos.device)
            target_eps = torch.argmax(torch.norm(joint_pos - center, dim=1))
            return int(target_eps.item())
        
        return current_eps

    def detect_collision(self, pos):
        collided_samples = []
        non_collided_samples = []
        for eps in range(pos.shape[0]):
            if pos[eps, 0] > min(self.bounds[0][0], self.bounds[1][0]) and \
                pos[eps, 0] < max(self.bounds[0][0], self.bounds[1][0]) and \
                pos[eps, 1] > min(self.bounds[0][1], self.bounds[1][1]) and \
                pos[eps, 1] < max(self.bounds[0][1], self.bounds[1][1]) and \
                pos[eps, 2] > min(self.bounds[0][2], self.bounds[1][2]) and \
                pos[eps, 2] < max(self.bounds[0][2], self.bounds[1][2]):

                collided_samples.append(eps)
            else:
                non_collided_samples.append(eps)

        return collided_samples, non_collided_samples