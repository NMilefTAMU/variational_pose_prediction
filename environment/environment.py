from typing import List
import yaml

class Environment:
    def __init__(self, base, environment_file):
        self.base = base
        self.environment_file = environment_file
        self.objects = []

        with open(environment_file, 'r') as stream:
            environment_dict = yaml.safe_load(stream)

            if "objects" in environment_dict:
                self.load_objects(base, environment_dict["objects"])

    def load_objects(self, base, object_list):
        with open("./environment/objects.yaml", 'r') as stream:
            object_map = yaml.safe_load(stream)

        for _, attributes in object_list.items():
            filename = object_map[attributes["type"]]["filename"]
            model = base.loader.load_model(filename)

            if "pos" in attributes:
                model.setPos(
                    attributes["pos"][0],
                    -attributes["pos"][1],
                    -attributes["pos"][2],
                )

            if "rot_yaw" in attributes:
                model.setHpr(0, 0, attributes["rot_yaw"])

            if "scale" in attributes:
                if type(attributes["scale"]) is float:
                    model.setScale(attributes["scale"])
                if type(attributes["scale"]) is List:
                    model.setScale(
                        attributes["scale"][0],
                        attributes["scale"][1],
                        attributes["scale"][2],
                    )

            model.reparent_to(base.render)
            self.objects.append(model)