# Viewer Instructions
A viewer utility is included for both mocap and VR setups. The VR setup script may require a bit of setup, so please let me know if you are interested in using that utility.

Below are sample commands for running the viewer. Note that the camera config is optional and is more for putting figures together for the paper and video. Omitting this data allows you to control the camera with your mouse.

When selecting a seed, understand that our algorithm is designed to prevent to the worst case scenarios. Some seeds produce good results, so our algorithm tends to ignore these cases. To isolate the effect of our method of sample selection and interpolation, we set seeds that produce bad samples as our algorithm tries to transition to better samples at runtime. For more information, read our paper. Note that the seeds below work for my machine and may differ for your pariticular machine.


## Normal animations
### Run
```
python viewer.py --experiment test_model_6d_8_25b --dataset mocap_ours --methods ours gt --lock_eps --num_gen 1 --num_tp 4 --camera_config viewer/config/ours0.yaml --dataset_type test --sequence_names locomotion_valid_001
```

### Step forward/backward
```
python viewer.py --experiment test_model_6d_8_25b --dataset mocap_ours --methods ours gt --lock_eps --num_gen 1 --num_tp 4 --camera_config viewer/config/ours2.yaml --dataset_type test3 --sequence_names capture_001_7
```

### Shuffle
```
python viewer.py --experiment test_model_6d_8_25b --dataset mocap_ours --methods ours gt --lock_eps --num_gen 1 --num_tp 4 --camera_config viewer/config/ours0.yaml --dataset_type test3 --sequence_names 69_42
```

## Invalid pose detection/sample selection
### Cross legs turn:
```
python viewer.py --experiment test_model_6d_8_25b --dataset mocap_ours --methods ours gt --lock_eps --smart_eps 100 --num_gen 1 --num_tp 4 --camera_config viewer/config/ours0.yaml --set_seed 3 --lookahead 10 --dataset_type test3 --sequence_names 132_55 -sm ip -smsf 490
```

### Shuffle leg drag
```
python viewer.py --experiment test_model_6d_8_25b --dataset mocap_ours --methods ours gt --lock_eps --smart_eps 100 --num_gen 1 --num_tp 4 --camera_config viewer/config/ours0.yaml --dataset_type test3 --sequence_names 69_42 -sm ip --set_seed 1 -smsf 1050 --lookahead 10
```

```
python viewer.py --experiment test_model_6d_8_25b --dataset mocap_ours --methods ours gt --lock_eps --num_gen 1 --num_tp 4 --camera_config viewer/config/ours0.yaml --lookahead 10 --smart_eps 100 --dataset_type test3 --sequence_names 69_42 --set_seed 1 -sm ip -smsf 950
```

### Walk leg drag
```
python viewer.py --experiment test_model_6d_8_25b --dataset mocap_ours --methods ours gt --lock_eps --smart_eps 100 --num_gen 1 --num_tp 4 --camera_config viewer/config/ours2.yaml --dataset_type test3 --sequence_names 132_15 -sm ip --set_seed 0 -smsf 100 --lookahead 10
```

### Backward leg drag
```
python viewer.py --experiment test_model_6d_8_25b --dataset mocap_ours --methods ours gt --lock_eps --smart_eps 100 --num_gen 1 --num_tp 4 --camera_config viewer/config/ours2.yaml --dataset_type test3 --sequence_names 69_35 -sm ip --set_seed 1 -smsf 0 --lookahead 10
```

## Target foot location
### Kick
```
python viewer.py --experiment test_model_6d_8_25b --dataset mocap_ours --methods ours gt --lock_eps --smart_eps 100 --num_gen 1 --num_tp 4 --set_seed 1 --lookahead 10 --dataset_type test3 --sequence_names 143_24 -sm jp --environment environment/environments/143_24_kick_mma.yaml --choreography environment/choreographies/143_24_mid_kick.yaml --camera_config viewer/config/ours_ss_kick.yaml
```

### Collision avoidance
```
python viewer.py --experiment test_model_6d_8_25b --dataset mocap_ours --methods ours gt --lock_eps --smart_eps 100 --num_gen 1 --num_tp 4 --set_seed 4 --lookahead 10 --dataset_type test3 --sequence_names 143_18 -sm jp --environment environment/environments/143_18_bus_sit.yaml --choreography environment/choreographies/143_18_sit_boundary.yaml --camera_config viewer/config/ours_ss_sit.yaml
```