import numpy as np
ntu_action_labels = [6, 7, 8, 9, 22, 23, 24, 38, 80, 93, 99, 100, 102]

kinect_vibe_extract_joints = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 21, 24, 38]


# Raw_offsets give the rough direction of each joint relative to root joint.
# For example, raw offset of root joint is [0,0,0]; raw offset of left hip is [-1, 0, 0].
# It could simply be [1, 0, 0] for all joint except root. However, this may affect the precision of transformation.
mocap_raw_offsets = np.array([[0, 0, 0],
                             [0, 1, 0],
                             [0, 1, 0],
                             [0, 1, 0],
                             [0, 1, 0],
                             [1, 0, 0],
                             [0, 1, 0],
                             [0, 1, 0],
                             [0, 1, 0],
                             [-1, 0, 0],
                             [-1, 0, 0],
                             [-1, 0, 0],
                             [0, -1, 0],
                             [0, -1, 0],
                             [0, -1, 0],
                             [0, -1, 0],
                             [0, -1, 0],
                             [0, -1, 0],
                             [0, -1, 0],
                             [0, -1, 0]])

mocap_lobstr_raw_offsets = np.array([[0, 0, 0],
                             [0, 1, 0],
                             [0, 1, 0],
                             [0, 1, 0],
                             [0, 1, 0],
                             [1, 0, 0],
                             [0, 1, 0],
                             [0, 1, 0],
                             [0, 1, 0],
                             [-1, 0, 0],
                             [-1, 0, 0],
                             [-1, 0, 0],
                             [0, -1, 0],
                             [0, -1, 0],
                             [0, -1, 0],
                             [0, -1, 0],
                             [0, -1, 0],
                             [0, -1, 0],
                             [0, -1, 0]])

# Define a kinematic tree for the skeletal struture
mocap_kinematic_chain = [[0, 1, 2, 3], [0, 12, 13, 14, 15], [0, 16, 17, 18, 19], [1, 4, 5, 6, 7], [1, 8, 9, 10, 11]]

mocap_lobstr_kinematic_chain = {}
mocap_lobstr_kinematic_chain[3] = [[0, 9, 2, 1, 10], [10, 15, 16, 17, 18], [10, 11, 12, 13, 14], [2, 6, 7, 8], [2, 3, 4, 5]]
mocap_lobstr_kinematic_chain[4] = [[0, 1, 2, 9, 10], [2, 3, 4, 5], [2, 6, 7, 8], [0, 11, 12, 13, 14], [0, 15, 16, 17, 18]]

#mocap_lobstr_kinematic_chain = [[0, 1, 2, 3, 4], [0, 5, 6, 7, 8], [0, 9, 10, 11, 12, 13, 14], [12, 15, 16, 17, 18], [12, 19, 20, 21, 22]]

def get_kinematic_chain(opt):
    kinematic_chains = {
        'mocap': mocap_kinematic_chain,
        'mocap_lobstr': mocap_lobstr_kinematic_chain[opt.num_tp],
        'mocap_pfnn': mocap_lobstr_kinematic_chain[opt.num_tp],
        'mocap_ours': mocap_lobstr_kinematic_chain[opt.num_tp],
    }
    k_chain = kinematic_chains[opt.dataset_type]
    return k_chain

def get_raw_offsets(opt):
    raw_offsets = {
        'mocap': mocap_raw_offsets,
        'mocap_lobstr': mocap_lobstr_raw_offsets,
        'mocap_pfnn': mocap_lobstr_raw_offsets,
        'mocap_ours': mocap_lobstr_raw_offsets,
    }
    raw_offset = raw_offsets[opt.dataset_type]
    return raw_offset

def chain_to_dict(chains):
    output = {}
    for chain in chains:
        for i in range(1, len(chain)):
            parent = chain[i - 1]
            child = chain[i]
            output[child] = parent

    return output

mocap_action_enumerator = {
    0: "Walk",
    1: "Wash",
    2: "Run",
    3: "Jump",
    4: "Animal Behavior",
    5: "Dance",
    6: "Step",
    7: "Climb"
}

mocap_lobstr_action_enumerator = {
    0: "Walk",
    1: "Wash",
    2: "Run",
    3: "Jump",
    4: "Animal Behavior",
    5: "Dance",
    6: "Step",
    7: "Climb"
}

mocap_pfnn_action_enumerator = {
    
}

mocap_ours_action_enumerator = {
    
}

# returns None if not found
def find_key_by_value(dict, value):
    for k, v in dict.items():
        if v == value:
            return k
    return None

def get_dataset_params(opt):
    dataset_params = {}
    if opt.dataset_type == "mocap":
        dataset_params['clip_path']         = './dataset/mocap/pose_clip.csv'
        dataset_params['dataset_path']      = "./dataset/mocap/mocap_3djoints/"
        dataset_params['enumerator']        = mocap_action_enumerator
        dataset_params['file_prefix']       = ""
        dataset_params['input_size']        = 60
        dataset_params['joints_num']        = 20
        dataset_params['kinematic_chain']   = mocap_kinematic_chain
        dataset_params['label_dec']         = [0, 1, 2, 3, 4, 5, 6, 7]
        dataset_params['labels']            = []
        dataset_params['motion_desc_file']  = ""
        dataset_params['raw_offsets']       = mocap_raw_offsets

    elif opt.dataset_type == "mocap_lobstr":
        dataset_params['clip_path']         = ''
        dataset_params['dataset_path']      = "./dataset/mocap_lobstr/output/"
        dataset_params['enumerator']        = mocap_lobstr_action_enumerator
        dataset_params['file_prefix']       = ""
        dataset_params['input_size']        = 19 * 6  # 6D representation
        dataset_params['joints_num']        = 19
        dataset_params['kinematic_chain']   = mocap_lobstr_kinematic_chain[opt.num_tp]
        dataset_params['label_dec']         = [0] # no labels
        dataset_params['labels']            = []
        dataset_params['motion_desc_file']  = ""
        dataset_params['raw_offsets']       = mocap_lobstr_raw_offsets

    elif opt.dataset_type == "mocap_pfnn":
        dataset_params['clip_path']         = ''
        dataset_params['dataset_path']      = "./dataset/mocap_pfnn/output/"
        dataset_params['enumerator']        = mocap_lobstr_action_enumerator
        dataset_params['file_prefix']       = ""
        dataset_params['input_size']        = 19 * 6  # 6D representation
        dataset_params['joints_num']        = 19
        dataset_params['kinematic_chain']   = mocap_lobstr_kinematic_chain[opt.num_tp]
        dataset_params['label_dec']         = [0] # no labels
        dataset_params['labels']            = []
        dataset_params['motion_desc_file']  = ""
        dataset_params['raw_offsets']       = mocap_lobstr_raw_offsets
    
    elif opt.dataset_type == "mocap_ours":
        dataset_params['clip_path']         = ''
        dataset_params['dataset_path']      = "./dataset/mocap_ours/output/"
        dataset_params['enumerator']        = mocap_lobstr_action_enumerator
        dataset_params['file_prefix']       = ""
        dataset_params['input_size']        = 18 * 6 + 3  # root pos + 6D representation
        dataset_params['joints_num']        = 19
        dataset_params['kinematic_chain']   = mocap_lobstr_kinematic_chain[opt.num_tp]
        dataset_params['label_dec']         = [0] # no labels
        dataset_params['labels']            = []
        dataset_params['motion_desc_file']  = ""
        dataset_params['raw_offsets']       = mocap_lobstr_raw_offsets
    
    else:
        raise NotImplementedError('This dataset is unrecognized!!!')

    return dataset_params