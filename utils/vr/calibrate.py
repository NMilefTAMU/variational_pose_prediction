import numpy as np

measured_values = {
    "tracker": np.array([-0.0237, 0.9874, 0.0080]),
    "hmd": np.array([-0.0217, 1.4260, 0.0080]),
    "controller_right": np.array([-0.8851, 1.2977, 0.0409]),
    "controller_left": np.array([0.8539, 1.2977, 0.0390]),
}

def get_vr_pose_scale(pose):
    # TODO: replace will calibration stage
    output = {}

    pos_tracker = pose['tracker'][:, 3]
    pos_hmd = pose['hmd'][:, 3]
    pos_cr = pose['controller_right'][:, 3]
    pos_cl = pose['controller_left'][:, 3]

    output["height_scale"] = measured_values["hmd"][1] / pos_hmd[1]
    output["waist_offset"] = measured_values["tracker"][1] - (pos_tracker[1] * output["height_scale"])
    return output