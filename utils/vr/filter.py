import numpy as np
from scipy import signal
from scipy.spatial.transform import Rotation
import matplotlib.pyplot as plt


class RotFilter:
    '''
    Class for filtering quaternions
    '''
    def __init__(self, plot_data=False):
        self.fs = 60.0 # 60 fps
        self.nyq = 0.5 * self.fs
        self.lowcut = 100
        self.num_dim = 4
        self.last_rot = None

        self.plot_data = plot_data
        if self.plot_data:
            self.timestep = 0
            self.raw_data = []
            self.filtered_data = []
            self.plot_every = 5
            self.plot_frame = 0
            plt.ion()
            plt.show()

        self.sos = signal.butter(
            6,
            self.lowcut / self.nyq,
            output="sos",
            btype="lowpass",
            fs=self.fs)

        self.zi = []
        self.reset()

    def filter(self, rot: np.ndarray) -> np.ndarray:
        '''
        Filter data point

        Args:
            rot: [4] numpy array (quaternion)

        Returns:
            np.ndarray: [4] numpy array (quaternion)
        '''
        if self.last_rot is not None:
            quat_sign = np.sum(rot * self.last_rot)
            if quat_sign < 0:
                rot *= -1
        self.last_rot = rot

        rot_quat = np.copy(rot)
        for i in range(len(rot)):
            rot_quat[i], self.zi[i] = signal.sosfilt(self.sos, [rot_quat[i]], zi=self.zi[i])

        if self.plot_data:
            self.plot(rot, rot_quat)
            self.plot_frame += 1
            self.timestep += 1.0 / self.fs

        return rot_quat

    def plot(self, raw_data, filtered_data):
        self.raw_data.append(raw_data)
        self.filtered_data.append(filtered_data)
        if self.plot_frame % self.plot_every == 0:
            raw_data_np = np.array(self.raw_data)[:, 0]
            filtered_data_np = np.array(self.filtered_data)[:, 0]
            plt.plot(np.arange(0, len(self.raw_data)) / self.fs, raw_data_np, color="blue")
            plt.plot(np.arange(0, len(self.filtered_data)) / self.fs, filtered_data_np, color="orange")
            plt.draw()
            plt.pause(0.001)

    def reset(self):
        for _ in range(self.num_dim):
            self.zi.append(signal.sosfilt_zi(self.sos))

class PointFilter:
    '''
    Class for filtering points
    '''
    def __init__(self, plot_data=False):
        self.fs = 60.0 # 60 fps
        self.nyq = 0.5 * self.fs
        self.lowcut = 100
        self.num_dim = 3
        self.last_pos = None

        self.sos = signal.butter(
            6,
            self.lowcut / self.nyq,
            output="sos",
            btype="lowpass",
            fs=self.fs)

        self.zi = []
        self.reset()

    def filter(self, pos: np.ndarray) -> np.ndarray:
        '''
        Filter data point

        Args:
            pos: [3] numpy array (3D point)

        Returns:
            np.ndarray: [3] numpy array (3D point)
        '''
        pos_3d = np.copy(pos)
        for i in range(len(pos)):
            pos_3d[i], self.zi[i] = signal.sosfilt(self.sos, [pos_3d[i]], zi=self.zi[i])

        return pos_3d

    def plot(self, raw_data, filtered_data):
        self.raw_data.append(raw_data)
        self.filtered_data.append(filtered_data)
        if self.plot_frame % self.plot_every == 0:
            raw_data_np = np.array(self.raw_data)[:, 0]
            filtered_data_np = np.array(self.filtered_data)[:, 0]
            plt.plot(np.arange(0, len(self.raw_data)) / self.fs, raw_data_np, color="blue")
            plt.plot(np.arange(0, len(self.filtered_data)) / self.fs, filtered_data_np, color="orange")
            plt.draw()
            plt.pause(0.001)

    def reset(self):
        for _ in range(self.num_dim):
            self.zi.append(signal.sosfilt_zi(self.sos))
