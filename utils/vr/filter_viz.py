'''
This file is just for helping to visualize RotFilter output.
'''
import math
import os
import sys

sys.path.insert(0, os.path.join(os.path.dirname(os.path.abspath(__file__)), "../.."))

import numpy as np
from scipy.spatial.transform import Rotation

from utils.vr.filter import RotFilter

import matplotlib.pyplot as plt


def create_quat_data(shape):
    quat_data = []
    rot_data = []

    for i in range(shape[0]):
        quat = np.array([0.0, 0.0, 0.0, 0.0])
        #for j in range(shape[1]):
        j = 0
        quat[j] = \
            math.sin((i / 30)*2*np.pi*1.2) + \
            math.sin((i / 30)*2*np.pi*9.0)*0.25

        rot_data.append(quat)
        quat_data.append(quat)

    noisy_quat_data = quat_data
    noisy_rot_data = rot_data

    return noisy_rot_data, noisy_quat_data


def filter_viz():
    num_samples = 200
    rot_data, raw_quat_data = create_quat_data((num_samples, 4))
    filter = RotFilter()

    f_data = []
    quat_data = []
    for i in range(len(rot_data)):
        filtered_rot = filter.filter(rot_data[i])
        f_data.append(filtered_rot)
        quat_data.append(filtered_rot)

    quat_data = np.array(quat_data)
    raw_quat_data = np.array(raw_quat_data)

    plt.plot(np.arange(0, num_samples), quat_data[:, 0])
    plt.plot(np.arange(0, num_samples), raw_quat_data[:, 0])
    plt.show()


if __name__ == "__main__":
    filter_viz()
