from models.root_pred.occlusion_network import OcclusionNet
import numpy as np
import openvr
import torch
from scipy.spatial.transform import Rotation

import utils.constants

from utils.vr.calibrate import measured_values

classes = {
    openvr.TrackedDeviceClass_HMD: "hmd",
    openvr.TrackedDeviceClass_Controller: "controller",
    openvr.TrackedDeviceClass_GenericTracker: "tracker",
    openvr.TrackedDeviceClass_TrackingReference: "reference",
}

roles = {
    openvr.TrackedControllerRole_LeftHand: "left",
    openvr.TrackedControllerRole_RightHand: "right",
}

device_names = [
    "tracker",
    "hmd",
    "controller_right",
    "controller_left",
]

def get_class_name(index):
    return classes[index]

def get_class_role(index):
    return roles[index]

def convert_mat_to_numpy(mat):
    return np.array(
                    ((mat.m[0][0], mat.m[0][1], mat.m[0][2], mat.m[0][3]),
                    (mat.m[1][0], mat.m[1][1], mat.m[1][2], mat.m[1][3]),
                    (mat.m[2][0], mat.m[2][1], mat.m[2][2], mat.m[2][3]),)
                    , np.float32)

def mat_to_rot(mat):
    # the Y and Z axes are flipped because the axes are the wrong direction
    # if we flip the X-axis the handedness would change
    mat[:, :, 1] *= -1
    mat[:, :, 2] *= -1
    rots = Rotation.from_matrix(mat)

    return rots

def preprocess_points(points, last_data, num_tp, initial_rot=None, scale_params=None, filter=None, pos_filters=None):
    tracked_points = np.zeros((1, num_tp * 3))

    positions = np.zeros((num_tp, 3))
    rot_mat = np.tile(np.identity(3), (num_tp, 1, 1))

    data = {}
    for i in range(num_tp):
        name = device_names[i]
        if name not in points.keys():
            continue

        positions[i] = points[name][:, 3] * scale_params["height_scale"]
        rot_mat[i, :, :] = points[name][:3, :3]

    rots = mat_to_rot(rot_mat)
    
    root_quat = np.expand_dims(rots[0].as_quat(), axis=0)
    root_quat[:, 0] = 0 #x
    root_quat[:, 2] = 0 # z
    mag = np.sqrt((root_quat[:, 1]**2) + (root_quat[:, 3]**2))
    root_quat[:, 3] /= mag # w
    root_quat[:, 1] /= mag # y
    #root_rot = torch.from_numpy(Rotation.from_quat(root_quat).as_matrix())
    #root_rot = Rotation.from_matrix(root_rot.detach().cpu().numpy())
    root_rot = Rotation.from_quat(root_quat)
    
    #root_rot = rots[0:1]

    if filter is not None:
        root_rot_filtered = filter.filter(root_rot[0].as_quat())
        root_rot = Rotation.from_quat(np.expand_dims(root_rot_filtered, axis=0))

    if pos_filters is not None:
        for i in range(4):
            positions[i] = pos_filters[i].filter(positions[i])

    z_offset_waist = -root_rot.apply(np.array([0, 0, 1])) * 0.1 # about 10cm depth waist
    y_offset_waist = root_rot.apply(np.array([0, 1, 0])) * scale_params["waist_offset"] # about 30cm height waist
    y_offset_hands = np.array([0, 1, 0]) * scale_params["waist_offset"] # about 30cm height waist

    positions[2:3] = positions[2:3] + y_offset_hands
    positions[3:4] = positions[3:4] + y_offset_hands

    root_pos = np.copy(positions[0:1, :]) + z_offset_waist + y_offset_waist
    positions[0:1, :] = root_pos

    output = {}

    positions_raw = np.copy(positions)
    output["position_raw"] = positions_raw

    root_pos = np.copy(positions[0:1, :])
    root_offset = np.copy(root_pos)
    root_offset[:, 1] = 0

    positions = positions - root_offset

    output["position"] = np.copy(positions)

    inv_root_rot = root_rot.inv()

    for i in range(num_tp):
        positions[i] = inv_root_rot.apply(positions[i])

    output["input"] = np.zeros((1, num_tp * 3 * 2))
    output["root_pos"] = root_offset
    output["root_rot"] = root_rot[0]

    for i in range(num_tp):
        output["input"][:, i*3:i*3+3] = positions[i:i+1]
        if last_data is not None:
            vel_offset = num_tp * 3
            velocity = output["position_raw"][i] - last_data["position_raw"][i]
            output["input"][:, i*3+vel_offset:i*3+3+vel_offset] = inv_root_rot.apply(velocity)

    return output


def get_next_vr_frame(system, poses):
    poses, _ = openvr.VRCompositor().getLastPoses(poses, None)
    data = {}
    for i in range(64):
        if poses[i].bPoseIsValid:
            tracker_class = get_class_name(system.getTrackedDeviceClass(i))
            tracker_role = None
            if tracker_class == "controller":
                tracker_role = get_class_role(system.getControllerRoleForTrackedDeviceIndex(i))

            if tracker_role is not None:
                tracker_name = tracker_class + "_" + tracker_role
            else:
                tracker_name = tracker_class

            mat = poses[i].mDeviceToAbsoluteTracking

            data[tracker_name] = convert_mat_to_numpy(mat)
    return data, poses


def data_to_np(data, num_tp):
    output = np.zeros((4, 3, 4))

    for i in range(num_tp):
        name = device_names[i]
        if name in data.keys():
            output[i] = data[name]

    return output

class RecordedVRReader:
    def __init__(self, recorded_tp, waist_occlusion=False):
        self.data = np.load(recorded_tp)

        self.device = torch.device("cpu")

        self.waist_occlusion = waist_occlusion
        if self.waist_occlusion == True:
            self.waist_occlusion = waist_occlusion

            input_size = 3 * 6
            output_size = 3 + 6
            hidden_size = 128
            n_layers = 1

            self.wo_network = OcclusionNet(input_size, output_size, hidden_size, n_layers, 1, self.device)
            self.wo_network.load_state_dict(torch.load("checkpoints/waist_occlusion/wo_model.pth"))
            self.wo_network.init_hidden(1)
            self.wo_network.eval()

            self.last_pos = None
            self.last_waist_pos = torch.zeros((3))

    def get_next_vr_frame_recorded(self, f, num_tp):
        frame = f % self.data.shape[0]

        # change for arbitrary missing points
        num_points = self.data[frame].shape[0]
        frame_data = self.data[frame]

        output = {}
        for i in range(4):
            name = device_names[i]
            output[name] = frame_data[i]

        if self.waist_occlusion:
            pos = torch.zeros((1, 9))
            vel = torch.zeros((1, 9))

            labels = ["controller_right", "controller_left", "hmd"]

            index = 0
            for label in labels:
                tracker_pos = torch.from_numpy(output[label][:, 3]).float()
                pos[0, index*3:index*3+3] = tracker_pos
                if self.last_pos is not None:
                    vel[0, index*3:index*3+3] = tracker_pos - self.last_pos[0, index*3:index*3+3]
                index += 1

            height_scale = 1.0#measured_values['hmd'][1] / pos[0, 7].item()

            #import pdb; pdb.set_trace()

            waist_output = self.wo_network(torch.cat((pos * height_scale, vel * height_scale), dim=-1))
            waist_output = OcclusionNet.compute_root_pos_and_rot(waist_output, self.device)

            waist_rot_mat = waist_output["root_rot_mat"][0].detach().cpu().numpy()
            waist_rot_mat[:, 1] *= -1
            waist_rot_mat[:, 2] *= -1
            waist_pos = waist_output["root_pos"][0].detach().cpu().numpy() / height_scale
            output["tracker"][:3, :3] = waist_rot_mat
            output["tracker"][:, 3] = waist_pos

            self.last_pos = pos

        return output

    def __len__(self):
        return self.data.shape[0]
