import torch

# adapted from https://github.com/papagina/RotationContinuity
# batch*n
def normalize_vector(v):
    batch=v.shape[0]
    v_mag = torch.norm(v, dim=-1)
    #v_mag = torch.sqrt(v.pow(2).sum(1))# batch
    #v_mag = torch.max(v_mag, torch.FloatTensor([1e-8]).to(device))
    v_mag = torch.clamp(v_mag, min=1e-8)
    v_mag = v_mag.unsqueeze(-1).expand(*list(v.size()))
    v = v/v_mag
    return v

def compute_rotation_matrix_from_ortho6d(poses):
    x_raw = poses[...,0:3]#batch*3
    y_raw = poses[...,3:6]#batch*3

    x = normalize_vector(x_raw) #batch*3
    z = torch.linalg.cross(x, y_raw) #batch*3
    z = normalize_vector(z)#batch*3
    y = torch.linalg.cross(z, x)#batch*3
        
    x = x.view(*list(poses.size())[:-1],3,1)
    y = y.view(*list(poses.size())[:-1],3,1)
    z = z.view(*list(poses.size())[:-1],3,1)
    matrix = torch.cat((x,y,z), -1) #batch*3*3
    return matrix

def get_44_rotation_matrix_from_33_rotation_matrix(m, device):
    batch = m.shape[0]
    
    row4 = torch.autograd.Variable(torch.zeros(batch, 1, 3).to(device))
    
    m43 = torch.cat((m, row4), 1)

    col4 = torch.autograd.Variable(torch.zeros(batch, 4, 1).to(device))
    col4[:, 3, 0] = col4[:, 3, 0] + 1

    out = torch.cat((m43, col4), 2)
    
    return out