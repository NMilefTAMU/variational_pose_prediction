from typing import List
import torch

def assert_tensor_shape(x: torch.Tensor, shape: List):
    '''
    Checks that a tensor is of a certain size

    Args:
        x: tensor
        shape: list of tensor dimensions. Use -1 to skip dimension check.
    '''

    assert(len(x.shape) == len(shape))
    for i in range(len(shape)):
        if shape[i] == -1:
            continue

        assert(x.shape[i] == shape[i])

def assert_tensor_dims(x: torch.Tensor, num_dims: int):
    '''
    Check that a tensor contains a certain number of dimensions

    Args:
        x: tensor
        num_dims: int
    '''
    assert(len(x.shape) == num_dims)