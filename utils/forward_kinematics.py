from typing import Dict
import torch

from utils.rotation_util import compute_rotation_matrix_from_ortho6d, get_44_rotation_matrix_from_33_rotation_matrix

import utils.paramUtil as paramUtil


def forward_kinematics(
    device: torch.device,
    joint_rot: torch.tensor,
    kinematic_chain: Dict,
    bone_lengths: torch.tensor
) -> Dict:
    """
    Differentiable forward kinematics function

    Args:
        joint_rot: tensor [N, J-1 * 6 + 3]
        kinematic_chain: dict
        bone_lengths: [N, J]

    Returns:
        dict: ["pos"]
    """
    joint_rot_6d = joint_rot[:, 3:].reshape(joint_rot.shape[0], -1, 6)
    batch_size = joint_rot_6d.shape[0]
    num_joints = bone_lengths.shape[1]

    pos_mat_one = torch.eye(4).to(device)
    pos_mat = pos_mat_one.repeat(batch_size, num_joints, 1, 1)
    for j in range(num_joints):
        pos_mat[:, j, 2, 3] = bone_lengths[:, j]

    tf_joints = [None] * num_joints
    tf_joints[0] = torch.eye(4).to(device).repeat(batch_size, 1, 1)
    tf_joints[0][:, 0:3, -1] = joint_rot[:, 0:3]

    for chain in kinematic_chain:
        for index in range(1, len(chain)):
            parent = chain[index - 1]
            child = chain[index]

            local_rot = get_44_rotation_matrix_from_33_rotation_matrix(compute_rotation_matrix_from_ortho6d(joint_rot_6d[:, child-1]), device)

            tf_joints[child] = torch.bmm(torch.bmm(tf_joints[parent], local_rot), pos_mat[:, child])

    tf_joints_tensor = torch.stack(tf_joints, dim=1)
    pos = tf_joints_tensor[:, :, 0:3, 3]

    output = {}
    output["pos"] = pos.reshape(pos.shape[0], -1)

    return output
    
def forward_kinematics_fast(
    device: torch.device,
    joint_rot: torch.tensor,
    kinematic_chain: Dict,
    bone_lengths: torch.tensor
) -> Dict:
    """
    Differentiable forward kinematics function

    Args:
        joint_rot: tensor [N, J-1 * 6 + 3]
        kinematic_chain: dict
        bone_lengths: [N, J]

    Returns:
        dict: ["pos"]
    """
    with torch.no_grad():
        joint_rot_6d = joint_rot[:, 3:].reshape(joint_rot.shape[0], -1, 6)
        batch_size = joint_rot_6d.shape[0]
        num_joints = bone_lengths.shape[1]

        pos_mat_one = torch.eye(4).to(device)
        pos_mat = pos_mat_one.repeat(batch_size, num_joints, 1, 1)
        for j in range(num_joints):
            pos_mat[:, j, 2, 3] = bone_lengths[:, j]

        tf_joints = [None] * num_joints
        tf_joints[0] = torch.eye(4).to(device).repeat(batch_size, 1, 1)
        tf_joints[0][:, 0:3, -1] = joint_rot[:, 0:3]

        local_rot = torch.eye(4).to(device).repeat(batch_size, 1, 1)

        for chain in kinematic_chain:
            for index in range(1, len(chain)):
                parent = chain[index - 1]
                child = chain[index]

                local_rot[:, :3, :3] = compute_rotation_matrix_from_ortho6d(joint_rot_6d[:, child-1])

                tf_joints[child] = torch.bmm(torch.bmm(tf_joints[parent], local_rot), pos_mat[:, child])

        tf_joints_tensor = torch.stack(tf_joints, dim=1)
        pos = tf_joints_tensor[:, :, :3, 3]
        rot = tf_joints_tensor[:, :, :3, :3]

        output = {}
        output["pos"] = pos.reshape(pos.shape[0], -1)
        output["rot"] = rot.reshape(rot.shape[0], -1)

        return output
        