import argparse
import os
import sys

from torchinfo import summary

sys.path.insert(0, os.path.join(os.path.dirname(os.path.abspath(__file__)), ".."))

from evaluate_batch import InferenceBatch
from models.motion_vae import GaussianGRU
from models.pose_network import PoseModelNetwork

def compute_network_stats(args):
    inference = InferenceBatch(
        "mocap_ours",
        args.name,
        motion_length=60,
        sequence_names=["14_01"],
        dataset_mode="valid"
    )

    pose_model = PoseModelNetwork(
        input_size=19 * 3,
        num_levels = 3,
    )

    prior_net_summary = summary(
        inference.decoder_final.encoder,
        input_size=(1, 24),
        col_names=["input_size", "output_size", 'num_params'])

    decoder_net_summary = summary(
        inference.decoder_final.decoder,
        input_size=(1, 54),
        col_names=["input_size", "output_size", 'num_params'])

    pose_model_summary = summary(
        pose_model,
        input_size=(1, 19*3),
        col_names=["input_size", "output_size", 'num_params'])

    with open(args.file, "w") as f:
        f.writelines("########## PRIOR NET ##########\n")
        f.writelines(str(prior_net_summary) + "\n")
        f.writelines("########## DECODER NET ##########\n")
        f.writelines(str(decoder_net_summary) + "\n")
        f.writelines("########## POSE MODEL NET ##########\n")
        f.writelines(str(pose_model_summary) + "\n")

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--name", help="experiment name", required=True)
    parser.add_argument("--file", help="file name", required=True)
    args = parser.parse_args()

    compute_network_stats(args)
