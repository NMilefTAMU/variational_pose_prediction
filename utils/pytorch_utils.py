"""
Utility functions to help with dictionary conversion and processing
"""
import numpy as np
import torch
from typing import Dict, List


def dict_unsqueeze(dictionary: Dict) -> Dict:
    '''
    Unsqueeze tensors in dictionary

    Args:
        dictionary: dictionary of tensors

    Returns:
        Dict: same content as dictionary but unsqueezed
    '''
    output = {}
    for k, _ in dictionary.items():
        if isinstance(dictionary[k], torch.Tensor):
            output[k] = torch.unsqueeze(dictionary[k], 0)
        else:
            output[k] = dictionary[k]

    return output


def dict_np_to_torch(dictionary: Dict) -> Dict:
    '''
    Convert dictionary of numpy arrays to dictionary of tensors

    Args:
        dictionary: dictionary of np.ndarray

    Returns:
        Dict: same content as dictionary but torch
    '''
    output = {}
    for k, _ in dictionary.items():
        if isinstance(dictionary[k], np.ndarray):
            output[k] = torch.from_numpy(dictionary[k])
        else:
            output[k] = dictionary[k]

    return output


def dict_extract_frame(dictionary: Dict, frame: int) -> Dict:
    '''
    Extract frame from dictionary. Tensor format expected: [N, F, ...]

    Args:
        dictionary: dictionary of torch.tensor or np.array

    Returns:
        Dict: single frame from dictionary
    '''
    output = {}
    for k, _ in dictionary.items():
        if isinstance(dictionary[k], np.ndarray) or isinstance(dictionary[k], torch.Tensor):
            output[k] = dictionary[k][:, [frame]]
        else:
            output[k] = dictionary[k]

    return output


def list_dict_extract_tensor_torch(data_list: List, key: str) -> torch.Tensor:
    """
    Extract tensor sequence from list of dictionary given a key

    Args:
        data_list: list of dictionaries
        key: string for sequence to return

    Returns:
        torch.Tensor: tensor for key
    """
    output = []
    for f in range(len(data_list)):
        output.append(data_list[f][key])

    return torch.cat(output, dim=0)