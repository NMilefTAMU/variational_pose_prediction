import torch
from typing import Dict
from scipy.spatial.transform import Rotation

def local_to_world(
    positions,
    rotations=None,
    root_position=None,
    root_rotation=None
) -> Dict:
    """
    Convert local coordinates to world coordinates
    Note: rotation is pass-through for now

    Args:
        position: [N, J, 3]
        rotations: [N, J, 3, 3]
        root_position: [N, 1, 3]
        root_rotation: [N, 1, 3, 3]

    Returns:
        Dict: ["position", "rotation"]
    """
    batch_size = positions.shape[0]
    num_joints = positions.shape[1]

    resize_positions = positions.view((-1, 3, 1))
    resize_root_rotation = torch.repeat_interleave(root_rotation.view((-1, 3, 3)), num_joints * batch_size, dim=0)
    rot_positions = torch.bmm(resize_root_rotation, resize_positions)
    resize_rot_positions = rot_positions.view((batch_size, -1, 3))

    output = {}
    output["position"] = resize_rot_positions + root_position
    output["rotation"] = rotations

    return output

def world_to_local(
    positions,
    rotations=None,
    root_position=None,
    root_rotation=None
) -> Dict:
    """
    Convert world coordinates to local coordinates
    Note: rotation is pass-through for now

    Args:
        position: [N, J, 3]
        rotations: [N, J, 3, 3]
        root_position: [N, 1, 3]
        root_rotation: [N, 1, 3, 3]

    Returns:
        Dict: ["position", "rotation"]
    """
    batch_size = positions.shape[0]
    num_joints = positions.shape[1]

    resize_positions = positions.view((-1, 3, 1)) - root_position.view(-1, 3, 1)
    inv_root_rotation = torch.linalg.inv(root_rotation)
    resize_root_rotation = torch.repeat_interleave(inv_root_rotation.view((-1, 3, 3)), num_joints * batch_size, dim=0)
    rot_positions = torch.bmm(resize_root_rotation, resize_positions)
    resize_rot_positions = rot_positions.view((batch_size, -1, 3))

    output = {}
    output["position"] = resize_rot_positions
    output["rotation"] = rotations

    return output