import torch
from options.options import Options
from typing import Dict

def select_correct_data(sample, opt, mode):
    if mode == "velocity":
        data = sample["velocity"]
    elif mode == "position":
        data = sample["position"]
    else:
        raise ValueError("\"mode\" must be in \"position\" or \"velocity\"")
    return data

def extract_tracked_points_sequence(sample, opt, mode):
    data = select_correct_data(sample, opt, mode)

    num_points = 4 if any(x in ['tracking4', 'vtracking4'] for x in opt.input_types) else 3

    output = torch.zeros(data.shape[0], data.shape[1], num_points * 3)

    num_frames = data.shape[1]
    for i in range(num_frames):
        tracked_points = extract_tracked_points(data, i, opt, mode)
        output[:, i, :] = tracked_points

    return output

# extract tracked points from motion sequence
def extract_next_tracked_points(sample: Dict, i: int, opt: Options, mode: str):
    '''
    Extract current frame's tracked points

    Args:
        sample: dict for sampled
        i: frame_number
        opt: Options object
        mode: str ["velocity", "position"]
    '''
    return extract_tracked_points(sample, i+1, opt, mode)

def extract_tracked_points(sample, i, opt, mode):
    data = select_correct_data(sample, opt, mode)

    num_points = 4 if any(x in ['tracking4', 'vtracking4'] for x in opt.input_types) else 3

    # not sure if left and right hand are correct, but doesn't really matter
    try:
        if opt.dataset_type == 'mocap':
            tracked_points = extract_tracked_points_mocap(data, i, num_points)
        elif opt.dataset_type == 'mocap_lobstr':
            tracked_points = extract_tracked_points_mocap_lobstr(data, i, num_points)
        elif opt.dataset_type == 'mocap_pfnn':
            tracked_points = extract_tracked_points_mocap_lobstr(data, i, num_points)
        elif opt.dataset_type == 'mocap_ours':
            tracked_points = extract_tracked_points_mocap_lobstr(data, i, num_points)
        return tracked_points

    except Exception as e:
        print(e)

    return None

def extract_tracked_points_mocap(data, i, num_points):
    l_hand = data[..., i, 21:24]
    r_hand = data[..., i, 33:36]
    head = data[..., i, 9:12]
    tracked_points = torch.cat((l_hand, r_hand, head), dim=-1)

    waist = data[..., i, 0:3]
    if num_points == 4:
        tracked_points = torch.cat((tracked_points, waist), dim=-1)

    return tracked_points

def extract_tracked_points_mocap_lobstr(data, i, num_points):
    l_hand = data[..., i, 15:18]
    r_hand = data[..., i, 24:27]

    if num_points == 3:
        head = data[..., i, 0:3]
        tracked_points = torch.cat((head, l_hand, r_hand), dim=-1)

    if num_points == 4:
        waist = data[..., i, 0:3]
        head = data[..., i, 30:33]
        tracked_points = torch.cat((waist, head, l_hand, r_hand), dim=-1)

    return tracked_points