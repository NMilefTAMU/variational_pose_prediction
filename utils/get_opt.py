import ast
import os
from argparse import Namespace
import utils.paramUtil as paramUtil
from options.options import Options

opt_conversion = {
    # Base
    'name': str,
    'gpu_id': str,
    'time_counter': lambda s: s == 'True',
    'motion_length': int,
    'dataset_type': str,
    'clip_set': str,
    'checkpoints_dir': str,
    'dim_z': int,
    'hidden_size': int,
    'prior_hidden_layers': int,
    'posterior_hidden_layers': int,
    'decoder_hidden_layers': int,
    'veloc_hidden_layers': int,
    'd_hidden_layers': int,
    'isTrain': lambda s: s == 'True',
    'num_tp': int,
    
    # Train
    'batch_size': int,
    'arbitrary_len': lambda s: s == 'True',
    'do_adversary': lambda s: s == 'True',
    'do_recognition': lambda s: s == 'True',
    'do_align': lambda s: s == 'True',
    'skip_prob': float,
    'tf_ratio': float,
    'lambda_kld': float,
    'lambda_align': float,
    'lambda_adversary': float,
    'lambda_recognition': float,
    'is_continue': lambda s: s == 'True',
    'iters': int,
    'plot_every': int,
    "save_every": int,
    "eval_every": int,
    "save_latest": int,
    'print_every': int,

    # Evaluation
    'which_epoch': str,
    'result_path': str,
    'replic_times': int,
    'do_random': lambda s: s == 'True',
    'num_samples': int,

    # Misc. options
    'dim_noise_pose': int,
    'dim_noise_motion': int,
    'hidden_size_pose': int,
    'hidden_size_motion': int,

    # Makeshift options
    'model_file_path_override': str,
}


dataset_opt = {
    'mocap': {
        "dataset_path": "./dataset/mocap/mocap_3djoints/",
        "clip_path": './dataset/mocap/pose_clip.csv',
        "input_size_raw": 60,
        "joints_num": 20,
        'label_dec': [0, 1, 2, 3, 4, 5, 6, 7],
        'enumerator': paramUtil.mocap_action_enumerator
    },
    'mocap_lobstr': {
        "dataset_path": "./dataset/mocap_lobstr/output/",
        "clip_path": "",
        "input_size_raw": 57,
        "joints_num": 19,
        'label_dec': [0],
        'enumerator': paramUtil.mocap_lobstr_action_enumerator
    },
    'mocap_pfnn': {
        "dataset_path": "./dataset/mocap_pfnn/output/",
        "clip_path": "",
        "input_size_raw": 57,
        "joints_num": 19,
        'label_dec': [0],
        'enumerator': paramUtil.mocap_pfnn_action_enumerator
    },
    'mocap_ours': {
        "dataset_path": "./dataset/mocap_ours/output/",
        "clip_path": "",
        "input_size_raw": 57,
        "joints_num": 19,
        'label_dec': [0],
        'enumerator': paramUtil.mocap_ours_action_enumerator
    }
}


def get_opt(opt_path, num_motions, device):
    opt = Namespace()
    opt_dict = vars(opt)
    opt.opt_path = opt_path
    skip = ('------------ Options -------------\n',
            '-------------- End ----------------\n')
    print('Reading', opt_path)
    with open(opt_path) as f:
        for line in f:
            if line not in skip:
                key, value = line.strip().split(': ')
                conversion = opt_conversion.get(key, lambda s: True if s == 'True' else False if s == 'False' else s)
                try:
                    opt_dict[key] = conversion(value)
                except:
                    opt_dict[key] = None

    opt_dict['isTrain'] = False

    opt_dict['which_epoch'] = 'latest'
    opt_dict['result_path'] = './eval_results/vae/'     # TODO: remove this
    opt_dict['do_random'] = True
    opt_dict['num_samples'] = num_motions   # but why?

    opt.device = device
    opt.save_root = os.path.join(opt.checkpoints_dir, opt.dataset_type, opt.name)
    opt.model_path = os.path.join(opt.save_root, 'model')
    opt.joints_path = os.path.join(opt.save_root, 'joints')
    opt.model_file_path = os.path.join(opt.model_path, opt.which_epoch + '.tar')
    opt.result_path = os.path.join(opt.result_path, opt.dataset_type, opt.name)
    # print(opt.coarse_grained)

    opt_dict.update(dataset_opt[opt.dataset_type])
    # print(opt_dict['label_dec'])

    opt.lie_enforce = False

    opt.dim_category = len(opt.label_dec)

    opt.pose_dim = opt.input_size_raw

    opt.input_types = ast.literal_eval(opt.input_types)

    opt.input_size = 0
    if opt.enable_last_p:
        opt.input_size = opt.input_size_raw
    if 'action' in opt.input_types:
        opt.input_size = opt.input_size + opt.dim_category
    if 'time_counter' in opt.input_types:
        opt.input_size = opt.input_size + 1
    if 'tracking3' in opt.input_types:
        opt.input_size = opt.input_size + 9
    if 'vtracking3' in opt.input_types:
        opt.input_size = opt.input_size + 9
    if 'avtracking3' in opt.input_types:
        opt.input_size = opt.input_size + 18
    if 'tracking4' in opt.input_types:
        opt.input_size = opt.input_size + 12
    if 'vtracking4' in opt.input_types:
        opt.input_size = opt.input_size + 12
    if 'avtracking4' in opt.input_types:
        opt.input_size = opt.input_size + 24
    if 'root_velocity' in opt.input_types:
        opt.input_size = opt.input_size + 3

    opt.veloc_input_size = opt.input_size_raw * 2 + 20

    opt.output_size = 18 * 6 + 3

    opt.actions = ast.literal_eval(opt.actions)

    if len(opt.actions) == 0:
        opt.actions = list(dataset_opt[opt.dataset_type]['enumerator'].keys())

    # ignore the following attributes
    opt.sequence_names = []

    return Options(opt)

