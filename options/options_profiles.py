options_profiles = {}

options_profiles["vae_6d"] = {}
options_profiles["vae_ours"] = {}
options_profiles["vae_moe"] = {}

# VAE Lie
options_profiles["vae_6d"]["mocap_lobstr"] = {
  "batch_size": 128,
  "dataset_type": "mocap_lobstr",
  "input_types": ["tracking4", "root_velocity"],
  "iters": 100000,
  "lambda_kld": 0.01,
  "model_type": "vae_6d",
  "motion_length": 45,
  "normalize_inputs": True,}

options_profiles["vae_6d"]["mocap_pfnn"] = options_profiles["vae_6d"]["mocap_lobstr"]
options_profiles["vae_6d"]["mocap_pfnn"].update({
  "dataset_type": "mocap_pfnn",
})
options_profiles["vae_6d"]["mocap_ours"] = options_profiles["vae_6d"]["mocap_lobstr"]
options_profiles["vae_6d"]["mocap_ours"].update({
  "dataset_type": "mocap_ours",
})

# VAE Ours
options_profiles["vae_ours"]["mocap_lobstr"] = options_profiles["vae_6d"]["mocap_lobstr"]

options_profiles["vae_moe"]["mocap_lobstr"] = options_profiles["vae_6d"]["mocap_lobstr"]
options_profiles["vae_moe"]["mocap_lobstr"].update({
  "dataset_type": "vae_moe",
})

options_profiles["vae_moe"]["mocap_pfnn"] = options_profiles["vae_ours"]["mocap_lobstr"]
options_profiles["vae_moe"]["mocap_pfnn"].update({
  "dataset_type": "mocap_ours",
})
options_profiles["vae_moe"]["mocap_ours"] = options_profiles["vae_ours"]["mocap_lobstr"]
options_profiles["vae_moe"]["mocap_ours"].update({
  "dataset_type": "mocap_ours",
})

# LoBSTr
options_profiles["lobstr"] = {}
options_profiles["lobstr"]["mocap_lobstr"] = {
  "batch_size": 128,
  "dataset_type": "mocap_lobstr",
  "input_types": ["tracking4", "root_velocity"],
  "iters": 50000,
  "lambda_kld": 0.01,
  "model_type": "lobstr",
  "motion_length": 45,
  "normalize_inputs": True,}

def get_options_profile(model_type, dataset_type):
    '''
    Utility to return options profiles for combinations of models and datasets
    '''
    return options_profiles[model_type][dataset_type]