import argparse
import copy
import os
import torch
from torch.nn.modules import loss
import utils.paramUtil as paramUtil
from utils.get_opt import dataset_opt
from options.options import Options
import sys
import csv

class BaseOptions():
    def __init__(self):
        self.parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter)
        self.initialized = False

    def initialize(self):
        self.parser.add_argument('--name', type=str, default="test", help='Name of experiment(For creating save root)')
        self.parser.add_argument("--gpu_id", type=str, default='0',
                                 help='Specify id of gpu for using')

        self.parser.add_argument('--coarse_grained', action='store_true', help='Use coarse_grained action type(only for HumanAct12)')

        self.parser.add_argument('--no_trajectory', action="store_true", help='Global trajectory will not be considered')
        self.parser.add_argument('--lie_enforce', action="store_true", help='Compute Loss on Lie Algebra Space, instead of Euclidean space')
        self.parser.add_argument('--input_types', nargs='+', help='select input types')
        self.parser.add_argument('--enable_last_p', action="store_true", help="Enable last position")
        self.parser.add_argument('--disable_recurrent', action="store_true", help="Disable recurrent prediction")
        self.parser.add_argument('--actions', nargs='+', help='action ID numbers', default=[])
        self.parser.add_argument('--sequence_names', nargs='+', help="animation sequence names")
        self.parser.add_argument('--history_length', type=int, help='length of history of tracking points to use', default=1)

        self.parser.add_argument("--motion_length", type=int, default=60, help="Length of motion")
        self.parser.add_argument('--dataset_type', type=str, help='Type of motion data', required=True)
        self.parser.add_argument('--model_type', type=str, help='model type', required=True)
        self.parser.add_argument('--clip_set', type=str, default="./dataset/pose_clip_full.csv", help='File path of clip data')
        self.parser.add_argument('--checkpoints_dir', type=str, default='./checkpoints/vae', help='Root path for saving checkpoint files')

        self.parser.add_argument("--dim_z", type=int, default=30, help='Dimension of motion noise')
        self.parser.add_argument('--hidden_size', type=int, default=128, help='Dimension of hidden unit in GRU')

        self.parser.add_argument('--prior_hidden_layers', type=int, default=1, help='Layers of GRU in prior net')
        self.parser.add_argument('--posterior_hidden_layers', type=int, default=1, help='Layers of GRU in posterior net')
        self.parser.add_argument('--decoder_hidden_layers', type=int, default=1, help='Layers of GRU in decoder net')

        self.parser.add_argument('--normalize_inputs', type=str, help='normalize inputs', default="true")
        self.parser.add_argument('--normalize_mode', type=str, help='normalize mode ["all", "yaw"]', default="yaw")
        self.parser.add_argument('--num_tp', type=int, help='number of tracked points', default=4)
        self.parser.add_argument('--warm_start', action="store_true", help="warm start (don't use zero for start state)")

        self.initialized = True

    def parse(self):
        if not self.initialized:
            self.initialize()

        args = self.parser.parse_args()

        # convert normalize_inputs to boolean
        if args.normalize_inputs.lower() == "true":
            args.normalize_inputs = True
        else:
            args.normalize_inputs = False

        self.opt = Options(args)

        self.opt.isTrain = self.isTrain

        if self.opt.gpu_id != '':
            self.opt.gpu_id = int(self.opt.gpu_id)
            torch.cuda.set_device(self.opt.gpu_id)
        else:
            self.opt.gpu_id = None
        args = vars(self.opt)

        print('------------ Options -------------')
        for k, v in sorted(args.items()):
            print('%s: %s' % (str(k), str(v)))
        print('-------------- End ----------------')
        if self.isTrain:
            # save to the disk
            expr_dir = os.path.join(self.opt.checkpoints_dir, self.opt.dataset_type, self.opt.name)
            if not os.path.exists(expr_dir):
                os.makedirs(expr_dir)
            file_name = os.path.join(expr_dir, 'opt.txt')
            with open(file_name, 'wt') as opt_file:
                opt_file.write('------------ Options -------------\n')
                for k, v in sorted(args.items()):
                    opt_file.write('%s: %s\n' % (str(k), str(v)))
                opt_file.write('-------------- End ----------------\n')

            cl_file_name = os.path.join(expr_dir, 'cl.txt')
            with open(cl_file_name, 'wt') as cl_file:
                cl = "python " + " ".join(sys.argv)
                cl_file.write(cl)

        # Replace action names with indices
        action_dict = dataset_opt[self.opt.dataset_type]['enumerator']
        if not self.opt.actions:
            self.opt.actions = list(action_dict.values())
        actions = copy.deepcopy(self.opt.actions)
        self.opt.actions.clear()
        for action in actions:
            try:
                action_index = list(action_dict.values()).index(action)
                action_id = list(action_dict.keys())[action_index]
                self.opt.actions.append(action_id)
            except ValueError as e:
                print('WARNING: action \"' + action + '\" not found')
                continue

        # make sure input_types is valid
        valid_input_types = [
            'action',
            'time_counter',
            'tracking3',
            'tracking4',
            'vtracking3',
            'vtracking4',
            'avtracking3',
            'avtracking4',
            'root_velocity'
        ]
        for input_type in self.opt.input_types:
            if input_type not in valid_input_types:
                raise ValueError('input_types must from this set of input types: ' + str(valid_input_types))

        # make sure loss_functions is valid
        loss_functions = ['mse', 'rot', 'contact', 'kld', 'velocity', 'action']
        for lf in self.opt.loss_functions:
            if lf not in loss_functions:
                raise ValueError(str(lf) + " is not a supported loss function")

        if self.opt.sequence_names is not None and self.opt.sequence_names[0].endswith(".csv"):
            if len(self.opt.sequence_names) > 1:
                raise ValueError("sequence_names can only have one argument when first element ends in '.csv'")

            csv_filename = self.opt.sequence_names[0]
            self.opt.sequence_names = []
            with open(csv_filename, 'r', newline='') as csvfile:
                csv_reader = csv.reader(csvfile, delimiter=",")
                for row in csv_reader:
                    self.opt.sequence_names.append(row[1].strip())

        return self.opt