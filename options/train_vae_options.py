from options.base_vae_options import BaseOptions


class TrainOptions(BaseOptions):
    def initialize(self):
        BaseOptions.initialize(self)
        self.parser.add_argument('--batch_size', type=int, default=128, help='Batch size of training process')

        self.parser.add_argument('--arbitrary_len', action='store_true', help='Enable variable length (batch_size has to'
                                                                              ' be 1 and motion_len will be disabled)')


        self.parser.add_argument('--skip_prob', type=float, default=0, help='Probability of skip frame while collecting loss')
        self.parser.add_argument('--tf_ratio', type=float, default=0.6, help='Teacher force learning ratio')

        self.parser.add_argument('--lambda_kld', type=float, help='Weight of KL Divergence')
        self.parser.add_argument('--lambda_align', type=float, default=0.5, help='Weight of align loss')

        self.parser.add_argument('--use_geo_loss', action='store_true', help='Compute Geodesic Loss(Only when lie_enforce is enabled)')
        self.parser.add_argument('--lambda_trajec', type=float, default=0.8, help='Calculate trajectory align loss(Only when lie_enforce is enabled)')

        self.parser.add_argument('--lr', type=float, default=0.0002, help='learning rate')

        self.parser.add_argument('--is_continue', action="store_true", help='Continue training of checkpoint models')
        self.parser.add_argument('--iters', type=int, help='Training iterations')
        self.parser.add_argument('--loss_functions', nargs='+', default=['mse', 'rot', 'contact', 'kld'])

        self.parser.add_argument('--mse_weight', type=float, default=1.0)
        self.parser.add_argument('--contact_weight', type=float, default=0.1)

        self.parser.add_argument('--plot_every', type=int, default=50, help='Sample frequency of iterations while plotting loss curve')
        self.parser.add_argument("--save_every", type=int, default=10000,
                            help='Frequency of saving intermediate models during training')
        self.parser.add_argument("--eval_every", type=int, default=2000,
                                 help='Frequency of save intermediate samples during training')
        self.parser.add_argument("--save_latest", type=int, default=50,
                                 help='Frequency of saving latest models during training')
        self.parser.add_argument('--print_every', type=int, default=20, help='Frequency of printing training progress')

        self.parser.add_argument('--num_workers', type=int, default=0, help="number of dataset workers")
        self.parser.add_argument('--balance', type=str, help='dataset balance mode', default='action')

        self.parser.add_argument('--learning_mode', type=str, help="learning mode type [vae, supervised]", default="vae")
        self.parser.add_argument('--step_lr', action="store_true", help="use step learning rate (step=100000, gamma=0.5)")

        self.isTrain = True
