from options.options_profiles import get_options_profile

class Options:
    '''
    Options container for current model
    '''
    def __init__(self, args=None, use_profile=True):
        '''
        Constructor for Options class

        Args:
            args: namespace object (such as from argparse)
            use_profile: uses prebuilt profiles for different training combinations
        '''
        self.actions        = self.assign([], args, "actions")
        self.arbitrary_len  = self.assign(False, args, "arbitrary_len")
        self.balance        = self.assign("action", args, "balance")
        self.batch_size     = self.assign(None, args, "batch_size")
        self.checkpoints_dir    = self.assign("./checkpoints/vae", args, "checkpoint_dir")
        self.clip_set       = self.assign("./dataset/pose_clip_full.csv", args, "clip_set")
        self.coarse_grained = self.assign(False, args, "coarse_grained")
        self.contact_weight = self.assign(1.0, args, "contact_weight")
        self.dataset_type   = self.assign(None, args, "dataset_type")
        self.decoder_hidden_layers  = self.assign(2, args, "decoder_hidden_layers")
        self.dim_z          = self.assign(30, args, "dim_z")
        self.enable_last_p  = self.assign(False, args, "enable_last_p")
        self.disable_recurrent  = self.assign(False, args, "disable_recurrent")
        self.eval_every     = self.assign(2000, args, "eval_every")
        self.gpu_id         = self.assign("0", args, "gpu_id")
        self.hidden_size    = self.assign(128, args, "hidden_size")
        self.history_length = self.assign(0, args, "history_length")
        self.input_size     = self.assign(None, args, "input_size")
        self.input_types    = self.assign(None, args, "input_types")
        self.is_continue    = self.assign(False, args, "is_continue")
        self.iters          = self.assign(None, args, "iters")
        self.lambda_align   = self.assign(0.5, args, "lambda_align")
        self.lambda_kld     = self.assign(None, args, "lambda_kld")
        self.lambda_trajectory  = self.assign(0.8, args, "lambda_trajectory")
        self.lr             = self.assign(0.0002, args, "lr")
        self.lie_enforce    = self.assign(None, args, "lie_enforce")
        self.loss_functions = self.assign(['mse', 'rot', 'contact', 'kld'], args, "loss_functions")
        self.model_path     = self.assign(None, args, "model_path")
        self.model_type     = self.assign(None, args, "model_type")
        self.motion_length  = self.assign(None, args, "motion_length")
        self.mse_weight     = self.assign(1.0, args, "mse_weight")
        self.name           = self.assign(None, args, "name")
        self.normalize_inputs   = self.assign(None, args, "normalize_inputs")
        self.normalize_mode = self.assign("yaw", args, "normalize_mode [all, yaw]")
        self.no_trajectory  = self.assign(False, args, "no_trajectory")
        self.num_tp         = self.assign(4, args, "num_tp")
        self.num_samples    = self.assign(None, args, "num_samples")
        self.num_workers    = self.assign(0, args, "num_workers")
        self.output_size    = self.assign(None, args, "output_size")
        self.plot_every     = self.assign(50, args, "plot_every")
        self.posterior_hidden_layers    = self.assign(1, args, "posterior_hidden_layers")
        self.print_every    = self.assign(20, args, "print_every")
        self.prior_hidden_layers    = self.assign(1, args, "prior_hidden_layers")
        self.save_every     = self.assign(2000, args, "save_every")
        self.save_latest    = self.assign(50, args, "save_latest")
        self.save_root      = self.assign(False, args, "save_root")
        self.scheduled_sampling = self.assign(False, args, "scheduled_sampling")
        self.sequence_names = self.assign(None, args, "sequence_names")
        self.skip_prob      = self.assign(0, args, "skip_prob")
        self.step_lr        = self.assign(False, args, "step_lr")
        self.learning_mode  = self.assign("vae", args, "learning_mode")
        self.tf_ratio       = self.assign(0.6, args, "tf_ratio")
        self.use_geo_loss   = self.assign(False, args, "use_geo_loss")
        self.warm_start     = self.assign(False, args, "warm_start")
        self.which_epoch    = self.assign(None, args, "which_epoch")

        if use_profile and args is not None:
            self.use_profile()

    def use_profile(self):
        '''
        Uses prebuilt profile setting only if variable is None
        '''
        profile = get_options_profile(self.model_type, self.dataset_type)
        for key, value in profile.items():
            if getattr(self, key) is None:
                setattr(self, key, value)

    def assign(self, default, args, attr):
        '''
        Smart assignment. If "args" does not contain "attr", the "default" value is used.

        Args:
            default: default value
            args: argument namespace
            attr: attribute to extract from "args"
        '''
        if args is not None and hasattr(args, attr):
            return getattr(args, attr)
        else:
            return default

    def __str__(self):
        output = ""
        d = self.__dict__
        for k, v in d.items():
            output += str(k) + ": " + str(v) + '\n'
        return output