import argparse
import math
import os

import numpy as np
from pymo.parsers import BVHParser
from pymo.preprocessing import *
from MocapParameterizer2 import MocapParameterizer2
from sklearn.pipeline import Pipeline

# Some joints are unnecessary (fingers)
valid_joints = [
    "Hips", "LeftUpLeg", "LeftLeg", "LeftFoot", "LeftToeBase", "RightUpLeg",
    "RightLeg", "RightFoot", "RightToeBase", "Spine", "Spine1", "Neck1",
    "Head", "LeftArm", "LeftForeArm", "LeftHand",
    "RightArm", "RightForeArm", "RightHand"
]

gait_labels = {
    0: "stand",
    1: "walk",
    2: "jogging",
    3: "run",
    4: "crouch",
    5: "jump",
    6: "crawl", # issue because on elevated surface
    7: "push",
}

def write_npy(args, filename, input_dir, output_dir):
    filename_stripped = filename.split(".bvh")[0]

    npy_filename = filename_stripped + ".npy"
    output_filename = output_dir + npy_filename
    if not args.force:
        if os.path.isfile(output_filename):
            print("Skipped", output_filename)
            return

    # pose reader
    parser = BVHParser()
    data = parser.parse(input_dir + filename)
    pos_param = Pipeline([
        ('param', MocapParameterizer2('position')),
    ])
    rot_param_global = Pipeline([
        ('param', MocapParameterizer2('euler_global')),
    ])
    rot_param_local = Pipeline([
        ('param', MocapParameterizer2('euler_local')),
    ])

    pos_data = pos_param.fit_transform([data])[0].values
    pos_data.rename(columns={0: "time"}, inplace=True)
    rot_global_data = rot_param_global.fit_transform([data])[0].values
    rot_global_data.rename(columns={0: "time"}, inplace=True)
    rot_local_data = rot_param_local.fit_transform([data])[0].values
    rot_local_data.rename(columns={0: "time"}, inplace=True)

    num_rows = pos_data.shape[0]
    data_raw = np.zeros((num_rows, (len(valid_joints) * 3) * 3))
    if args.dataset_type == "mocap_pfnn":
        actions_raw = np.zeros((num_rows, 8)) # 8 actions
    times = np.zeros((num_rows))
    frequency = data.framerate
    framerate = round(1.0 / frequency)

    # parse times
    index = 0
    for time in pos_data.index.values:
        times[index] = int(time) / 1e6
        index += 1

    # parse actions
    if args.dataset_type == "mocap_pfnn":
        gait_filename = os.path.join(input_dir, filename_stripped + ".gait")

        with open(gait_filename, "r") as f:            
            action_rows = f.readlines()

    for row in range(num_rows):        
        # parse global rotations (in radians)
        index = 0
        for label in rot_global_data.columns:
            value = rot_global_data[label][row]
            if label.rsplit("_", 1)[0] in valid_joints:
                if "Xrotation" in label:
                    data_raw[row, index] = float(value)
                    index += 1
                elif "Yrotation" in label:
                    data_raw[row, index] = float(value)
                    index += 1
                elif "Zrotation" in label:
                    data_raw[row, index] = float(value)
                    index += 1

        # parse local rotations (in degrees, convert to radians)
        for label in rot_local_data.columns:
            value = rot_local_data[label][row]
            if label.rsplit("_", 1)[0] in valid_joints:
                if "Xrotation" in label:
                    data_raw[row, index] = np.radians(float(value))
                    index += 1
                elif "Yrotation" in label:
                    data_raw[row, index] = np.radians(float(value))
                    index += 1
                elif "Zrotation" in label:
                    data_raw[row, index] = np.radians(float(value))
                    index += 1

        if args.dataset_type == "pfnn":
            actions_raw[row] = 0

        # parse positions
        for label in pos_data.columns:
            value = pos_data[label][row]
            if label.rsplit("_", 1)[0] in valid_joints:
                if "Xposition" in label:
                    data_raw[row, index] = float(value)
                    index += 1
                elif "Yposition" in label:
                    data_raw[row, index] = float(value)
                    index += 1
                elif "Zposition" in label:
                    data_raw[row, index] = float(value)
                    index += 1

    ds_data = []
    action_data = []

    # downsample from 120Hz to 60Hz
    if framerate == 120:
        ds_data = data_raw[::2] # select every other frame
    elif framerate == 60:
        ds_data = data_raw
    else:
        raise ValueError("we do not support the frequency of " + str(framerate))

    if args.dataset_type == "mocap_pfnn":
        ds_action_rows = action_rows[::2]
        for i in range(len(ds_action_rows)):
            actions = np.array([float(r) for r in ds_action_rows[i][:-1].split(" ")])
            action_data.append(actions)

    if args.dataset_type == "mocap_pfnn":
        action_data = np.stack(action_data)

    # pose .npy filename
    np.save(output_filename, ds_data)
    print("Saved", output_filename)

    # action .npy filename
    if args.dataset_type == "mocap_pfnn":
        action_npy_filename = filename_stripped + ".gait.npy"
        np.save(output_dir + action_npy_filename, action_data)
        print(output_dir + action_npy_filename)

def convert_dir(args, mocap_type):
    input_dir = "./dataset/" + args.dataset_type + "/input/" + mocap_type + "/"
    output_dir = "./dataset/" + args.dataset_type + "/output/" + mocap_type + "/"
    os.makedirs(output_dir, exist_ok=True)

    files = os.listdir(input_dir)

    # file out only bvh files
    files = [f for f in files if f.endswith(".bvh")]

    for f in files:
        write_npy(args, f, input_dir, output_dir)

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("--dataset_type", type=str, default="mocap_ours")
    parser.add_argument("--force", action="store_true", help="force rerun")
    parser.add_argument("--mode", nargs="+", help="dataset mode", default=["train", "valid", "test"])
    args = parser.parse_args()

    for mode in args.mode:
        convert_dir(args, mode)
