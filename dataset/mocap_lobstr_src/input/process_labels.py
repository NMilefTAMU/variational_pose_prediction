import csv
import re
import os

supported_actions = [
    "walk",
    "run",
    "jump",
]

def main():
    input_filename = "cmu_dataset_labels_raw.txt"

    action_labels = {""}

    with open(input_filename, "r") as f:
        lines = f.readlines()

        current_subject = 0
        i = 0
        while i < len(lines):
            result = re.search(r'Subject #\d+', lines[i])
            if result is not None:
                current_subject = int(result.group().split("Subject #")[1])
                i += 3
                continue

            line_processed = lines[i].split("\t")
            trial = int(line_processed[1])
            action = line_processed[2]
            if action in supported_actions:
                bvh_filename = str(current_subject).nfill(2) + "_" + str(trial).nfill(2)
                action_labels[bvh_filename] = 

            i += 1


if __name__ == "__main__":
    main()