import numpy as np
import pandas as pd
from pymo.preprocessing import MocapParameterizer, UnsupportedParamError
from pymo.rotation_tools import Rotation
from scipy.spatial.transform.rotation import Rotation as R


class MocapParameterizer2(MocapParameterizer):
    '''
    Extends MocapParameterizer and adds support for global Euler angle extraction
    '''
    def __init__(self, param_type="euler_local"):
        '''
        param_type = {'euler_local, 'euler_global', 'position'}
        '''
        self.param_type = param_type

    def transform(self, X, y=None):
        if self.param_type == 'euler_local':
            return X
        elif self.param_type == 'euler_global':
            return self._to_euler_global(X)
        elif self.param_type == 'position':
            return self._to_pos(X)
        else:
            raise UnsupportedParamError('Unsupported param: %s. Value param types are: euler_local, euler_global, position' % self.param_type)

    def _to_euler_global(self, X):
        '''
        Convert local Euler angles to global Euler angles
        '''
        Q = []
        for track in X:
            channels = []
            titles = []
            l_euler_df = track.values

            g_euler_df = pd.DataFrame(index=l_euler_df.index)

            l_rot_cols = [c for c in l_euler_df.columns if ('rotation' in c)]
            g_rot_cols = [c for c in g_euler_df.columns if ('rotation' in c)]

            joints = (joint for joint in track.skeleton)

            tree_data = {}

            for joint in track.traverse():
                parent = track.skeleton[joint]['parent']

                # Get the rotation columns that belong to this joint
                lrc = l_euler_df[[c for c in l_rot_cols if joint in c]]

                if lrc.shape[1] < 3:
                    euler_values = [[0,0,0] for f in lrc.iterrows()]
                else:
                    euler_values = [[f[1]['%s_Xrotation'%joint],
                                     f[1]['%s_Yrotation'%joint],
                                     f[1]['%s_Zrotation'%joint]] for f in lrc.iterrows()]

                    rotation_order = lrc.columns[0][lrc.columns[0].find('rotation') - 1] + \
                                     lrc.columns[1][lrc.columns[1].find('rotation') - 1] + \
                                     lrc.columns[2][lrc.columns[2].find('rotation') - 1]

                rotmats = np.asarray([Rotation([f[0], f[1], f[2]], 'euler', rotation_order, from_deg=True).rotmat for f in euler_values])

                tree_data[joint] = [[]]

                if track.root_name == joint:
                    tree_data[joint][0] = rotmats
                else:
                    tree_data[joint][0] = np.asarray([np.matmul(rotmats[i], tree_data[parent][0][i]) for i in range(len(tree_data[parent][0]))])

                euler_joint = -R.from_matrix(tree_data[joint][0]).as_euler("XYZ", degrees=False)

                g_euler_df['%s_Xrotation'%joint] = pd.Series(data=[e[0] for e in euler_joint], index=g_euler_df.index)
                g_euler_df['%s_Yrotation'%joint] = pd.Series(data=[e[1] for e in euler_joint], index=g_euler_df.index)
                g_euler_df['%s_Zrotation'%joint] = pd.Series(data=[e[2] for e in euler_joint], index=g_euler_df.index)

            new_track = track.clone()
            new_track.values = g_euler_df
            Q.append(new_track)

        return Q