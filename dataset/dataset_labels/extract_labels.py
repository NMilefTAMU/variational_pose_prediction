import json

def parse_cmu_txt(action_map):
    with open("./CMU_mocap.txt", "r") as f:
        lines = f.readlines()

    i = 0
    while i < len(lines):
        if "Subject #" in lines[i]:
            nums = lines[i].split("Subject #")[1]
            nums = [int(s) for s in nums.split() if s.isdigit()]
            subject_num = "{:02d}".format(int(nums[0]))
            i += 3

            while i < len(lines):
                if not lines[i].strip():
                    break
                extract_cmu_line(action_map, lines[i], subject_num)
                i += 1
            continue

        i += 1

    return action_map

def parse_csv(action_map, path):
    with open(path, "r") as f:
        lines = f.readlines()

    for row in lines:
        name = row.split(",", 1)[0]
        actions = [x.strip() for x in row.split(",")[1:]]
        action_map[name] = actions

def extract_cmu_line(action_map, line, subject_num):
    cols = line.split("\t")
    trial_num = "{:02d}".format(int(cols[1]))        
    actions = extract_actions(cols[2])
    name = subject_num + "_" + trial_num
    action_map[name] = actions

def extract_actions(action_labels):
    prefix = ""
    action_groups = action_labels.split(";")
    action_groups = [x.strip() for x in action_groups]

    for group in action_groups:
        labels = group
        if " - " in group:
            prefix = group.split(" - ")[0] + "_"
            labels = group.split(" - ")[1]

        actions = labels.replace(";", ",").split(",")
        actions = [prefix + x.strip().replace(" ", "_").lower() for x in actions]

    return actions


def calc_num_action_type(action_map):
    actions = {}
    for _, v in action_map.items():
        for action in v:
            actions[action] = 1
    return len(actions)


if __name__ == "__main__":
    action_map = {}
    parse_cmu_txt(action_map)
    parse_csv(action_map, "./PFNN_labels.csv")
    parse_csv(action_map, "./edin_punching_labels.csv")
    parse_csv(action_map, "./edin_xsens_split_labels.csv")
    parse_csv(action_map, "./edin_terrain.csv")
    parse_csv(action_map, "./hdm05_labels.csv")
    #parse_csv(action_map, "./edin_xsens_labels.csv")

    # save action_map to json
    with open("action_map.json", "w") as f:
        print("Processed", str(len(action_map)), "animations")
        print("Found", calc_num_action_type(action_map), "action types")
        json.dump(action_map, f)
