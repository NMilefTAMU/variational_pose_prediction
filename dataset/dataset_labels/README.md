# Dataset
The datasets used don't always have labels, especially datasets from Edinburgh. I manually added action classifications for these datasets. The CMU motion sequence labels are extracted from the HTML webpage. These labels may not be correct or may be subjective in some cases. A single animation can have multiple labels.

The script "extract_labels.py" extracts labels from multiple sources. The raw data is in either the .csv or .txt files in this folder. To run:

```
python extract_labels.py
```

The output is a JSON file with keys of the filenames and values of a list of action classifications. The file name is called "action_map.json". Put this file in the following location: ./dataset/mocap_ours/output/action_map.json.