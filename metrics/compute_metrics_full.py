'''
This file is for computing metrics on the entire paper.
'''

import argparse
import csv
import os
import random
import sys

import numpy as np
import torch
import yaml

sys.path.insert(0, os.path.join(os.path.dirname(os.path.abspath(__file__)), ".."))

from evaluate_batch import InferenceBatch
from viewer.lobstr_reader import convert_lobstr_skeleton_to_mocap_ours, read_lobstr_results
from viewer_base import ViewerArgumentParser

from metrics.sequence_metrics import compute_acceleration_error, compute_contact_accuracy, compute_skating, compute_spatial_error, compute_stiffness, compute_velocity_error

def compute_metrics_network(test_name, test_settings, global_settings):
    if global_settings["set_seed"] is not None:
        torch.manual_seed(global_settings["set_seed"])
        random.seed(global_settings["set_seed"])

    device = torch.device("cuda:0")

    inference = InferenceBatch(
        global_settings["dataset"],
        test_settings["experiment"],
        sequence_names=global_settings["sequence_names"],
        dataset_mode=global_settings["dataset_type"],
        ik_mode=(not global_settings["disable_ik"]),
        root_transform=(not global_settings["disable_root_transform"]),
        save_z=global_settings["save_z"],
        lock_eps=global_settings["lock_eps"],
        smart_eps=test_settings["smart_eps"],
        disable_interpolation=global_settings["disable_interpolation"],
        selection_mode="ip",
        lookahead=global_settings["lookahead"],
    )

    results = [["name",
                "spatial_error_mean", "spatial_error_std",
                "velocity_error_mean", "velocity_error_std",
                "acceleration_error_mean", "acceleration_error_std",
                "contact_accuracy_mean", "contact_accuracy_std",
                "stiffness_mean", "stiffness_std",
                "stiffness_mean_gt",
                "skating_error_mean", "skating_error_std",
                "skating_error_mean_gt"]]

    for i in range(len(inference.dataset)):
        errors_ours = {"spatial_error": np.zeros((test_settings["num_samples"])),
                       "velocity_error": np.zeros((test_settings["num_samples"])),
                       "acceleration_error": np.zeros((test_settings["num_samples"])),
                       "contact_accuracy": np.zeros((test_settings["num_samples"])),
                       "stiffness": np.zeros((test_settings["num_samples"])),
                       "skating_error": np.zeros((test_settings["num_samples"]))}

        current_sequence_index = i
        current_sequence_name = inference.dataset.get_anim_names()[i]
        skating_threshold = 1 #0.025
        print(current_sequence_name)

        # compute for multiple samples
        for j in range(test_settings["num_samples"]):
            gt_sequence = inference.gt_sequence(current_sequence_index)
            num_frames = gt_sequence['position'].shape[0]
            if test_settings['algorithm'] == "ours":
                generated_sequence = inference.evaluate_sequence(current_sequence_index)
                frame_offset = 59
            elif test_settings['algorithm'] == "lobstr":
                try:
                    generated_sequence = read_lobstr_results(device, current_sequence_name.split(".npy")[0], current_sequence_index, inference, gt_sequence['position'][59:, 0:3], ik_mode=True, lobstr_dataset=test_settings['lobstr_dataset'])
                except:
                    print("error finding lobstr file", current_sequence_name)
                    continue
                generated_sequence["position"] = convert_lobstr_skeleton_to_mocap_ours(generated_sequence["position"])
                frame_offset = 0

            # spatial error
            errors_ours["spatial_error"][j] = compute_spatial_error(generated_sequence['position'][frame_offset:], gt_sequence['position'][59:])

            # velocity error
            errors_ours["velocity_error"][j] = compute_velocity_error(generated_sequence["position"][frame_offset:], gt_sequence['position'][59:])

            # acceleration
            errors_ours["acceleration_error"][j] = compute_acceleration_error(generated_sequence["position"][frame_offset:], gt_sequence['position'][59:])

            # contact accuracy
            errors_ours["contact_accuracy"][j] = compute_contact_accuracy(generated_sequence["contact"][frame_offset:], gt_sequence['contact'][59:])

            # contact accuracy
            errors_ours["stiffness"][j] = compute_stiffness(generated_sequence['position'][frame_offset:], kinematic_chain=inference.kinematic_chain, degrees=True)

            # skating error
            errors_ours["skating_error"][j] = compute_skating(generated_sequence['position'][frame_offset:], inference.dataset.bone_map, skating_threshold)

        stiffness_gt = compute_stiffness(gt_sequence['position'][59:], inference.kinematic_chain, degrees=True)
        skating_error_gt = compute_skating(gt_sequence['position'][59:], inference.dataset.bone_map, skating_threshold)

        # save results
        anim_name = inference.dataset.get_anim_names()[i]

        results_vector = [
            anim_name,
            np.mean(errors_ours["spatial_error"]),
            np.std(errors_ours["spatial_error"]),
            np.mean(errors_ours["velocity_error"]),
            np.std(errors_ours["velocity_error"]),
            np.mean(errors_ours["acceleration_error"]),
            np.std(errors_ours["acceleration_error"]),
            np.mean(errors_ours["contact_accuracy"]),
            np.std(errors_ours["contact_accuracy"]),
            np.mean(errors_ours["stiffness"]),
            np.std(errors_ours["stiffness"]),
            stiffness_gt,
            np.mean(errors_ours["skating_error"]),
            np.std(errors_ours["skating_error"]),
            skating_error_gt]

        results.append(results_vector)

    print(
        "####################",
        test_settings["experiment"],
        "####################",
    )
    print("Spatial Error (Ours):\t", np.mean(errors_ours["spatial_error"]), "\t", np.std(errors_ours["spatial_error"]))
    print("Velocity Error (Ours):\t", np.mean(errors_ours["velocity_error"]), "\t", np.std(errors_ours["velocity_error"]))
    print("Acceleration Error (Ours):\t", np.mean(errors_ours["acceleration_error"]), "\t", np.std(errors_ours["acceleration_error"]))
    print("Contact Accuracy (Ours):\t", np.mean(errors_ours["contact_accuracy"]), "\t", np.std(errors_ours["contact_accuracy"]))
    print("Stiffness (Ours):\t", np.mean(errors_ours["stiffness"]), "\t", np.std(errors_ours["stiffness"]))
    print("Stiffness (GT):\t", stiffness_gt)
    print("Skating Error (Ours):\t", np.mean(errors_ours["skating_error"]), "\t", np.std(errors_ours["skating_error"]))
    print("Skating Error (GT):\t", skating_error_gt)

    return results

if __name__ == "__main__":  
    # use yaml file to specify tests and settings
    with open("metrics/metrics.yaml", "r") as f:
        metrics_config = yaml.safe_load(f)

    if metrics_config["global_settings"]["sequence_names"][0].endswith(".csv"):
        sequence_names = []
        with open(metrics_config["global_settings"]["sequence_names"][0], 'r', newline='') as csvfile:
            csv_reader = csv.reader(csvfile, delimiter=",")
            for row in csv_reader:
                sequence_names.append(row[1].strip())
        metrics_config["global_settings"]["sequence_names"] = sequence_names

    os.makedirs("results", exist_ok=True)

    test_results = {}
    global_settings = metrics_config["global_settings"]
    for test_name, test_settings in metrics_config["tests"].items():
        results = compute_metrics_network(test_name, test_settings, global_settings)

        test_results[test_name] = results

    # write results to files
    for test_name, test_result in test_results.items():
        with open(os.path.join("results", test_name + ".csv"), "w") as f:
            writer = csv.writer(f)
            writer.writerows(test_result)
