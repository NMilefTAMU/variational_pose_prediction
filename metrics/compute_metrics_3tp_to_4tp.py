'''
This file is for computing metrics on the 3tp to 4tp conversion network.
'''

import os
import sys

sys.path.insert(0, os.path.join(os.path.dirname(os.path.abspath(__file__)), ".."))

from types import SimpleNamespace

import numpy as np
import torch
from dataProcessing.dataset_mocap_ours import DatasetMocapOurs
from models.root_pred.occlusion_network import OcclusionNet
from options.options import Options
from scipy.spatial.transform import Rotation
from torch.utils.data import DataLoader
from utils import paramUtil
from utils.vr.calibrate import get_vr_pose_scale
from utils.vr.utils import RecordedVRReader, preprocess_points


def compute_pos_error(pred, gt):
    '''
    Computes the Euclidean distance as spatial error
    '''
    distance = float(torch.sqrt(torch.sum((pred - gt)**2)))
    return distance

def extract_y_rot(rot):
    root_quat = np.expand_dims(rot.as_quat(), axis=0)
    root_quat[:, 0] = 0 #x
    root_quat[:, 2] = 0 # z
    mag = np.sqrt((root_quat[:, 1]**2) + (root_quat[:, 3]**2))
    root_quat[:, 3] /= mag # w
    root_quat[:, 1] /= mag # y
    #root_rot = torch.from_numpy(Rotation.from_quat(root_quat).as_matrix())
    #root_rot = Rotation.from_matrix(root_rot.detach().cpu().numpy())
    rot = Rotation.from_quat(root_quat)
    return rot

def compute_rot_yaw_error(pred, gt):
    '''
    Computes the rotational error (dot-product between vectors from rotation matrices)
    '''
    unit_vec = np.array([1, 0, 0])

    pred_rot = extract_y_rot(Rotation.from_matrix(pred.detach().cpu().numpy()))
    gt_rot = extract_y_rot(Rotation.from_matrix(gt.detach().cpu().numpy()))

    pred_vec = pred_rot.apply(unit_vec)
    gt_vec = gt_rot.apply(unit_vec)

    dot = np.sum(pred_vec * gt_vec)
    angle = np.arccos(dot) * (180 / np.pi)
    return float(angle)

def compute_rot_error(pred, gt):
    '''
    Computes the rotational error (dot-product between vectors from rotation matrices)
    '''
    unit_vec = np.array([1, 0, 0])

    pred_rot = Rotation.from_matrix(pred.detach().cpu().numpy())
    gt_rot = Rotation.from_matrix(gt.detach().cpu().numpy())

    pred_vec = pred_rot.apply(unit_vec)
    gt_vec = gt_rot.apply(unit_vec)

    dot = np.sum(pred_vec * gt_vec)
    angle = np.arccos(dot) * (180 / np.pi)
    return float(angle)

def compute_metrics_3tp_to_4tp(args):
    opt = Options(args, use_profile=True)

    device = torch.device("cuda:" + str(args.gpu_id) if torch.cuda.is_available() else "cpu")

    dataset_params  = paramUtil.get_dataset_params(opt)
    dataset_path    = dataset_params['dataset_path'] 

    test_dataset = DatasetMocapOurs(dataset_path, opt, "test")

    network = OcclusionNet(device=device)
    network.load_state_dict(torch.load("checkpoints/waist_occlusion/wo_model.pth"))
    network.to(device)

    pos_errors = []
    rot_errors = []
    rot_yaw_errors = []

    for i in range(len(test_dataset)):
        sequence = test_dataset[i]
        network.init_hidden(1)
        sequence_length = sequence['position'].shape[0]

        seq_pos_error = 0
        seq_rot_error = 0
        seq_rot_yaw_error = 0

        for frame in range(0, sequence_length):
            raw_pos = torch.from_numpy(sequence["raw_tracker_pos"][[frame], 3:]).to(device)
            raw_vel = torch.from_numpy(sequence["raw_tracker_vel"][[frame], 3:]).to(device)

            gt_pos = torch.from_numpy(sequence["root_pos_raw"][frame]).to(device)
            gt_rot_mat = torch.from_numpy(sequence["root_rot_mat"][frame]).to(device)

            input_data = torch.cat([raw_pos, raw_vel], axis=-1).float().to(device)

            output = network(input_data)
            output = OcclusionNet.compute_root_pos_and_rot(output, device)

            pos_error = compute_pos_error(output['root_pos'][0].double(), gt_pos)
            rot_error = compute_rot_error(output['root_rot_mat'][0].double(), gt_rot_mat)
            rot_yaw_error = compute_rot_yaw_error(output['root_rot_mat'][0].double(), gt_rot_mat)

            seq_pos_error += pos_error / sequence_length
            seq_rot_error += rot_error / sequence_length
            seq_rot_yaw_error += rot_yaw_error / sequence_length

        pos_errors.append(seq_pos_error)
        rot_errors.append(seq_rot_error)
        rot_yaw_errors.append(seq_rot_yaw_error)

        print('Pos:', seq_pos_error, 'Rot:', seq_rot_error, 'Rot Yaw:', seq_rot_yaw_error)

    pos_errors = np.array(pos_errors)
    rot_errors = np.array(rot_errors)
    rot_yaw_errors = np.array(rot_yaw_errors)

    print("Final pos error mean:", np.mean(pos_errors), "\tstd:", np.std(pos_errors))
    print("Final rot error mean:", np.mean(rot_errors), "\tstd:", np.std(rot_errors))
    print("Final rot yaw error mean:", np.mean(rot_yaw_errors), "\tstd:", np.std(rot_yaw_errors))

def compute_metrics_3tp_to_4tp_vr(args):
    opt = Options(args, use_profile=True)

    device = torch.device("cuda:" + str(args.gpu_id) if torch.cuda.is_available() else "cpu")

    dataset_params  = paramUtil.get_dataset_params(opt)
    dataset_path    = dataset_params['dataset_path'] 

    network = OcclusionNet(device=device)
    network.load_state_dict(torch.load("checkpoints/waist_occlusion/wo_model.pth"))
    network.to(device)

    pos_errors = []
    rot_errors = []
    rot_yaw_errors = []

    folders = os.listdir("recorded_tp")

    for j in range(len(folders)):
        folder = folders[j]
        files = os.listdir(os.path.join("recorded_tp", folder))
        files = [x for x in files if x != "calibrate.npy"]
        calibration_file = os.path.join("recorded_tp", folder, "calibrate.npy")

        calibration_reader_3tp = RecordedVRReader(calibration_file, waist_occlusion=True)
        for i in range(60):
            cal_pose_3tp = calibration_reader_3tp.get_next_vr_frame_recorded(i, 3)
        vr_scale_params_3tp = get_vr_pose_scale(cal_pose_3tp)

        calibration_reader_4tp = RecordedVRReader(calibration_file, waist_occlusion=False)
        for i in range(60):
            cal_pose_4tp = calibration_reader_4tp.get_next_vr_frame_recorded(i, 4)
        vr_scale_params_4tp = get_vr_pose_scale(cal_pose_4tp)

        for i in range(files):
            recorded_tp_reader_3tp = RecordedVRReader(os.path.join(folder, files[i]), waist_occlusion=True)
            recorded_tp_reader_4tp = RecordedVRReader(os.path.join(folder, files[i]), waist_occlusion=False)

            last_data_3tp = None
            last_data_4tp = None

            for frame in range(0, len(recorded_tp_reader_4tp)):
                frame_3tp = recorded_tp_reader_3tp.get_next_vr_frame_recorded(frame, num_tp=3)
                frame_4tp = recorded_tp_reader_4tp.get_next_vr_frame_recorded(frame, num_tp=4)

                data_3tp = preprocess_points(frame_3tp, last_data_3tp, num_tp=4, initial_rot=None, scale_params=vr_scale_params_3tp)
                data_4tp = preprocess_points(frame_4tp, last_data_4tp, num_tp=4, initial_rot=None, scale_params=vr_scale_params_4tp)

                import pdb; pdb.set_trace()

                raw_pos = torch.from_numpy(sequence["raw_tracker_pos"][[frame], 3:]).to(device)
                raw_vel = torch.from_numpy(sequence["raw_tracker_vel"][[frame], 3:]).to(device)

                gt_pos = torch.from_numpy(sequence["root_pos_raw"][frame]).to(device)
                gt_rot_mat = torch.from_numpy(sequence["root_rot_mat"][frame]).to(device)

                input_data = torch.cat([raw_pos, raw_vel], axis=-1).float().to(device)

                output = network(input_data)
                output = OcclusionNet.compute_root_pos_and_rot(output, device)

                pos_error = compute_pos_error(output['root_pos'][0].double(), gt_pos)
                rot_error = compute_rot_error(output['root_rot_mat'][0].double(), gt_rot_mat)
                rot_yaw_error = compute_rot_yaw_error(output['root_rot_mat'][0].double(), gt_rot_mat)

                seq_pos_error += pos_error / sequence_length
                seq_rot_error += rot_error / sequence_length
                seq_rot_yaw_error += rot_yaw_error / sequence_length

            pos_errors.append(seq_pos_error)
            rot_errors.append(seq_rot_error)
            rot_yaw_errors.append(seq_rot_yaw_error)

            print('Pos:', seq_pos_error, 'Rot:', seq_rot_error, 'Rot Yaw:', seq_rot_yaw_error)

    pos_errors = np.array(pos_errors)
    rot_errors = np.array(rot_errors)
    rot_yaw_errors = np.array(rot_yaw_errors)

    print("Final pos error mean:", np.mean(pos_errors), "\tstd:", np.std(pos_errors))
    print("Final rot error mean:", np.mean(rot_errors), "\tstd:", np.std(rot_errors))
    print("Final rot yaw error mean:", np.mean(rot_yaw_errors), "\tstd:", np.std(rot_yaw_errors))

if __name__ == "__main__":
    args = {}
    args["model_type"] = "vae_6d"
    args["dataset_type"] = "mocap_ours"
    args["motion_length"] = -1
    args["gpu_id"] = 0
    args = SimpleNamespace(**args)

    #compute_metrics_3tp_to_4tp(args)
    compute_metrics_3tp_to_4tp_vr(args)
