'''
This file is for computing metrics on applications/ablations.
'''

import csv
import os
import sys
import numpy as np
import torch

sys.path.insert(0, os.path.join(os.path.dirname(os.path.abspath(__file__)), ".."))

from environment.choreography import Choreography
from evaluate_batch import InferenceBatch
from dataProcessing.dataset_mocap import DatasetMocap

def compute_sit_collisions(num_samples, num_runs):
    device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")

    inference = InferenceBatch(
        "mocap_ours",
        "test_model_6d_8_25b",
        motion_length=-1,
        sequence_names=["143_18"],
        dataset_mode="test3",
        ik_mode=True,
        root_transform=True,
        save_z=False,
        lock_eps=True,
        smart_eps=num_samples,
        disable_interpolation=[],
        selection_mode="jp",
        lookahead=10,
        device = device,
        choreography_file="environment/choreographies/143_18_sit_boundary.yaml",
    )

    choreography = Choreography("environment/choreographies/143_18_sit_boundary.yaml")
    start_frame = 50
    end_frame = 140
    generated_sequence = inference.evaluate_sequence(0)

    collisions = 0

    for f in range(start_frame, end_frame):
        event = choreography.get_next_event(f)
        target_joint_index = DatasetMocap.get_bone_map()[event.joint]
        bounds = event.bounds
        pos = generated_sequence["position"][f, target_joint_index*3:target_joint_index*3+3]

        if pos[0] > min(bounds[0][0], bounds[1][0]) and \
            pos[0] < max(bounds[0][0], bounds[1][0]) and \
            pos[1] > min(bounds[0][1], bounds[1][1]) and \
            pos[1] < max(bounds[0][1], bounds[1][1]) and \
            pos[2] > min(bounds[0][2], bounds[1][2]) and \
            pos[2] < max(bounds[0][2], bounds[1][2]):

            collisions += 1

    return collisions / (end_frame - start_frame)

def compute_high_kick_accuracy(num_samples, num_runs):
    device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")

    inference = InferenceBatch(
        "mocap_ours",
        "test_model_6d_8_25b",
        motion_length=-1,
        sequence_names=["143_24"],
        dataset_mode="test3",
        ik_mode=True,
        root_transform=True,
        save_z=False,
        lock_eps=True,
        smart_eps=num_samples,
        disable_interpolation=[],
        selection_mode="jp",
        lookahead=10,
        device = device,
        choreography_file="environment/choreographies/143_24_high_kick.yaml",
    )

    choreography = Choreography("environment/choreographies/143_24_high_kick.yaml")
    target_frame = 149
    target_pos = np.array(choreography.get_next_event(target_frame).pos)
    target_joint = choreography.get_next_event(target_frame).joint
    target_joint_index = DatasetMocap.get_bone_map()[target_joint]

    generated_sequence = inference.evaluate_sequence(0)
    joint_prediction = generated_sequence["position"][target_frame, target_joint_index*3:target_joint_index*3+3]
    joint_prediction = joint_prediction.detach().cpu().numpy()

    error = np.linalg.norm(joint_prediction - target_pos)
    return error

def compute_mid_kick_accuracy(num_samples, num_runs):
    device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")

    inference = InferenceBatch(
        "mocap_ours",
        "test_model_6d_8_25b",
        motion_length=-1,
        sequence_names=["143_24"],
        dataset_mode="test3",
        ik_mode=True,
        root_transform=True,
        save_z=False,
        lock_eps=True,
        smart_eps=num_samples,
        disable_interpolation=[],
        selection_mode="jp",
        lookahead=10,
        device = device,
        choreography_file="environment/choreographies/143_24_mid_kick.yaml",
    )

    choreography = Choreography("environment/choreographies/143_24_mid_kick.yaml")
    target_frame = 149
    target_pos = np.array(choreography.get_next_event(target_frame).pos)
    target_joint = choreography.get_next_event(target_frame).joint
    target_joint_index = DatasetMocap.get_bone_map()[target_joint]

    generated_sequence = inference.evaluate_sequence(0)
    joint_prediction = generated_sequence["position"][target_frame, target_joint_index*3:target_joint_index*3+3]
    joint_prediction = joint_prediction.detach().cpu().numpy()

    error = np.linalg.norm(joint_prediction - target_pos)
    return error

if __name__ == "__main__":  
    os.makedirs("results", exist_ok=True)

    num_runs = 100

    sit_error = {}
    high_kick_error = {}
    mid_kick_error = {}

    num_samples_list = [1, 10, 100, 1000]

    # ablations
    for num_samples in num_samples_list:
        sit_error[num_samples] = []
        high_kick_error[num_samples] = []
        mid_kick_error[num_samples] = []
        for _ in range(num_runs):
            sit_error[num_samples].append(compute_sit_collisions(num_samples, num_runs))
            high_kick_error[num_samples].append(compute_high_kick_accuracy(num_samples, num_runs))
            mid_kick_error[num_samples].append(compute_high_kick_accuracy(num_samples, num_runs))

    with open("results/high_kick_metrics.csv", "w") as f:
        writer = csv.writer(f)
        for num_samples in num_samples_list:
            writer.writerow([num_samples] + high_kick_error[num_samples])

    with open("results/low_kick_metrics.csv", "w") as f:
        writer = csv.writer(f)
        for num_samples in num_samples_list:
            writer.writerow([num_samples] + mid_kick_error[num_samples])

    with open("results/sit_metrics.csv", "w") as f:
        writer = csv.writer(f)
        for num_samples in num_samples_list:
            writer.writerow([num_samples] + sit_error[num_samples])
