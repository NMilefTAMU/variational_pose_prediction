'''
This file is for computing timing metrics
'''
import argparse
import os
import sys
import torch

sys.path.insert(0, os.path.join(os.path.dirname(os.path.abspath(__file__)), ".."))

from models.motion_vae import DecoderGRU6d
from models.motion_vae_conv import ConvVAE
from models.motion_vae_transformer import DecoderTransformer
from models.motion_vae_conv import ConvVAE
from models.pose_network import PoseModelNetwork


def compute_timing_data(args):
    device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")

    input_size = 24
    output_size = 111
    dim_z = 30

    with torch.no_grad():
        time_pose_model(100, device)
        time_pose_model(1, device)
        time_ours_vae(100, input_size, output_size, dim_z, device)
        time_ours_vae(1, input_size, output_size, dim_z, device)
        time_conv_vae(input_size, output_size, dim_z, device)
        time_transformer(input_size, output_size, dim_z, device)

def time_ours_vae(batch_size, input_size, output_size, dim_z, device):
    ours_vae = DecoderGRU6d(input_size, output_size, 128, 1, batch_size, device)
    ours_vae.to(device)
    ours_vae.eval()

    start_time = torch.cuda.Event(enable_timing=True)
    end_time = torch.cuda.Event(enable_timing=True)

    input_data = torch.zeros((batch_size, input_size)).to(device)
    for _ in range(0, 1000):
        ours_vae(input_data)

    start_time.record()
    for _ in range(1000, 11000):
        ours_vae(input_data)
    end_time.record()

    torch.cuda.synchronize()
    print(start_time.elapsed_time(end_time) / 10000)

def time_conv_vae(input_size, output_size, dim_z, device):
    conv_vae = ConvVAE(input_size, output_size, dim_z, 1, device, lock_eps=True, num_frames=16)
    conv_vae.to(device)
    conv_vae.eval()

    start_time = torch.cuda.Event(enable_timing=True)
    end_time = torch.cuda.Event(enable_timing=True)

    input_data = torch.zeros((1, 16 * input_size)).to(device)
    for _ in range(0, 1000):
        conv_vae(input_data)

    start_time.record()
    for _ in range(1000, 11000):
        conv_vae(input_data)
    end_time.record()

    torch.cuda.synchronize()
    print(start_time.elapsed_time(end_time) / 10000)

def time_transformer(input_size, output_size, dim_z, device):
    transformer = DecoderTransformer(input_size, output_size, 3, 256, 8, device)
    transformer.to(device)
    transformer.eval()

    start_time = torch.cuda.Event(enable_timing=True)
    end_time = torch.cuda.Event(enable_timing=True)

    input_data = torch.zeros((1, 40, input_size)).to(device)
    for _ in range(0, 1000):
        transformer(input_data)

    start_time.record()
    for _ in range(1000, 11000):
        transformer(input_data)
    end_time.record()

    torch.cuda.synchronize()
    print(start_time.elapsed_time(end_time) / 10000)

def time_pose_model(batch_size, device):
    pose_model = PoseModelNetwork(
        input_size=19 * 3,
        num_levels = 3,
    )
    pose_model.to(device)
    pose_model.eval()

    start_time = torch.cuda.Event(enable_timing=True)
    end_time = torch.cuda.Event(enable_timing=True)
    input_data = torch.zeros((batch_size, 19*3)).to(device)
    for _ in range(0, 1000):
        pose_model(input_data)

    start_time.record()
    for _ in range(1000, 11000):
        pose_model(input_data)
    end_time.record()

    torch.cuda.synchronize()
    print(start_time.elapsed_time(end_time) / 10000)

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    args = parser.parse_args()

    compute_timing_data(args)