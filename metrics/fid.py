import random

from numpy.lib.type_check import iscomplexobj
from evaluate import Inference

import models.motion_vae as vae_models
import numpy as np
import torch
from classifier.classifier import Classifier
from dataProcessing.dataset_mocap_lobstr import DatasetMocapLoBSTr
from options.options import Options
from torch.utils.data import DataLoader
from utils import paramUtil
from utils.get_opt import get_opt
from scipy import linalg


def calc_fid_metrics(dataset, experiment, num_samples=10):
    device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")

    num_motions = 128
    inference = Inference(dataset, experiment, 45)

    dataset_params = paramUtil.get_dataset_params(inference.opt)

    # load classifier network
    nn_classifier = Classifier(dataset_params["input_size"], 128, 2, len(inference.dataset.label_names))
    nn_classifier.load_state_dict(torch.load("checkpoints/classifier/" + inference.opt.dataset_type + ".pth"))
    nn_classifier.to(device)
    nn_classifier.eval()

    activations = {}
    def get_activation(name):
        def hook(model, input, output):
            activations[name] = output.detach()
        return hook

    nn_classifier.linear1.register_forward_hook(get_activation("linear1"))

    output = {
        'fid': 0,
        'diversity': 0,
        'multimodality': None
    }

    # calculate FID
    with torch.no_grad():
        train_activations = np.zeros((num_samples, 30))
        gt_activations = np.zeros((num_samples, 30))

        samples = random.sample(range(len(inference.dataset)), num_samples)
        index = 0
        for i in samples:
            train_output = inference.evaluate_sequence(i)
            _ = nn_classifier(torch.unsqueeze(train_output['position'], dim=0))
            train_activations[index] = activations['linear1'].squeeze().detach().cpu().numpy()

            data = inference.dataset[i]
            data = torch.unsqueeze(torch.from_numpy(data["position"]).float(), dim=0)
            _ = nn_classifier(data.to(device))
            gt_activations[index] = activations['linear1'].squeeze().detach().cpu().numpy()
            index += 1
        
        train_mu, train_sigma = compute_stats(train_activations)
        gt_mu, gt_sigma = compute_stats(gt_activations)

        output['fid'] = compute_fid(train_mu, train_sigma, gt_mu, gt_sigma)

    # calculate diversity
    s_d = 200
    with torch.no_grad():
        activations_a = np.zeros((s_d, 30))
        activations_b = np.zeros((s_d, 30))

        index = 0
        samples = random.sample(range(len(inference.dataset)), s_d)
        for i in samples:
            train_output = inference.evaluate_sequence(i)
            _ = nn_classifier(torch.unsqueeze(train_output['position'], dim=0))
            activations_a[index] = activations['linear1'].squeeze().detach().cpu().numpy()
            index += 1

        index = 0
        samples = random.sample(range(len(inference.dataset)), s_d)
        for i in samples:
            train_output = inference.evaluate_sequence(i)
            _ = nn_classifier(torch.unsqueeze(train_output['position'], dim=0))
            activations_b[index] = activations['linear1'].squeeze().detach().cpu().numpy()
            index += 1

    output['diversity'] = compute_diversity(activations_a, activations_b)

    # calculate multimodality
    num_categories = 100
    num_samples = 20
    with torch.no_grad():
        cat_activations_a = []
        cat_activations_b = []
        for c in range(num_categories):
            cat_activations_a.append(np.zeros((num_samples, 30)))
            cat_activations_b.append(np.zeros((num_samples, 30)))

        index = 0
        samples = random.sample(range(len(inference.dataset)), s_d)
        for c in range(num_categories):
            for i in range(num_samples):
                train_output = inference.evaluate_sequence(i)
                _ = nn_classifier(torch.unsqueeze(train_output['position'], dim=0))
                cat_activations_a[c][i] = activations['linear1'].squeeze().detach().cpu().numpy()
                index += 1

        index = 0
        samples = random.sample(range(len(inference.dataset)), s_d)
        for c in range(num_categories):
            for i in range(num_samples):
                train_output = inference.evaluate_sequence(i)
                _ = nn_classifier(torch.unsqueeze(train_output['position'], dim=0))
                cat_activations_b[c][i] = activations['linear1'].squeeze().detach().cpu().numpy()
                index += 1

    output['multimodality'] = compute_multimodality(cat_activations_a, cat_activations_b)
    return output

def compute_stats(output):
    mu = np.mean(output, axis=0)
    sigma = np.cov(output, rowvar=False)

    return mu, sigma

def compute_fid(train_mu, train_sigma, gt_mu, gt_sigma):
    ssdiff = np.sum((train_mu - gt_mu)**2.0)
    covmean = linalg.sqrtm(train_sigma.dot(gt_sigma))
    if iscomplexobj(covmean):
        covmean = covmean.real

    fid = ssdiff + np.trace(train_sigma + gt_sigma - 2.0 *covmean)
    return fid

def compute_diversity(activations_a, activations_b):
    '''
    Compute diversity metric from Action2Motion

    Args:
        activations_a: [N, 30]
        activations_b: [N, 30]
    '''
    diff = activations_a - activations_b
    norm = np.sqrt(np.sum(diff**2, axis=1))
    norm = np.sum(norm)
    norm /= diff.shape[0]
    return norm

def compute_multimodality(cat_activations_a, cat_activations_b):
    '''
    Compute multimodality metric from Action2Motion

    Instead of actions, we use input as the category

    Args:
        cat_activations_a: list([N, 30])
        cat_activations_b: list([N, 30])
    '''
    num_categories = len(cat_activations_a)
    num_samples = cat_activations_a[0].shape[0]

    norm = np.zeros((num_categories, num_samples))
    for c in range(num_categories):
        diff = cat_activations_a[c] - cat_activations_b[c]
        norm_v = np.sqrt(np.sum(diff**2, axis=1))
        norm += np.sum(norm_v)

    norm /= (num_categories * num_samples)
    return norm