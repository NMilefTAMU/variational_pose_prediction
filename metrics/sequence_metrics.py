import warnings
from typing import Dict, List

import numpy as np
import torch

def compute_skating(pred: torch.Tensor, bone_map: Dict, H_value: float) -> float:
    '''
    Foot skating metric (from Zhang et al. 2018)

    Formula: s = v(2 - 2^clamp(h/H, 0, 1))
    v = velocity magnitude in horizontal plane
    h = current height
    H = maximum threshold (2.5 cm in paper)

    We use the thresholds computed by the data loader instead.

    Args:
        pred: [F, 57]
        gt: [F, 57]
        H: maximum threshold

    Returns:
        float: skating error
    '''
    
    s_l_sum = 0
    s_r_sum = 0
    motion_length = pred.shape[0]

    for i in range(0, motion_length):
        current_pose = pred[[i]]
        last_pose = pred[[i-1]]

        left_foot_index = bone_map["LeftToeBase"]
        right_foot_index = bone_map["RightToeBase"]

        c_left_foot = current_pose[:, left_foot_index*3:left_foot_index*3+3]
        c_right_foot = current_pose[:, right_foot_index*3:right_foot_index*3+3]
        l_left_foot = last_pose[:, left_foot_index*3:left_foot_index*3+3]
        l_right_foot = last_pose[:, right_foot_index*3:right_foot_index*3+3]

        v_left_foot = c_left_foot - l_left_foot
        v_right_foot = c_right_foot - l_right_foot

        v_left_foot_2d = v_left_foot[..., [0,2]]
        v_right_foot_2d = v_right_foot[..., [0,2]]

        m_v_left_foot = torch.norm(v_left_foot_2d, dim=-1, keepdim=True)
        m_v_right_foot = torch.norm(v_right_foot_2d, dim=-1, keepdim=True)

        h_left_foot = c_left_foot[..., [1]]
        h_right_foot = c_right_foot[..., [1]]

        exp_left_foot = torch.clamp(h_left_foot/ H_value, 0, 1)
        exp_right_foot = torch.clamp(h_right_foot / H_value, 0, 1)

        s_l = m_v_left_foot * (2 - torch.pow(2, exp_left_foot))
        s_r = m_v_right_foot * (2 - torch.pow(2, exp_right_foot))

        s_l_sum += s_l
        s_r_sum += s_r

    s_l_avg = float(torch.sum(s_l_sum).item()) / motion_length
    s_r_avg = float(torch.sum(s_r_sum).item()) / motion_length

    return s_l_avg + s_r_avg

def compute_spatial_error(pred: torch.Tensor, gt: torch.Tensor) -> float:
    '''
    Compute Euclidean distance as spatial error

    Args:
        pred: [N, 57]
        gt: [N, 57]

    Returns:
        torch.Tensor: mean error across all samples
    '''
    num_frames = pred.shape[0]
    pred_3d = torch.reshape(pred.clone().detach(), (num_frames, -1, 3)).cpu().double()
    gt_3d = torch.reshape(gt.clone().detach(), (num_frames, -1, 3)).cpu().double()

    error = torch.sqrt(torch.sum((pred_3d - gt_3d) ** 2, dim=-1))
    mean_error = torch.mean(error, dim=1).double()
    return float(torch.mean(mean_error))

def compute_velocity_error(pred: torch.Tensor, gt: torch.Tensor) -> float:
    '''
    Compute velocity error

    Args:
        pred: [N, 57]
        gt: [N, 57]

    Returns:
        torch.Tensor: mean error across all samples
    '''

    num_frames = pred.shape[0]
    pred_3d = torch.reshape(pred.clone().detach(), (num_frames, -1, 3)).cpu().double()
    gt_3d = torch.reshape(gt.clone().detach(), (num_frames, -1, 3)).cpu().double()

    # compute velocity
    pred_v = diff(pred_3d)
    gt_v = diff(gt_3d)

    error = torch.sqrt(torch.sum((pred_v - gt_v) ** 2, dim=-1))
    mean_error = torch.mean(error, dim=1).double()
    return float(torch.mean(mean_error))


def compute_acceleration_error(pred: torch.Tensor, gt: torch.Tensor) -> float:
    '''
    Compute acceleration error

    Args:
        pred: [N, 57]
        gt: [N, 57]

    Returns:
        torch.Tensor: mean error across all samples
    '''

    num_frames = pred.shape[0]
    pred_3d = torch.reshape(pred.clone().detach(), (num_frames, -1, 3)).cpu().double()
    gt_3d = torch.reshape(gt.clone().detach(), (num_frames, -1, 3)).cpu().double()

    # compute acceleration
    pred_v = diff(pred_3d)
    gt_v = diff(gt_3d)

    pred_a = diff(pred_v)
    gt_a = diff(gt_v)

    error = torch.sqrt(torch.sum((pred_a - gt_a) ** 2, dim=-1))
    mean_error = torch.mean(error, dim=1).double()
    return float(torch.mean(mean_error))

def compute_contact_accuracy(pred: torch.Tensor, gt: torch.Tensor) -> float:
    '''
    Compute contact accuracy

    Args:
        pred: [N, 2]
        gt: [N, 2]

    Returns:
        torch.Tensor: mean error across all samples
    '''
    pred_c = torch.round(pred.clone().detach().cpu().double())
    gt_c = torch.round(gt.clone().detach().cpu().double())

    contact_error = torch.abs(pred_c - gt_c)

    return float(1.0 - torch.mean(contact_error))

def compute_stiffness(pred: torch.Tensor, kinematic_chain: List, degrees: bool=False) -> float:
    '''
    Compute stiffness of joints. This is based on MANN but extended over entire body.

    Args:
        pred: [N, 57]
        kinematic_chain: List
        degrees: True if output is degrees, otherwise in radians

    Returns:
        torch.Tensor: mean error across all samples
    '''
    num_frames = pred.shape[0]
    pred_3d = torch.reshape(pred.clone().detach(), (num_frames, -1, 3)).cpu().double()

    pred_ja = extract_joint_angles(pred_3d, kinematic_chain)

    if degrees:
        pred_ja = rad2deg(pred_ja)

    pred_ja_diff = diff(pred_ja)

    mean_error = torch.mean(pred_ja_diff, dim=1).double()
    return float(torch.mean(mean_error))

def compute_npss(pred: torch.Tensor, gt: torch.Tensor) -> float:
    # TODO
    pass

########## UTILS ##########
def diff(x):
    '''
    useful for temporal differencing (similar to np.diff)
    '''
    return x[1:] - x[:-1]

def rad2deg(x):
    constant = 180 / np.pi
    return constant * x

def extract_joint_angles(x, kinematic_chain):
    '''
    Extracts joint angle by calculating dot product between two vectors
    '''
    num_angles = 0
    for chain in kinematic_chain:
        num_angles += max(len(chain)-2, 0)

    output = torch.zeros((x.shape[0], num_angles)).double()

    angle_index = 0
    for chain in kinematic_chain:
        for i in range(1, len(chain)-1):
            p_i = chain[i-1] # parent index
            j_i = chain[i] # joint index
            c_i = chain[i+1] # child index

            p_pos = x[:, p_i]
            j_pos = x[:, j_i]
            c_pos = x[:, c_i]

            b0 = normalize_vector(p_pos - j_pos)
            b1 = normalize_vector(c_pos - j_pos)

            dot = torch.sum(b0 * b1, dim=-1, keepdim=True)
            angle = torch.acos(dot)
            output[:, [angle_index]] = angle

            angle_index += 1
        
    return output

def normalize_vector(x):
    '''
    Normalize vector
    '''
    x_l = torch.norm(x, dim=-1, keepdim=True)
    return x / x_l
