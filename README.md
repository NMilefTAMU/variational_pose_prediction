# Variational pose prediction with dynamic sample selection from sparse tracking signals
This is the official implementation of the Eurographics 2023 paper. See the [project webpage](http://www.nicholasmilef.com/projects/variational_pose_prediction) for more information.

![image](image.png)

## Instructions

### Setup
Run the following command to create an Anaconda environment:
```
conda env create -f environment.yaml
```

Then activate the environment:
```
conda activate variational_pose_predict
```

Install the PyMo package:
```
cd PyMo
pip install .
```

## Dataset
Download the dataset from [here](http://theorangeduck.com/media/uploads/other_stuff/pfnn.zip). Create a folder structure like the following:
```
dataset
..mocap_ours
....input
......train
........18_01.bvh
........18_02.bvh
........[more files]
......valid
......test
....output
```

The input directory will only include .bvh files while the output directory will include .npy files. To view the train, valid, and test splits, look at these files named `train_list.txt`, `valid_list.txt`, etc. The test split used for the metrics in the paper is under `subsets/paper.csv`. We removed some animations that had significant artifacts in the mocap recording.

To preprocess the dataset, run this command:
```
python dataset/preprocess.py --dataset_type mocap_ours --mode valid
```

### Batch viewer
This viewer runs through inference code before displaying so that playback speed can be controlled easily:
```
python viewer.py --experiment test_model_6d_8_25b --dataset mocap_ours --dataset_type test --methods ours gt --lock_eps --smart_eps 100 --set_seed 0 --camera_config viewer/config/ours1.yaml --sequence_names locomotion_valid_001 --use_eps_net
```

See [`docs/viewer.md`](docs/viewer.md) for more examples.

### VR viewer
Playing back a recorded animation:
```
python viewer.py --experiment test_model_6d_8_25b --methods gt ours --dataset mocap_ours --dataset_type test --sequence_name 18_01 --vr --recorded_tp ./recorded_tp/nick_rec_room/leap.npy --calibration_tp ./recorded_tp/nick_rec_room/calibrate.npy --camera_config viewer/config/ours_vr.yaml
```

The sequence name is the just a reference mocap clip that provides normalization information. Ideally, you would pick a T-pose animation to align with the calibration clip.


### Train motion synthesis model
This trains the model presented in the paper
```
python train_motion_vae.py --name test_model --dataset_type mocap_ours --model_type vae_6d --lr 0.0002 --input_types tracking4 vtracking4 --loss_functions mse kld contact --normalize_inputs True --batch_size 128 --motion_length 60 --iters 500000 --lambda_kld 5.0 --balance uniform --learning_mode vae_fp --decoder_hidden_layers 1 --normalize_mode yaw
```

### Train root prediction model
This was not included in the paper, but this was research developed that allowed for 3-point tracking. Note that this code may work properly currently.
```
python train_occlusion.py --iters 100000
```

## Used Projects
This project used Action2Motion as a base:
https://github.com/EricGuo5513/action-to-motion

We also include the source files for the following projects:
https://github.com/papagina/RotationContinuity
https://github.com/omimo/PyMO

## Citation
If you found repository or paper helpful, please consider citing:
```
@inproceedings{milef2023variational,
    title={Variational Pose Prediction with Dynamic Sample Selection from Sparse Tracking Signals},
    author={Milef, Nicholas and Sueda, Shinjiro and Kalantari, Nima Khademi},
    booktitle={Computer Graphics Forum},
    volume={42},
    issue={2},
    year={2023}
}
```

## Questions?
If you have any questions about the paper or code, please contact Nick Milef (first author) via email.

