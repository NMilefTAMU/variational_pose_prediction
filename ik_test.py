import argparse
from math import floor

import numpy as np
import torch
from direct.showbase.ShowBase import ShowBase
from panda3d.core import LineSegs, NodePath
from direct.task import Task

from scipy.spatial.transform import Rotation
from viewer.postprocess import solve_ik, look_at
from viewer.scene import ViewerScene


class Joint:
    def __init__(self, base, pos, i, color):
        self.pos = pos
        self.model = base.loader.load_model('assets/models/cube.obj')
        self.model.setScale(0.1)
        cf = (3 - i) / 3
        self.model.setColor(cf * color[0], cf * color[1], cf * color[2])
        self.model.reparent_to(base.render)

        self.axes = [
            np.array([1, 0, 0]),
            np.array([0, 1, 0]),
            np.array([0, 0, 1]),
        ]
        self.axes_lines = [None]*3
        self.axes_lines_node_path = [None]*3

        colors = [
            [1, 0, 0],
            [0, 1, 0],
            [0, 0, 1],
        ]

        for i in range(3):
            self.axes_lines[i] = base.loader.load_model('assets/models/cylinder.obj')
            self.axes_lines[i].setColor(colors[i][0], colors[i][1], colors[i][2])
            self.axes_lines[i].setScale(0.3, 0.3, 0.3)

    def update(self, base, pos, rot):
        self.pos = pos
        self.model.setPos(self.pos[0], self.pos[1], self.pos[2])
        self.update_axes(base, rot)

    def update_axes(self, base, rot):
        for i in range(3):
            vector = Rotation.apply(rot, self.axes[i])

            offset_pos = [self.pos[0] + vector[0], self.pos[1] + vector[1], self.pos[2] + vector[2]]
            self.axes_lines[i].setPos(self.pos[0], self.pos[1], self.pos[2])
            self.axes_lines[i].lookAt(offset_pos[0], offset_pos[1], offset_pos[2])
            self.axes_lines[i].reparentTo(base.render)

class IKApp(ShowBase):
    def __init__(self, args):
        ShowBase.__init__(self)

        self.device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
        torch.cuda.set_device(0)

        self.scene = ViewerScene(self, floor_offset=0.5)
        self.monkey = self.loader.load_model('assets/models/monkey.obj')

        self.joint_pos = []
        self.joint_pos.append(np.array([0,-2,0]))
        self.joint_pos.append(np.array([0.2,-1,0]))
        self.joint_pos.append(np.array([1.5,0,0]))

        self.num_joints = len(self.joint_pos)

        self.joints_ik = [None] * 3
        for i in range(self.num_joints):
            self.joints_ik[i] = Joint(self, self.joint_pos[i], i, [0, 0, 1])

        self.target_pos = np.array([1.0, 0, 2.0])
        self.target = self.loader.load_model('assets/models/cube.obj')
        self.target.setScale(0.1)
        self.target.setPos(self.target_pos[0], self.target_pos[1], self.target_pos[2])
        self.target.reparent_to(self.render)
        self.target.setColor(1, 0, 0)

        self.update_ik()

        self.accept('arrow_up', self.move_up)
        self.accept('arrow_down', self.move_down)
        self.accept('arrow_left', self.move_left)
        self.accept('arrow_right', self.move_right)
        self.task_mgr.add(self.update, "Update Task")

    def move_right(self):
        self.update_target(np.array([0.1, 0, 0]))

    def move_left(self):
        self.update_target(np.array([-0.1, 0, 0]))

    def move_up(self):
        self.update_target(np.array([0, 0, 0.1]))

    def move_down(self):
        self.update_target(np.array([0, 0, -0.1]))

    def update_target(self, offset):
        pos = self.target.getPos()
        pos[0] += offset[0]
        pos[1] += offset[1]
        pos[2] += offset[2]
        self.target_pos[0] = pos[0]
        self.target_pos[1] = pos[1]
        self.target_pos[2] = pos[2]
        self.target.setPos(pos)
        self.update_ik()

    def update_ik(self):
        self.joint_pos, joint_rot = solve_ik(
            self.joint_pos,
            self.target_pos)

        for i in range(self.num_joints):
            self.joints_ik[i].update(self, self.joint_pos[i], joint_rot[i])

    def update(self, task):

        return Task.cont

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    args = parser.parse_args()

    app = IKApp(args)
    app.run()
