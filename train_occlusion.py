import argparse
import math
import os
import time

import numpy as np
import torch
import torch.nn as nn
import torch.optim as optim
from scipy.spatial.transform import Rotation
from torch.utils.data import DataLoader
from torch.utils.tensorboard import SummaryWriter

from dataProcessing.dataset_mocap_ours import DatasetMocapOurs
from models.root_pred.occlusion_network import OcclusionNet
from models.root_pred.occlusion_network_vae import OcclusionNetDecoder, OcclusionNetEncoder, OcclusionNetVAE
from options.options import Options
from utils import paramUtil


def kl_criterion(batch_size, mu1, logvar1, mu2, logvar2):
    # KL( N(mu1, sigma2_1) || N(mu_2, sigma2_2))
    # loss = log(sigma2/sigma1) / 2 + (sigma1 + (mu1 - mu2)^2)/(2*sigma2) - 1/2
    sigma1 = logvar1.mul(0.5).exp()
    sigma2 = logvar2.mul(0.5).exp()
    kld = torch.log(sigma2/sigma1) + (torch.exp(logvar1) + (mu1-mu2)**2)/(2*torch.exp(logvar2)) - 1/2
    return kld.sum() / batch_size


def augment_data(output):
    yaw_offset = np.random.rand() * (2 * math.pi) * 0
    xz_offset_scale = 1.0
    xz_offset = ((np.random.rand(2) * 2) - 1) * xz_offset_scale

    # extract and augment root rotation
    root_rot = output["root_rot"]
    root_rot = Rotation.from_quat(root_rot)
    up_vector = np.array([0, 1, 0])
    yaw_rot = Rotation.from_rotvec(up_vector * yaw_offset)
    root_rot = yaw_rot * root_rot

    # extract and augment root position
    root_pos = output["root_pos"]
    root_pos[:, 0] += xz_offset[0]
    root_pos[:, 2] += xz_offset[1]
    root_pos = yaw_rot.apply(root_pos)

    root_pos_raw = output["root_pos_raw"]
    root_pos_raw[:, 0] += xz_offset[0]
    root_pos_raw[:, 2] += xz_offset[1]
    root_pos_raw = yaw_rot.apply(root_pos_raw)

    # raw tracker positions and velocities
    raw_tracker_pos = output["raw_tracker_pos"]
    raw_tracker_pos[:, 0::3] += xz_offset[0]
    raw_tracker_pos[:, 2::3] += xz_offset[1]

    raw_tracker_vel = output["raw_tracker_vel"]

    for i in range(4):
        raw_tracker_pos[:, i*3:i*3+3] = yaw_rot.apply(raw_tracker_pos[:, i*3:i*3+3])
        raw_tracker_vel[:, i*3:i*3+3] = yaw_rot.apply(raw_tracker_vel[:, i*3:i*3+3])

    output["raw_tracker_pos"] = raw_tracker_pos
    output["raw_tracker_vel"] = raw_tracker_vel
    output["root_pos"] = root_pos
    output["root_pos_raw"] = root_pos_raw
    output["root_rot"] = root_rot.as_quat()
    output["root_rot_mat"] = root_rot.as_matrix()

    return output

def train_waist_occlusion(args):
    num_iterations = args.iters

    writer = SummaryWriter("./runs_waist_occlusion/" + args.name)
    device = torch.device("cuda:" + str(args.gpu_id) if torch.cuda.is_available() else "cpu")

    opt = Options(args, use_profile=True)

    dataset_params      = paramUtil.get_dataset_params(opt)
    dataset_path        = dataset_params['dataset_path']

    if args.augment_data:
        augment_data_func = augment_data
    else:
        augment_data_func = None

    train_dataset = DatasetMocapOurs(dataset_path, opt, "train", augment_data_func=augment_data_func)
    train_dataloader = DataLoader(train_dataset, int(args.batch_size), shuffle=True, drop_last=True)
    valid_dataset = DatasetMocapOurs(dataset_path, opt, "valid", augment_data_func=augment_data_func)
    valid_dataloader = DataLoader(valid_dataset, int(args.batch_size), shuffle=True, drop_last=True)

    input_size = (3*6)
    output_size = 3 + 6 # 3 for position and 6 for rotation
    hidden_size = 128
    n_layers = 1
    z_size = 8

    model = OcclusionNetVAE(
        input_size=input_size,
        z_size=z_size,
        output_size=output_size,
        hidden_size=hidden_size,
        n_layers=n_layers,
        batch_size=args.batch_size,
        learning_mode=args.model_type_root,
        lock_eps=False,
        device=device)

    model.to(device)

    mse_loss = nn.MSELoss()
    kld_loss = kl_criterion

    optimizer = optim.Adam(model.parameters(), lr=0.0002, betas=(0.9, 0.999))

    # train
    iterations = 1
    while iterations < num_iterations + 1:
        # training loss
        sample = next(iter(train_dataloader))

        optimizer.zero_grad()

        model.init_hidden(args.batch_size)
        model.select_new_eps(args.batch_size)

        output_vector = None
        gt_vector = None

        for i in range(0, opt.motion_length - 1):
            raw_pos = sample["raw_tracker_pos"][:, i, 3:]
            raw_vel = sample["raw_tracker_vel"][:, i, 3:]
            input_data = torch.cat([raw_pos, raw_vel], axis=-1).float().to(device)

            # run networks
            output, mu, logvar = model(input_data)

            output_vector = torch.cat((output["root_pos"], output["root_rot_mat"].reshape(-1, 9)), dim=-1).float().to(device)
            gt_vector = torch.cat((sample["root_pos_raw"][:, i], sample["root_rot_mat"][:, i].reshape(-1, 9)), dim=-1).float().to(device)

        # losses
        train_loss = {}
        train_loss["mse"] = mse_loss(output_vector, gt_vector)
        if args.model_type_root == "vae":
            mu_p = torch.zeros_like(logvar)
            logvar_p = torch.log(torch.ones_like(logvar) ** 2)
            train_loss["kld"] = 0.001 * kld_loss(args.batch_size, mu, logvar, mu_p, logvar_p)

        # total loss
        keys = train_loss.keys()
        train_loss["loss"] = 0
        for key in keys:
            train_loss["loss"] += train_loss[key]

        train_loss["loss"].backward()
        optimizer.step()

        # validation loss
        if iterations % 100 == 0:
            val_sample = next(iter(valid_dataloader))

            optimizer.zero_grad()

            model.init_hidden(args.batch_size)
            model.select_new_eps(args.batch_size)

            output_vector = None
            gt_vector = None
            with torch.no_grad():
                for i in range(0, opt.motion_length - 1):
                    raw_pos = val_sample["raw_tracker_pos"][:, i, 3:]
                    raw_vel = val_sample["raw_tracker_vel"][:, i, 3:]
                    input_data = torch.cat([raw_pos, raw_vel], axis=-1).float().to(device)
                    
                    # run networks
                    output, mu, logvar = model(input_data)

                    output_vector = torch.cat((output["root_pos"], output["root_rot_mat"].reshape(-1, 9)), dim=-1).float().to(device)
                    gt_vector = torch.cat((val_sample["root_pos_raw"][:, i], val_sample["root_rot_mat"][:, i].reshape(-1, 9)), dim=-1).float().to(device)

                # losses
                valid_loss = {}
                valid_loss["mse"] = mse_loss(output_vector, gt_vector)
                if args.model_type_root == "vae":
                    mu_p = torch.zeros_like(logvar)
                    logvar_p = torch.log(torch.ones_like(logvar) ** 2)
                    valid_loss["kld"] = 1.0 * kld_loss(args.batch_size, mu, logvar, mu_p, logvar_p)

                # total loss
                keys = valid_loss.keys()
                valid_loss["loss"] = 0
                for key in keys:
                    valid_loss["loss"] += valid_loss[key]

                print("Step:",
                      str(iterations),
                      "\tTrain Loss:",
                      float(train_loss["loss"].item()),
                      "\tValid Loss:",
                      float(valid_loss["loss"].item()))

                train_loss_data = {}
                valid_loss_data = {}
                for key, _ in train_loss.items():
                    if key != "loss":
                        train_loss_data[key] = train_loss[key]
                        valid_loss_data[key] = valid_loss[key]

                writer.add_scalars("train_loss", train_loss_data, iterations)
                writer.add_scalars("valid_loss", valid_loss_data, iterations)

        iterations += 1

        if iterations % args.save_every == 0:
            dir_name = "./checkpoints/waist_occlusion/"
            os.makedirs(dir_name, exist_ok=True)
            torch.save(model.state_dict(), os.path.join(dir_name, args.name + ".pth"))


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--augment_data", action="store_true", help="augment data")
    parser.add_argument("--batch_size", type=int, default=128)
    parser.add_argument("--dataset_type", default="mocap_ours")
    parser.add_argument("--gpu_id", type=int, default=0)
    parser.add_argument("--iters", type=int, default=1000000)
    parser.add_argument("--model_type", default="vae_6d")
    parser.add_argument("--motion_length", type=int, default=60)
    parser.add_argument("--name", type=str, help="name of checkpoint")
    parser.add_argument("--model_type_root", default="ae", help="[ae, vae]")
    parser.add_argument("--save_every", type=int, default=100)
    parser.add_argument("--sequence_names", nargs="+")
    args = parser.parse_args()

    train_waist_occlusion(args)
