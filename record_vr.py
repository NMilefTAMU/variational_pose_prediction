import argparse
import os
import time

import numpy as np
import openvr

from utils.vr.utils import data_to_np, get_next_vr_frame


class RecordVR:
    def __init__(self, args):
        # extract arguments
        self.save_interval = args.save_interval
        self.startup_delay = args.startup_delay
        self.framerate = args.framerate

        self.last_time = 0
        self.last_save_time = self.startup_delay
        self.num_tp = 4
        self.frame = 0

        self.system = openvr.init(openvr.VRApplication_Scene)
        self.poses = []

        self.dir_name = str(int(time.time()*1000))
        os.makedirs(os.path.join("./recorded_tp", self.dir_name), exist_ok=True)
        self.recorded_filename = os.path.join("./recorded_tp", self.dir_name, str(int(time.time()*1000)) + ".npy")
        self.recorded_data = []

    def start(self):
        while True:
            current_time = time.time() * 1000
            dT = 1000 / self.framerate

            if self.last_time == 0:
                self.last_time = current_time

            if current_time - self.last_time >= dT:
                data, self.poses = get_next_vr_frame(self.system, self.poses)
            
                if self.frame == self.last_save_time + self.save_interval:
                    np.save(self.recorded_filename, np.asarray(self.recorded_data))
                    print("Saved", self.recorded_filename)
                    self.recorded_filename = os.path.join("./recorded_tp", self.dir_name, str(int(time.time()*1000)) + ".npy")
                    self.recorded_data.clear()
                    self.last_save_time = self.last_save_time + self.save_interval
                if self.frame >= self.last_save_time and self.frame < self.last_save_time + self.save_interval:
                    self.recorded_data.append(data_to_np(data, self.num_tp))

                self.last_time = current_time
                self.frame += 1


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('--framerate', type=int, help="framerate to record", default=60)
    parser.add_argument("--startup_delay", type=int, help="startup delay by number of frames", default=400)
    parser.add_argument("--save_interval", type=int, help="number of frames to record before saving off the file", default=1000)
    args = parser.parse_args()

    record_vr = RecordVR(args)
    record_vr.start()

