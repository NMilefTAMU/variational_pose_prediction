import os
from direct.gui.OnscreenText import OnscreenText
from panda3d.core import CardMaker, LVector3, LVector4, TransparencyAttrib, TextNode

def normalize(x):
    return (x * 2) - 1

def get_coord(x, y):
    return LVector3(normalize(x), 1, normalize(y))

def set_coords(nodepath, bounds):
    lx = bounds[0]
    ly = bounds[1]
    ux = bounds[2]
    uy = bounds[3]
    center = [(ux + lx) / 2, (uy + ly) / 2]
    nodepath.setPos(get_coord(center[0], center[1]))
    nodepath.setScale(LVector3(ux - lx, 1, uy - ly))

class GraphDisplay:
    def __init__(self, base):
        cm = CardMaker("card")
        cm.setFrameFullscreenQuad()
        self.background = base.render2d.attachNewNode(cm.generate())
        self.background.setTransparency(TransparencyAttrib.M_alpha)
        self.background.setColor(LVector4(0, 0, 0, 0.2))

class GateDisplay(GraphDisplay):
    def __init__(self, base):
        super().__init__(base)

        self.bounds = [0.75, 0.75, 0.95, 0.95]

        set_coords(self.background, self.bounds)

        self.num_weights = 4
        self.weights = [None] * self.num_weights
        for i in range(self.num_weights):
            cm = CardMaker("card")
            cm.setFrameFullscreenQuad()
            self.weights[i] = base.render2d.attachNewNode(cm.generate())

    def render(self, gate_weights):
        eps_min = 0.001
        width = (self.bounds[2] - self.bounds[0]) / self.num_weights
        height = self.bounds[3] - self.bounds[1]

        for i in range(self.num_weights):
            bounds = [
                width*i + self.bounds[0],
                self.bounds[1],
                width*(i+1) + self.bounds[0],
                (max(height * gate_weights[i], eps_min) + self.bounds[1])]

            set_coords(self.weights[i], bounds)
            self.weights[i].setColor(LVector4(0, 0, 1, 1.0))

class EPSDisplay(GraphDisplay):
    def __init__(self, base, num_eps):
        super().__init__(base)

        self.bounds = [0.75, 0.45, 0.95, 0.63]

        '''
        self.horizontal_legend = OnscreenText(
            text="eps ID",
            parent=self.background,
            pos=(0.0, -1.30),
            scale=(0.2, 0.4),
            fg=(0, 0, 0, 1),
            bg=(0, 0, 0, 0),
            align=TextNode.ACenter)

        self.vertical_legend = OnscreenText(
            text="a",
            parent=self.background,
            pos=(-1.1, -0.1),
            scale=(0.2, 0.4),
            fg=(0, 0, 0, 1),
            bg=(0, 0, 0, 0),
            align=TextNode.ACenter)

        self.vertical_legend_top = OnscreenText(
            text="1.0-",
            parent=self.background,
            pos=(-1.14, 0.91),
            scale=(0.2, 0.4),
            fg=(0, 0, 0, 1),
            bg=(0, 0, 0, 0),
            align=TextNode.ACenter)

        self.vertical_legend_bottom= OnscreenText(
            text="0.0-",
            parent=self.background,
            pos=(-1.18, -1.07),
            scale=(0.2, 0.4),
            fg=(0, 0, 0, 1),
            bg=(0, 0, 0, 0),
            align=TextNode.ACenter)
        '''

        set_coords(self.background, self.bounds)

        self.num_eps = num_eps
        self.weights = [None] * self.num_eps
        for i in range(self.num_eps):
            self.cm = CardMaker("card")
            self.cm.setFrameFullscreenQuad()
            self.weights[i] = base.render2d.attachNewNode(self.cm.generate())

    def render(self, eps_weights):
        eps_min = 0.001
        width = (self.bounds[2] - self.bounds[0]) / self.num_eps
        height = self.bounds[3] - self.bounds[1]

        for i in range(self.num_eps):
            bounds = [
                width*i + self.bounds[0],
                self.bounds[1],
                width*(i+1) + self.bounds[0],
                (max(height * eps_weights[i], eps_min) + self.bounds[1])]

            set_coords(self.weights[i], bounds)
            self.weights[i].setColor(LVector4(0, 0, 1, 1.0))

class EPSRatingsDisplay(GraphDisplay):
    def __init__(self, base, num_eps):
        super().__init__(base)

        self.bounds = [0.75, 0.35, 0.95, 0.51]

        set_coords(self.background, self.bounds)

        self.num_eps = num_eps
        self.weights = [None] * self.num_eps
        for i in range(self.num_eps):
            self.cm = CardMaker("card")
            self.cm.setFrameFullscreenQuad()
            self.weights[i] = base.render2d.attachNewNode(self.cm.generate())

    def render(self, eps_ratings):
        eps_min = 0.001
        width = (self.bounds[2] - self.bounds[0]) / self.num_eps
        height = self.bounds[3] - self.bounds[1]

        for i in range(self.num_eps):
            bounds = [
                width*i + self.bounds[0],
                self.bounds[1],
                width*(i+1) + self.bounds[0],
                (max(height * eps_ratings[i], eps_min) + self.bounds[1])]

            set_coords(self.weights[i], bounds)
            self.weights[i].setColor(LVector4(0, 0, 1, 1.0))