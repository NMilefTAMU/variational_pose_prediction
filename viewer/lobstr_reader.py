import csv
import math
import os

import numpy as np
import torch

from dataProcessing.dataset_mocap import DatasetMocap
from viewer.postprocess import ikSolver


def read_lobstr_results(device, name, index, inference, root_pos, ik_mode=True, ik_breakage=False, lobstr_dataset="lobstr", pad_beginning=False):
    directory = "lobstr_results/Animations/" + lobstr_dataset + "/"
    files = os.listdir(directory)
    files = [x for x in files if x.endswith("_output_results.csv")]
    anim_names = [x.split("_LoBSTr_output_results.csv")[0] for x in files]

    leg_k_chain = [
        ["Hips", "LeftUpLeg", "LeftLeg", "LeftFoot", "LeftToeBase"],
        ["Hips", "RightUpLeg", "RightLeg", "RightFoot", "RightToeBase"]]
    k_chain = [
        ["Hips", "LeftUpLeg", "LeftLeg", "LeftFoot", "LeftToeBase"],
        ["Hips", "RightUpLeg", "RightLeg", "RightFoot", "RightToeBase"],
        ["Hips", "Spine", "Spine1", "Neck1", "Head"],
        ["Spine1", "LeftArm", "LeftForeArm", "LeftHand"],
        ["Spine1", "RightArm", "RightForeArm", "RightHand"]]

    for i in range(len(files)):
        if name == anim_names[i]:
            data, bone_map, contact = convert_csv_to_npy(os.path.join(directory, files[i]))
            ik_solver = ikSolver(device, 1, bone_map, ik_breakage=False)
            data = torch.from_numpy(data)
            data = correct_leg_bone_lengths(directory, name, data, bone_map, leg_k_chain)
            data = scale_bone_lengths(data, bone_map, k_chain, inference, index)
            contact = torch.from_numpy(contact)

            if pad_beginning:
                data_padding = torch.zeros((59, 57))
                data = torch.cat((data_padding, data), dim=0)
                contact_padding = torch.zeros((59, 2))
                contact = torch.cat((contact_padding, contact), dim=0)

            frames = torch.zeros_like(data)
            for f in range(data.shape[0]-1):
                input_data = {}

                offset_pos = -data[f:f+1, 0:3] + root_pos[f:f+1].detach().cpu()

                input_data["position"] = data[f:f+1]
                input_data["position"][:, 0::3] = data[f:f+1, 0::3] + offset_pos[:, 0:1]
                input_data["position"][:, 1::3] = data[f:f+1, 1::3] + offset_pos[:, 1:2]
                input_data["position"][:, 2::3] = data[f:f+1, 2::3] + offset_pos[:, 2:3]
                input_data["contact"] = contact[f:f+1]
                frames[f:f+1] = input_data["position"]

                if ik_mode:
                    ik_output = ik_solver.postprocess_ik(input_data)
                    frames[f:f+1] = ik_output["position"]

            output = {}

            output["position"] = frames
            output["contact"] = contact
            output["bone_map"] = bone_map
            return output

    raise ValueError("Cannot find LoBSTr file: " + name)

def distance(parent, child):
    dx = parent[:, 0] - child[:, 0]
    dy = parent[:, 1] - child[:, 1]
    dz = parent[:, 2] - child[:, 2]
    dist = torch.sqrt((dx*dx) + (dy*dy) + (dz*dz))
    return dist

def scale_bone_lengths(data, bone_map, k_chain, inference, index):
    for chain in k_chain:
        offsets = []
        dists = []
        lengths = []

        for j in range(1, len(chain)):
            c = bone_map[chain[j]]
            p = bone_map[chain[j-1]]
            child = data[:, c*3:c*3+3]
            parent = data[:, p*3:p*3+3]

            dists.append(distance(parent, child).unsqueeze(dim=1))
            offsets.append(child - parent)
            lengths.append(inference.get_bone_length(index, chain[j-1], chain[j]))

        offset_index = 0
        for j in range(1, len(chain)):
            c = bone_map[chain[j]]
            p = bone_map[chain[j-1]]
            child = data[:, c*3:c*3+3]
            parent = data[:, p*3:p*3+3]

            data[:, c*3:c*3+3] = (offsets[offset_index] / dists[offset_index]) * lengths[offset_index] + parent
            offset_index += 1

    return data

def correct_leg_bone_lengths(directory, name, data, bone_map, leg_k_chain):
    bone_lengths = extract_bone_lengths(directory, name, data)

    for leg in leg_k_chain:
        offsets = []
        dists = []
        c_dists = []
        for j in range(1, len(leg)):
            c = bone_map[leg[j]]
            p = bone_map[leg[j-1]]
            child = data[:, c*3:c*3+3]
            parent = data[:, p*3:p*3+3]

            dists.append(distance(parent, child).unsqueeze(dim=1))
            c_dists.append(bone_lengths[leg[j]])
            offsets.append(child - parent)

        offset_index = 0
        for j in range(1, len(leg)):
            c = bone_map[leg[j]]
            p = bone_map[leg[j-1]]
            child = data[:, c*3:c*3+3]
            parent = data[:, p*3:p*3+3]

            data[:, c*3:c*3+3] = (offsets[offset_index] / dists[offset_index]) * c_dists[offset_index] + parent
            offset_index += 1

    return data

def extract_bone_lengths(directory, name, data):
    bone_lengths = {}
    bone_map = DatasetMocap.get_bone_map()
    filename = os.path.join(directory, name + "_bl.csv")
    with open(filename) as f:
        reader = csv.reader(f, delimiter=",", quotechar="|")
        for row in reader:
            bone_lengths[row[0]] = float(row[1])
    return bone_lengths

def convert_csv_to_npy(csv_file):
    with open(csv_file) as f:
        reader = csv.reader(f, delimiter=',', quotechar='|')
        data = []
        contact = []
        header = None
        row_index = 0

        bone_map = {}
        for row in reader:
            if row_index == 0:
                header = row
                row_index += 1

                # swap right with left
                for i in range(len(header)):
                    if header[i] == "LeftHand" or header[i] == "RightHand":
                        continue
                    if "Left" in header[i]:
                        header[i] = header[i].replace("Left", "Right")
                    elif "Right" in header[i]:
                        header[i] = header[i].replace("Right", "Left")

                continue

            data_row = []
            contact_row = [0, 0]
            row_crop = row[:-1] # cuts off empty character
            data_index = 0
            for i in range(0, len(row_crop)):
                if header[i] in ["LeftShoulder", "RightShoulder", "LHipJoint", "RHipJoint"]:
                    continue
                elif header[i] == "LContact":
                    contact_row[0] = float(row_crop[i])
                    continue
                elif header[i] == "RContact":
                    contact_row[1] = float(row_crop[i])
                    continue

                data_row.append(float(row_crop[i]))

                if header[i] not in bone_map:
                    bone_map[header[i]] = data_index // 3

                data_index += 1

            for i in range(0, len(data_row), 3):
                data_row[i] *= -1
                data_row[i+1] -= 2

            data.append(data_row)
            contact.append(contact_row)

            row_index += 1


    row_data = np.asarray(data)
    row_contact = np.asarray(contact)

    return row_data, bone_map, row_contact

def convert_lobstr_skeleton_to_mocap_ours(input_tensor: torch.Tensor) -> torch.Tensor:
    """
    LoBSTr output uses a different organization of joints, which makes it difficult to compare for metrics.
    This doesn't matter for the viewer, however.

    Args:
        input_tensor: [N, 57]

    Returns:
        output: [N, 57]
    """
    output = torch.zeros_like(input_tensor)

    joint_map = {
        0: 0,
        1: 10,
        2: 11,
        3: 14,
        4: 15,
        5: 3,
        6: 12,
        7: 13,
        8: 2,
        9: 16,
        10: 1,
        11: 7,
        12: 8,
        13: 9,
        14: 17,
        15: 4,
        16: 5,
        17: 6,
        18: 18,
    }

    # root joint
    for src_joint, dst_joint in joint_map.items():
        output[:, src_joint*3:src_joint*3+3] = input_tensor[:, dst_joint*3:dst_joint*3+3]

    return output