import math

from viewer.skeleton import Skeleton
from viewer.utils import convert_3d_to_2d


class SkeletonLoBSTr(Skeleton):
    def __init__(self, base, opt, num_joints, color=(0, 1.0, 0), offset=(0, 0, 0), k_chain=None):
        super(SkeletonLoBSTr, self).__init__(base, opt, num_joints, color, offset, k_chain)

    def render_frame(self, frame_data_gt, frame_data_lobstr):
        frame_data = frame_data_lobstr.cpu().detach().numpy().squeeze()
        index = 0

        for joint in self.joints:
            pos = frame_data[index:index+3].copy()
            joint.setPos(
                pos[0] + self.offset[0],# + offset_pos[0],
                -pos[1] + self.offset[1],# - offset_pos[1],
                -pos[2] + self.offset[2])# + offset_pos[2])
            self.text[index // 3].setText(str(index // 3))
            text_pos = convert_3d_to_2d(joint, self.base)
            self.text[index // 3].setPos(text_pos[0], text_pos[1])
            index += 3

        index = 0

        for k, v in self.k_chain.items():
            child_pos = self.joints[k].getPos()
            parent_pos = self.joints[v].getPos()
            length = math.sqrt(
                (child_pos[0]-parent_pos[0])**2 + (child_pos[1]-parent_pos[1])**2 + (child_pos[2]-parent_pos[2])**2)
            length = max(1e-6, length)
            #self.bones[index].setScale(0.1, 1.0 * length, 0.1)
            self.bones[index].setScale(length, length, length)
            self.bones[index].setPos(parent_pos)

            if self.bones[index].getPos()[0] == child_pos[0] and \
                self.bones[index].getPos()[1] == child_pos[1] and \
                self.bones[index].getPos()[2] == child_pos[2]:
                pass
            else:
                self.bones[index].lookAt(child_pos)

            self.update_bl_text(index, length, self.joints[k], self.joints[v])

            index += 1