import math
from typing import Dict

import numpy as np
import torch
from scipy.spatial.transform import Rotation


def normalize(x):
    if (np.linalg.norm(x, axis=-1) == 0).any():
        import pdb; pdb.set_trace()
    return x / np.broadcast_to(np.expand_dims(np.linalg.norm(x, axis=-1), axis=1), (x.shape[0], 3))

def axis_angle(axis, angle):
    return

def look_at(
    center: np.ndarray,
    target: np.ndarray,
    up: np.ndarray):
    '''
    LookAt function for getting rotation from position and target
    Based on https://www.scratchapixel.com/lessons/mathematics-physics-for-computer-graphics/lookat-function

    Args:
        center: point
        target: point
        up: vector

    Returns:
        quat: rotation from LookAt function
    '''
    up_vector = normalize(up)
    forward_vector = normalize(target - center)
    right_vector = np.cross(up_vector, forward_vector)
    up_vector = np.cross(forward_vector, right_vector)

    mat = np.zeros((target.shape[0], 3, 3))
    mat[:, 0, 0] = right_vector[:, 0]
    mat[:, 0, 1] = right_vector[:, 1]
    mat[:, 0, 2] = right_vector[:, 2]
    mat[:, 1, 0] = up_vector[:, 0]
    mat[:, 1, 1] = up_vector[:, 1]
    mat[:, 1, 2] = up_vector[:, 2]
    mat[:, 2, 0] = forward_vector[:, 0]
    mat[:, 2, 1] = forward_vector[:, 1]
    mat[:, 2, 2] = forward_vector[:, 2]

    mat = np.transpose(mat, axes=(0, 2, 1)) # corrects order, tested so should be correct

    return Rotation.from_matrix(mat)

def solve_ik(
    points: np.ndarray,
    target: np.ndarray):
    '''
    IK solver for 2 bones
    Based on https://theorangeduck.com/page/simple-two-joint

    Args:
        points: list of 3 points
        target: point

    Returns:
        list: IK solution
    '''
    hip_pos = points[0]
    knee_pos = points[1]
    ankle_pos = points[2]
    up_vector = np.tile(np.array([0, 1, 0]), (hip_pos.shape[0], 1))

    global_hip_rot = look_at(hip_pos, knee_pos, up_vector)
    global_knee_rot = look_at(knee_pos, ankle_pos, up_vector)
    local_hip_rot = look_at(hip_pos, knee_pos, up_vector)
    local_knee_rot = Rotation.inv(global_hip_rot) * global_knee_rot

    return solve_ik_full(points, target, global_hip_rot, global_knee_rot, local_hip_rot, local_knee_rot)

def dot_axis(a, b, axis=-1):
    return np.sum((a * b), axis=axis)

def solve_ik_full(
    points: np.ndarray,
    target: np.ndarray,
    global_hip_rot: Rotation,
    global_knee_rot: Rotation,
    local_hip_rot: Rotation,
    local_knee_rot: Rotation) -> np.ndarray:
    '''
    IK solver for 2 bones
    Based on https://theorangeduck.com/page/simple-two-joint

    Args:
        points: list of 3 points
        target: point
        global_hip_rot: global hip rotation quaternion
        global_knee_rot: global knee rotation quaternion
        local_hip_rot: local hip rotation quaternion
        local_knee_rot: local knee rotation quaternion

    Returns:
        list: IK solution
    '''
    assert(len(points) == 3)

    use_stable = False # prevents some popping artifacts from appearing

    a = np.array(points[0])
    b = np.array(points[1])
    c = np.array(points[2])
    t = np.array(target)

    # otherwise will fail if foot already in correct position
    indices = (np.min(np.abs(c - t), axis=-1) > 1e-5).nonzero()[0]
    not_indices = (np.min(np.abs(c - t), axis=-1) <= 1e-5).nonzero()[0]
    #if np.allclose(c, t, atol=1e-5):
        #return points, [a_gr, b_gr, Rotation.identity()]

    a_gr = global_hip_rot[indices]
    b_gr = global_knee_rot[indices]
    a_lr = local_hip_rot[indices]
    b_lr = local_knee_rot[indices]

    a = a[indices]
    b = b[indices]
    c = c[indices]
    t = t[indices]

    ik_points = [
        np.zeros((points[0].shape[0], 3)),
        np.zeros((points[0].shape[0], 3)),
        np.zeros((points[0].shape[0], 3))
    ]

    if len(indices) > 0:
        # Step 1: extend/contract joint chain
        eps = 0.01
        lab = np.linalg.norm(b - a, axis=-1) # length of thigh
        lcb = np.linalg.norm(b - c, axis=-1) # length of calf
        # distance from hip to target, clamped to keep extension within bone range
        lat = np.clip(np.linalg.norm(t - a, axis=-1), eps, lab + lcb - eps)

        # interior angle of hip
        ac_ab_0 = np.arccos(np.clip(dot_axis(normalize(c - a), normalize(b - a), axis=-1), -1, 1))
        # interior angle of knee
        ba_bc_0 = np.arccos(np.clip(dot_axis(normalize(a - b), normalize(c - b), axis=-1), -1, 1))

        # desired angle of hip
        ac_ab_1 = np.arccos(np.clip((lcb*lcb-lab*lab-lat*lat) / (-2*lab*lat), -1, 1))
        # desired angle of knee
        ba_bc_1 = np.arccos(np.clip((lat*lat-lab*lab-lcb*lcb) / (-2*lab*lcb), -1, 1))

        # rotation angle
        if use_stable:
            d = Rotation.apply(b_gr, np.array([0, 0, 1]))
            axis0 = normalize(np.cross(normalize(c - a), d))
        else:
            axis0 = normalize(np.cross(normalize(c - a), normalize(b - a)))

        r0 = Rotation.from_rotvec(np.expand_dims(ac_ab_1 - ac_ab_0, axis=1) * Rotation.apply(Rotation.inv(a_gr), axis0))
        r1 = Rotation.from_rotvec(np.expand_dims(ba_bc_1 - ba_bc_0, axis=1) * Rotation.apply(Rotation.inv(b_gr), axis0))

        a_lr = a_lr * r0
        b_lr = b_lr * r1

        # Step 2: rotate the heel into place
        ac_at_0 = np.arccos(np.clip(dot_axis(normalize(c - a), normalize(t - a), axis=-1), -1, 1))
        axis1 = normalize(np.cross(c - a, t - a))
        r2 = Rotation.from_rotvec(np.expand_dims(ac_at_0, axis=-1) * Rotation.apply(Rotation.inv(a_gr), axis1))

        a_lr = a_lr * r2

        # Run forward kinematics
        ik_points[0][indices] = points[0][indices]

        a_gr2 = a_lr

        #offset1 = np.array([0, 0, lab])
        offset1 = np.zeros((lab.shape[0], 3))
        offset1[:, 2] = lab
        ik_points[1][indices] = Rotation.apply(a_gr2, offset1) + points[0][indices]

        b_gr2 = a_lr * b_lr

        #offset2 = np.array([0, 0, lcb])
        offset2 = np.zeros((lcb.shape[0], 3))
        offset2[:, 2] = lcb
        ik_points[2][indices] = Rotation.apply(b_gr2, offset2) + ik_points[1][indices]

        # Package rotations
        ik_rots = [a_gr2, b_gr2, Rotation.identity(len(a_gr2))]

    ik_points[0][not_indices] = points[0][not_indices]
    ik_points[1][not_indices] = points[1][not_indices]
    ik_points[2][not_indices] = points[2][not_indices]

    return ik_points, None  # ik_rots

def compute_foot_skating_error(pos, l_pos, H):
    v = pos - l_pos
    v = torch.from_numpy(v)
    v_2d = v[:, [0,2]]
    m_v = torch.norm(v, dim=-1, keepdim=True)

    h = torch.from_numpy(pos[:, [1]])

    exponent = torch.clamp(h / H, 0, 1)

    s = m_v * (2 - torch.pow(2, exponent))

    return s.numpy()

class ikSolver:
    '''
    Because the IK solver has state, it has it's own class
    '''
    def __init__(self,
                 device,
                 num_samples,
                 bone_map,
                 breakage_interpolation=True,
                 ik_breakage=True,
                 clamp_foot=True):

        self.device = device
        self.num_samples = num_samples
        self.bone_map = bone_map
        self.breakage_interpolation = breakage_interpolation
        self.ik_breakage = ik_breakage
        self.clamp_foot = clamp_foot

        self.interpolation_alpha = np.ones((self.num_samples, 2))
        self.in_contact = np.zeros((self.num_samples, 2))
        self.last_target_toe_pos = np.zeros((self.num_samples, 2, 3))
        self.last_position = None
        self.breakage = np.zeros((self.num_samples, 2))
        self.breakage_override = False
        self.contact_override = [False, False]  # [left, right]

        self.l_hip_index = self.bone_map["LeftUpLeg"]
        self.r_hip_index = self.bone_map["RightUpLeg"]
        self.l_knee_index = self.bone_map["LeftLeg"]
        self.r_knee_index = self.bone_map["RightLeg"]
        self.l_foot_index = self.bone_map["LeftFoot"]
        self.r_foot_index = self.bone_map["RightFoot"]
        self.l_toe_index = self.bone_map["LeftToeBase"]
        self.r_toe_index = self.bone_map["RightToeBase"]

    def __len__(self):
        return self.num_samples

    def __getitem__(self, i):
        output = {}
        output["interpolate_alpha"] = self.interpolation_alpha[i]
        output["in_contact"] = self.in_contact[i]
        output["last_target_toe_pos"] = self.last_target_toe_pos[i]
        output["breakage"] = self.breakage[i]
        return output

    def postprocess_ik(self, output):
        position = output["position"][:].cpu().detach().numpy()
        if self.last_position is None:
            self.last_position = np.array(position)

        l_contact = output["contact"][:, 0].cpu().detach().numpy()
        r_contact = output["contact"][:, 1].cpu().detach().numpy()

        if self.contact_override[0]:
            l_contact = np.ones_like(l_contact)
        if self.contact_override[1]:
            r_contact = np.ones_like(r_contact)

        l_output = self.postprocess_ik_leg_frame(
            self.l_hip_index,
            self.l_knee_index,
            self.l_foot_index,
            self.l_toe_index,
            position,
            l_contact,
            self.interpolation_alpha[:, 0],
            self.in_contact[:, 0],
            self.last_target_toe_pos[:, 0, :],
            self.breakage[:, 0])

        self.in_contact[:, 0] = l_output["in_contact"]
        position = l_output["position"]
        l_contact = l_output["contact"]
        if l_output["num_indices"] > 0:
            self.last_target_toe_pos[:, 0] = l_output["last_target_toe_pos"]
            self.interpolation_alpha[:, 0] = l_output["interpolation_alpha"]
            self.breakage[:, 0] = l_output["breakage"]

        r_output = self.postprocess_ik_leg_frame(
            self.r_hip_index,
            self.r_knee_index,
            self.r_foot_index,
            self.r_toe_index,
            position,
            r_contact,
            self.interpolation_alpha[:, 1],
            self.in_contact[:, 1],
            self.last_target_toe_pos[:, 1, :],
            self.breakage[:, 1])

        self.in_contact[:, 1] = r_output["in_contact"]
        position = r_output["position"]
        r_contact = r_output["contact"]
        if r_output["num_indices"] > 0:
            self.last_target_toe_pos[:, 1] = r_output["last_target_toe_pos"]
            self.interpolation_alpha[:, 1] = r_output["interpolation_alpha"]
            self.breakage[:, 1] = r_output["breakage"]

        contact = np.zeros((l_contact.shape[0], 2))
        contact[:, 0]  = l_contact
        contact[:, 1]  = r_contact
        output['position'] = torch.from_numpy(position).to(self.device)
        output['contact'] = torch.from_numpy(contact).float().to(self.device)
        output['breakage'] = torch.from_numpy(self.breakage).to(self.device)

        self.last_position = np.array(position)
        return output

    def postprocess_ik_leg_frame(
        self,
        hip_index: int,
        knee_index: int,
        foot_index: int,
        toe_index: int,
        position: torch.Tensor,
        contact: torch.Tensor,
        interpolation_alpha: torch.Tensor,
        in_contact: torch.Tensor,
        last_target_toe_pos: torch.Tensor,
        breakage: torch.Tensor) -> Dict:
        '''
        Post-process a single leg using inverse kinematics and contact labels

        Args:
            self: class instance
            hip_index: hip bone index
            knee_index: knee bone index
            foot_index: foot bone index
            toe_index: toe bone index
            position: positions [N, 57]
            contact: contact labels [N]
            interpolation_alpha: interpolation alpha
            in_contact: contact boolean [N]
            last_target_toe_pose: last target toe pose [N, 3]

        Returns:
            Dict: ["in_contact", "last_target_toe_pose", "interpolation_alpha", "stretch_factor"]
        '''
        # parameters
        interpolation_window = 10.0 # window for interpolating between IK and neural network

        breakage_dist = 0.3
        breakage_window = 10.0
        breakage_eps =  1.0 / (2.0 * breakage_window)

        foot_skating = compute_foot_skating_error(
            position[:, toe_index*3:toe_index*3+3],
            self.last_position[:, toe_index*3:toe_index*3+3],
            0.1).squeeze()

        breakage[(foot_skating < 0.01) & (np.round(contact)==1)] = 0


        #breakage[(interpolation_alpha == 1).nonzero()[0]] = 0.0

        contact[(position[:, toe_index*3+1]) > 0.1] = 0

        contact_0_indices = (np.round(contact) == 0).nonzero()[0]
        breakage[contact_0_indices] = 0
        breakage_indices = (np.round(breakage) == 1).nonzero()[0]
        contact[breakage_indices] = 0



        #breakage[0] = 100

        #breakage = np.maximum(breakage - (1.0 / breakage_window), 0.0)
        #if self.breakage_interpolation:
            #breakage_indices = (breakage > breakage_eps).nonzero()
            #contact[breakage_indices] = 0.0

        floor_enforce = True # not used (would be used for locking to floor)
        stretch_factor = torch.zeros((1))[0]

        diff = np.round(contact) - in_contact

        contact_indices = (np.round(contact) == 1).nonzero()[0]
        in_contact_indices = (diff == 1).nonzero()[0]
        inv_contact_indices = (np.round(contact) == 0).nonzero()[0]

        # if not in_contact and contact >= 0.5:
        in_contact[in_contact_indices] = 1.0        
        #in_contact[contact_indices] = np.where(interpolation_alpha[contact_indices] >= 1, 0, in_contact[contact_indices])

        contact_alpha = np.repeat(np.expand_dims(interpolation_alpha[contact_indices], axis=1), 3, axis=-1)

        '''
        last_target_toe_pos[contact_indices] = np.where(
            (contact_alpha <= 1) & (contact_alpha > 0),
            position[contact_indices, toe_index*3:toe_index*3+3],
            last_target_toe_pos[contact_indices])
        '''
        last_target_toe_pos[in_contact_indices] = position[in_contact_indices, toe_index*3:toe_index*3+3]
        if self.clamp_foot:
            last_target_toe_pos[:, 1] = np.clip(last_target_toe_pos[:, 1], 0, 0.05)
        else:
            last_target_toe_pos[:, 1] = np.maximum(last_target_toe_pos[:, 1], 0)
        #last_target_toe_pos[:, 1] = np.maximum(last_target_toe_pos[:, 1], 0) # clip to floor

        #interpolation_alpha[contact_indices] = 0.0
        # if not interpolating and in contact, make interpolation 0
        #interpolation_alpha[contact_indices] = np.where(interpolation_alpha[contact_indices] >= 1, 0, interpolation_alpha[contact_indices])
        interpolation_alpha[contact_indices] = 0.0

        # if between 0 and 1 and in contact, increment
        '''
        inv_interpolate_indices = ((interpolation_alpha < 1.0) & (interpolation_alpha > 0.0) & (np.round(contact) == 1)).nonzero()[0]
        interpolation_alpha[inv_interpolate_indices] = np.clip(
            interpolation_alpha[inv_interpolate_indices] + (1.0 / interpolation_window),
            0.0,
            1.0
        )
        '''

        #print(interpolation_alpha)

        in_contact[inv_contact_indices] = 0

        # if not in contact, increment
        interpolation_alpha[inv_contact_indices] = np.clip(
            interpolation_alpha[inv_contact_indices] + (1.0 / interpolation_window),
            0.0,
            1.0
        )

        '''
        interpolation_alpha[breakage_indices] = np.clip(
            interpolation_alpha[breakage_indices] + (1.0 / interpolation_window),
            0.0,
            1.0
        )
        '''

        '''
        if contact > 0.5:
            # first contact
            if not in_contact:
                in_contact = True
                last_target_toe_pos = position[toe_index*3:toe_index*3+3]
            interpolation_alpha = 0
        else:
            in_contact = False
            interpolation_alpha = min(1.0, interpolation_alpha + (1.0 / interpolation_window))
        '''

        # compute IK if in contact
        # if interpolation_alpha < 1:
        indices = (interpolation_alpha < 1.0).nonzero()[0]

        if len(indices > 0):
            pos = []
            alpha = np.sin(interpolation_alpha[indices] * (math.pi / 2))

            d_last_pos_foot = position[indices, foot_index*3:foot_index*3+3] - position[indices, toe_index*3:toe_index*3+3]
            last_target_foot_pos = last_target_toe_pos[indices] + d_last_pos_foot

            target_foot_pos = (np.expand_dims(1.0 - alpha, axis=1) * last_target_foot_pos) + (np.expand_dims(alpha, axis=1) * position[indices, foot_index*3:foot_index*3+3])

            pos.append(position[indices, hip_index*3:hip_index*3+3])
            pos.append(position[indices, knee_index*3:knee_index*3+3])
            pos.append(position[indices, foot_index*3:foot_index*3+3])
            ik_pos, _ = solve_ik(pos, np.array(target_foot_pos))

            foot_dist = np.linalg.norm(position[indices, foot_index*3:foot_index*3+3] - ik_pos[2], axis=-1)
            if self.ik_breakage:
                breakage[indices[(foot_dist > breakage_dist).nonzero()[0]]] = 1.0

            position[indices, hip_index*3:hip_index*3+3] = ik_pos[0]
            position[indices, knee_index*3:knee_index*3+3] = ik_pos[1]
            position[indices, foot_index*3:foot_index*3+3] = ik_pos[2]

            # lock toe as well
            position[indices, toe_index*3:toe_index*3+3] = position[indices, foot_index*3:foot_index*3+3] - d_last_pos_foot

            stretch_factor = np.sqrt(np.sum((position[indices, foot_index*3:foot_index*3+3] - last_target_foot_pos) ** 2, axis=-1))


        # ground indices
        ground_indices = (position[:, toe_index*3+1] < 0).nonzero()[0]
        ground_pos = np.array(position[:, toe_index*3:toe_index*3+3] + 1e-4)
        ground_pos[:, 1] = np.maximum(ground_pos[:, 1], 0)

        if len(ground_indices > 0):
            pos = []

            d_last_pos_foot = position[ground_indices, foot_index*3:foot_index*3+3] - position[ground_indices, toe_index*3:toe_index*3+3]
            last_target_foot_pos = ground_pos[ground_indices] + d_last_pos_foot

            target_foot_pos = last_target_foot_pos

            pos.append(position[ground_indices, hip_index*3:hip_index*3+3])
            pos.append(position[ground_indices, knee_index*3:knee_index*3+3])
            pos.append(position[ground_indices, foot_index*3:foot_index*3+3])
            ik_pos, _ = solve_ik(pos, np.array(target_foot_pos))

            position[ground_indices, hip_index*3:hip_index*3+3] = ik_pos[0]
            position[ground_indices, knee_index*3:knee_index*3+3] = ik_pos[1]
            position[ground_indices, foot_index*3:foot_index*3+3] = ik_pos[2]

            # lock toe as well
            position[ground_indices, toe_index*3:toe_index*3+3] = position[ground_indices, foot_index*3:foot_index*3+3] - d_last_pos_foot

        output = {}
        output["num_indices"] = len(indices)
        output["in_contact"] = in_contact
        output["last_target_toe_pos"] = last_target_toe_pos
        output["interpolation_alpha"] = interpolation_alpha
        output["breakage"] = breakage
        #output["stretch_factor"] = stretch_factor
        output["position"] = position
        output["contact"] = contact

        return output

    def interpolate(self, frame_output, target_eps_ik_state, eps_alpha):
        if len(self) != 1:
            raise ValueError("Length must be equal to 1 to use interpolation")

        if eps_alpha >= 1.0:
            return

        position = frame_output["position"].cpu().detach().numpy()
        last_target_toe_pos = np.expand_dims(target_eps_ik_state["last_target_toe_pos"], axis=0)

        # interpolate target solver
        if target_eps_ik_state["in_contact"][0]:
            self.contact_override[0] = True
            self.last_target_toe_pos[:, 0] = (last_target_toe_pos[:, 0] * eps_alpha) + (position[:, self.l_toe_index*3:self.l_toe_index*3+3] * (1.0 - eps_alpha))

        if target_eps_ik_state["in_contact"][1]:
            self.contact_override[1] = True
            self.last_target_toe_pos[:, 1] = (last_target_toe_pos[:, 1] * eps_alpha) + (position[:, self.r_toe_index*3:self.r_toe_index*3+3] * (1.0 - eps_alpha))

        # if done interpolating
        if eps_alpha >= 1.0:
            self.contact_override[0] = False
            self.contact_override[1] = False