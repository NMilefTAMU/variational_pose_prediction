import math
from logging import root
from typing import List

import numpy as np
import torch
from direct.gui.OnscreenText import OnscreenText
from panda3d.core import TextNode

import utils.paramUtil as paramUtil
from viewer.utils import convert_3d_to_2d


class Skeleton:
    def __init__(self, base, opt, num_joints, color=(0, 1.0, 0), offset=(0, 0, 0), k_chain=None):
        if k_chain is None:
            self.k_chain = paramUtil.chain_to_dict(paramUtil.get_kinematic_chain(opt))
        else:
            self.k_chain = k_chain
        self.base = base
        self.offset = offset
        self.color = color
        self.contact_mode = True
        #self.joint_color_set = False
        self.joint_color_set = False

        self.joints = []
        self.bones = []
        self.text = []
        self.bl_text = []

        self.joint_scale = 0.025

        self.joint_colors = np.zeros((num_joints, 3))

        for i in range(num_joints):
            joint = self.base.loader.load_model('assets/models/sphere.obj')
            joint.setScale(self.joint_scale, self.joint_scale, self.joint_scale)
            joint.setColor(color[0], color[1], color[2])
            joint.reparent_to(self.base.render)
            self.joints.append(joint)
            self.text.append(OnscreenText(
                text="",
                parent=self.base.aspect2d,
                pos=(0, -0.15),
                scale=0.05,
                fg=(1,1,1,1),
                bg=(0, 0, 0, 0.5),
                align=TextNode.ACenter))
            self.text[-1].hide()

        for k, v in self.k_chain.items():
            bone = base.loader.load_model('assets/models/bone.obj')
            bone.setScale(0.1, 0.1, 0.1)
            bone.setPos(0.0, -100, 0)
            bone.setColor(color[0], color[1], color[2])
            bone.reparent_to(base.render)
            bone.setDepthOffset(-1)
            self.bones.append(bone)
            self.bl_text.append(OnscreenText(
                text="",
                parent=self.base.aspect2d,
                pos=(0, -0.15),
                scale=0.05,
                fg=(1,1,1,1),
                bg=(0, 0, 0, 0.5),
                align=TextNode.ACenter))
            self.bl_text[-1].hide()

        self.tracked_points = []
        for _ in range(opt.num_tp):
            tracked_point = self.base.loader.load_model('assets/models/sphere.obj')
            tracked_point.setPos(0, -100, 0)
            tracked_point.setColor(1, 1, 0)
            tracked_point.setScale(0.05, 0.05, 0.05)
            tracked_point.reparent_to(self.base.render)
            tracked_point.hide()
            self.tracked_points.append(tracked_point)

        self.root_velocity_vector = base.loader.load_model('assets/models/sphere.obj')
        self.root_velocity_vector.setScale(self.joint_scale, self.joint_scale, self.joint_scale)
        self.root_velocity_vector.setPos(0.0, -100, 0)
        self.root_velocity_vector.setColor(color[0], color[1], color[2])
        self.root_velocity_vector.reparent_to(base.render)
        self.root_velocity_vector.hide()

        self.root_axes = []
        color = [[1,0,0], [0,1,0], [0,0,1]]
        for i in range(3):
            root_axis = self.base.loader.load_model('assets/models/cylinder.obj')
            root_axis.setPos(0, -100, 0)
            root_axis.setColor(color[i][0], color[i][1], color[i][2])
            root_axis.setScale(0.1, 0.1, 0.1)
            root_axis.reparent_to(self.base.render)
            self.root_axes.append(root_axis)

    def render_frame(self, frame_data):
        frame_data = frame_data.cpu().detach().numpy().squeeze()
        index = 0

        for joint in self.joints:
            pos = frame_data[index:index+3]
            joint.setPos(
                pos[0] + self.offset[0],
                -pos[1] + self.offset[1],
                -pos[2] + self.offset[2])
            self.text[index // 3].setText(str(index // 3))
            text_pos = convert_3d_to_2d(joint, self.base)
            self.text[index // 3].setPos(text_pos[0], text_pos[1])

            if self.joint_color_set:
                joint.setColor(self.joint_colors[index // 3, 0],
                            self.joint_colors[index // 3, 1],
                            self.joint_colors[index // 3, 2])

            index += 3

        index = 0
        for k, v in self.k_chain.items():
            child_pos = self.joints[k].getPos()
            parent_pos = self.joints[v].getPos()
            length = math.sqrt(
                (child_pos[0]-parent_pos[0])**2 + (child_pos[1]-parent_pos[1])**2 + (child_pos[2]-parent_pos[2])**2)
            length = max(1e-6, length)
            #self.bones[index].setScale(0.1, 1.0 * length, 0.1)
            self.bones[index].setScale(length, length, length)
            self.bones[index].setPos(parent_pos)
            self.bones[index].lookAt(child_pos)

            self.update_bl_text(index, length, self.joints[k], self.joints[v])

            index += 1

    def update_bl_text(self, index, length, parent, child):
            self.bl_text[index].setText(str(round(length, 3)))
            parent_text_pos = convert_3d_to_2d(parent, self.base)
            child_text_pos = convert_3d_to_2d(child, self.base)
            bone_text_pos = [0, 0]
            bone_text_pos[0] = (parent_text_pos[0] + child_text_pos[0]) / 2
            bone_text_pos[1] = (parent_text_pos[1] + child_text_pos[1]) / 2
            self.bl_text[index].setPos(bone_text_pos[0], bone_text_pos[1])

    def render_contact(self, contacts, l_index, r_index):
        if contacts[0] < 0.5 or not self.contact_mode:
            self.joints[l_index].setColor(self.color[0], self.color[1], self.color[2])
            self.joints[l_index].setScale(self.joint_scale, self.joint_scale, self.joint_scale)
        else:
            self.joints[l_index].setColor(1, 1, 1)
            self.joints[l_index].setScale(self.joint_scale * 2, self.joint_scale * 2, self.joint_scale * 2)

        if contacts[1] < 0.5 or not self.contact_mode:
            self.joints[r_index].setColor(self.color[0], self.color[1], self.color[2])
            self.joints[r_index].setScale(self.joint_scale, self.joint_scale, self.joint_scale)
        else:
            self.joints[r_index].setColor(1, 1, 1)
            self.joints[r_index].setScale(self.joint_scale * 2, self.joint_scale * 2, self.joint_scale * 2)

    def render_root_velocity(self, root_velocity):
        self.root_velocity_vector.setPos(
            float(root_velocity[0]*5) + self.offset[0],
            -float(root_velocity[1]*5) + self.offset[1],
            -float(root_velocity[2]*5) + self.offset[2],
        )

    def render_tracked_points(self, tracked_points: torch.Tensor):
        '''
        Render tracked points

        Args:
            tracked_points: torch.Tensor [3 * num_tp]
        '''

        if tracked_points is None:
            return

        index = 0
        for tracked_point in self.tracked_points:
            tracked_point.setPos(
                float(tracked_points[index * 3 + 0]) + self.offset[0],
                -float(tracked_points[index * 3 + 1]) + self.offset[1],
                -float(tracked_points[index * 3 + 2]) + self.offset[2],
            )
            index += 1

    def render_root_axes(self, root_rot, root_pos):
        index = 0

        x_vector = np.array([1, 0, 0])
        y_vector = np.array([0, 1, 0])
        z_vector = np.array([0, 0, 1])
        axes = [x_vector, y_vector, z_vector]

        for root_axis in self.root_axes:
            axis = axes[index]
            new_point = root_rot.apply(axis)
            #import pdb; pdb.set_trace()
            origin = [
                float(root_pos[0]) + self.offset[0],
                -float(root_pos[1]) + self.offset[1],
                -float(root_pos[2]) + self.offset[2],
            ]

            target = [
                float(root_pos[0]+new_point[0]) + self.offset[0],
                -float(root_pos[1]+new_point[1]) + self.offset[1],
                -float(root_pos[2]+new_point[2]) + self.offset[2],
            ]

            root_axis.setPos(origin[0], origin[1], origin[2])
            root_axis.lookAt(target[0], target[1], target[2])

            index += 1

        return
    
    def update_joint_colors(self, scale):
        self.joint_color_set = True
        for i in range(len(self.joint_colors)):
            alpha = float(scale[i].item())
            self.joint_colors[i, 0] = alpha
            self.joint_colors[i, 1] = 0
            self.joint_colors[i, 2] = 1.0 - alpha

    def hide_debug_text(self):
        for text in self.text: text.hide()

    def show_debug_text(self):
        for text in self.text: text.show()

    def hide_bl_text(self):
        for text in self.bl_text: text.hide()

    def show_bl_text(self):
        for text in self.bl_text: text.show()

    def show_tracked_points(self):
        for tp_point in self.tracked_points: tp_point.show()

    def hide_tracked_points(self):
        for tp_point in self.tracked_points: tp_point.hide()

    def show_contact_labels(self):
        self.contact_mode = True

    def hide_contact_labels(self):
        self.contact_mode = False