from panda3d.core import DirectionalLight, AmbientLight, SamplerState

class ViewerScene:
    def __init__(self, base, floor_offset=0):

        self.floor = []
        self.floor_texture = base.loader.loadTexture('assets/models/texture.png')
        self.floor_texture.setMinfilter(SamplerState.FT_linear_mipmap_linear)
        self.floor_texture.setMagfilter(SamplerState.FT_linear_mipmap_linear)

        for x in range(-1, 2):
            for y in range(-1, 2):
                floor = base.loader.load_model('assets/models/cube.obj')
                floor.setTexture(self.floor_texture)
                floor.setScale(8.0, 0.1, 8.0)
                floor.setPos(x * 16, 0.1 + floor_offset, y * 16)
                floor.reparent_to(base.render)
                self.floor.append(floor)

        self.light = DirectionalLight('directional_light')
        self.light.setColor((1.0, 1.0, 1.0, 1.0))
        self.light.setShadowCaster(True, 4096, 4096)
        self.light.getLens().setNearFar(0.1, 20)
        self.lightNP = base.render.attachNewNode(self.light)
        self.lightNP.setScale(15.0, 15.0, 15.0)
        self.lightNP.setPos(10, -10, -15)
        self.lightNP.lookAt(0, 0, 0)
        self.lightNP.setDepthOffset(-1)
        base.render.setLight(self.lightNP)
        base.render.setShaderAuto()


        self.a_light = AmbientLight('ambient_light')
        self.a_light.setColor((0.3, 0.3, 0.3, 1.0))
        self.a_lightNP = base.render.attachNewNode(self.a_light)
        base.render.setLight(self.a_lightNP)