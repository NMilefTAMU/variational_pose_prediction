import os
from panda3d.core import Point2, Point3

def convert_3d_to_2d(node, base):
    pos_3d = base.cam.getRelativePoint(node, Point3(0, 0, 0))
    pos_2d = Point2()
    base.camLens.project(pos_3d, pos_2d)
    pos_in_render_2d = Point3(pos_2d[0], 0, pos_2d[1])
    pos_in_aspect_2d = base.aspect2d.getRelativePoint(base.render2d, pos_in_render_2d)
    
    # normalize point
    output = Point2()
    output[0] = pos_in_aspect_2d[0]#((pos_in_aspect_2d[0] / base.getAspectRatio()) + 1.0) / 2.0
    output[1] = pos_in_aspect_2d[2]#(pos_in_aspect_2d[2] + 1.0) / 2.0

    return output
