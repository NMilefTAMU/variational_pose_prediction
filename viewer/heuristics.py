import torch
from dataProcessing.dataset_mocap import DatasetMocap

def pose_dist(pose0, pose1):
    pose0_3d = torch.reshape(pose0, (1, -1, 3))
    pose1_3d = torch.reshape(pose1, (1, -1, 3))
    error = torch.mean(torch.sqrt(torch.sum((pose1_3d - pose0_3d) ** 2, dim=-1)))
    return float(error)

def foot_dist(pose0, pose1):
    lf_index = DatasetMocap.get_bone_index("LeftFoot")
    rf_index = DatasetMocap.get_bone_index("RightFoot")

    pose0_3d = torch.reshape(pose0, (1, -1, 3))[:, [lf_index, rf_index], :]
    pose1_3d = torch.reshape(pose1, (1, -1, 3))[:, [lf_index, rf_index], :]

    error = torch.mean(torch.sqrt(torch.sum((pose1_3d - pose0_3d) ** 2, dim=-1)))
    return float(error)

def foot_height(pose0):
    threshold = 0.05

    lf_index = DatasetMocap.get_bone_index("LeftFoot")
    rf_index = DatasetMocap.get_bone_index("RightFoot")

    foot_height = torch.reshape(pose0, (1, -1, 3))[:, [lf_index, rf_index], 1]

    return foot_height

def foot_height_eval(pose, contact):
    eps = 0.01
    h = torch.clamp(0.06 / (foot_height(pose) + eps), min=0, max=1)

    if contact[0] < 1.0:
        h[:, 0] = 1
    if contact[1] < 1.0:
        h[:, 1] = 1

    return float(torch.mean(h))