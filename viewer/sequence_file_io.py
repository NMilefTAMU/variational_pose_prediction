import os
import time

import numpy as np


def save_data(data, args):
    milliseconds = int(time.time() * 1000)
    os.makedirs("./results/", exist_ok=True)
    save_metadata(args, milliseconds)

    filename = "data_" + args.sequence_names[0] + "_" + str(milliseconds) + ".npz"
    full_filename = os.path.join("./results/", filename)
    np.savez(full_filename, **data)

def load_data(filename):
    data = np.load(filename)
    output = {}
    for key in data.files:
        output[key] = data[key]

    return output

def save_metadata(args, milliseconds):
    filename = "metadata_" + args.sequence_names[0] + "_" + str(milliseconds) + ".txt"
    full_filename = os.path.join("./results/", filename)
    with open(full_filename, "w") as f:
        for arg in vars(args):
            k = arg
            v = getattr(args, arg)
            f.write(k + ": " + str(v) + "\n")