import os
from argparse import ArgumentParser

import torch
from direct.gui.OnscreenText import OnscreenText
from direct.task import Task
from panda3d.core import TextNode

from utils.get_opt import get_opt
from viewer.sequence_file_io import load_data
from viewer.skeleton import Skeleton
from viewer.stats_display import EPSDisplay
from viewer.utils import convert_3d_to_2d
from viewer_base import ViewerArgumentParser, ViewerBase


def extract_property(metadata_filename, prop):
    with open(metadata_filename, "r") as f:
        lines = f.readlines()

    output = {}
    for line in lines:
        split = line.strip().split(": ")
        output[split[0]] = split[1]

    return output[prop]

class PosePredictionAppOffline(ViewerBase):
    '''
    Viewer for offline data that has been save to a .csv file
    '''
    def __init__(self, args):
        super().__init__(args)

        self.sequences = []
        self.skeleton_generated = []
        self.text_anchors = []
        self.skeleton_labels = []

        self.device = torch.device("cpu")# torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
        torch.cuda.set_device(0)

        num_motions = 128
        dataset_opt_path = './checkpoints/vae/' + args.dataset + '/' + args.experiment + '/opt.txt'
        opt = get_opt(dataset_opt_path, num_motions, self.device)

        index = 0
        
        if os.path.isdir(args.files[0]):
            all_files = os.listdir(args.files[0])
            import pdb; pdb.set_trace()
            files = sorted([os.path.join(args.files[0], f) for f in all_files if f.startswith("data")])
            metadata_files = sorted([os.path.join(args.files[0], f) for f in all_files if f.startswith("metadata")])
        else:
            files = [args.files[0]]
            metadata_files = ["meta" + args.files[0]]

        for i in range(len(files)):
            filename = files[i]
            metadata_filename = metadata_files[i]

            data = load_data(os.path.join(filename))
            for k, v in data.items():
                data[k] = torch.from_numpy(v)
            self.sequences.append(data)
 
            offset_pos = (-1.5 + index * 1.5, 0, 0)
            self.skeleton_generated.append(Skeleton(self, opt, self.num_joints, color=(0, 1, 0), offset=offset_pos))

            text_anchor = self.loader.load_model('assets/models/sphere.obj')
            text_anchor.setPos(offset_pos[0], offset_pos[1], offset_pos[2] - 1)
            text_anchor.hide()
            text_anchor.reparent_to(self.render)
            self.text_anchors.append(text_anchor)

            parent_text_pos = convert_3d_to_2d(text_anchor, self)

            label = extract_property(metadata_files[i], "disable_interpolation")
            self.skeleton_labels.append(OnscreenText(
                text=label,
                parent=self.aspect2d,
                pos=(parent_text_pos[0], parent_text_pos[1]),
                scale=0.05,
                fg=(1, 1, 1, 1),
                bg=(0, 0, 0, 0.5),
                align=TextNode.ACenter))
            index += 1

        self.num_frames = self.sequences[0]["position"].shape[0]

        self.epsStatsData = self.sequences[0]["eps"]
        self.epsStats = EPSDisplay(self, self.epsStatsData.shape[1])

        self.task_mgr.add(self.update, "Update Task")

    def update(self, task):
        self.update_time()

        for i in range(len(self.text_anchors)):
            parent_text_pos = convert_3d_to_2d(self.text_anchors[i], self)
            self.skeleton_labels[i].setPos(parent_text_pos[0], parent_text_pos[1])

        for i in range(len(self.skeleton_generated)):
            skeleton = self.skeleton_generated[i]
            position = self.sequences[i]["position"]
            skeleton.render_frame(position[self.frame])

        self.epsStats.render(self.epsStatsData[self.frame])

        return Task.cont

if __name__ == "__main__":
    parser = ViewerArgumentParser()
    parser.parser.add_argument("--files", nargs="+", help="files to load")
    args = parser.parse_args()

    app = PosePredictionAppOffline(args)
    app.run()
