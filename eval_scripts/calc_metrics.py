import argparse
import os
import sys

import torch

sys.path.append(os.getcwd())

from dataProcessing.dataset import get_batch_from_dataset
from motion_loaders.dataset_motion_loader import get_dataset_motion_loader
from motion_loaders.motion_loader import get_motion_loader
from utils.get_opt import get_opt
import utils.paramUtil as paramUtil
from trainer.trainer_vae import TrainerVAE6d
import models.motion_vae as vae_models
import numpy as np
from scipy.spatial.distance import euclidean
import statistics
from sklearn.preprocessing import normalize


def calc_positional_error(generated, gt, opt):
    a = generated.clone().reshape((-1, 3)).cpu().detach().numpy()
    b = gt.clone().reshape((-1, 3)).cpu().detach().numpy()
    num_points = a.shape[0]

    error = np.zeros(num_points)
    for i in range(num_points):
        point_a = a[i]
        point_b = b[i]

        dist = euclidean(point_a, point_b)
        error[i] = dist

    return error

def calc_vector(sequence, child_index, parent_index):
    child_pos = sequence[:, child_index]
    parent_pos = sequence[:, parent_index]
    return normalize(child_pos - parent_pos)

# input should be [num_samples, 3]
def calc_dot(a, b):
    assert len(a.shape) == 2, 'tensor must be of form (N, 3)'
    assert len(b.shape) == 2, 'tensor must be of form (N, 3)'
    assert a.shape[-1] == 3, 'tensor must be of form (N, 3)'
    assert b.shape[-1] == 3, 'tensor must be of form (N, 3)'
    return np.sum(a*b, axis=1)

def calc_rotational_error(generated, gt, opt):
    num_joints = gt.shape[-1] // 3
    a = generated.clone().squeeze().reshape((-1, num_joints, 3)).cpu().detach().numpy()
    b = gt.clone().squeeze().reshape((-1, num_joints, 3)).cpu().detach().numpy()
    num_points = a.shape[0]

    k_chain = paramUtil.get_kinematic_chain(opt)

    error = np.zeros((a.shape[0], num_joints))
    k_chain_dict = paramUtil.chain_to_dict(k_chain)
    for k, v in k_chain_dict.items():
        generated_vector = calc_vector(a, k, v)
        gt_vector = calc_vector(b, k, v)
        error[:,k] = np.degrees(np.arccos(calc_dot(generated_vector, gt_vector)))

    return error

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--experiment', '-e', help='experiment name', required=True)
    parser.add_argument('--dataset', '-d', help='dataset name', required=True)
    args = parser.parse_args()

    device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
    torch.cuda.set_device(0)

    num_motions = 16
    label_spe = 3
    dataset_opt_path = './checkpoints/vae/' + args.dataset + '/' + args.experiment + '/opt.txt'
    dataset_opt = get_opt(dataset_opt_path, num_motions, device)
    opt = dataset_opt

    dataset_params = paramUtil.get_dataset_params(dataset_opt)

    motion_loader = get_dataset_motion_loader(dataset_opt, num_motions, device, label=label_spe)
    motion_loader.dataset
    trainer = TrainerVAE6d(
        motion_loader,
        dataset_opt,
        device,
        dataset_params['raw_offsets'],
        dataset_params['kinematic_chain'])

    model_file_path = os.path.join(opt.model_path, opt.which_epoch + '.tar')
    model = torch.load(model_file_path, map_location='cuda:0')
    prior_net = vae_models.GaussianGRU(opt.input_size, opt.dim_z, opt.hidden_size,
                                       opt.prior_hidden_layers, opt.num_samples, device,
                                       lock_eps=False, learning_mode=opt.learning_mode)
    decoder = vae_models.DecoderGRU6d(opt.input_size + opt.dim_z, opt.output_size, opt.hidden_size,
                                        opt.decoder_hidden_layers,
                                        opt.num_samples, device, last_pose=opt.enable_last_p)

    prior_net.load_state_dict(model['prior_net'])
    decoder.load_state_dict(model['decoder'])
    prior_net.to(device)
    decoder.to(device)

    pos_errors = []
    rot_errors = []
    for i in range(num_motions):
        generated_sequence, _, _ = trainer.evaluate(prior_net, decoder, 1, sample_index=i)
        gt_sequence, _ = get_batch_from_dataset(motion_loader.dataset, i)
        gt_sequence = gt_sequence[:, gt_sequence.shape[1]-generated_sequence.shape[1]:gt_sequence.shape[1], :]

        pos_error = calc_positional_error(generated_sequence, gt_sequence, opt)
        pos_errors.append(np.average(pos_error))
        rot_error = calc_rotational_error(generated_sequence, gt_sequence, opt)
        rot_errors.append(np.average(rot_error))

    print("Pos error:\t", statistics.mean(pos_errors))
    print("Rot error:\t", statistics.mean(rot_errors))