from pose_model.pose_model_params import PoseModelParams
import torch

from dataProcessing.dataset_mocap_ours import DatasetMocapOurs
from evaluate_batch import InferenceBatch
from models.pose_network import PoseModelNetwork, combine_lower_and_full_body, extract_lower_body
from pose_model.pose_error_display import PoseErrorDisplay
from utils.forward_kinematics import forward_kinematics_fast
from utils.get_opt import get_opt
from viewer.skeleton import Skeleton
from viewer_base import ViewerArgumentParser, ViewerBase
from direct.task import Task
from utils.pose_utils import local_to_world, world_to_local
import utils.paramUtil as paramUtil


class PoseModelApp(ViewerBase):
    def __init__(self, args):
        super().__init__(args)

        device = torch.device("cuda:" + str(args.gpu_id) if torch.cuda.is_available() else "cpu")

        dataset_opt_path = './checkpoints/vae/' + args.dataset + '/' + args.vae_name + '/opt.txt'
        opt = get_opt(dataset_opt_path, 128, device)
        opt.motion_length = -1
        self.pose_predict = args.pose_predict

        if len(args.sequence_names) == 1:
            opt.sequence_names = args.sequence_names
        else:
            raise ValueError("Only one sequence_name is supported at this time")

        self.dataset = DatasetMocapOurs("./dataset/mocap_ours/output/", opt, args.dataset_type)

        self.num_frames = self.dataset[0]["position"].shape[0]
        self.positions_tensor = torch.from_numpy(self.dataset[0]["position"]).float().to(device).unsqueeze(0)
        self.positions = []
        for f in range(self.num_frames):
            self.positions.append(self.positions_tensor[:, f])

        # get positions and rotations
        self.root_pos = torch.from_numpy(self.dataset[0]["root_pos"]).float().to(device).unsqueeze(0)
        self.root_rot_mat = torch.from_numpy(self.dataset[0]["root_rot_mat"]).float().to(device).unsqueeze(0)

        # use VR pose predict
        if self.pose_predict:
            self.network_pp = InferenceBatch(
                args.dataset,
                args.experiment,
                sequence_names=args.sequence_names,
                dataset_mode=args.dataset_type,
                ik_mode=(not args.disable_ik),
                root_transform=(not args.disable_root_transform),
                save_z=args.save_z,
                lock_eps=args.lock_eps,
                smart_eps=args.smart_eps,
                disable_interpolation=args.disable_interpolation,
                waist_occlusion=True if args.num_tp == 3 else False,
                use_eps_net=args.use_eps_net,
                set_seed=args.set_seed,
                selection_mode=args.selection_mode,
                lock_sample=args.lock_sample,
                lookahead=args.lookahead,
                choreography_file=args.choreography,                
            )

            output_pp = self.network_pp.evaluate_sequence(0)

            self.positions_pp = []
            for f in range(self.num_frames):
                position_world = world_to_local(
                    output_pp["position"][[f]].reshape(1, -1, 3),
                    None,
                    self.root_pos[:, [f]],
                    self.root_rot_mat[:, [f]]
                )
                self.positions_pp.append(position_world["position"].reshape(1, -1))
        
        dataset_params = paramUtil.get_dataset_params(opt)
        self.kinematic_chain = dataset_params["kinematic_chain"]

        bone_lengths = torch.from_numpy(self.dataset[0]["bone_lengths"]).float().to(device).unsqueeze(0)

        self.play = True

        # pose model network
        pose_model_params = PoseModelParams()
        pose_model_params.load("./checkpoints/eps/" + args.pose_model + ".yaml")

        input_dim = 0
        if "pos" in pose_model_params.input_types:
            input_dim += 3
        if "vel" in pose_model_params.input_types:
            input_dim += 3

        if pose_model_params.train_mode == "full":
            network = PoseModelNetwork(
                input_size=19 * input_dim,
                num_levels = pose_model_params.num_levels,
            )
        elif pose_model_params.train_mode == "lower":
            network = PoseModelNetwork(
                input_size=9 * input_dim,
                num_levels = pose_model_params.num_levels,
            )
        
        network.load_state_dict(state_dict = torch.load("./checkpoints/eps/" + args.pose_model + ".pth"))
        network.to(device)

        self.position_pms = []
        for f in range(self.num_frames):
            if self.pose_predict:
                full_body = self.positions_pp[f]
            else:
                full_body = self.positions[f]

            #lower_body = extract_lower_body(full_body)
            position_pm = network(full_body)
            position_pm = forward_kinematics_fast(device, position_pm, self.kinematic_chain, bone_lengths)["pos"]
            #position_pm = combine_lower_and_full_body(network(lower_body), full_body)
            self.position_pms.append(position_pm)

        # convert to world coordinate system
        for f in range(self.num_frames):
            self.positions[f] = local_to_world(
                self.positions[f].reshape((1, -1, 3)),
                None,
                self.root_pos[:, [f]],
                self.root_rot_mat[:, [f]],
            )["position"].flatten()

            self.position_pms[f] = local_to_world(
                self.position_pms[f].reshape((1, -1, 3)),
                None,
                self.root_pos[:, [f]],
                self.root_rot_mat[:, [f]],
            )["position"].flatten()

            self.positions_pp[f] = local_to_world(
                self.positions_pp[f].reshape((1, -1, 3)),
                None,
                self.root_pos[:, [f]],
                self.root_rot_mat[:, [f]],
            )["position"].flatten()

        # skeleton
        num_joints = 19
        self.skeleton_gt = Skeleton(self, opt, num_joints, color=(0, 1, 0), offset=(0, 0, 0))
        self.skeleton_pm = Skeleton(self, opt, num_joints, color=(1, 0, 0), offset=(2, 0, 0))
        if args.pose_predict:
            self.skeleton_pp = Skeleton(self, opt, num_joints, color=(0, 0, 1), offset=(4, 0, 0))

        # GUI
        self.pose_error_display = PoseErrorDisplay(self, self.dataset.bone_map)

        self.task_mgr.add(self.update, "Update Task")


    def update(self, task):
        self.update_time()

        self.skeleton_gt.render_frame(self.positions[self.frame])
        self.skeleton_pm.render_frame(self.position_pms[self.frame])
        if self.pose_predict:
            self.skeleton_pp.render_frame(self.positions_pp[self.frame])

        self.pose_error_display.render_new_stats(
            self.position_pms[self.frame],
            self.positions_pp[self.frame],
            self.kinematic_chain,
        )

        return Task.cont


if __name__ == "__main__":
    parser = ViewerArgumentParser()
    parser.parser.add_argument("--gpu_id", type=int, default=0)
    parser.parser.add_argument("--pose_model", type=str, required=True)
    parser.parser.add_argument("--pose_predict", action="store_true")
    parser.parser.add_argument("--vae_name", type=str, default="test_model_6d_8_25b")
    args = parser.parse_args()

    app = PoseModelApp(args)
    app.run()