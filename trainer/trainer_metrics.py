import torch

def compute_spatial_error(pred: torch.Tensor, gt: torch.Tensor) -> torch.Tensor:
    '''
    Compute Euclidean distance as spatial error

    Args:
        pred: [N, 57]
        gt: [N, 57]

    Returns:
        torch.Tensor: mean error across all samples
    '''

    batch_size = pred.shape[0]
    pred_3d = torch.reshape(pred.clone().detach(), (batch_size, -1, 3))
    gt_3d = torch.reshape(gt.clone().detach(), (batch_size, -1, 3))
    error = torch.sqrt(torch.sum((pred_3d - gt_3d) ** 2, dim=-1))
    return torch.mean(error).double()

def compute_contact_accuracy(pred: torch.Tensor, gt: torch.Tensor) -> torch.Tensor:
    '''
    Compute contact accuracy for a single foot

    Args:
        pred: [N, 1]
        gt: [N, 1]

    Returns:
        torch.Tensor: mean error across all samples
    '''

    accuracy = 1.0 - torch.mean(torch.abs(torch.round(pred.clone().detach()) - gt.detach()))
    return accuracy

def compute_action_accuracy(pred: torch.Tensor, gt: torch.Tensor) -> torch.Tensor:
    '''
    Compute action accuracy for a single foot

    Args:
        pred: [N, A]
        gt: [N, A]

    Returns:
        torch.Tensor: mean error across all samples
    '''

    if pred is None:
        return 0
    
    gt_c = gt.detach()
    pred_c = torch.round(pred.clone().detach()) * gt_c
    num_actions = torch.sum(gt_c, dim=-1)
    error = torch.abs(pred_c - gt_c) / num_actions
    accuracy = 1.0 - error
    return accuracy
