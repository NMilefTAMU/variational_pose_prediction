import torch
import torch.nn as nn
from utils.forward_kinematics import forward_kinematics

from trainer.trainer_vae import TrainerVAE6d

class TrainerOurs(TrainerVAE6d):
    def __init__(self, motion_sampler, opt, device, raw_offsets, kinematic_chain):
        super(TrainerOurs, self).__init__(motion_sampler, opt, device)

        self.device = device
        self.raw_offsets = torch.from_numpy(raw_offsets).to(device).detach()
        self.kinematic_chain = kinematic_chain
        self.Tensor = torch.Tensor if self.opt.gpu_id is None else torch.cuda.FloatTensor

        self.recon_criterion = self.loss_function
        self.mse_loss = nn.MSELoss()

    def loss_function(self, eval_pos, gt_pos):
        """
        Eval contains joint rotations

        Args:
            eval: tensor [N, 6 * (J - 1)]
            gt: tensor [N, J, 3]

        Returns:
            tensor: loss
        """
        loss = self.mse_loss(eval_pos, gt_pos)
        return loss