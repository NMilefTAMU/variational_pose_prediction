import random
from collections import OrderedDict
from functools import partial
from math import e

import torch
import torch.optim as optim
import torch.nn as nn
from dataProcessing.dataset import get_batch_from_dataset
from models.loss_function import ForwardKinematicsLoss, VelocityLoss, RotationLoss
from utils.forward_kinematics import forward_kinematics
from utils.paramUtil import *
from utils.pose_processing import extract_next_tracked_points
from utils.rotation_util import compute_rotation_matrix_from_ortho6d
from utils.utils_ import *
from trainer.trainer_metrics import compute_spatial_error, compute_contact_accuracy

from trainer.trainer import Trainer


class TrainerVAE6d(Trainer):
    '''
    Main trainer
    '''
    def __init__(self, train_data_loader, valid_data_loader, opt, device, raw_offsets, kinematic_chain):
        self.opt = opt
        self.device = device
        self.train_data_loader = train_data_loader
        self.valid_data_loader = valid_data_loader
        self.motion_enumerator = {}
        self.opt_generator = None
        self.raw_offsets = torch.from_numpy(raw_offsets).to(device).detach()
        self.kinematic_chain = kinematic_chain

        self.Tensor = torch.Tensor if self.opt.gpu_id is None else torch.cuda.FloatTensor
        self.device = device

        self.align_criterion = nn.MSELoss()
        self.recon_criterion = ForwardKinematicsLoss(self.device)
        self.velocity_loss = VelocityLoss()
        self.contact_loss = nn.BCELoss()
        self.rot_loss = RotationLoss()

    def identity(self, x):
        return x

    def ones_like(self, t, val=1):
        return torch.Tensor(t.size()).fill_(val).requires_grad_(False).to(self.device)

    def zeros_like(self, t, val=0):
        return torch.Tensor(t.size()).fill_(val).requires_grad_(False).to(self.device)

    def tensor_fill(self, tensor_size, val=0):
        return torch.zeros(tensor_size).fill_(val).requires_grad_(False).to(self.device)

    def sample_real_motion_batch(self, sample_index=None, mode="train"):
        if mode not in ["train", "valid"]:
            raise ValueError("Mode must be either \"train\" or \"valid\"")

        if mode == "train":
            data_loader = self.train_data_loader
        elif mode == "valid":
            data_loader = self.valid_data_loader

        if mode not in self.motion_enumerator:
            self.motion_enumerator[mode] = enumerate(data_loader)

        if sample_index is None:
            batch_idx, batch = next(self.motion_enumerator[mode])
        else:
            batch_idx = sample_index
            batch = get_batch_from_dataset(data_loader.dataset, sample_index)

        if batch_idx == len(data_loader) - 1:
            self.motion_enumerator[mode] = enumerate(data_loader)
        self.real_motion_batch = batch
        return batch

    def kl_criterion(self, mu1, logvar1, mu2, logvar2):
        # KL( N(mu1, sigma2_1) || N(mu_2, sigma2_2))
        # loss = log(sigma2/sigma1) / 2 + (sigma1 + (mu1 - mu2)^2)/(2*sigma2) - 1/2
        sigma1 = logvar1.mul(0.5).exp()
        sigma2 = logvar2.mul(0.5).exp()
        kld = torch.log(sigma2/sigma1) + (torch.exp(logvar1) + (mu1-mu2)**2)/(2*torch.exp(logvar2)) - 1/2
        return kld.sum() / self.opt.batch_size

    def calc_bone_velocity_loss(self, eval_data, gt_data_prev, gt_data, bone_name):
        index = self.train_data_loader.dataset.dataset.get_bone_index(bone_name)
        eval       = eval_data[:, index*3:index*3+3]
        gt         = gt_data[:, index*3:index*3+3]
        prev_gt    = gt_data_prev[:, index*3:index*3+3]

        return self.contact_loss(eval, prev_gt, gt)

    def append_to_condition_vec(self, data_vec, condition_vec):
        if condition_vec is None:
            condition_vec = data_vec
        else:
            condition_vec = torch.cat((condition_vec, data_vec), dim=1)
        return condition_vec

    def train(self, iter_num, prior_net, posterior_net, decoder, opt_prior_net, opt_posterior_net, opt_decoder, sample_true, tf_ratio):
        prior_net.train()
        posterior_net.train()
        decoder.train()

        opt_prior_net.zero_grad()
        opt_posterior_net.zero_grad()
        opt_decoder.zero_grad()

        prior_net.init_hidden()
        posterior_net.init_hidden()
        decoder.init_hidden(self.opt.batch_size)

        prior_net.select_new_eps(self.opt.batch_size, self.opt.dim_z)
        posterior_net.select_new_eps(self.opt.batch_size, self.opt.dim_z)

        # data(batch_size, motion_len, joints_num * 3)
        sample = sample_true()
        sample['position'] = sample['position'].float().to(self.device)
        sample['velocity'] = sample['velocity'].float().to(self.device)
        sample['angular_velocity'] = sample['angular_velocity'].float().to(self.device)
        sample['root_velocity'] = sample['root_velocity'].float().to(self.device)
        data = sample["position"]
        contact_data = sample["contact"]
        self.real_data = data
        data = torch.clone(data).float().detach_().to(self.device)
        contact_data = torch.clone(contact_data).float().detach_().to(self.device)
        motion_length = data.shape[1]

        # dim(batch_size, pose_dim), initial prior is a zero vector
        prior_vec = self.tensor_fill((data.shape[0], data.shape[2]), 0)
        if self.opt.warm_start:
            prior_vec = data[:, 0, :]

        log_dict = OrderedDict({'g_loss': 0})

        teacher_force = True if random.random() < tf_ratio else False
        losses = {}
        for lf in self.opt.loss_functions:
            losses[lf] = 0

        metrics = {'contact_accuracy': 0, 'spatial_error': 0}
        
        opt_step_cnt = 0

        # looping over motion_length-1 instead of motion_length because we need current hand position
        for i in range(0, motion_length-1):
            condition_vec = None
            if 'time_counter' in self.opt.input_types:
                time_counter = i / (motion_length - 1)
                time_counter_vec = self.tensor_fill((data.shape[0], 1), time_counter)
                condition_vec = self.append_to_condition_vec(time_counter_vec, condition_vec)
            if any(x in ['tracking3', 'tracking4'] for x in self.opt.input_types):
                tracked_points = extract_next_tracked_points(sample, i, self.opt, 'position')
                condition_vec = self.append_to_condition_vec(tracked_points, condition_vec)
            if any(x in ['vtracking3', 'vtracking4'] for x in self.opt.input_types):
                tracked_points = extract_next_tracked_points(sample, i, self.opt, 'velocity')
                condition_vec = self.append_to_condition_vec(tracked_points, condition_vec)
            if any(x in ['avtracking3', 'avtracking4'] for x in self.opt.input_types):
                tracked_points = sample['angular_velocity'][:, i, :]
                condition_vec = self.append_to_condition_vec(tracked_points, condition_vec)
            if 'root_velocity' in self.opt.input_types:
                condition_vec = self.append_to_condition_vec(sample["root_velocity"][:,i,:], condition_vec)

            if condition_vec is not None:
                if self.opt.enable_last_p:
                    h = torch.cat((prior_vec, condition_vec), dim=1)
                else:
                    h = condition_vec
                h_target = torch.cat((data[:, i], condition_vec), dim=1)
            else:
                h = prior_vec
                h_target = data[:, i].clone()

            if self.opt.learning_mode == "vae":
                z_t, mu, logvar, h_in_p = posterior_net(h_target)
                _, mu_p, logvar_p, _ = prior_net(h)
            elif self.opt.learning_mode == "vae_fp":
                z_t, mu, logvar, h_in_p = prior_net(h)
            elif self.opt.learning_mode == "supervised":
                z_t, mu, logvar, h_in_p = prior_net(h)

            h_mid = torch.cat((h, z_t), dim=1)
            output = decoder(h_mid)
            x_pred = output["output"]
            contact = output["contact"]

            if i == motion_length - 2:
                opt_step_cnt += 1
                if "mse" in self.opt.loss_functions:
                    losses['mse'] += self.recon_criterion(x_pred, data[:, i], self.kinematic_chain, sample["bone_lengths"]) * self.opt.mse_weight
                if "rot" in self.opt.loss_functions:
                    gt_rots3x3 = sample["local_rot_mat"][:, i].float().to(self.device)
                    rots3x3 = output["output"][:, 3:].reshape((output["output"].shape[0], -1, 6))
                    rots3x3 = compute_rotation_matrix_from_ortho6d(rots3x3)
                    losses["rot"] += self.rot_loss(rots3x3, gt_rots3x3[:, 1:])
                if "contact" in self.opt.loss_functions:
                    losses["contact"] += self.contact_loss(contact, contact_data[:, i]) * self.opt.contact_weight
                if "velocity" in self.opt.loss_functions:
                    losses["velocity"] += self.velocity_loss(x_pred, data[:, i-1], data[:, i])
                if "kld" in self.opt.loss_functions:
                    if self.opt.learning_mode in ["vae", "vae_fp"]:
                        mu_p = torch.zeros_like(logvar)
                        logvar_p = torch.log(torch.ones_like(logvar) ** 2)

                    losses['kld'] += self.kl_criterion(mu, logvar, mu_p, logvar_p)

                if iter_num % self.opt.print_every == 0:
                    x_pred_3d = forward_kinematics(self.device, x_pred, self.kinematic_chain, sample["bone_lengths"])["pos"]
                    metrics['contact_accuracy'] += compute_contact_accuracy(contact, contact_data[:, i])
                    metrics['spatial_error'] += compute_spatial_error(x_pred_3d, data[:, i])

            if teacher_force:
                prior_vec = x_pred
            else:
                prior_vec = data[:, i]

        for lf, value in losses.items():
            log_dict['g_' + lf + '_loss'] = value.item() / opt_step_cnt

        log_dict['contact_accuracy'] = metrics['contact_accuracy'] / opt_step_cnt
        log_dict['spatial_error'] = metrics['spatial_error'] / opt_step_cnt

        total_loss = 0
        for lf, value in losses.items():
            if lf == "kld":
                total_loss += value * self.opt.lambda_kld
            else:
                total_loss += value

        avg_loss = total_loss.item() / opt_step_cnt
        total_loss.backward()

        opt_prior_net.step()
        opt_posterior_net.step()
        opt_decoder.step()
        log_dict['g_loss'] = avg_loss

        if self.opt.step_lr:
            self.scheduler_decoder.step()
            self.scheduler_prior.step()
            self.scheduler_posterior.step()

        return log_dict

    def evaluate(self, iter_num, prior_net, posterior_net, decoder, opt_prior_net, opt_posterior_net, opt_decoder, sample_true, tf_ratio):
        prior_net.eval()
        posterior_net.eval()
        decoder.eval()

        prior_net.init_hidden()
        posterior_net.init_hidden()
        decoder.init_hidden(self.opt.batch_size)

        prior_net.select_new_eps(self.opt.batch_size, self.opt.dim_z)
        posterior_net.select_new_eps(self.opt.batch_size, self.opt.dim_z)

        # data(batch_size, motion_len, joints_num * 3)
        sample = sample_true()
        sample['position'] = sample['position'].float().to(self.device)
        sample['velocity'] = sample['velocity'].float().to(self.device)
        sample['angular_velocity'] = sample['angular_velocity'].float().to(self.device)
        sample['root_velocity'] = sample['root_velocity'].float().to(self.device)
        data = sample["position"]
        contact_data = sample["contact"]
        self.real_data = data
        data = torch.clone(data).float().detach_().to(self.device)
        contact_data = torch.clone(contact_data).float().detach_().to(self.device)
        motion_length = data.shape[1]

        # dim(batch_size, pose_dim), initial prior is a zero vector
        prior_vec = self.tensor_fill((data.shape[0], data.shape[2]), 0)

        log_dict = OrderedDict({'g_loss': 0})

        teacher_force = True if random.random() < tf_ratio else False
        losses = {}
        for lf in self.opt.loss_functions:
            losses[lf] = 0

        metrics = {'contact_accuracy': 0, 'spatial_error': 0}

        opt_step_cnt = 0

        # looping over motion_length-1 instead of motion_length because we need current hand position
        for i in range(0, motion_length-1):
            condition_vec = None
            if 'time_counter' in self.opt.input_types:
                time_counter = i / (motion_length - 1)
                time_counter_vec = self.tensor_fill((data.shape[0], 1), time_counter)
                condition_vec = self.append_to_condition_vec(time_counter_vec, condition_vec)
            if any(x in ['tracking3', 'tracking4'] for x in self.opt.input_types):
                tracked_points = extract_next_tracked_points(sample, i, self.opt, 'position')
                condition_vec = self.append_to_condition_vec(tracked_points, condition_vec)
            if any(x in ['vtracking3', 'vtracking4'] for x in self.opt.input_types):
                tracked_points = extract_next_tracked_points(sample, i, self.opt, 'velocity')
                condition_vec = self.append_to_condition_vec(tracked_points, condition_vec)
            if any(x in ['avtracking3', 'avtracking4'] for x in self.opt.input_types):
                tracked_points = sample['angular_velocity'][:, i, :]
                condition_vec = self.append_to_condition_vec(tracked_points, condition_vec)
            if 'root_velocity' in self.opt.input_types:
                condition_vec = self.append_to_condition_vec(sample["root_velocity"][:,i,:], condition_vec)

            # print(prior_vec.shape, condition_vec.shape)
            if condition_vec is not None:
                if self.opt.enable_last_p:
                    h = torch.cat((prior_vec, condition_vec), dim=1)
                else:
                    h = condition_vec
                h_target = torch.cat((data[:, i], condition_vec), dim=1)
            else:
                h = prior_vec
                h_target = data[:, i].clone()

            if self.opt.learning_mode == "vae":
                z_t, mu, logvar, h_in_p = posterior_net(h_target)
                _, mu_p, logvar_p, _ = prior_net(h)
            elif self.opt.learning_mode == "vae_fp":
                z_t, mu, logvar, h_in_p = prior_net(h)
            elif self.opt.learning_mode == "supervised":
                z_t, mu, logvar, h_in_p = prior_net(h)

            h_mid = torch.cat((h, z_t), dim=1)
            output = decoder(h_mid)
            x_pred = output["output"]
            contact = output["contact"]

            if i == motion_length - 2:
                opt_step_cnt += 1
                if "mse" in self.opt.loss_functions:
                    losses['mse'] += self.recon_criterion(x_pred, data[:, i], self.kinematic_chain, sample["bone_lengths"]) * self.opt.mse_weight
                if "rot" in self.opt.loss_functions:
                    gt_rots3x3 = sample["local_rot_mat"][:, i].float().to(self.device)
                    rots3x3 = output["output"][:, 3:].reshape((output["output"].shape[0], -1, 6))
                    rots3x3 = compute_rotation_matrix_from_ortho6d(rots3x3)
                    losses["rot"] += self.rot_loss(rots3x3, gt_rots3x3[:, 1:])
                if "contact" in self.opt.loss_functions:
                    losses["contact"] += self.contact_loss(contact, contact_data[:, i]) * self.opt.contact_weight
                if "velocity" in self.opt.loss_functions:
                    losses["velocity"] += self.velocity_loss(x_pred, data[:, i-1], data[:, i])
                if "kld" in self.opt.loss_functions:
                    if self.opt.learning_mode in ["vae", "vae_fp"]:
                        mu_p = torch.zeros_like(logvar)
                        logvar_p = torch.log(torch.ones_like(logvar) ** 2)

                    losses['kld'] += self.kl_criterion(mu, logvar, mu_p, logvar_p)

                x_pred_3d = forward_kinematics(self.device, x_pred, self.kinematic_chain, sample["bone_lengths"])["pos"]
                metrics['contact_accuracy'] += compute_contact_accuracy(contact, contact_data[:, i])
                metrics['spatial_error'] += compute_spatial_error(x_pred_3d, data[:, i])

            if teacher_force:
                prior_vec = x_pred
            else:
                prior_vec = data[:, i]

        for lf, value in losses.items():
            log_dict['g_' + lf + '_loss'] = value.item() / opt_step_cnt

        log_dict['contact_accuracy'] = metrics['contact_accuracy'] / opt_step_cnt
        log_dict['spatial_error'] = metrics['spatial_error'] / opt_step_cnt

        total_loss = 0
        for lf, value in losses.items():
            if lf == "kld":
                total_loss += value * self.opt.lambda_kld
            else:
                total_loss += value

        avg_loss = total_loss.item() / opt_step_cnt

        log_dict['g_loss'] = avg_loss

        return log_dict

    def initialize_optimizers(self, prior_net, posterior_net, decoder):
        self.opt_decoder = optim.Adam(decoder.parameters(), lr=self.opt.lr, betas=(0.9, 0.999), weight_decay=0.00001)
        self.opt_prior_net = optim.Adam(prior_net.parameters(), lr=self.opt.lr, betas=(0.9, 0.999), weight_decay=0.00001)
        self.opt_posterior_net = optim.Adam(posterior_net.parameters(), lr=self.opt.lr, betas=(0.9, 0.999), weight_decay=0.00001)
        if self.opt.step_lr:
            self.scheduler_decoder = optim.lr_scheduler.StepLR(self.opt_decoder, step_size=100000, gamma=0.5)
            self.scheduler_prior = optim.lr_scheduler.StepLR(self.opt_prior_net, step_size=100000, gamma=0.5)
            self.scheduler_posterior = optim.lr_scheduler.StepLR(self.opt_posterior_net, step_size=100000, gamma=0.5)

    def trainIters(self, prior_net, posterior_net, decoder, writer):
        self.initialize_optimizers(prior_net, posterior_net, decoder)

        prior_net.to(self.device)
        posterior_net.to(self.device)
        decoder.to(self.device)

        def save_model(file_name):
            state = {
                "prior_net": prior_net.state_dict(),
                "posterior_net": posterior_net.state_dict(),
                "decoder": decoder.state_dict(),
                "opt_prior_net": self.opt_prior_net.state_dict(),
                "opt_posterior_net": self.opt_posterior_net.state_dict(),
                "opt_decoder": self.opt_decoder.state_dict(),
                "iterations": iter_num
            }

            torch.save(state, os.path.join(self.opt.model_path, file_name + ".tar"))

        def load_model(file_name):
            model = torch.load(os.path.join(self.opt.model_path, file_name + '.tar'), map_location=self.device)
            prior_net.load_state_dict(model['prior_net'])
            posterior_net.load_state_dict(model['posterior_net'])
            decoder.load_state_dict(model['decoder'])
            self.opt_prior_net.load_state_dict(model['opt_prior_net'])
            self.opt_posterior_net.load_state_dict(model['opt_posterior_net'])
            self.opt_decoder.load_state_dict(model['opt_decoder'])


        if self.opt.is_continue and self.opt.isTrain:
            load_model('latest')

        iter_num = 0
        logs = OrderedDict()
        start_time = time.time()

        sample_train_batch = partial(self.sample_real_motion_batch, mode="train")
        sample_valid_batch = partial(self.sample_real_motion_batch, mode="valid")

        while True:
            if self.opt.scheduled_sampling:
                tf_start = 50000
                tf_end = 75000
                if iter_num < tf_start:
                    tf_ratio = 0.0
                elif iter_num < tf_end:
                    tf_ratio = (iter_num - tf_start) / (tf_end - tf_start)
                else:
                    tf_ratio = 1.0
            else:
                tf_ratio = self.opt.tf_ratio

            gen_log_dict = self.train(iter_num, prior_net, posterior_net, decoder, self.opt_prior_net, self.opt_posterior_net, self.opt_decoder, sample_train_batch, tf_ratio)

            for k, v in gen_log_dict.items():
                if k not in logs:
                    logs[k] = [v]
                else:
                    logs[k].append(v)

            iter_num += 1

            if iter_num % self.opt.print_every == 0:
                mean_loss = OrderedDict()
                for k, v in logs.items():
                    mean_loss[k] = sum(logs[k][-1 * self.opt.print_every:]) / self.opt.print_every

                loss_data = {'g_loss': mean_loss['g_loss']}
                for lf in self.opt.loss_functions:
                    loss_data['g_' + lf + '_loss'] = mean_loss['g_' + lf + '_loss']
                writer.add_scalars('loss', loss_data, iter_num)

                metric_data = {}
                for metric in ['contact_accuracy', 'spatial_error']:
                    metric_data[metric] = mean_loss[metric]

                writer.add_scalar('contact_accuracy', metric_data['contact_accuracy'], iter_num)
                writer.add_scalar('spatial_error', metric_data['spatial_error'], iter_num)
                print_current_loss(start_time, iter_num, self.opt.iters, mean_loss)

            if iter_num % self.opt.eval_every == 0:
                with torch.no_grad():
                    gen_log_dict = self.evaluate(iter_num, prior_net, posterior_net, decoder, self.opt_prior_net, self.opt_posterior_net, self.opt_decoder, sample_valid_batch, tf_ratio)

                mean_loss = OrderedDict()
                for k, v in logs.items():
                    mean_loss[k] = sum(logs[k][-1 * self.opt.print_every:]) / self.opt.print_every

                loss_data = {'g_loss': mean_loss['g_loss']}
                for lf in self.opt.loss_functions:
                    loss_data['g_' + lf + '_loss'] = mean_loss['g_' + lf + '_loss']
                writer.add_scalars('val_loss', loss_data, iter_num)

                metric_data = {}
                for metric in ['contact_accuracy', 'spatial_error']:
                    metric_data[metric] = mean_loss[metric]

                writer.add_scalar('val_contact_accuracy', metric_data['contact_accuracy'], iter_num)
                writer.add_scalar('val_spatial_error', metric_data['spatial_error'], iter_num)

            if iter_num % self.opt.save_every == 0:
                save_model(str(iter_num))

            if iter_num % self.opt.save_latest == 0:
                save_model('latest')

            if iter_num >= self.opt.iters:
                break
        return logs
