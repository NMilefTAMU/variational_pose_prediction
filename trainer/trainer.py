from abc import abstractmethod

class Trainer:
    '''
    Base trainer class
    '''
    def __init__(self):
        pass

    @abstractmethod
    def trainIters(self):
        pass