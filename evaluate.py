import argparse
from copy import deepcopy
import os
import random
import time
from typing import Dict, List
from eps.applications.ss_base import SampleSelectBase
from eps.applications.ss_foot_placement import SampleSelectJointPlacement
from eps.applications.ss_invalid_pose import SampleSelectInvalidPose
from models.inference.vae_gru import DecoderGRU6dFull4tp

import torch

import models.motion_vae as vae_models
import utils.paramUtil as paramUtil
from dataProcessing.dataset import *
from dataProcessing.dataset_mocap_lobstr import DatasetMocapLoBSTr
from dataProcessing.dataset_mocap_ours import DatasetMocapOurs
from dataProcessing.dataset_mocap_pfnn import DatasetMocapPFNN
from models.pose_network import PoseModelNetwork
from utils.check import assert_tensor_dims, assert_tensor_shape
from utils.get_opt import get_opt
from utils.pose_processing import (extract_next_tracked_points,
                                   extract_tracked_points)
from utils.pose_utils import world_to_local
from viewer.postprocess import ikSolver


class Inference():
    def __init__(self,
        dataset,
        experiment,
        motion_length=-1,
        sequence_names=[],
        dataset_mode="train",
        ik_mode=False,
        root_transform=True,
        save_z=False,
        lock_eps=False,
        smart_eps=1,
        disable_interpolation=[],
        waist_occlusion=False,
        use_eps_net=False,
        set_seed=None,
        selection_mode='fs',
        selection_mode_start_frame=0,
        selection_mode_passthrough=False,
        lock_sample=False,
        lookahead=10,
        choreography_file=None):

        if set_seed is not None:
            torch.manual_seed(set_seed)
            random.seed(set_seed)

        num_motions = 128
        dataset_opt_path = './checkpoints/vae/' + dataset + '/' + experiment + '/opt.txt'
        opt = get_opt(dataset_opt_path, num_motions, self.device)
        self.opt = opt
        self.ik_mode = ik_mode
        self.save_z = save_z
        self.root_transform = root_transform
        self.smart_eps = smart_eps
        self.lock_eps = lock_eps
        self.disable_interpolation = disable_interpolation
        self.waist_occlusion = waist_occlusion
        self.use_eps_net = use_eps_net
        self.selection_mode = selection_mode
        self.selection_mode_start_frame = selection_mode_start_frame
        self.selection_mode_passthrough = selection_mode_passthrough
        self.lock_sample = lock_sample
        self.lookahead=lookahead
        self.choreography_file = choreography_file

        interpolation_types = ["hidden", "eps", "last_p"]
        for x in disable_interpolation:
            if x not in interpolation_types:
                raise Exception("Interpolation types not in " + ", ".join(interpolation_types))

        # for IK
        self.interpolation_alpha = [[1.0]*2] * (self.smart_eps + 1)
        self.in_contact = [[False]*2] * (self.smart_eps + 1)
        self.last_target_toe_pos = [[None]*2] * (self.smart_eps + 1)

        if sequence_names:
            self.opt.sequence_names = sequence_names

        self.opt.motion_length = motion_length
        self.dataset = DatasetMocapOurs("./dataset/mocap_ours/output/", opt, mode=dataset_mode)

        # Create skeleton
        self.kinematic_chain = paramUtil.get_kinematic_chain(self.opt)
        self.bone_lengths = self.dataset[0]["bone_lengths"]
        self.bone_names = list(self.dataset.bone_map.keys())
        raw_offsets = paramUtil.get_raw_offsets(self.opt)
        self.raw_offsets = torch.from_numpy(raw_offsets).to(self.device).detach()

        num_tp = 0
        if 'tracking3' in self.opt.input_types:
            num_tp += 3
        if 'vtracking3' in self.opt.input_types:
            num_tp += 3
        if 'avtracking3' in self.opt.input_types:
            num_tp += 3
        if 'tracking4' in self.opt.input_types:
            num_tp += 4
        if 'vtracking4' in self.opt.input_types:
            num_tp += 4
        if 'avtracking4' in self.opt.input_types:
            num_tp += 4

        # Load models
        decoder_path = os.path.join(self.opt.model_path, self.opt.which_epoch + '.tar')
        wo_path = "./checkpoints/waist_occlusion/vae_model_0_1_vae.pth"

        self.decoder_sample = self.load_networks(self.smart_eps, decoder_path, wo_path)
        self.decoder_final = self.load_networks(1, decoder_path, wo_path)

        # for IK
        if self.selection_mode == "fs":
            self.ik_solver_sample = ikSolver(self.device, self.smart_eps, self.dataset.bone_map, ik_breakage=True, breakage_interpolation=True)
            self.ik_solver_final = ikSolver(self.device, 1, self.dataset.bone_map, ik_breakage=True, breakage_interpolation=True)
        else:
            self.ik_solver_sample = ikSolver(self.device, self.smart_eps, self.dataset.bone_map, ik_breakage=False, breakage_interpolation=True, clamp_foot=False)
            self.ik_solver_final = ikSolver(self.device, 1, self.dataset.bone_map, ik_breakage=False, breakage_interpolation=True, clamp_foot=False)

        self.sample_selection = None
        if self.selection_mode == "ip":
            self.sample_selection = SampleSelectInvalidPose(self.device, self.kinematic_chain, self.bone_lengths, self.bone_names, self.selection_mode_start_frame, self.selection_mode_passthrough)
        elif self.selection_mode == "jp":
            self.sample_selection = SampleSelectJointPlacement(choreography_file, DatasetMocapOurs)
        elif self.selection_mode == "none":
            self.sample_selection = SampleSelectBase()

    def load_networks(self, num_samples, decoder_path, wo_path):
        output = DecoderGRU6dFull4tp(
            self.opt.input_size + self.opt.dim_z,
            self.opt.output_size,
            self.opt.hidden_size,
            self.opt.decoder_hidden_layers,
            num_samples,
            self.kinematic_chain,
            self.bone_lengths,
            self.opt.learning_mode,
            self.device,
        )
        output.load(decoder_path)

        return output

    def smart_cat(self, x, y):
        if x is None:
            return y.clone()
        else:
            return torch.cat((x, y), dim=1)

    def get_sequence_name(self, index):
        return self.dataset.get_anim_names()[index]

    def get_num_sequences(self):
        return len(self.dataset.get_anim_names())

    def convert_to_world(self, local_frames, root_pos, root_rot):
        # convert to world
        temp_world_positions = local_frames.clone().cpu().detach().numpy()
        num_joints = local_frames.shape[-1] // 3

        if self.root_transform:
            #root_pos = self.dataset[index]["root_pos"][f:f+1]
            #root_rot = self.dataset[index]["root_rot"][f:f+1]
            world_positions = self.dataset.local_to_world(temp_world_positions, root_rot)
            world_positions += np.tile(root_pos, (1, num_joints))
            temp_world_positions = torch.from_numpy(world_positions)
        else:
            temp_world_positions = torch.from_numpy(temp_world_positions)

        return temp_world_positions

    def convert_to_local(self, local_frames, root_pos, root_rot):
        assert_tensor_shape(local_frames, [-1, 57])

        temp_local_positions = local_frames.clone().cpu().detach().numpy()
        num_joints = local_frames.shape[-1] // 3

        # convert to local
        if self.root_transform:
            #root_pos = self.dataset[index]["root_pos"][f:f+1]
            #root_rot = self.dataset[index]["root_rot"][f:f+1]
            temp_local_positions -= np.tile(root_pos, (1, num_joints))
            local_positions = self.dataset.world_to_local(temp_local_positions, root_rot)
            temp_local_positions = torch.from_numpy(local_positions)
        else:
            temp_local_positions = torch.from_numpy(local_positions)

        return temp_local_positions

    def blend_hidden_states(self, hs_0, hs_1, alpha):
        output = [None] * len(hs_0)
        for i in range(len(hs_0)):
            output[i] = ((1.0 - alpha) * hs_0[i].detach()) + (alpha * hs_1[i].detach())
        return output

    def blend_values(self, x, y, alpha):
        return ((1.0 - alpha) * x) + (alpha * y)
    
    def detach_hidden(self, hs):
        output = [None] * len(hs)
        for i in range(len(hs)):
            output[i] = hs[i].detach()
        return output

    def gt_sequence(self, index):
        '''
        Evaluate single frame

        Args:
            start_pose: input tensor

        Returns:
            next_frame: output_tensor
        '''
        positions = self.dataset[index]["position"]
        contact = self.dataset[index]["contact"]
        root_pos = self.dataset[index]["root_pos"]
        root_velocity = self.dataset[index]["root_velocity"]

        output = {}
        if self.root_transform:
            root_rot = self.dataset[index]["root_rot"]
            world_positions = self.dataset.local_to_world(positions, root_rot)
            world_positions += np.tile(root_pos, (1,19))
            output["position"] = torch.from_numpy(world_positions)
        else:
            local_positions = positions
            output["position"] = torch.from_numpy(local_positions)

        tracked_points = np.zeros((positions.shape[0], self.opt.num_tp * 3))
        for f in range(positions.shape[0]):
            sample = {}
            sample["position"] = output["position"]
            points = extract_tracked_points(sample, f, self.opt, "position")
            tracked_points[f, :] = points

        output["contact"] = torch.from_numpy(contact)
        output["root_pos"] = torch.from_numpy(root_pos)
        output["root_rot"] = torch.from_numpy(root_rot)
        output["root_velocity"] = torch.from_numpy(root_velocity)
        output["tracked_points"] = torch.from_numpy(tracked_points)

        return output

    def center_of_mass(self, output):
        l_toe_index = self.dataset.get_bone_index("LeftToeBase")
        r_toe_index = self.dataset.get_bone_index("RightToeBase")
        hip_index = self.dataset.get_bone_index("Hips")

        l_toe = output[:, l_toe_index*3:l_toe_index*3+3]
        r_toe = output[:, r_toe_index*3:r_toe_index*3+3]
        hip = output[:, hip_index*3:hip_index*3+3]

        center = (l_toe + r_toe) / 2

        radius = torch.sqrt((l_toe[:, 0]-r_toe[:, 0])**2 + (l_toe[:, 2]-r_toe[:, 2])**2) / 2
        diff = hip[:, [0,2]] - center[:, [0,2]]
        dist = torch.sqrt(diff[:, 0]**2 + diff[:, 1]**2)

        torch.clamp(dist - radius, min=0)

        return dist

    @staticmethod
    def ik_world_to_local(frame_output, sample, frame):
        batch_size = frame_output["position"].shape[0]
        device = frame_output["position"].device
        output = world_to_local(
            frame_output["position"].reshape(batch_size, -1, 3),
            None,
            sample["root_pos"][:, [frame]].float().to(device),
            sample["root_rot_mat"][:, [frame]].float().to(device),
        )
        return output["position"].reshape((batch_size, -1))

    @staticmethod
    def compute_manifold_spatial_error(manifold_pose, pose):
        error = torch.reshape(manifold_pose - pose, (-1, 19, 3))
        error = torch.norm(error, dim=-1)
        return error

    @staticmethod
    def compute_manifold_angular_error_vector(manifold_pose, pose, kinematic_tree):
        manifold_pose_vectors = Inference.get_vectors(manifold_pose, kinematic_tree)
        pose_vectors = Inference.get_vectors(pose, kinematic_tree)
        error = torch.acos(torch.sum(manifold_pose_vectors * pose_vectors, dim=-1))
        return error

    @staticmethod
    def compute_manifold_angular_error_angle(manifold_pose, pose, kinematic_tree):
        manifold_pose_angles = Inference.get_angles(manifold_pose, kinematic_tree)
        pose_angles = Inference.get_angles(pose, kinematic_tree)
        error = torch.abs(manifold_pose_angles[:, :, 0] - pose_angles[:, :, 0])
        return error

    @staticmethod
    def select_lower_body_error(error, bone_map, with_foot=False):
        bone_names = ["LeftUpLeg", "LeftLeg", "LeftFoot", "RightUpLeg", "RightLeg", "RightFoot"]
        if with_foot:
            bone_names.extend(["LeftToeBase", "RightToeBase"])
        indices = []
        for bone_name in bone_names:
            indices.append(bone_map[bone_name])

        new_error = torch.zeros_like(error)
        new_error[..., indices] = error[..., indices]
        return new_error

    @staticmethod
    def get_vectors(pose, kinematic_chain):
        pose_t = torch.reshape(pose, (-1, 19, 3))
        vectors = torch.zeros_like(pose_t)
        for chain in kinematic_chain:
            for i in range(len(chain)-1):
                parent_i = chain[i]
                child_i = chain[i + 1]
                parent = pose_t[:, parent_i, :]
                child = pose_t[:, child_i, :]
                diff = parent - child
                length = torch.norm(diff, dim=-1, keepdim=True)
                length[length==0] = 1
                diff = diff / length
                vectors[:, child_i, :] = diff
        return vectors

    @staticmethod
    def get_angles(pose, kinematic_chain):
        pose_t = torch.reshape(pose, (-1, 19, 3))
        angles = torch.zeros_like(pose_t)

        for chain in kinematic_chain:
            for i in range(1, len(chain)-1):                
                parent_i = chain[i - 1]
                joint_i = chain[i]
                child_i = chain[i + 1]

                parent = pose_t[:, parent_i, :]
                joint = pose_t[:, joint_i, :]
                child = pose_t[:, child_i, :]

                vec0 = parent - joint
                length0 = torch.norm(vec0, dim=-1, keepdim=True)
                length0[length0==0] = 1

                vec1 = child - joint
                length1 = torch.norm(vec1, dim=-1, keepdim=True)
                length1[length1==0] = 1

                vec0 = vec0 / length0
                vec1 = vec1 / length1

                dot = torch.sum(vec0 * vec1, dim=-1, keepdim=True)
                angle = torch.acos(dot)

                angles[:, joint_i, :] = angle

        return angles

    @staticmethod
    def compute_foot_skating_error(
        current_pose: torch.Tensor,
        last_pose: torch.Tensor,
        bone_map: Dict,
        H_value=0.1,
        height_penalty=False):

        '''
        Compute foot skating
        The equation for skating is from the MANN paper

        Args:
            current_pose: [N, 57]
            last_pose: [N, 57]
            bone_map: Dict of bone map
            H_value: floor height threshold used in MANN paper

        Returns:
            torch.Tensor: skating for left foot [N, 1]
            torch.Tensor: skating for right foot [N, 1]
        '''
        left_foot_index = bone_map["LeftToeBase"]
        right_foot_index = bone_map["RightToeBase"]

        c_left_foot = current_pose[:, left_foot_index*3:left_foot_index*3+3]
        c_right_foot = current_pose[:, right_foot_index*3:right_foot_index*3+3]
        l_left_foot = last_pose[:, left_foot_index*3:left_foot_index*3+3]
        l_right_foot = last_pose[:, right_foot_index*3:right_foot_index*3+3]

        v_left_foot = c_left_foot - l_left_foot
        v_right_foot = c_right_foot - l_right_foot

        v_left_foot_2d = v_left_foot[..., [0,2]]
        v_right_foot_2d = v_right_foot[..., [0,2]]

        m_v_left_foot = torch.norm(v_left_foot_2d, dim=-1, keepdim=True)
        m_v_right_foot = torch.norm(v_right_foot_2d, dim=-1, keepdim=True)

        h_left_foot = c_left_foot[..., [1]]
        h_right_foot = c_right_foot[..., [1]]

        exp_left_foot = torch.clamp(h_left_foot/ H_value, 0, 1)
        exp_right_foot = torch.clamp(h_right_foot / H_value, 0, 1)

        s_l = m_v_left_foot * (2 - torch.pow(2, exp_left_foot))
        s_r = m_v_right_foot * (2 - torch.pow(2, exp_right_foot))

        if height_penalty:
            s_l[h_left_foot >= H_value] = h_left_foot[h_left_foot >= H_value] - H_value + 1
            s_r[h_right_foot >= H_value] = h_right_foot[h_right_foot >= H_value] - H_value + 1

        return s_l, s_r

    @staticmethod
    def mark_error(error):
        # error shape: [N, 19]

        # threshold over 30 degrees
        temp_error = torch.clone(error)

        # hip error
        hip_threshold = 30
        temp_error[:, 11] = torch.clamp(temp_error[:, 11] / hip_threshold, 0, 1)
        temp_error[:, 15] = torch.clamp(temp_error[:, 17] / hip_threshold, 0, 1)

        # knee error
        knee_threshold = 30
        temp_error[:, 12] = torch.clamp(temp_error[:, 12] / knee_threshold, 0, 1)
        temp_error[:, 16] = torch.clamp(temp_error[:, 16] / knee_threshold, 0, 1)

        # foot error
        foot_threshold = 40
        temp_error[:, 13] = torch.clamp(temp_error[:, 13] / foot_threshold, 0, 1)
        temp_error[:, 17] = torch.clamp(temp_error[:, 17] / foot_threshold, 0, 1)

        temp_error = torch.floor(temp_error)
        return temp_error

    def get_bone_length(self, index, parent, child):
        return self.dataset.get_bone_length(index, parent, child)

    @staticmethod
    def eps_to_one_hot(
        num_samples: int,
        current_eps: int,
        target_eps: int,
        alpha: float
    ) -> torch.tensor:

        one_hot = torch.zeros((num_samples))
        one_hot[current_eps] = 1.0 - alpha
        one_hot[target_eps] = alpha

        return one_hot
