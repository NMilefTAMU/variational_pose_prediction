import math
import time

import numpy as np
import torch

from evaluate_batch import InferenceBatch
from utils.check import assert_tensor_dims
from utils.forward_kinematics import forward_kinematics
from viewer.postprocess import ikSolver

from models.pose_network import convert_to_full_body, extract_lower_body


class InferenceOnline(InferenceBatch):
    '''
    Online evaluate evaluates one frame at a time
    '''
    def __init__(self,
        dataset,
        experiment,
        motion_length=-1,
        sequence_names=[],
        dataset_mode="train",
        ik_mode=False,
        root_transform=True,
        save_z=False,
        lock_eps=False,
        smart_eps=1,
        disable_interpolation=[],
        waist_occlusion=False,
        use_eps_net=False,
        set_seed=None,
        selection_mode='fs',
        lock_sample=False,
        lookahead=0,
        choreography_file=None
    ):

        self.device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")

        super().__init__(
            dataset,
            experiment,
            motion_length,
            sequence_names,
            dataset_mode,
            ik_mode,
            root_transform,
            save_z,
            lock_eps,
            smart_eps,
            disable_interpolation,
            waist_occlusion,
            use_eps_net,
            self.device,
            set_seed=set_seed,
            selection_mode=selection_mode,
            lock_sample=lock_sample,
            lookahead=0,
            choreography_file=None)

        self.real_joints = torch.reshape(torch.from_numpy(self.dataset[0]["position"][0]), (1, 19, 3)).float().to(self.device)

        num_networks = 2

        current_eps = 0

        self.last_frames = [None] * num_networks
        self.last_frames[0] = torch.zeros((self.smart_eps, 57))
        self.last_frames[1] = torch.zeros((1, 57))

        self.last_world_frames = [None] * num_networks
        self.last_contact = [None] * num_networks
        self.eps_ratings = [0] * smart_eps
        self.eps_data = torch.zeros(1, self.smart_eps).to(self.device)

        if self.opt.learning_mode != "vae_6d_conv":
            self.prior_net[0].init_hidden(self.smart_eps)
            self.decoder[0].init_hidden(self.smart_eps)

        self.prior_net[0].select_new_eps(self.smart_eps, self.opt.dim_z)
        self.decoder[0].eval()
        self.prior_net[0].eval()

        if self.opt.learning_mode != "vae_6d_conv":
            self.prior_net[1].init_hidden(1)
            self.decoder[1].init_hidden(1)

        self.prior_net[1].select_new_eps(1, self.opt.dim_z)
        self.decoder[1].eval()
        self.prior_net[1].eval()

        self.prior_net[-1].set_eps(self.prior_net[current_eps].get_eps()[0:1])
        
        if self.opt.learning_mode == "vae_6d_conv":
            self.prior_net[1].init_hidden(1)
            self.decoder[1].init_hidden(1)

    def load_inference_networks(self):
        self.load_networks(2)

    def evaluate_frame(self, start_pose, tracked_points, root_velocity, bone_lengths, root_pos, root_rot, network_index=0):
        '''
        Evaluate single frame

        Args:
            current_frame: input tensor

        Returns:
            next_frame: output_tensor
        '''
        assert_tensor_dims(start_pose, 2)
        assert_tensor_dims(tracked_points, 2)

        decoder_net = self.decoder[network_index]
        prior_net = self.prior_net[network_index]
        decoder_net.eval()
        prior_net.eval()

        output = {}

        with torch.no_grad():
            if self.opt.enable_last_p:
                x = start_pose
            else:
                x = None

            if any(x in ["tracking3", "tracking4"] for x in self.opt.input_types):
                x = self.smart_cat(x, tracked_points)

            z_t_p, mu, logvar, _ = prior_net(x)
            h_mid = torch.cat((x, z_t_p), dim=1)
            d_output = decoder_net(h_mid)
            pred = d_output["output"]
            contact = d_output["contact"]

            output["pose"] = pred
            output["contact"] = contact
            output["mu"] = mu
            output["logvar"] = logvar

        return output

    def evaluate_frame_multisample(self, start_pose, tracked_points, root_velocity, bone_lengths, root_pos, root_rot, frame=-1, network_index=0):
        current_eps = 0
        last_eps_value, last_prior_hidden, last_decoder_hidden = 0, 0, 0
        target_eps_value, target_prior_hidden, target_decoder_hidden = 0, 0, 0
        current_toe_pose = None
        change_eps = False
        target_eps = 0
        self.eps_alpha = 1.0
        eps_window = 20

        num_networks = 2

        output = {}

        start_index = 1 if self.smart_eps == 1 else 0

        world_frames = [None] * num_networks
        frames = [None] * num_networks

        contact = [None] * num_networks
        t_contact = [None] * num_networks

        d_contact = [None] * num_networks
        breakages = [None] * num_networks
        stretch_factor = [0]

        for i in range(start_index, num_networks):
            if i == 0:
                output = self.evaluate_frame(self.last_frames[i], torch.repeat_interleave(tracked_points, self.smart_eps, dim=0), None, bone_lengths, root_pos, root_rot, network_index=i)
            else:
                output = self.evaluate_frame(self.last_frames[i], tracked_points, None, bone_lengths, root_pos, root_rot, network_index=i)

            frames[i] = output['pose'].clone()

            # forward kinematics
            if self.opt.model_type in ["vae_6d", "vae_moe"]:
                if i == 0:
                    bone_lengths_tensor = torch.from_numpy(bone_lengths).unsqueeze(0).repeat_interleave(frames[i].shape[0], dim=0)
                    pos = forward_kinematics(self.device, frames[i], self.kinematic_chain, bone_lengths_tensor)["pos"]
                    world_frames[i] = pos
                else:
                    bone_lengths_tensor = torch.from_numpy(bone_lengths).unsqueeze(0).repeat_interleave(frames[i].shape[0], dim=0)
                    pos = forward_kinematics(self.device, frames[i], self.kinematic_chain, bone_lengths_tensor)["pos"]
                    world_frames[i] = pos
        
            world_frames[i] = self.convert_to_world(world_frames[i], root_pos, root_rot.as_quat())

            contact[i] = output['contact'].clone()
            t_contact[i] = output['contact'].clone()
            if self.last_contact[i] is None:
                self.last_contact[i] = contact[i]
            d_contact[i] = torch.round(contact[i]) - torch.round(self.last_contact[i])
            self.last_contact[i] = contact[i]

            if self.ik_mode:
                output_frame = {}
                output_frame["position"] = world_frames[i]

                if self.eps_alpha < 1.0 and i == 1 and self.selection_mode== "pm":
                    output_frame["contact"] = torch.ones_like(contact[i])
                    self.ik_solver[i].in_contact = np.ones_like(self.ik_solver[i].in_contact)
                else:
                    output_frame["contact"] = contact[i]

                output_frame["contact"] = contact[i]
                ik_output = self.ik_solver[i].postprocess_ik(output_frame)
                world_frames[i] = ik_output["position"]
                breakages[i] = ik_output["breakage"]
                contact[i] = ik_output["contact"]
            else:
                stretch_factor.append(0)

        self.divergence_reason = None

        if self.use_eps_net:
            eps_ratings_index = 0 if self.smart_eps > 1 else 1
            local_frame = world_frames[eps_ratings_index]
            local_frame = self.convert_to_local(local_frame, root_pos, root_rot.as_quat())
            local_frame = extract_lower_body(local_frame)

            manifold_local_frame = self.eps_net(local_frame.to(self.device))

            # convert to full body pose
            manifold_local_frame = convert_to_full_body(manifold_local_frame)
            local_frame = convert_to_full_body(local_frame)

            manifold_angular_error = self.compute_manifold_angular_error_vector(
                manifold_local_frame,
                local_frame.to(self.device),
                self.kinematic_chain
            )

            manifold_angular_error = torch.acos(manifold_angular_error) * (180 / math.pi)
            lower_body_error = self.select_lower_body_error(manifold_angular_error, self.dataset.bone_map)
            eps_ratings = lower_body_error
            marked_eps_errors = self.mark_error(lower_body_error[current_eps:current_eps+1])

            if frame > 0:
                foot_skating_error = self.compute_foot_skating_error(
                    world_frames[eps_ratings_index].to(self.device),
                    self.last_world_frames[eps_ratings_index].to(self.device),
                    self.dataset.bone_map,
                    height_penalty=True)
            else:
                foot_skating_error = self.compute_foot_skating_error(
                    world_frames[eps_ratings_index],
                    world_frames[eps_ratings_index],
                    self.dataset.bone_map,
                    height_penalty=True)

        if self.smart_eps > 1 and self.use_eps_net:
            self.eps_alpha += 1.0 / eps_window
            self.eps_data[0, current_eps] = 1
            if self.eps_alpha <= 1.0:
                eps = ((1.0 - self.eps_alpha) * last_eps_value) + (self.eps_alpha * target_eps_value)

                if self.opt.learning_mode != "vae_6d_conv":
                    last_prior_hidden = self.get_hidden(self.prior_net[0].get_hidden(), current_eps)
                    last_decoder_hidden = self.get_hidden(self.decoder[0].get_hidden(), current_eps)
                    target_prior_hidden = self.get_hidden(self.prior_net[0].get_hidden(), target_eps)
                    target_decoder_hidden = self.get_hidden(self.decoder[0].get_hidden(), target_eps)

                    prior_hidden = self.blend_hidden_states(last_prior_hidden, target_prior_hidden, self.eps_alpha)
                    decoder_hidden = self.blend_hidden_states(last_decoder_hidden, target_decoder_hidden, self.eps_alpha)

                #frames[-1][-1] = self.convert_to_local(index, f, world_frames[-1][-1]).to(self.device)

                if "eps" not in self.disable_interpolation:
                    self.prior_net[-1].set_eps(eps)
                if "hidden" not in self.disable_interpolation:
                    self.prior_net[-1].set_hidden(prior_hidden)
                    self.decoder[-1].set_hidden(decoder_hidden)
                if "last_p" not in self.disable_interpolation:
                    frames[-1][-1] = self.blend_values(
                        frames[0][-1][current_eps:current_eps+1],
                        frames[0][-1][target_eps:target_eps+1],
                        self.eps_alpha)

                if self.selection_mode == "pm":
                    self.ik_solver[1].last_target_toe_pos = self.blend_values(
                        torch.reshape(world_frames[1][-1][:, [42,43,44,54,55,56]], (1, 2, 3)).detach().cpu().numpy(),
                        torch.reshape(world_frames[0][-1][target_eps:target_eps+1, [42,43,44,54,55,56]], (1, 2, 3)).detach().cpu().numpy(),
                        self.eps_alpha)

                    self.ik_solver[1].in_contact[:] = (self.ik_solver[0].in_contact[target_eps:target_eps+1] * 0) + 1
                    self.ik_solver[1].interpolation_alpha[:] = np.zeros_like(self.ik_solver[1].interpolation_alpha[:])

                self.eps_data[0, current_eps] = (1.0 - self.eps_alpha)
                self.eps_data[0, target_eps] = self.eps_alpha

            if self.eps_alpha >= 1.0 and change_eps:
                self.eps_alpha = 1.0
                if not self.lock_sample:
                    self.prior_net[0].select_new_eps_index(self.opt.dim_z, index=current_eps)

                self.prior_net[-1].set_eps(target_eps_value)
                if self.opt.learning_mode != "vae_6d_conv":
                    self.prior_net[-1].set_hidden(self.detach_hidden(self.get_hidden(self.prior_net[0].get_hidden(), target_eps)))
                    self.decoder[-1].set_hidden(self.detach_hidden(self.get_hidden(self.decoder[0].get_hidden(), target_eps)))

                self.eps_data[0, current_eps] = 0
                self.eps_data[0, target_eps] = 1
                current_eps = target_eps
                change_eps = False
                frames[-1][-1] = frames[0][-1][target_eps:target_eps+1]

                '''
                if self.selection_mode == "pm":
                    self.ik_solver[1].interpolation_alpha[:] = np.copy(self.ik_solver[0].interpolation_alpha[target_eps:target_eps+1])
                    self.ik_solver[1].in_contact[:] = (self.ik_solver[0].in_contact[target_eps:target_eps+1] * 0) + 1
                    self.ik_solver[1].last_target_toe_pos = torch.reshape(world_frames[0][target_eps:target_eps+1, [42,43,44,54,55,56]], (1,2,3)).detach().cpu().numpy()
                '''
                #self.ik_solver[1].breakage = np.zeros_like(self.ik_solver[1].breakage)

            elif self.eps_alpha >= 1.0 and self.detect_divergence(current_eps, eps_ratings, foot_skating_error, contact[-1][0], t_contact[-1][0]):
                selected_eps = self.select_eps(current_eps, eps_ratings, foot_skating_error, world_frames)
                if self.lock_sample:
                    target_eps = current_eps
                else:
                    target_eps = selected_eps["target_eps"]
                if target_eps != selected_eps:
                    eps_window = selected_eps["window_scale"]
                    last_eps_value = self.prior_net[0].get_eps()[current_eps:current_eps+1].detach()
                    target_eps_value = self.prior_net[0].get_eps()[target_eps:target_eps+1].detach()

                    '''
                    if self.selection_mode == "pm":
                        self.ik_solver[1].interpolation_alpha = np.zeros_like(self.ik_solver[1].interpolation_alpha)
                        self.ik_solver[1].last_target_toe_pos = torch.reshape(world_frames[1][:, [42,43,44,54,55,56]], (1,2,3)).detach().cpu().numpy()
                        self.ik_solver[1].in_contact = np.ones_like(self.ik_solver[1].in_contact)
                    '''

                    self.eps_alpha = 0.0
                    change_eps = True

                #frames[-1][-1] = self.convert_to_local(index, f, world_frames[-1][-1]).to(self.device)

                print("New EPS", target_eps, frame, self.divergence_reason)

        for i in range(start_index, num_networks):
            self.last_world_frames[i] = world_frames[i].clone()

        world_frames_final = world_frames[-1].to(self.device)

        i = 0

        output = {}
        positions = world_frames_final.cpu().detach().numpy()
        output["position"] = torch.from_numpy(positions).to(self.device)
        #output["local_position"] = torch.from_numpy(frames[-1]).to(self.device)
        output['local_position'] = output['position']
        #output["contact"] = torch.from_numpy(contact[-1], dim=0)
        #output["d_contact"] = torch.cat(d_contact[-1], dim=0)
        output["root_pos"] = torch.from_numpy(root_pos)
        output["root_rot"] = torch.from_numpy(root_rot.as_quat())
        output['tracked_points'] = self.convert_to_world(tracked_points[:, :12], root_pos, root_rot.as_quat())
        output['eps'] = self.eps_data.clone()
        #output["root_velocity"] = root_velocity[0]

        return output

    def extract_conv_inputs(self, tracked_points, frame, num_samples):
        num_frames = self.prior_net[0].num_frames
        indices = list(range(max(frame - num_frames, 0), frame))

        if len(indices) < num_frames:
            diff_len = num_frames - len(indices)
            padding = [0] * diff_len
            indices = padding + indices

        tp = []
        for i in range(num_frames):
            tp.append(tracked_points[indices[i]].clone())

        output = torch.reshape(torch.cat(tp), (1, -1))
        output = torch.repeat_interleave(output, num_samples, dim=0)

        return output
