import argparse
import os

import torch
import torch.nn as nn
import torch.optim as optim
from torch.utils.data import DataLoader
from torch.utils.data import WeightedRandomSampler

from dataProcessing.dataset_mocap_pfnn import DatasetMocapPFNN
from models.gating.gating_network import GatingNetwork
from options.options import Options
from utils import paramUtil
from utils.pose_processing import extract_tracked_points_sequence

class SoftCrossEntropyLoss(nn.Module):
    def __init__(self):
        super(SoftCrossEntropyLoss, self).__init__()

    def forward(self, x, y):
        log_probs = torch.log_softmax(x, dim=1)
        return -(y * log_probs).sum() / x.shape[0]

def train(args):
    opt = Options()
    opt.motion_length = 45
    opt.dataset_type = "mocap_pfnn"
    opt.model_type = "vae_6d"
    opt.normalize_inputs = True
    opt.input_types = ["tracking4"]
    dataset_params = paramUtil.get_dataset_params(opt)

    # train dataset
    dataset_train = DatasetMocapPFNN("./dataset/mocap_pfnn/output/", opt, mode="train")
    data_train_sampler = WeightedRandomSampler(dataset_train.get_sample_weights(), len(dataset_train), replacement=True)
    data_train_loader = DataLoader(dataset_train, int(args.batch_size), drop_last=True, sampler=data_train_sampler)

    # validation dataset
    dataset_valid = DatasetMocapPFNN("./dataset/mocap_pfnn/output/", opt, mode="valid")
    data_valid_sampler = WeightedRandomSampler(dataset_valid.get_sample_weights(), len(dataset_valid), replacement=True)
    data_valid_loader = DataLoader(dataset_valid, int(args.batch_size), drop_last=True, sampler=data_valid_sampler)

    if any(x in ["tracking4", "vtracking4"] for x in opt.input_types):
        network = GatingNetwork(12, 128, 2, 8, args.batch_size)
    elif any(x in ["tracking3", "vtracking3"] for x in opt.input_types):
        network = GatingNetwork(9, 128, 2, 8, args.batch_size)

    loss_function = SoftCrossEntropyLoss()
    optimizer = optim.Adam(network.parameters(), lr=0.0002, betas=(0.5, 0.999), weight_decay=0.00001)

    def compute_accuracy(num_batches, mode):
        accuracy = 0
        if mode == "train":
            data_loader = data_train_loader
        elif mode == "valid":
            data_loader = data_valid_loader

        with torch.no_grad():
            for i in range(num_batches):
                network.init_hidden(args.batch_size)

                sample = next(iter(data_loader))
                input_points = extract_tracked_points_sequence(sample, opt)

                optimizer.zero_grad()

                output = network(input_points.float())

                target = sample["gait"].float()[:, -1]
                #loss = loss_function(output, target)

                eval_labels = torch.argmax(output, dim=1)
                target_labels = torch.argmax(target, dim=1)
                count = float((eval_labels == target_labels).sum())
                accuracy += count / (output.shape[0] * num_batches)

        return accuracy

    iterations = 0
    num_iters_update = 1000
    accuracy = 0
    while iterations < int(args.num_iters):
        network.init_hidden(args.batch_size)

        sample = next(iter(data_train_loader))
        input_points = extract_tracked_points_sequence(sample, opt)

        optimizer.zero_grad()

        output = network(input_points.float())

        target = sample["gait"].float()[:, -1]
        loss = loss_function(output, target)

        loss.backward()
        optimizer.step()

        # calculate accuracy
        if iterations % num_iters_update == 0:
            os.makedirs("checkpoints/gating/", exist_ok=True)
            torch.save(network.state_dict(), "checkpoints/gating/" + opt.dataset_type + ".pth")
            print(
                "Iterations:", iterations,
                "\tLoss:", float(loss),
                "\tTrain Accuracy:", compute_accuracy(100, "train"),
                "\tValid Accuracy:", compute_accuracy(100, "valid"))

        iterations += 1


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--num_iters", default=50000)
    parser.add_argument("--batch_size", default=128)
    args = parser.parse_args()

    train(args)
