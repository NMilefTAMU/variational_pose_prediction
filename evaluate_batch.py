import cProfile
import copy
import math
import time
from typing import Dict, List, Tuple

import numpy as np
from models.inference.vae_base import VAEFull
from models.root_pred.occlusion_network_vae import OcclusionNetVAE
import torch
from scipy.spatial.transform import Rotation

import models.motion_vae as vae_models
from utils.forward_kinematics import forward_kinematics, forward_kinematics_fast
from utils.pose_utils import world_to_local
from utils.pytorch_utils import dict_extract_frame, dict_np_to_torch, dict_unsqueeze, list_dict_extract_tensor_torch
import viewer.heuristics as heuristics
from dataProcessing.dataset_mocap import DatasetMocap
from evaluate import Inference
from models.pose_network import convert_to_full_body, extract_lower_body
from models.root_pred.occlusion_network import OcclusionNet
from utils.check import assert_tensor_dims
from utils.pose_processing import extract_next_tracked_points, extract_tracked_points
from viewer.postprocess import ikSolver


class InferenceBatch(Inference):
    '''
    A more efficient class for a large number of samples (GPU-based)
    '''
    def __init__(self,
        dataset,
        experiment,
        motion_length=-1,
        sequence_names=[],
        dataset_mode="train",
        ik_mode=False,
        root_transform=True,
        save_z=False,
        lock_eps=False,
        smart_eps=1,
        disable_interpolation=[],
        waist_occlusion=False,
        use_eps_net=False,
        device=None,
        set_seed=None,
        selection_mode='fs',
        selection_mode_start_frame=0,
        selection_mode_passthrough=False,
        lock_sample=False,
        lookahead=0,
        choreography_file=None):

        if device is None:
            self.device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
        else:
            self.device = device

        super().__init__(
            dataset,
            experiment,
            motion_length,
            sequence_names,
            dataset_mode,
            ik_mode,
            root_transform,
            save_z,
            lock_eps,
            smart_eps,
            disable_interpolation,
            waist_occlusion,
            use_eps_net,
            set_seed=set_seed,
            selection_mode=selection_mode,
            selection_mode_start_frame=selection_mode_start_frame,
            selection_mode_passthrough=selection_mode_passthrough,
            lock_sample=lock_sample,
            lookahead=lookahead,
            choreography_file=choreography_file)

    def get_sample(self, index):
        sample_torch = dict_np_to_torch(self.dataset[index])
        sample = dict_unsqueeze(sample_torch)
        return sample

    def evaluate_sequence(self, index):
        #pr = cProfile.Profile()
        #pr.enable()

        # extract single animation
        sample = self.get_sample(index)

        num_frames = sample["position"].shape[1]

        # current epsilon value
        current_eps = 0
        target_eps = 0
        eps_alpha = 1.0
        eps_window = 10

        # reset both networks (epsilon and hidden state)
        self.decoder_sample.reset()
        self.decoder_final.reset()

        # initialize second network to first current_eps value
        if isinstance(self.decoder_final, VAEFull):
            self.decoder_final.set_eps(self.decoder_sample.get_eps()[0:1])

        start_time = time.time() * 1000

        output_frames_sample = []
        output_frames_final = []

        eps_data = []

        target_eps_ik_state = None

        # f is frame for the sample timeline
        # (f - self.lookahead) is for the final delayed timeline
        for f in range(num_frames + self.lookahead):
            if f < num_frames:
                # Pass 1: process samples
                if self.smart_eps > 1:
                    frame_output = self.decoder_sample.evaluate_frame(sample, f)
                    if self.ik_mode:
                        frame_output = self.ik_solver_sample.postprocess_ik(frame_output)
                    frame_output["local_position"] = Inference.ik_world_to_local(frame_output, sample, f)
                    output_frames_sample.append(copy.deepcopy(frame_output))

            if f - self.lookahead >= 0:
                if f < num_frames - self.lookahead and self.smart_eps > 1:
                    eps_alpha = np.clip(eps_alpha + (1.0 / eps_window), 0.0, 1.0)

                    if eps_alpha >= 1.0:
                        current_eps = target_eps
                    if eps_alpha >= 1.0 and self.sample_selection.detect_error(output_frames_sample[f], current_eps, f):
                        target_eps = self.sample_selection.select_sample(output_frames_sample[f], current_eps, f)
                        eps_alpha = 0.0
                        target_eps_ik_state = self.ik_solver_sample[target_eps]

                    self.decoder_final.interpolate(
                        output_frames_sample[f - self.lookahead],
                        current_eps,
                        target_eps,
                        eps_alpha,
                    )

                one_hot = self.eps_to_one_hot(self.smart_eps, current_eps, target_eps, eps_alpha)
                eps_data.append(one_hot)

                # Pass 2: process final
                frame_output = self.decoder_final.evaluate_frame(sample, f - self.lookahead)
                #self.ik_solver_final.interpolate(frame_output, target_eps_ik_state, eps_alpha)
                if self.ik_mode:
                    frame_output = self.ik_solver_final.postprocess_ik(frame_output)
                frame_output["local_position"] = Inference.ik_world_to_local(frame_output, sample, f - self.lookahead)
                output_frames_final.append(copy.deepcopy(frame_output))

        output = {}
        output["local_position"] = list_dict_extract_tensor_torch(output_frames_final, "local_position")
        output["position"] = list_dict_extract_tensor_torch(output_frames_final, "position")
        output["contact"] = list_dict_extract_tensor_torch(output_frames_final, "contact")
        output["root_pos"] = torch.from_numpy(self.dataset[index]["root_pos"][1:])
        output["root_rot"] = torch.from_numpy(self.dataset[index]["root_rot"][1:])
        output["root_velocity"] = torch.from_numpy(self.dataset[index]["root_velocity"][1:])
        output["eps"] = torch.stack(eps_data, dim=0)

        if self.use_eps_net:
            output["eps_ratings"] = torch.cat(eps_ratings, dim=0)
            output["marked_eps_errors"] = torch.cat(marked_eps_errors, dim=0)

        #pr.disable()
        #pr.dump_stats("perf.log")

        return output

    def apply_inv_rot(self, x, inv_rot):
        x_np = inv_rot.apply(x.detach().cpu().numpy())
        return torch.from_numpy(x_np).float().to(self.device)

    def get_hidden(self, hidden_state, index):
        output = [None] * len(hidden_state)
        for i in range(len(hidden_state)):
            output[i] = hidden_state[i][index:index+1]
        return output