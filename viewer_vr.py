import csv
import os
import time

from direct.gui.OnscreenText import OnscreenText
from evaluate_online import InferenceOnline
from panda3d.core import *

import numpy as np
import openvr
import torch
from direct.task import Task

from utils.vr.calibrate import get_vr_pose_scale
from utils.vr.filter import PointFilter, RotFilter
from utils.vr.utils import (RecordedVRReader, convert_mat_to_numpy, data_to_np,
                            get_class_name, get_class_role, get_next_vr_frame,
                            preprocess_points)
from viewer.lobstr_reader import read_lobstr_results
from viewer.skeleton import Skeleton
from viewer.skeleton_lobstr import SkeletonLoBSTr
from viewer_base import ViewerArgumentParser, ViewerBaseOnline


class PosePredictionAppVR(ViewerBaseOnline):
    def __init__(self, args):
        args.inference_mode = "vr"
        args.sequence_names = ["05_01"] # needed for bone lengths

        super().__init__(args, torch.device("cuda"))

        self.inference = []
        for i in range(self.num_gen):
            self.inference.append(InferenceOnline(
                args.dataset,
                args.experiment,
                sequence_names=args.sequence_names,
                dataset_mode=args.dataset_type,
                ik_mode=(not args.disable_ik),
                root_transform=(not args.disable_root_transform),
                save_z=args.save_z,
                lock_eps=args.lock_eps,
                smart_eps=args.smart_eps,
                disable_interpolation=args.disable_interpolation,
                waist_occlusion=True if args.num_tp == 3 else False,
                use_eps_net=args.use_eps_net,
                set_seed=args.set_seed,
                selection_mode=args.selection_mode,
                lock_sample=args.lock_sample,
                choreography_file=args.choreography)
            )

        self.recorded_tp = None

        # parameters for recording
        self.save_interval = args.save_interval
        self.startup_delay = args.startup_delay
        self.last_save_time = self.startup_delay
        self.cache = args.cache

        if self.cache:
            self.ours_cached = []
            for i in range(self.num_gen):
                self.ours_cached.append({
                    'position': {},
                    'eps': {},
                    'tracked_points': {},
                })

        if args.recorded_tp is not None:
            print("Using pre-recorded file")
            waist_occlusion = True if args.num_tp == 3 else False
            self.recorded_tp = RecordedVRReader(args.recorded_tp, waist_occlusion)
            self.calibration_tp = RecordedVRReader(args.calibration_tp, waist_occlusion)
            for i in range(60):
                cal_pose = self.calibration_tp.get_next_vr_frame_recorded(i, args.num_tp)
            self.vr_scale_params = get_vr_pose_scale(cal_pose)
        else:
            self.vr_scale_params = {}
            self.vr_scale_params["height_scale"] = 0.8
            self.vr_scale_params["waist_offset"] = 0.1

        self.dir_name = str(int(time.time()*1000))

        self.current_frame = 0
        self.last_frame_time = 0
        self.fps = 0
        self.last_fps_update_time = 0

        self.system = None
        if args.recorded_tp is None:
            self.system = openvr.init(openvr.VRApplication_Scene)
        self.poses = []
        self.play = True
        self.enable_tracked_points = True

        self.skeleton_ours = []
        for i in range(self.num_gen):
            self.skeleton_ours.append(Skeleton(self, self.inference[i].opt, self.num_joints, color=(1, 0, 0), offset=(0, 0, 0)))
            self.skeleton_ours[-1].show_tracked_points()

        if "lobstr" in self.methods:
            filename = args.recorded_tp.split("/")[-1].split(".npy")[0]
            session_name = args.recorded_tp.split("/")[-2]
            filename = session_name + "_" + filename
            lobstr_root_pos = torch.from_numpy(self.recorded_tp.data[:, 0, :, 3].copy())
            lobstr_root_pos[:, 0] *= -1
            self.lobstr_sequence = read_lobstr_results(self.device, filename, 0, self.inference[0], lobstr_root_pos, (not args.disable_ik), ik_breakage=False, lobstr_dataset=args.lobstr_dataset)
            self.lobstr_sequence['position'] = self.lobstr_sequence['position'][:-1]
            self.lobstr_sequence['position'][:, 0::3] *= -1
            self.lobstr_sequence['contact'] = self.lobstr_sequence['contact'][:-1]
            lobstr_k_chain = {7:0, 8:7, 9:8, 17:9, 4:0, 5:4, 6:5, 18:6, 10:0, 11:10, 16:11, 1:16, 12:11, 13:12, 2:13, 14:11, 15:14, 3:15}
            self.skeleton_lobstr = SkeletonLoBSTr(self, self.inference[0].opt, self.lobstr_sequence["position"].shape[-1] // 3, color=(1, 0, 0), offset=(3.0, 0, 0), k_chain=lobstr_k_chain)

        self.last_data = None
        self.last_pose = []
        for i in range(self.num_gen):
            self.last_pose.append(torch.zeros((1, 57)).to(self.device))

        self.initial_rot = None
        self.bone_lengths = self.inference[0].dataset.bone_lengths[0]
        self.num_tp = 4

        if args.filter_root:
            self.rot_filter = RotFilter(plot_data=args.plot_filter)
        else:
            self.rot_filter = None

        if args.filter_points:
            self.pos_filters = []
            for i in range(4):
                self.pos_filters.append(PointFilter(plot_data=args.plot_filter))
        else:
            self.pos_filters = None

        globalClock.setMode(ClockObject.MLimited)
        globalClock.setFrameRate(args.framerate)

        self.task_mgr.add(self.update, "Update Task")

        self.accept('t', self.toggle_tracked_points)

        self.setupGUI()

    def toggle_tracked_points(self):
        self.enable_tracked_points = not self.enable_tracked_points

        if self.enable_tracked_points:
            self.skeleton.show_tracked_points()
        else:
            self.skeleton.hide_tracked_points()

    def setupGUI(self):
        offset = -0.05
        self.title = OnscreenText(
            text="VR Viewer Program",
            parent=self.a2dTopLeft,
            pos=(0, offset*1),
            scale=0.05,
            fg=(1,1,1,1),
            bg=(0, 0, 0, 0.5),
            align=TextNode.ALeft)

        self.frame_info = OnscreenText(
            text="",
            parent=self.a2dTopLeft,
            pos=(0, offset*2),
            scale=0.05,
            fg=(1,1,1,1),
            bg=(0, 0, 0, 0.5),
            align=TextNode.ALeft)

        self.fps_info = OnscreenText(
            text="",
            parent=self.a2dTopLeft,
            pos=(0, offset*3),
            scale=0.05,
            fg=(1,1,1,1),
            bg=(0, 0, 0, 0.5),
            align=TextNode.ALeft)

        self.anim_time_info = OnscreenText(
            text="",
            parent=self.a2dTopLeft,
            pos=(0, offset*4),
            scale=0.05,
            fg=(1,1,1,1),
            bg=(0, 0, 0, 0.5),
            align=TextNode.ALeft)

    def update_text_info(self):
        self.frame_info.setText("Frame: " + str(self.frame))
        self.fps_info.setText("FPS: " + str(self.fps))
        self.anim_time_info.setText("Anim Time: " + "{:0.2f}".format(self.frame / self.framerate))

    def update(self, task):
        # process joints
        if self.recorded_tp:
            data = self.recorded_tp.get_next_vr_frame_recorded(self.frame, self.num_tp)
        else:
            data, self.poses = get_next_vr_frame(self.system, self.poses)

        #if self.initial_rot is None:
            #self.initial_rot = get_initial_rot_offset(data, self.num_tp)

        data = preprocess_points(data, self.last_data, self.num_tp, self.initial_rot, self.vr_scale_params, filter=self.rot_filter, pos_filters=self.pos_filters)
        tracked_points = torch.from_numpy(data["input"][:, :]).float().to(self.device)

        if self.follow_camera:
            self.update_camera(data['root_pos'][0])

        for i in range(self.num_gen):
            if self.cache and self.frame in self.ours_cached[i]['position']:
                output = {
                    'position': self.ours_cached[i]['position'][self.frame],
                    'eps': self.ours_cached[i]['eps'][self.frame],
                    'tracked_points': self.ours_cached[i]['tracked_points'][self.frame],
                }
            else:
                output = self.inference[i].evaluate_frame_multisample(
                    self.last_pose,
                    tracked_points,
                    None,
                    self.bone_lengths,
                    data["root_pos"],
                    data["root_rot"],
                    frame=self.frame)

                self.last_pose = output["local_position"]

            if self.cache and self.frame not in self.ours_cached:
                self.ours_cached[i]['position'][self.frame] = output['position']
                self.ours_cached[i]['eps'][self.frame] = output['eps']
                self.ours_cached[i]['tracked_points'][self.frame] = output['tracked_points']


            #self.skeleton_ours[i].render_tracked_points(output["tracked_points"][0, :3 * self.num_tp])

            root_pos = tracked_points[0][0:3]

            #self.skeleton_ours[i].render_root_axes(data["root_rot"], root_pos)

            self.skeleton_ours[i].render_frame(output["position"])

        if "lobstr" in self.methods:
            self.skeleton_lobstr.render_frame(None, self.lobstr_sequence['position'][self.frame, :])

        self.last_data = data

        self.frame += 1

        if self.rot_filter is not None:
            if self.recorded_tp is not None and self.frame % len(self.recorded_tp) == 0:
                self.rot_filter.reset()

        if self.frame >= len(self.recorded_tp):
            self.frame = self.frame % len(self.recorded_tp)

        self.update_text_info()

        return Task.cont


if __name__ == '__main__':
    parser = ViewerArgumentParser()
    parser.parser.add_argument("--recorded_tp", help="recorded tracked points", default=None)
    parser.parser.add_argument("--calibration_tp", type=str, help="sequence name for calibration")
    parser.parser.add_argument("--startup_delay", type=int, help="startup delay by number of frames", default=400)
    parser.parser.add_argument("--save_interval", type=int, help="number of frames to record before saving off the file", default=1000)
    parser.parser.add_argument("--filter_root", action="store_true")
    parser.parser.add_argument("--filter_points", action="store_true")
    parser.parser.add_argument("--plot_filter", action="store_true")
    parser.parser.add_argument("--cache", action="store_true", help="preprocess and cache data")
    args = parser.parse_args()

    app = PosePredictionAppVR(args)
    app.run()
