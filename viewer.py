import random
import time
from evaluate_batch_vr import InferenceBatchVR

import torch
from direct.gui.OnscreenText import OnscreenText
from direct.task import Task
from panda3d.core import *

from evaluate_batch import InferenceBatch
from viewer.lobstr_reader import read_lobstr_results
from viewer.sequence_file_io import save_data
from viewer.skeleton import Skeleton
from viewer.skeleton_lobstr import SkeletonLoBSTr
from viewer.stats_display import EPSDisplay, EPSRatingsDisplay, GateDisplay
from viewer_base import ViewerArgumentParser, ViewerBaseOnline


def set_method(map, method, select_methods):
    if method in select_methods:
        map[method] = True
    else:
        map[method] = False

class PosePredictionApp(ViewerBaseOnline):
    def __init__(self, args):
        super().__init__(args)

        if args.vr:
            self.inference = InferenceBatchVR(
                args.dataset,
                args.experiment,
                sequence_names=args.sequence_names,
                dataset_mode=args.dataset_type,
                ik_mode=(not args.disable_ik),
                root_transform=(not args.disable_root_transform),
                save_z=args.save_z,
                lock_eps=args.lock_eps,
                smart_eps=args.smart_eps,
                disable_interpolation=args.disable_interpolation,
                waist_occlusion=True if args.num_tp == 3 else False,
                use_eps_net=args.use_eps_net,
                set_seed=args.set_seed,
                selection_mode=args.selection_mode,
                selection_mode_passthrough=args.sm_passthrough,
                selection_mode_start_frame=args.sm_start_frame,
                lock_sample=args.lock_sample,
                lookahead=args.lookahead,
                choreography_file=args.choreography,
                recorded_tp=args.recorded_tp,
                calibration_tp=args.calibration_tp,
            )
        else:
            self.inference = InferenceBatch(
                args.dataset,
                args.experiment,
                sequence_names=args.sequence_names,
                dataset_mode=args.dataset_type,
                ik_mode=(not args.disable_ik),
                root_transform=(not args.disable_root_transform),
                save_z=args.save_z,
                lock_eps=args.lock_eps,
                smart_eps=args.smart_eps,
                disable_interpolation=args.disable_interpolation,
                waist_occlusion=True if args.num_tp == 3 else False,
                use_eps_net=args.use_eps_net,
                set_seed=args.set_seed,
                selection_mode=args.selection_mode,
                selection_mode_passthrough=args.sm_passthrough,
                selection_mode_start_frame=args.sm_start_frame,
                lock_sample=args.lock_sample,
                lookahead=args.lookahead,
                choreography_file=args.choreography,
            )

        self.current_sequence_index = 0
        self.current_sequence_name = ""

        self.vr_mode = args.vr

        self.eps_net_debug = False
        self.view_eps_graph = True

        self.enable_tracked_points = False
        self.enabled_animations = {}
        set_method(self.enabled_animations, 'ours', args.methods)
        set_method(self.enabled_animations, 'gt', args.methods)
        set_method(self.enabled_animations, 'lobstr', args.methods)
        self.lobstr_dataset = args.lobstr_dataset

        self.set_current_sequence_index(self.current_sequence_index)

        self.generated_sequence = []
        self.set_animation(self.current_sequence_index)
        if args.save_npz:
            save_data(self.generated_sequence[0], args)

        self.accept('arrow_up', self.increase_playback_speed)
        self.accept('arrow_down', self.decrease_playback_speed)
        self.accept('d', self.toggle_debug)
        self.accept('l', self.toggle_bl_debug)
        self.accept('t', self.toggle_tracked_points)
        self.accept('c', self.toggle_contact)


        if self.enabled_animations["ours"]:
            self.skeleton_generated = []
            for i in range(self.num_gen):
                self.skeleton_generated.append(Skeleton(self, self.inference.opt, self.num_joints, color=(1, 0, 0), offset=(0, 0, 0)))
        if self.enabled_animations["gt"]:
            y_offset = 0
            if self.vr_mode:
                y_offset = 100
            self.skeleton_gt = Skeleton(self, self.inference.opt, self.num_joints, color=(1, 0, 0), offset=(-3.0, y_offset, 0))
        if self.enabled_animations["lobstr"]:
            lobstr_k_chain = {7:0, 8:7, 9:8, 17:9, 4:0, 5:4, 6:5, 18:6, 10:0, 11:10, 16:11, 1:16, 12:11, 13:12, 2:13, 14:11, 15:14, 3:15}
            self.skeleton_lobstr = SkeletonLoBSTr(self, self.inference.opt, self.lobstr_sequence["position"].shape[-1] // 3, color=(1, 0, 0), offset=(3.0, 0, 0), k_chain=lobstr_k_chain)

        self.setupGUI()
        if self.enabled_animations["ours"] and self.inference.opt.model_type == "vae_moe":
            self.gateStats = GateDisplay(self)
        if args.smart_eps > 1 and self.view_eps_graph:
            self.epsStats = EPSDisplay(self, args.smart_eps)
        #if args.use_eps_net:
            #self.epsRatings = EPSRatingsDisplay(self, args.smart_eps)

        self.task_mgr.add(self.update, "Update Task")

    def set_current_sequence_index(self, index):
        self.current_sequence_index = index
        self.frame = 0
        self.current_sequence_name = self.inference.get_sequence_name(self.current_sequence_index).split(".npy")[0]

    def set_animation(self, index):
        self.set_current_sequence_index(index)

        self.gt_sequence = self.inference.gt_sequence(self.current_sequence_index)
        self.num_frames = self.gt_sequence['position'].shape[0] - 1

        if self.enabled_animations["lobstr"]:
            self.lobstr_sequence = read_lobstr_results(self.device, self.current_sequence_name, self.current_sequence_index, self.inference, self.gt_sequence['position'][:, 0:3], (not args.disable_ik), lobstr_dataset=self.lobstr_dataset, pad_beginning=True)

        if self.enabled_animations["ours"]:
            self.generated_sequence.clear()
            for i in range(self.num_gen):
                generated_sequence = self.inference.evaluate_sequence(self.current_sequence_index)

                if self.vr_mode:
                    generated_sequence["position"][:, ::3] *= 1

                self.generated_sequence.append(generated_sequence)

        if self.follow_camera and not self.vr_mode:
            self.update_camera(self.gt_sequence["tracked_points"][0][0:3])
        elif self.follow_camera:
            self.update_camera(self.generated_sequence[0]["position"][0, 0:3].detach().cpu())

    def toggle_debug(self):
        self.debug = not self.debug

        if self.debug:
            if self.enabled_animations['ours']:
                for gen_skeleton in self.skeleton_generated:
                    gen_skeleton.show_debug_text()
            if self.enabled_animations['gt']:
                self.skeleton_gt.show_debug_text()
            if self.enabled_animations['lobstr']:
                self.skeleton_lobstr.show_debug_text()
        else:
            if self.enabled_animations['ours']:
                for gen_skeleton in self.skeleton_generated:
                    gen_skeleton.hide_debug_text()
            if self.enabled_animations['gt']:
                self.skeleton_gt.hide_debug_text()
            if self.enabled_animations['lobstr']:
                self.skeleton_lobstr.hide_debug_text()

    def toggle_bl_debug(self):
        self.debug_bl = not self.debug_bl
        print("toggle_bl")

        if self.debug_bl:
            if self.enabled_animations['ours']:
                for gen_skeleton in self.skeleton_generated:
                    gen_skeleton.show_bl_text()
            if self.enabled_animations['gt']:
                self.skeleton_gt.show_bl_text()
            if self.enabled_animations['lobstr']:
                self.skeleton_lobstr.show_bl_text()
        else:
            if self.enabled_animations['ours']:
                for gen_skeleton in self.skeleton_generated:
                    gen_skeleton.hide_bl_text()
            if self.enabled_animations['gt']:
                self.skeleton_gt.hide_bl_text()
            if self.enabled_animations['lobstr']:
                self.skeleton_lobstr.hide_bl_text()

    def toggle_tracked_points(self):
        self.enable_tracked_points = not self.enable_tracked_points

        if self.enable_tracked_points:
            if self.enabled_animations['ours']:
                for gen_skeleton in self.skeleton_generated:
                    gen_skeleton.show_tracked_points()
            if self.enabled_animations['gt']:
                self.skeleton_gt.show_tracked_points()
            if self.enabled_animations['lobstr']:
                self.skeleton_lobstr.show_tracked_points()
        else:
            if self.enabled_animations['ours']:
                for gen_skeleton in self.skeleton_generated:
                    gen_skeleton.hide_tracked_points()
            if self.enabled_animations['gt']:
                self.skeleton_gt.hide_tracked_points()
            if self.enabled_animations['lobstr']:
                self.skeleton_lobstr.hide_tracked_points()

    def toggle_contact(self):
        if self.enable_tracked_points:
            if self.enabled_animations['ours']:
                for gen_skeleton in self.skeleton_generated:
                    gen_skeleton.show_contact_labels()
            if self.enabled_animations['gt']:
                self.skeleton_gt.show_contact_labels()
            if self.enabled_animations['lobstr']:
                self.skeleton_lobstr.show_contact_labels()
        else:
            if self.enabled_animations['ours']:
                for gen_skeleton in self.skeleton_generated:
                    gen_skeleton.hide_contact_labels()
            if self.enabled_animations['gt']:
                self.skeleton_gt.hide_contact_labels()
            if self.enabled_animations['lobstr']:
                self.skeleton_lobstr.hide_contact_labels()
        
    def setupGUI(self):
        offset = -0.05
        self.title = OnscreenText(
            text="Animation Viewer Program",
            parent=self.a2dTopLeft,
            pos=(0, offset*1),
            scale=0.05,
            fg=(1,1,1,1),
            bg=(0, 0, 0, 0.5),
            align=TextNode.ALeft)

        self.text_info = OnscreenText(
            text="",
            parent=self.a2dTopLeft,
            pos=(0, offset*2),
            scale=0.05,
            fg=(1,1,1,1),
            bg=(0, 0, 0, 0.5),
            align=TextNode.ALeft)

        self.frame_info = OnscreenText(
            text="",
            parent=self.a2dTopLeft,
            pos=(0, offset*3),
            scale=0.05,
            fg=(1,1,1,1),
            bg=(0, 0, 0, 0.5),
            align=TextNode.ALeft)

        self.speed_info = OnscreenText(
            text="",
            parent=self.a2dTopLeft,
            pos=(0, offset*4),
            scale=0.05,
            fg=(1,1,1,1),
            bg=(0, 0, 0, 0.5),
            align=TextNode.ALeft)

    def update_text_info(self):
        self.text_info.setText("Animation: " + self.current_sequence_name + " (" + str(self.current_sequence_index) + ")")
        self.frame_info.setText("Frame: " + str(self.frame))
        self.speed_info.setText("Speed: " + str(self.playback_speed) + "x")

    def update(self, task):
        if self.play:
            self.update_time()

        gt_frame_data_tracked_points = None

        if self.enabled_animations["ours"]:
            frame_data = []
            frame_data_contact = []
            frame_data_root_velocity = []
            frame_data_gate = []

            for i in range(self.num_gen):
                frame_data.append(self.generated_sequence[i]["position"][self.frame, :])
                frame_data_contact.append(self.generated_sequence[i]["contact"][self.frame, :])
                frame_data_root_velocity.append(self.generated_sequence[i]["root_velocity"][self.frame, :])
                if self.inference.opt.model_type == "vae_moe":
                    frame_data_gate.append(self.generated_sequence[i]["gate"][self.frame, :])
                if self.inference.use_eps_net and self.eps_net_debug:
                    self.skeleton_generated[i].update_joint_colors(self.generated_sequence[i]["marked_eps_errors"][self.frame, :])

            if self.inference.opt.model_type == "vae_moe":
                self.gateStats.render(frame_data_gate[0])
            if self.inference.smart_eps > 1 and self.view_eps_graph:
                self.epsStats.render(self.generated_sequence[0]["eps"][self.frame])
            #if self.inference.use_eps_net:
                #self.epsRatings.render(self.generated_sequence[0]["eps_ratings"][self.frame])
            if self.follow_camera and self.vr_mode:
                self.update_camera(self.generated_sequence[0]["position"][self.frame, 0:3].detach().cpu())

        if self.enabled_animations["gt"]:
            gt_frame_data = self.gt_sequence["position"][self.frame, :]
            gt_frame_data_contact = self.gt_sequence["contact"][self.frame, :]
            gt_frame_data_root_velocity = self.gt_sequence["root_velocity"][self.frame, :]
            gt_frame_data_tracked_points = self.gt_sequence["tracked_points"][self.frame, :]
            if self.follow_camera and not self.vr_mode:
                self.update_camera(gt_frame_data_tracked_points[0:3])
        if self.enabled_animations["lobstr"]:
            #lobstr_frame = (self.frame / self.gt_sequence["position"].shape[0]) * (self.lobstr_sequence["position"].shape[0])
            lobstr_frame_data = self.lobstr_sequence["position"][self.frame, :]
            lobstr_frame_data_contact = self.lobstr_sequence["contact"][self.frame, :]

        # render skeletons
        if self.enabled_animations["ours"]:
            for i in range(self.num_gen):
                self.skeleton_generated[i].render_frame(frame_data[i])
            self.skeleton_generated[0].render_tracked_points(gt_frame_data_tracked_points)
        if self.enabled_animations["gt"]:
            self.skeleton_gt.render_frame(gt_frame_data)
            self.skeleton_gt.render_tracked_points(gt_frame_data_tracked_points)
        if self.enabled_animations["lobstr"]:
            self.skeleton_lobstr.render_frame(gt_frame_data, lobstr_frame_data)
            self.skeleton_lobstr.render_tracked_points(gt_frame_data_tracked_points)

        # render contact points
        lf_index = self.inference.dataset.get_bone_index("LeftToeBase")
        rf_index = self.inference.dataset.get_bone_index("RightToeBase")
        if self.enabled_animations["ours"]:
            for i in range(self.num_gen):
                self.skeleton_generated[i].render_contact(frame_data_contact[i], lf_index, rf_index)
        if self.enabled_animations["gt"]:
            self.skeleton_gt.render_contact(gt_frame_data_contact, lf_index, rf_index)
        if self.enabled_animations["lobstr"]:
            lf_index_lobstr = self.lobstr_sequence["bone_map"]["LeftToeBase"]
            rf_index_lobstr = self.lobstr_sequence["bone_map"]["RightToeBase"]
            self.skeleton_lobstr.render_contact(lobstr_frame_data_contact, lf_index_lobstr, rf_index_lobstr)

        if self.enabled_animations["ours"]:
            for i in range(self.num_gen):
                self.skeleton_generated[i].render_root_velocity(frame_data_root_velocity[i])
        if self.enabled_animations["gt"]:
            self.skeleton_gt.render_root_velocity(gt_frame_data_root_velocity)

        self.update_text_info()

        return Task.cont

if __name__ == '__main__':
    parser = ViewerArgumentParser()
    args = parser.parse_args()

    app = PosePredictionApp(args)
    app.run()
