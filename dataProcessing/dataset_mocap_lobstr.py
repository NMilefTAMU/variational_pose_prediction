from dataProcessing.dataset_mocap import DatasetMocap

class DatasetMocapLoBSTr(DatasetMocap):
    def __init__(self, datapath, opt, mode="train", do_offset=True):
        super(DatasetMocapLoBSTr, self).__init__(datapath, opt, mode, do_offset)