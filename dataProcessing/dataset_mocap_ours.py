from dataProcessing.dataset_mocap import DatasetMocap

class DatasetMocapOurs(DatasetMocap):
    def __init__(self, datapath, opt, mode="train", do_offset=True, augment_data_func=None):
        super(DatasetMocapOurs, self).__init__(datapath, opt, mode, do_offset, augment_data_func)