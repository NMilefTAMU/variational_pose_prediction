from numpy.core.shape_base import block
import torch
from torch.utils import data
import pandas as pd
import csv
import os
import numpy as np
import numpy.matlib
import codecs as cs
import scipy.io as sio

import utils.paramUtil as paramUtil
import codecs
import joblib


class MotionFolderDatasetMocap(data.Dataset):
    def __init__(self, filename, datapath, opt, do_offset=True):
        self.clip = pd.read_csv(filename, index_col=False).dropna(how='all').dropna(axis=1, how='all')
        self.datapath = datapath
        self.lengths = []
        self.data = []
        self.labels = []
        self.opt = opt
        for i in range(self.clip.shape[0]):
            motion_name = self.clip.iloc[i]['motion']
            action_type = self.clip.iloc[i]['action_type']
            npy_path = os.path.join(datapath, motion_name + '.npy')

            # motion_length, joints_num, 3
            pose_raw = np.load(npy_path)
            # rescale the pose
            pose_raw = pose_raw / 20

            # Locate the root joint of initial pose at origin
            if do_offset:
                # get the offset and return the final pose
                offset_mat = np.tile(pose_raw[0, 0], (pose_raw.shape[1], 1))
                pose_mat = pose_raw - offset_mat
            else:
                pose_mat = pose_raw

            pose_mat = pose_mat.reshape((-1, 20 * 3))

            # not used any more
            if self.opt.no_trajectory:
                # for lie params, just exclude the root translation part
                if self.opt.lie_enforce:
                    pose_mat = pose_mat[:, 3:]
                else:
                    offset = np.tile(pose_mat[..., :3], (1, int(pose_mat.shape[1] / 3)))
                    pose_mat = pose_mat - offset

            label = paramUtil.find_key_by_value(paramUtil.mocap_action_enumerator, action_type)

            # remove actions that are not used for training
            if hasattr(opt, 'actions') and int(label) not in opt.actions:
                continue

            if action_type not in self.labels:
                self.labels.append(action_type)
            self.data.append((pose_mat, action_type))
            self.lengths.append(pose_mat.shape[0])
        self.cumsum = np.cumsum([0] + self.lengths)
        print("Total number of frames {}, videos {}, action types {}".format(self.cumsum[-1], self.clip.shape[0],
                                                                             len(self.labels)))
        self.label_enc = dict(zip(self.labels, np.arange(len(self.labels))))
        self.label_enc_rev = dict(zip(np.arange(len(self.labels)), self.labels))
        with codecs.open(os.path.join(opt.save_root, "label_enc_rev_mocap.txt"), 'w', 'utf-8') as f:
            for item in self.label_enc_rev.items():
                f.write(str(item) + "\n")

    def __len__(self):
        return len(self.data)

    def get_label_reverse(self, enc_label):
        return self.label_enc_rev.get(enc_label)

    def __getitem__(self, index):
        pose_mat, label = self.data[index]
        label = self.label_enc[label]
        return {"position": pose_mat, "label": label}


class PoseDataset(data.Dataset):
    def __init__(self, dataset, lie_enforce=False, no_trajectory=False):
        self.dataset = dataset
        self.lie_enforce = lie_enforce
        self.no_trajectory = no_trajectory

    def __getitem__(self, item):
        if item != 0:
            motion_id = np.searchsorted(self.dataset.cumsum, item) - 1
            pose_num = item - self.dataset.cumsum[motion_id] - 1
        else:
            motion_id = 0
            pose_num = 0
        motion, label = self.dataset[motion_id]
        pose = motion[pose_num]
        # offset for each pose
        if self.lie_enforce:
            if not self.no_trajectory:
                pose[:3] = 0.0
                pose_o = pose
            else:
                pose_o = pose
        else:
            offset = np.tile(pose[0:3], int(pose.shape[0] / 3))
            pose_o = pose - offset
        return pose_o, label

    def __len__(self):
        return self.dataset.cumsum[-1]


class MotionDataset(data.Dataset):
    def __init__(self, dataset, opt):
        self.dataset = dataset
        self.motion_length = opt.motion_length
        self.opt = opt

    def __getitem__(self, item):
        data = self.dataset[item]
        motion = data["position"]
        label = data["label"]
        contact = data["contact"]
        floor_height = data["floor"]
        bone_lengths = data["bone_lengths"]
        if "root_rot" in data.keys():
            root_rot = data["root_rot"]

        motion = np.array(motion)
        motion_len = motion.shape[0]
        # Motion can be of various length, we randomly sample sub-sequence
        # or repeat the last pose for padding

        # random sample
        if motion_len >= self.motion_length:
            gap = motion_len - self.motion_length
            start = 0 if gap == 0 else np.random.randint(0, gap, 1)[0]
            end = start + self.motion_length
            r_motion = motion[start:end]
            # offset deduction
            r_motion = r_motion - np.tile(r_motion[0, :3], (1, int(r_motion.shape[-1]/3)))
        # padding
        else:
            gap = self.motion_length - motion_len
            last_pose = np.expand_dims(motion[-1], axis=0)
            pad_poses = np.repeat(last_pose, gap, axis=0)
            r_motion = np.concatenate([motion, pad_poses], axis=0)

        output = {}
        output["position"] = r_motion
        output["label"] = label
        output["contact"] = contact
        output["floor"] = floor_height
        output["bone_lengths"] = bone_lengths
        if "root_rot" in data.keys():
            output["root_rot"] = root_rot

        return output

    def __len__(self):
        return len(self.dataset)

class MotionDataset4One(data.Dataset):
    def __init__(self, dataset, opt, cate_id):
        self.motion_length = opt.motion_length
        self.opt = opt
        self.dataset = []
        self.label = cate_id
        for i in range(len(dataset)):
            motion, label = dataset[i]
            # print(label, self.label)
            if self.label == label:
                self.dataset.append(motion)

    def __getitem__(self, item):
        motion = self.dataset[item]
        label = self.label
        motion = np.array(motion)
        motion_len = motion.shape[0]
        # Motion can be of various length, we randomly sample sub-sequence
        # or repeat the last pose for padding

        # random sample
        if motion_len >= self.motion_length:
            if self.motion_length == -1:
                gap = 0
            else:
                gap = motion_len - self.motion_length

            start = 0 if gap == 0 else np.random.randint(0, gap, 1)[0]

            if self.motion_length == -1:
                end = start + motion_len
            else:
                end = start + self.motion_length

            r_motion = motion[start:end]
            # offset deduction
            r_motion = r_motion - np.tile(r_motion[0, :3], (1, int(r_motion.shape[-1]/3)))
        # padding
        else:
            gap = self.motion_length - motion_len
            last_pose = np.expand_dims(motion[-1], axis=0)
            pad_poses = np.repeat(last_pose, gap, axis=0)
            r_motion = np.concatenate([motion, pad_poses], axis=0)
        return r_motion, label

    def __len__(self):
        return len(self.dataset)
    
def get_batch_from_dataset(dataset, index):
    batch_temp = dataset[index]
    batch = []
    batch.append(torch.from_numpy(batch_temp[0]).unsqueeze(0))
    batch.append(torch.from_numpy(np.atleast_1d(batch_temp[1])))
    return batch