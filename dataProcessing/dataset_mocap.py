import json
import os

import numpy as np
import torch
import utils.constants
import utils.paramUtil as paramUtil
from scipy.spatial.transform import Rotation

from dataProcessing.dataset_base import DatasetBase


class DatasetMocap(DatasetBase):
    def __init__(self, datapath, opt, mode="train", do_offset=True, augment_data_func=None):
        self.datapath = datapath
        self.lengths = []
        self.data = []
        self.global_rot_mat = []
        self.action_indices = {}
        self.indices_action = []
        self.contact_labels = []
        self.floor_heights = []
        self.labels = []
        self.root_velocities = []
        self.velocities = []
        self.angular_velocities = []
        self.raw_tracker_pos = []
        self.raw_tracker_vel = []
        self.root_pos = []
        self.root_pos_raw = []
        self.bone_lengths = []
        self.bone_map = self.__class__.get_bone_map()
        self.opt = opt
        r_datapath = os.path.join(datapath, mode)
        files = os.listdir(r_datapath)
        self.files = []
        self.augment_data_func = augment_data_func

        self.data_offsets = []
        self.anim_length = self.opt.motion_length
        self.total_length = 0
        self.root_rot = []
        self.root_rot_mat = []
        self.action_freq = {}

        labels_dict = self.load_labels(opt.dataset_type)
        self.dataset_params = paramUtil.get_dataset_params(opt)

        for f in files:
            if self.opt.sequence_names and f.split(".npy")[0] not in self.opt.sequence_names:
                continue
            if f.endswith(".gait.npy"):
                continue
            npy_path = os.path.join(r_datapath, f)
            self.files.append(f) # ensure only selected animations are listed

            # 0:57 (global_rot) 57:114 (local_rot), 114:171 (position)
            # motion_length, joints_num, 3
            pose_raw = np.load(npy_path)

            # swap root joints (head and hips)
            if self.opt.num_tp == 3:
                pose_raw_temp = np.array(pose_raw)
                pose_raw[:, 0:3] = pose_raw_temp[:, 30:33]
                pose_raw[:, 30:33] = pose_raw_temp[:, 0:3]
                pose_raw[:, 57:60] = pose_raw_temp[:, 87:90]
                pose_raw[:, 87:90] = pose_raw_temp[:, 57:60]
                pose_raw[:, 114:117] = pose_raw_temp[:, 144:147]
                pose_raw[:, 144:147] = pose_raw_temp[:, 114:117]
            root_quat_raw = Rotation.from_euler("ZYX", np.array(pose_raw[:, 0:3])).as_quat()

            tp_quat_raw = np.zeros((root_quat_raw.shape[0], self.opt.num_tp, 4)) # [batchsize, num_tracked_points, quat]

            start_index = 0 if self.opt.num_tp == 3 else 3 # start index of rotations

            tp_indices = [self.bone_map["Hips"], self.bone_map["LeftHand"], self.bone_map["RightHand"], self.bone_map["Head"]]
            for i in range(self.opt.num_tp): # num of tracked points
                index = tp_indices[i]
                tp_quat_raw[:, i, :] = Rotation.from_euler("ZYX", np.array(pose_raw[:,start_index+3*index:start_index+3*index+3])).as_quat()
                
            # extract yaw rotation
            # https://stackoverflow.com/questions/5782658/extracting-yaw-from-a-quaternion
            if self.opt.normalize_mode == "yaw":
                root_quat = root_quat_raw
                root_quat[:, 0] = 0 #x
                root_quat[:, 2] = 0 # z
                mag = np.sqrt((root_quat[:, 1]**2) + (root_quat[:, 3]**2))
                root_quat[:, 3] /= mag # w
                root_quat[:, 1] /= mag # y
            elif self.opt.normalize_mode == "all":
                root_quat = root_quat_raw

            self.root_rot.append(root_quat)

            root_rot_mat = Rotation.from_quat(root_quat).as_matrix()
            self.root_rot_mat.append(root_rot_mat)

            # extract local joints
            global_rot_mat = np.zeros((pose_raw.shape[0], 19, 3, 3))
            start_index = 0
            for i in range(19):
                global_rot_mat[:, i] = Rotation.from_euler("ZYX", np.array(pose_raw[:,start_index+3*i:start_index+3*i+3])).as_matrix()

            # extract positions and rescale the pose
            pose_raw = pose_raw[:, 114:].reshape(-1, self.dataset_params["joints_num"], 3)
            pose_raw = pose_raw * utils.constants.cmu_scale

            # Save raw tracking points
            raw_tracker_pos = np.array(pose_raw[:, tp_indices, :])
            raw_tracker_pos = raw_tracker_pos.reshape(raw_tracker_pos.shape[0], self.opt.num_tp * 3)
            raw_tracker_vel = np.array(np.diff(pose_raw[:, tp_indices, :], axis=0))
            raw_tracker_vel = raw_tracker_vel.reshape(raw_tracker_vel.shape[0], self.opt.num_tp * 3)
            raw_tracker_vel = np.concatenate((np.zeros((1, self.opt.num_tp * 3)), raw_tracker_vel), axis=0)

            # Locate the root joint of initial pose at origin
            root_offset = np.array(pose_raw[:, 0:1, :])
            root_pos_raw = np.squeeze(np.array(pose_raw[:, 0:1, :]))
            root_offset[:, :, 1] = root_offset[:, :, 1] * 0
            pose_mat = pose_raw - root_offset

            # root joint
            root_pos = np.squeeze(root_offset)
            root_velocity = np.diff(pose_raw[:, 0, :], axis=0)
            root_velocity = np.concatenate((np.zeros((1, 3)), root_velocity), axis=0)

            # compute joint velocities
            velocity = np.diff(pose_raw[:, :, :], axis=0)
            velocity = np.concatenate((np.zeros((1, self.dataset_params["joints_num"], 3)), velocity), axis=0)

            # compute joint angular velocities
            if self.opt.num_tp == 3:
                angular_velocity = np.zeros((root_quat.shape[0]-1, 3 * 6)) # 3 for 3-point tracking
            elif self.opt.num_tp == 4:
                angular_velocity = np.zeros((root_quat.shape[0]-1, 4 * 6)) # 4 for 4-point tracking

            # extract angular velocity
            for i in range(self.opt.num_tp):
                rot_current = Rotation.from_quat(tp_quat_raw[1:, i])
                inv_rot_last = Rotation.from_quat(tp_quat_raw[:-1, i]).inv()
                w_rot_mat = (rot_current * inv_rot_last).as_matrix()[:, 1:3, :]
                angular_velocity[:, i*6:i*6+6] = w_rot_mat.reshape((w_rot_mat.shape[0], 6))
            angular_velocity = np.concatenate((np.zeros((1, self.opt.num_tp*6)), angular_velocity), axis=0)

            # inverse transformation to be in local space
            if self.opt.normalize_inputs:
                inv_rot_mat = Rotation.from_quat(root_quat).inv()
                for j in range(self.dataset_params["joints_num"]):
                    pose_mat[:, j, :] = inv_rot_mat.apply(pose_mat[:, j, :])

                root_velocity = inv_rot_mat.apply(root_velocity)

                for j in range(self.dataset_params["joints_num"]):
                    velocity[:, j, :] = inv_rot_mat.apply(velocity[:, j, :])

            self.bone_lengths.append(self.get_bone_lengths(pose_mat[0], self.dataset_params["kinematic_chain"]))

            pose_mat = pose_mat.reshape((-1, self.dataset_params["joints_num"] * 3))
            label = labels_dict[f.split(".npy")[0].split("_mirror")[0]]

            ltb_index = self.bone_map["LeftToeBase"]
            rtb_index = self.bone_map["RightToeBase"]
            ltb_pos = pose_mat[:, ]

            pose_raw_flat = np.reshape(pose_raw, (-1, self.dataset_params["joints_num"] * 3))
            velocity = np.reshape(velocity, (-1, self.dataset_params["joints_num"] * 3))

            contacts, floor_height = self.compute_contact_and_floor(
                pose_raw_flat[:, ltb_index*3:ltb_index*3+3],
                pose_raw_flat[:, rtb_index*3:rtb_index*3+3])

            self.floor_heights.append(floor_height)
            self.labels.append(label)
            self.data.append(pose_mat)
            self.global_rot_mat.append(global_rot_mat)
            self.lengths.append(pose_mat.shape[0])
            self.contact_labels.append(contacts)
            self.raw_tracker_pos.append(raw_tracker_pos)
            self.raw_tracker_vel.append(raw_tracker_vel)
            self.root_velocities.append(root_velocity)
            self.velocities.append(velocity)
            self.angular_velocities.append(angular_velocity)
            self.root_pos.append(root_pos)
            self.root_pos_raw.append(root_pos_raw)

            self.process_file(npy_path)

        self.cumsum = np.cumsum([0] + self.lengths)
        print("Total number of frames {}, videos {}, action types {}".format(self.cumsum[-1], len(self.files),
                                                                             len(self.labels)))

        for d in self.data:
            self.data_offsets.append(self.total_length)
            if self.anim_length == -1:
                self.total_length += 1
            else:
                self.total_length += d.shape[0] - self.anim_length + 1

        if len(self.data) == 0:
            raise ValueError("\"" + mode + "\" dataset doesn't contain any sequences")

        self.compute_sample_weights(self.opt.balance)
        self.compute_mean_and_std()

    def get_bone_map():
        bone_map = {
            "Hips": 0,
            "Spine": 1,
            "Spine1": 2,
            "LeftArm": 3,
            "LeftForeArm": 4,
            "LeftHand": 5,
            "RightArm": 6,
            "RightForeArm": 7,
            "RightHand": 8,
            "Neck1": 9,
            "Head": 10,
            "LeftUpLeg": 11,
            "LeftLeg": 12,
            "LeftFoot": 13,
            "LeftToeBase": 14,
            "RightUpLeg": 15,
            "RightLeg": 16,
            "RightFoot": 17,
            "RightToeBase": 18,
        }
        return bone_map

    def get_bone_name(self, index):
        for k, v in self.bone_map.items():
            if v == index:
                return k
        raise ValueError("Bone with index " + str(index) + " not found")

    def get_bone_lengths(self, first_frame_positions, kinematic_chain):
        """
        Get bone lengths

        Args:
            first_frame_positions: tensor [N, J, 3]
            kinematic_chain: dict of joint chains

        Returns:
            tensor: bone lengths [J]
        """
        assert(len(first_frame_positions.shape) == 2)
        assert(first_frame_positions.shape[1] == 3)

        chain_dict = paramUtil.chain_to_dict(kinematic_chain)
        output = np.zeros(first_frame_positions.shape[0])

        for k, v in chain_dict.items():
            head = first_frame_positions[k]
            tail = first_frame_positions[v]
            output[k] = np.sqrt(np.sum((head - tail) ** 2))

        return output

    def get_bone_length(self, index, parent, child):
        chain_dict = paramUtil.chain_to_dict(self.dataset_params['kinematic_chain'])
        lengths = self.bone_lengths[index]

        for k, v in chain_dict.items():
            k_name = self.get_bone_name(k)
            v_name = self.get_bone_name(v)
            if v_name == parent and k_name == child:
                return lengths[k]

        raise IndexError("Could not find bone with parent " + parent + " and child " + child)

    def get_anim_names(self):
        return self.files

    def __len__(self):
        return self.total_length

    def i_to_index_offset(self, i):
        block_index = 0
        current_block = 0
        for off in self.data_offsets:
            if i >= off:
                current_block = block_index
                block_index += 1
        offset = i - self.data_offsets[current_block]
        return current_block, offset

    def get_label_reverse(self, enc_label):
        return self.label_enc_rev.get(enc_label)

    def get_floor_height(self, index):
        d_index, _ = self.i_to_index_offset(index)
        try:
            return self.floor_heights[d_index]
        except:
            raise ValueError("index of " + str(index) + " is invalid for length of " + str(len(self.floor_heights)))

    def compute_mean_and_std(self):
        num_var = self.data[0].shape[1]
        self.mean = np.zeros((num_var))
        self.std = np.zeros((1, num_var))
        num_frames = 0

        for block in self.data:
            for i in range(block.shape[0]):
                self.mean += block[i]
                num_frames += 1
        self.mean /= num_frames

        for block in self.data:
            for i in range(block.shape[0]):
                std_block = (block[i] - self.mean)
                self.std += std_block * std_block
        self.std = np.sqrt(self.std / num_frames)

    def local_to_world(self, positions, root_rot):
        """
        Convert local coordinates to world coordinates

        Args:
            positions: positions in local coordinate space
            root_rot: root rotation            

        Returns:
            np: positions in world
        """
        rot_mat = Rotation.from_quat(root_rot)
        l_pos = np.array(positions)

        joints_num = positions.shape[-1] // 3
        for j in range(joints_num):
            l_pos[:, j*3:j*3+3] = rot_mat.apply(l_pos[:, j*3:j*3+3])
        return l_pos

    def world_to_local(self, positions, root_rot):
        rot_mat = Rotation.from_quat(root_rot).inv()
        l_pos = np.array(positions)
        for j in range(self.dataset_params["joints_num"]):
            l_pos[:, j*3:j*3+3] = rot_mat.apply(l_pos[:, j*3:j*3+3])
        return l_pos

    def labels_to_one_hot(self, labels):
        action_one_hot = np.zeros(len(self.action_indices))

        for label in labels:
            index = self.action_indices[label]
            action_one_hot[index] = 1

        return action_one_hot

    def __getitem__(self, index):
        d_index, offset = self.i_to_index_offset(index)
        data_block = self.data[d_index]
        global_rot_mat_block = self.global_rot_mat[d_index]
        raw_tracker_pos_block = self.raw_tracker_pos[d_index]
        raw_tracker_vel_block = self.raw_tracker_vel[d_index]
        contact_label_block = self.contact_labels[d_index]
        action_label_block = self.labels_to_one_hot(self.labels[d_index])
        floor_height = self.floor_heights[d_index]
        root_pos_block = self.root_pos[d_index]
        root_pos_raw_block = self.root_pos_raw[d_index]
        velocity_block = self.velocities[d_index]
        angular_velocity_block = self.angular_velocities[d_index]
        root_velocity_block = self.root_velocities[d_index]
        root_rot_block = self.root_rot[d_index]
        root_rot_mat_block = self.root_rot_mat[d_index]

        if self.anim_length == -1:
            pos = data_block
            global_rot_mat = global_rot_mat_block
            raw_tracker_pos = raw_tracker_pos_block
            raw_tracker_vel = raw_tracker_vel_block
            contact = contact_label_block
            root_pos = root_pos_block
            root_pos_raw = root_pos_raw_block
            velocity = velocity_block
            angular_velocity = angular_velocity_block
            root_velocity = root_velocity_block
            root_rot = root_rot_block if self.opt.normalize_inputs else None
            root_rot_mat = root_rot_mat_block if self.opt.normalize_inputs else None
        else:
            pos = data_block[offset:offset+self.anim_length]
            global_rot_mat = global_rot_mat_block[offset:offset+self.anim_length]
            raw_tracker_pos = raw_tracker_pos_block[offset:offset+self.anim_length]
            raw_tracker_vel = raw_tracker_vel_block[offset:offset+self.anim_length]
            contact = contact_label_block[offset:offset+self.anim_length]
            root_pos = root_pos_block[offset:offset+self.anim_length]
            root_pos_raw = root_pos_raw_block[offset:offset+self.anim_length]
            velocity = velocity_block[offset:offset+self.anim_length]
            angular_velocity = angular_velocity_block[offset:offset+self.anim_length]
            root_velocity = root_velocity_block[offset:offset+self.anim_length]
            root_rot = root_rot_block[offset:offset+self.anim_length] if self.opt.normalize_inputs else None
            root_rot_mat = root_rot_mat_block[offset:offset+self.anim_length] if self.opt.normalize_inputs else None

        output = {}
        output["position"] = pos
        output["global_rot_mat"] = global_rot_mat
        output["raw_tracker_pos"] = raw_tracker_pos
        output["raw_tracker_vel"] = raw_tracker_vel
        output["action"] = action_label_block
        output["contact"] = contact
        output["floor"] = floor_height
        output["bone_lengths"] = self.bone_lengths[d_index]
        output["root_velocity"] = root_velocity
        output["root_pos"] = root_pos
        output["root_pos_raw"] = root_pos_raw
        output["velocity"] = velocity
        output["angular_velocity"] = angular_velocity

        if root_rot is not None:
            output["root_rot"] = root_rot
            output["root_rot_mat"] = root_rot_mat

        if self.augment_data_func is not None:
            output = self.augment_data_func(output)

        return output

    def load_labels(self, dataset_type):
        labels_dict = {}

        labels_filename = "dataset/dataset_labels/action_map.json"
        with open(labels_filename) as f:
            labels_dict = json.load(f)

        index = 0
        for action_list in labels_dict.values():
            for action in action_list:
                if action not in self.action_indices:
                    self.action_indices[action] = index
                    self.indices_action.append(action)
                    index += 1

        return labels_dict

    def process_file(self, filename):
        pass

    def compute_sample_weights(self, mode="uniform"):
        '''
        Compute sample weights given different types of sampling modes

        Args:
            mode: [uniform, action]
        '''
        if mode == "uniform":
            self.sample_weights = np.ones(len(self))
        elif mode == "action":
            self.sample_weights = np.zeros(len(self))
            self.action_freq = {}

            for i in range(len(self)):
                index, _ = self.i_to_index_offset(i)
                actions = self.labels[index]
                for action in actions:
                    if action not in self.action_freq:
                        self.action_freq[action] = 0
                    self.action_freq[action] += 1.0 / len(actions)

            total_weight = 0
            for i in range(len(self)):
                index, _ = self.i_to_index_offset(i)
                action_prob = 0
                for action in self.labels[index]:
                    action_prob += self.action_freq[action] / (len(self) * len(self.labels[index]))

                weight = 1.0 / action_prob
                self.sample_weights[i] = weight
                total_weight += weight

    def get_sample_weights(self):
        return self.sample_weights
    
    def get_num_labels(self):
        return len(self.action_indices)

    def get_top_k_actions(self, actions, k=1):
        indices = np.argsort(actions, axis=-1)[:, -k:]
        all_actions = []
        for i in range(indices.shape[0]):
            actions = []
            for j in range(indices.shape[1]):
                actions.append(self.indices_action[j])
            all_actions.append(actions)
        return all_actions

    @staticmethod
    def convert_sample_to_torch(sample, device=None):
        output = {}
        for key in sample.keys():
            if type(sample[key]) == np.ndarray:
                output[key] = torch.from_numpy(sample[key]).float()
                if device is not None:
                    output[key] = output[key].to(device)
            else:
                output[key] = sample[key]
        return output

    @staticmethod
    def convert_sample_to_numpy(sample):
        output = {}
        for key in sample.keys():
            if type(sample[key]) == torch.Tensor:
                output[key] = sample[key].detach().cpu().numpy()
            else:
                output[key] = sample[key]
        return output