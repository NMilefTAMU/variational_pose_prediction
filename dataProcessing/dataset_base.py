from abc import abstractmethod

import numpy as np
import torch
from scipy.signal import savgol_filter
from torch.utils import data


class DatasetBase(data.Dataset):
    '''
    Dataset base class
    '''
    def __init__(self):
        self.bone_map = {}
    
    @classmethod
    @abstractmethod
    def get_bone_map():
        pass

    @classmethod
    def get_bone_index(cls, bone_name):
        '''
        Get bone index

        Args:
            bone_name: string of bone name

        Returns:
            int: bone index (-1 if not found)
        '''
        bone_map = cls.get_bone_map()
        return bone_map[bone_name]

    def compute_contact_and_floor(self, left_toe_pos, right_toe_pos):
        '''
        Compute contact (<= 2cm offset from floor)

        Args:
            left_toe_pos: [N, 3] sequence of left toe positions
            right_toe_pos: [N, 3] sequence of right toe positions
        '''
        assert(len(left_toe_pos.shape)==2)
        assert(len(right_toe_pos.shape)==2)
        assert(left_toe_pos.shape[1]==3)
        assert(right_toe_pos.shape[1]==3)

        # floor is calculated using the min of both legs
        lt_min = np.percentile(left_toe_pos[:, 1], 10)
        rt_min = np.percentile(right_toe_pos[:, 1], 10)
        floor_height = min(lt_min, rt_min)
        height_threshold = 0.05 # 2 cm
        l_height_mask = (left_toe_pos[:, 1] <= floor_height + height_threshold).astype(float)
        r_height_mask = (right_toe_pos[:, 1] <= floor_height + height_threshold).astype(float)

        # calc toe velocity
        pad_vel = np.zeros((1, 3))
        lt_vel = np.concatenate((pad_vel, np.diff(left_toe_pos, axis=0)), axis=0)
        rt_vel = np.concatenate((pad_vel, np.diff(right_toe_pos, axis=0)), axis=0)
        lt_speed = savgol_filter(np.linalg.norm(lt_vel, axis=-1), 11, 3)
        rt_speed = savgol_filter(np.linalg.norm(rt_vel, axis=-1), 11, 3)
        vel_threshold = 0.01 # 2 cm/frame
        l_vel_mask = (lt_speed <= vel_threshold).astype(float)
        r_vel_mask = (rt_speed <= vel_threshold).astype(float)

        # calc full mask
        l_mask = l_height_mask * l_vel_mask
        r_mask = r_height_mask * r_vel_mask

        contact_labels = np.stack((l_mask, r_mask), axis=1) # [N, 2]

        return contact_labels, floor_height

    def compute_mean_and_std(self):
        pass
