import csv
import os

import numpy as np
import utils.paramUtil as paramUtil
from dataProcessing.dataset_mocap_lobstr import DatasetMocapLoBSTr
from dataProcessing.dataset_base import DatasetBase
from scipy.spatial.transform import Rotation

class DatasetMocapPFNN(DatasetMocapLoBSTr):
    def __init__(self, datapath, opt, mode="train", do_offset=True):
        self.gait_data = []
        self.gait_count = [0]*8
        super(DatasetMocapPFNN, self).__init__(datapath, opt, mode, do_offset)

        self.sample_weights = np.zeros((len(self)))
        self.gait_weight = np.zeros((8))
        min_count = min(i for i in self.gait_count if i > 0)
        total_gait_count = sum(self.gait_count)

        # calculate weights
        for i in range(8):
            if self.gait_count[i] == 0:
                continue
            self.gait_weight[i] = min_count / self.gait_count[i]

        # renormalize weights
        total_weight = sum(self.gait_weight)
        for i in range(8):
            self.gait_weight[i] = self.gait_weight[i] / total_weight

    def __getitem__(self, index):
        output = super().__getitem__(index)

        d_index, offset = self.i_to_index_offset(index)
        gait_block = self.gait_data[d_index]

        output["gait"] = gait_block[offset:offset+self.anim_length]
        return output

    def get_sample_weights(self):
        index = 0
        weights = np.array(self.gait_weight)

        for gait_data in self.gait_data:
            sequence = gait_data[self.anim_length-1:]
            action_label = np.argmax(sequence, axis=1)                
            sequence_gait_weight = np.take(self.gait_weight, action_label)
            self.sample_weights[index:index+sequence.shape[0]] = sequence_gait_weight
            index += sequence.shape[0]

        return self.sample_weights

    def process_file(self, filename):
        super().process_file(filename)

        gait_filename = filename.split(".npy")[0] + ".gait.npy"
        gait_data = np.load(gait_filename)
        self.gait_data.append(gait_data)

        num_frames = gait_data.shape[0]
        action_labels = np.argmax(gait_data[self.anim_length-1:], axis=1)
        unique, counts = np.unique(action_labels, return_counts=True)
        for i in range(unique.shape[0]):
            self.gait_count[unique[i]] += counts[i]
