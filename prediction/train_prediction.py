import argparse
import os
import sys
import time

sys.path.insert(0, os.path.join(os.path.dirname(os.path.abspath(__file__)), ".."))

from dataProcessing.dataset_mocap_ours import DatasetMocapOurs
from utils import paramUtil
from utils.pose_processing import extract_next_tracked_points

import torch
import torch.nn as nn
import torch.optim as optim
from torch.utils.data import DataLoader
from torch.utils.tensorboard import SummaryWriter
import models.motion_vae as vae_models

from models.prediction_network import PredictionNet
from models.motion_vae import DecoderGRU6d


def train_prediction_net(args):
    opt = args
    opt.num_tp = 4
    opt.dim_z = 30
    opt.motion_length = 60
    opt.output_size = 100
    opt.sequence_names = []
    opt.normalize_inputs = "true"
    opt.normalize_mode = "yaw"
    opt.balance = "uniform"
    opt.num_workers = 0
    opt.enable_last_p = False
    opt.input_types = ["tracking4", "vtracking4"]

    opt.checkpoints_dir = "./checkpoints/prediction/"
    opt.dataset_type = "mocap_ours"

    opt.save_root = os.path.join(opt.checkpoints_dir, opt.dataset_type, opt.name)
    opt.model_path = os.path.join(opt.save_root, 'model')
    opt.joints_path = os.path.join(opt.save_root, 'joints')
    opt.log_path = os.path.join(opt.save_root, "log.txt")

    device = torch.device("cuda:" + str(opt.gpu_id) if torch.cuda.is_available() else "cpu")

    num_iterations = args.iters

    filename = "prediction_model.pth"

    writer = SummaryWriter("./runs_eps/" + str(time.time() * 1000))
    device = torch.device("cuda:" + str(args.gpu_id) if torch.cuda.is_available() else "cpu")

    dataset_params      = paramUtil.get_dataset_params(opt)
    dataset_path        = dataset_params['dataset_path']
    input_size          = dataset_params['input_size']
    joints_num          = dataset_params['joints_num']
    label_dec           = dataset_params['label_dec']
    raw_offsets         = dataset_params['raw_offsets']
    kinematic_chain     = dataset_params['kinematic_chain']
    enumerator          = dataset_params['enumerator']
    clip_path           = dataset_params['clip_path']
    motion_desc_file    = dataset_params['motion_desc_file']
    labels              = dataset_params['labels']
    file_prefix         = dataset_params['file_prefix']

    train_data = DatasetMocapOurs(dataset_path, opt, mode="train")
    valid_data = DatasetMocapOurs(dataset_path, opt, mode="valid")
    train_motion_loader = DataLoader(train_data, batch_size=opt.batch_size, drop_last=True, num_workers=opt.num_workers, shuffle=True)
    valid_motion_loader = DataLoader(valid_data, batch_size=opt.batch_size, drop_last=True, num_workers=opt.num_workers, shuffle=True)

    writer = SummaryWriter("./runs_prediction/" + str(time.time() * 1000))

    sample_model = vae_models.DecoderGRU6d(
        opt.dim_z + 24,
        18 * 6 + 3,
        128,
        1,
        opt.batch_size,
        device,
        -1,
        last_pose=opt.enable_last_p
    )
    sample_model.to(device)

    prediction_model = PredictionNet()

    loss_function = nn.MSELoss()
    optimizer = optim.Adam(prediction_model.parameters(), lr=0.0002, betas=(0.5, 0.999), weight_decay=0.00001)

    lookahead = 10  # number of frames to predict into the future

    iterations = 1
    while iterations < args.iters + 1:
        # training loop
        sample = next(iter(train_motion_loader))
        optimizer.zero_grad()

        prediction_model.init_hidden(args.batch_size)
        sample_model.init_hidden(args.batch_size)

        eps = torch.zeros((args.batch_size, opt.dim_z)).to(device).normal_()

        for i in range(args.motion_length - lookahead):
            tracked_points_position = extract_next_tracked_points(sample, i, opt, 'position')
            tracked_points_velocity = extract_next_tracked_points(sample, i, opt, 'velocity')
            tracked_points = torch.cat((tracked_points_position, tracked_points_velocity), dim=1).to(device)

            input_data = torch.cat((tracked_points, eps), dim=1).float()

            output = sample_model(input_data)

        train_loss = loss_function()


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--batch_size", type=int, default=128)
    parser.add_argument("--iters", type=int, default=100000)
    parser.add_argument("--gpu_id", type=int, default=0)
    parser.add_argument("--name", type=str, required=True)
    parser.add_argument("--pose_name", type=str, required=True)
    parser.add_argument("--save_every", type=int, default=100)
    parser.add_argument("--framerate", type=int, default=60)
    args = parser.parse_args()

    train_prediction_net(args)